package tableInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DialogueControllerTable;
import cd.I18N;
import gamePart.Caches;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import cd.DataController.screenList;


	public class DifferentCardRuleTable3 extends BorderPane {

		private DialogueControllerTable cDialogue = null;
		//On donne un nom a cette interface pour permettre l'identification par le systeme
		private static final screenList nameScreen = screenList.DIFFERENTCARDRULETABLE3;
		
			HBox Htop;

			Label lTitle;
		
			VBox Vbottom;
			VBox Vleft;
			VBox Vright;
			
			VBox Vmiddle;
			HBox HmiddleBot;

			Button bBack1;
			Button bBack2;
			
			Button bback;
			
			
			//3eme partie
			
			Label ld31;
			Label lwitch;
			Label lpands;
			Label lhermit;
			Label llittleg;
			Label lromeo;
			Label ldragon;
			Label lbeggar;
			Label llookalike;
			
			FileInputStream inp33;
			Image i33;
			ImageView iv33;
			FileInputStream inp34;
			Image i34;
			ImageView iv34;
			FileInputStream inp35;
			Image i35;
			ImageView iv35;
			FileInputStream inp36;
			Image i36;
			ImageView iv36;
			FileInputStream inp37;
			Image i37;
			ImageView iv37;
			FileInputStream inp38;
			Image i38;
			ImageView iv38;
			FileInputStream inp39;
			Image i39;
			ImageView iv39;
			FileInputStream inp310;
			Image i310;
			ImageView iv310;
			
			VBox v3;
			VBox v34;
			HBox h33;
			HBox h34;
			HBox h35;
			HBox h36;
			HBox h356;
			HBox h37;
			HBox h347;
			HBox h38;
			HBox h39;
			HBox h310;
			
			
			// On créé les differents composants de l'interface (Label, Button)
		    private void createWidgets() throws FileNotFoundException {
				//***LABEL***//
		    	//creation et initialisation du Label lTitle
				lTitle = new Label();
				lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 200));
				lTitle.textProperty().bind(I18N.createStringBinding("lb.titleDcr"));
		    	
				
				//creation et initialisation du Button bBack1
				bBack1 = new Button();
				bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack1.setPrefSize(200, 50);
				bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack1.setTranslateY(-75);
				bBack1.setTranslateX(50);

				// creation et initialisation du Button bBack2
				bBack2 = new Button();
				bBack2.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack2.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack2.setPrefSize(200, 50);
				bBack2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack2.setTranslateY(-75);
				bBack2.setTranslateX(-50);
				
				bback = new Button("<");
				bback.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
				bback.setPrefSize(30, 30);
				bback.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
			    
				
				 //création et initialisation de tous les elements de la 2eme partie
			    
				lwitch= new Label();
				lwitch.setAlignment(Pos.CENTER);
				lwitch.setFont(Font.font("Cambria", 20));
				lwitch.textProperty().bind(I18N.createStringBinding("lb.witch"));
				
				lpands= new Label();
				lpands.setAlignment(Pos.CENTER);
				lpands.setFont(Font.font("Cambria", 20));
				lpands.textProperty().bind(I18N.createStringBinding("lb.pands"));
				
				lhermit= new Label();
				lhermit.setAlignment(Pos.CENTER);
				lhermit.setFont(Font.font("Cambria", 20));
				lhermit.textProperty().bind(I18N.createStringBinding("lb.hermit"));
				
				llittleg= new Label();
				llittleg.setAlignment(Pos.CENTER);
				llittleg.setFont(Font.font("Cambria", 20));
				llittleg.textProperty().bind(I18N.createStringBinding("lb.littleg"));
				
				lromeo= new Label();
				lromeo.setAlignment(Pos.CENTER);
				lromeo.setFont(Font.font("Cambria", 20));
				lromeo.textProperty().bind(I18N.createStringBinding("lb.romeo"));
				
				ldragon= new Label();
				ldragon.setAlignment(Pos.CENTER);
				ldragon.setFont(Font.font("Cambria", 20));
				ldragon.textProperty().bind(I18N.createStringBinding("lb.dragon"));
				
				lbeggar= new Label();
				lbeggar.setAlignment(Pos.CENTER);
				lbeggar.setFont(Font.font("Cambria", 20));
				lbeggar.textProperty().bind(I18N.createStringBinding("lb.beggar"));
				
				llookalike= new Label();
				llookalike.setAlignment(Pos.CENTER);
				llookalike.setFont(Font.font("Cambria", 20));
				llookalike.textProperty().bind(I18N.createStringBinding("lb.lookalike"));
				
				inp33 = new FileInputStream("./src/ressources/Rules/witch.png");			
				i33  = new Image(inp33, 80, 80, false, true);
				iv33 = new ImageView(i33);
				
				inp34 = new FileInputStream("./src/ressources/Rules/pands.png");			
				i34 = new Image(inp34, 80, 150, false, true);
				iv34 = new ImageView(i34);
				
				inp35 = new FileInputStream("./src/ressources/Rules/hermit.png");			
				i35  = new Image(inp35, 80, 80, false, true);
				iv35 = new ImageView(i35);
				
				inp36 = new FileInputStream("./src/ressources/Rules/littleg.png");			
				i36  = new Image(inp36, 80, 80, false, true);
				iv36 = new ImageView(i36);
				
				inp37 = new FileInputStream("./src/ressources/Rules/romeo.png");			
				i37  = new Image(inp37, 80, 80, false, true);
				iv37 = new ImageView(i37);
				
				ld31= new Label();
			    ld31.setAlignment(Pos.CENTER);
			    ld31.setFont(Font.font("Cambria", FontWeight.BOLD, 20));
			    ld31.textProperty().bind(I18N.createStringBinding("text.d31"));
				
			    inp38 = new FileInputStream("./src/ressources/Rules/dragon.png");			
				i38  = new Image(inp38, 80, 80, false, true);
				iv38 = new ImageView(i38);
				
				inp39 = new FileInputStream("./src/ressources/Rules/beggar.png");			
				i39  = new Image(inp39, 80, 80, false, true);
				iv39 = new ImageView(i39);
				
				inp310 = new FileInputStream("./src/ressources/Rules/lookalike.png");			
				i310  = new Image(inp310, 80, 80, false, true);
				iv310 = new ImageView(i310);
			    
			}
				
		    
			// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
		    private void createContainers() {

				// CREATION ET CONFIGURATION DE LA HBOX HAUTE/TITRE
				Htop = new HBox();
				Htop.setAlignment(Pos.CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vbottom = new VBox();
				Vbottom.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vleft = new VBox();
				Vleft.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX DROITE
				Vright = new VBox();
				Vright.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX middle
				Vmiddle = new VBox();
				Vmiddle.setAlignment(Pos.TOP_LEFT);
				Vmiddle.setSpacing(15);
				
				HmiddleBot = new HBox();
				HmiddleBot.setAlignment(Pos.BOTTOM_CENTER);
				
							
				//3eme partie
				v3 = new VBox();
				v3.setAlignment(Pos.CENTER_LEFT);
				v3.setSpacing(15);
				
				v34 = new VBox();
				v34.setAlignment(Pos.CENTER_LEFT);
				
				h33 = new HBox();
				h33.setAlignment(Pos.CENTER_LEFT);
				h33.setSpacing(10);
				
				h34 = new HBox();
				h34.setAlignment(Pos.CENTER_LEFT);
				h34.setSpacing(10);

				h35 = new HBox();
				h35.setAlignment(Pos.CENTER_LEFT);
				h35.setSpacing(10);
				
				h36 = new HBox();
				h36.setAlignment(Pos.CENTER_LEFT);
				h36.setSpacing(10);
				
				h356 = new HBox();
				h356.setAlignment(Pos.CENTER_LEFT);
				h356.setSpacing(10);
				
				h37 = new HBox();
				h37.setAlignment(Pos.CENTER_LEFT);
				h37.setSpacing(10);
				
				h347 = new HBox();
				h347.setAlignment(Pos.CENTER_LEFT);
				h347.setSpacing(10);
				
				h38 = new HBox();
				h38.setAlignment(Pos.CENTER_LEFT);
				h38.setSpacing(10);
				
				h39 = new HBox();
				h39.setAlignment(Pos.CENTER_LEFT);
				h39.setSpacing(10);
				
				h310 = new HBox();
				h310.setAlignment(Pos.CENTER_LEFT);
				h310.setSpacing(10);
		    }

			// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
		    private void placementWidgetsInContainers() {
				
				// ON AJOUTE TITRE DANS TOP
				Htop.getChildren().add(lTitle);
								
				// ON AJOUTE REGLES DU JEU DANS VGAUCHE
				Vleft.getChildren().add(bBack1);

				// ON AJOUTE PARAMETRE DANS VDROITE
				Vright.getChildren().add(bBack2);
				
				
				
				//3eme partie
				h33.getChildren().add(iv33);
				h33.getChildren().add(lwitch);
				
				h34.getChildren().add(iv34);
				h34.getChildren().add(lpands);
				
				h35.getChildren().add(iv35);
				h35.getChildren().add(lhermit);
				
				h36.getChildren().add(iv36);
				h36.getChildren().add(llittleg);
				
				h356.getChildren().add(h35);
				h356.getChildren().add(h36);
				
				h37.getChildren().add(iv37);
				h37.getChildren().add(lromeo);
				
				h347.getChildren().add(h34);
				h347.getChildren().add(h37);
				
				h38.getChildren().add(iv38);
				h38.getChildren().add(ldragon);
				
				h39.getChildren().add(iv39);
				h39.getChildren().add(lbeggar);
				
				h310.getChildren().add(iv310);
				h310.getChildren().add(llookalike);
				
				v3.getChildren().add(h33);
				v3.getChildren().add(h347);
				v3.getChildren().add(h356);
				v3.getChildren().add(h38);
				v3.getChildren().add(h39);
				v3.getChildren().add(h310);
				
				// ON AJOUTE PARAMETRE DANS VMIDDLE
				Vmiddle.getChildren().add(v3);
				
				HmiddleBot.getChildren().add(bback);
				Vmiddle.getChildren().add(HmiddleBot);

			}

			// On définit l'emplacement des HBox et VBox dans l'interface
		    private void placementContainersInPane() {
			
				// ON ASSIGNE A CHAQUE BOX LEUR EMPLACEMENT
				this.setTop(Htop);
				this.setBottom(Vbottom);
				this.setLeft(Vleft);
				this.setRight(Vright);
				this.setCenter(Vmiddle);
			}

			
		public DifferentCardRuleTable3(DialogueControllerTable cDialogue2) throws FileNotFoundException{

			cDialogue = cDialogue2;
			
			createWidgets();
		    createContainers();
		    placementWidgetsInContainers();
		    placementContainersInPane();
			
			 //
			Image image1 = Caches.BG;
			// Creation de l'image en arriere plan/ image de fond
			BackgroundImage backgroundimage = new BackgroundImage(image1,
			BackgroundRepeat.NO_REPEAT,
			BackgroundRepeat.NO_REPEAT,
			BackgroundPosition.DEFAULT,
			new BackgroundSize(1.0, 1.0, true, true, false, false));
		
			Background background = new Background(backgroundimage);
			//On affecte à l'écran courant l'image de fond défini
			this.setBackground(background);

			bBack1.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.RULES);
		});



			bBack2.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.RULES);
		});
			
			
			
			bback.setOnAction(event -> {
				cDialogue.getSettings().effectSounds();
				cDialogue.showScreen(screenList.DIFFERENTCARDRULETABLE2);
		});
			

			this.setVisible(false);
			cDialogue.saveScreen(nameScreen, this);

		}
			
	}





