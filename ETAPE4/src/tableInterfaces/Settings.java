package tableInterfaces;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.swing.JSlider;

import card.InfluenceCard;
import card.ObjectiveCard;
import cd.DataController;
import cd.DataController.screenList;
import cd.DialogueControllerTable;
import cd.I18N;
import cd.I18N.AppLanguage;
import gamePart.Caches;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;
import javafx.util.StringConverter;

public class Settings extends BorderPane {
	
	private DialogueControllerTable cDialogue = null;
	private static final screenList screenName = screenList.SETTINGS;
	
	//declaration Label
	Label lTitle;
	Label lTitleLanguages;
	Label lCardStyle; //
	Label lTitleSong;
	Label lEffectSound;
	Label lMusicText;
	Label lMusicOff;
	Label lSonText;
	Label lSonOff;
	
	//declaration Button de Bottom
	Button bBack;
	Button bBack1;

	//declaration Vbox
	VBox vbLeft;
	VBox vbRight;
	VBox vbCenter;
	VBox vbSongs;
	VBox vbLanguages;
	VBox vbCStyle;
	VBox vbRestore;
	
	//declaration Hbox
	HBox hbTop;
	HBox hbCenter;
	HBox hbBottomRight;
	HBox hbBottomLeft;
	HBox hbEffectSounds;
	HBox hbMusics;
	HBox hbGroupLanguages;
	HBox hbGroupeCStyle;
	
	//declaration Slider
	Slider sEffectSound;
	Slider sMusic;
	JSlider slider;
	
	//declaration TextField
	TextField tfEffectSounds;
	TextField tfVolumeMusics;
	
	//declaration Button choix de langue
	Button bFrenchLanguage;
	Button bEnglishLanguage;
	Button bEspagnolLanguage;
	
	
	
	//Boutton Styles de cartes
	Button bCSClassic;
	Button bCSSpecial;	

	//Button restaurer paramÃƒÆ’Ã‚Â¨tre par defaut
	Button bRestore;
	
	private Border bSelected = new Border(new BorderStroke(Color.AQUA, 
			BorderStrokeStyle.SOLID,
			new CornerRadii(5),
			new BorderWidths(3),
			new Insets(0)
			));
	
	private Border bUnselected = new Border(new BorderStroke(Color.AQUA, 
			BorderStrokeStyle.NONE,
			new CornerRadii(5),
			new BorderWidths(3),
			new Insets(0)
			));
	
	private void widgetsCreation() {
		lTitle = new Label ();
		lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 120)); 
		lTitle.textProperty().bind(I18N.createStringBinding("btn.settings"));
		
		bBack = new Button();
		bBack.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
		bBack.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack.setPrefSize(200, 50);
		bBack.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bBack.setTranslateY(-75);
		bBack.setTranslateX(50);
		
		bBack1 = new Button();
		bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
		bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack1.setPrefSize(200, 50);
		bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bBack1.setTranslateY(-75);
		bBack1.setTranslateX(-50);
		
		lEffectSound = new Label();
		lEffectSound.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		lEffectSound.setPrefWidth(300);
		lEffectSound.textProperty().bind(I18N.createStringBinding("lb.sound"));
		
		lMusicText = new Label();
		lMusicText.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		lMusicText.setPrefWidth(300);
		lMusicText.textProperty().bind(I18N.createStringBinding("lb.music"));
		
		bRestore = new Button();
		bRestore.setFont(Font.font("Gabriola", FontWeight.BOLD, 30)); 
		bRestore.textProperty().bind(I18N.createStringBinding("btn.restore"));
		bRestore.setPrefSize(300, 50);	
		bRestore.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
		bRestore.addEventHandler(MouseEvent.MOUSE_ENTERED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bRestore.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
	              }
	            });
		bRestore.addEventHandler(MouseEvent.MOUSE_EXITED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bRestore.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	              }
	            });
		bRestore.addEventHandler(MouseEvent.MOUSE_PRESSED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bRestore.setPrefSize(296, 10);	
	            	  bRestore.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
	              }
	            });	
		bRestore.addEventHandler(MouseEvent.MOUSE_RELEASED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bRestore.setPrefSize(300, 50);	
	            	  bRestore.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	              }
	            });			
		
	}
	
	private void containersCreation() {
		hbTop = new HBox();
		hbTop.setAlignment(Pos.CENTER);
		
		vbLeft = new VBox();
		vbLeft.setAlignment(Pos.BOTTOM_LEFT);
		
		vbCenter = new VBox();
		vbCenter.setAlignment(Pos.TOP_CENTER);
		vbCenter.setSpacing(50);
		
		vbRight = new VBox();
		vbRight.setAlignment(Pos.BOTTOM_RIGHT);
		
		hbCenter = new HBox();
		hbCenter.setAlignment(Pos.CENTER);
		
		hbBottomRight = new HBox();
		hbBottomRight.setAlignment(Pos.BOTTOM_RIGHT);
		
		hbBottomLeft = new HBox();
		hbBottomLeft.setAlignment(Pos.BOTTOM_LEFT);
		
		vbRestore =  new VBox();
		vbRestore.setAlignment(Pos.CENTER);
		
		
	}
	
	private void placementWidgetsInContainers() {
		hbTop.getChildren().add(lTitle);
		hbBottomLeft.getChildren().add(bBack);
		hbBottomRight.getChildren().add(bBack1);
		hbCenter.getChildren().add(vbCenter);
		vbRestore.getChildren().add(bRestore);
	}
	
	private void placementContainersInPane() {
		this.setTop(hbTop);
		this.setRight(vbRight);
		this.setLeft(vbLeft);		
		this.setCenter(hbCenter);
		this.setRight(hbBottomRight);
		this.setLeft(hbBottomLeft);
	}
	

	public void son() {
		lTitleSong = new Label();
		lTitleSong.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		lTitleSong.textProperty().bind(I18N.createStringBinding("lb.change"));
		
		EffectMusic();
		Music();
		
		vbSongs = new VBox();
		vbSongs.setAlignment(Pos.CENTER);
		vbSongs.getChildren().add(lTitleSong);
		vbSongs.getChildren().add(hbEffectSounds);
		vbSongs.getChildren().add(hbMusics);
		vbSongs.getChildren().add(new Label("Medieval Loop One, Alexander Nakarada"));
		vbSongs.getChildren().add(new Label("https://www.free-stock-music.com/alexander-nakarada-medieval-loop-one.html"));
		
		vbCenter.getChildren().add(vbSongs);
		vbSongs.setSpacing(20);
	
	}

//	protected void EffetSounds() {
//	    
//	    
//	    Media media = new Media(new File(DataController.SoundButton).toURI().toString());  
//	    MediaPlayer mediaPlayer = new MediaPlayer(media);  
//	    mediaPlayer.setAutoPlay(true);
//	    mediaPlayer.volumeProperty().bind(sPMusic.valueProperty().divide(100));
//	    //mediaPlayer.setAutoPlay(true);
//	    // Quand la valeur du slider change
//	    sPMusic.valueProperty().addListener(event -> {
//	        
//	        // Si 0 eteindre la music
//	        if (sPMusic.getValue() == 0) {
//	            mediaPlayer.pause(); 
//	        }
//	        
//	        // Attention la methode stop arrete completement la musique elle est jouer
//	        // depuis le debut apres un play et pause juste pause
//	        else {
//	            mediaPlayer.play();
//	        }
//	        
//	    });
//	    
//	    
//	    
	    
	    //Media buzzer = new Media(getClass().getResource("/audio/longTrack.mp3").toExternalForm());
	    //MediaPlayer mediaPlayer2 = new MediaPlayer(buzzer);
	    //Button myButton = new Button("Press me for sound!");
	    //myButton.setOnAction(event -> {
	    //    if(mediaPlayer.getStatus() != MediaPlayer.Status.PLAYING){
	    //        mediaPlayer.play();
	    //    }
//	    });
	    
//	}


private void Langues() {
	lTitleLanguages = new Label ();
	lTitleLanguages.setFont(Font.font("Gabriola", FontWeight.BOLD,40)); 
	lTitleLanguages.textProperty().bind(I18N.createStringBinding("lb.settings"));

	bFrenchLanguage = new Button();
	bFrenchLanguage.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
	bFrenchLanguage.textProperty().bind(I18N.createStringBinding("btn.fr"));
	bFrenchLanguage.setPrefSize(200, 50);	
	bFrenchLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	bFrenchLanguage.addEventHandler(MouseEvent.MOUSE_ENTERED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
          		bFrenchLanguage.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
              }
            });
	bFrenchLanguage.addEventHandler(MouseEvent.MOUSE_EXITED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bFrenchLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });
	bFrenchLanguage.addEventHandler(MouseEvent.MOUSE_PRESSED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            		bFrenchLanguage.setPrefSize(196, 10);	
            		bFrenchLanguage.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
              }
            });	
	bFrenchLanguage.addEventHandler(MouseEvent.MOUSE_RELEASED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            		bFrenchLanguage.setPrefSize(200, 50);	
            		bFrenchLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });	
	
	bEnglishLanguage = new Button();
	bEnglishLanguage.setFont(Font.font("Gabriola", FontWeight.BOLD, 30)); 
	bEnglishLanguage.textProperty().bind(I18N.createStringBinding("btn.en"));
	bEnglishLanguage.setPrefSize(200, 50);	
	bEnglishLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	bEnglishLanguage.addEventHandler(MouseEvent.MOUSE_ENTERED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEnglishLanguage.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
              }
            });
	bEnglishLanguage.addEventHandler(MouseEvent.MOUSE_EXITED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEnglishLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
            	  
              }
            });
	bEnglishLanguage.addEventHandler(MouseEvent.MOUSE_PRESSED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEnglishLanguage.setPrefSize(196, 10);	
            	  bEnglishLanguage.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
              }
            });	
	bEnglishLanguage.addEventHandler(MouseEvent.MOUSE_RELEASED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEnglishLanguage.setPrefSize(200, 50);	
            	  bEnglishLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });	
	
	
	bEspagnolLanguage = new Button();
	bEspagnolLanguage.setFont(Font.font("Gabriola", FontWeight.BOLD, 30)); 
	bEspagnolLanguage.textProperty().bind(I18N.createStringBinding("btn.es"));
	bEspagnolLanguage.setPrefSize(200, 50);	
	bEspagnolLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	bEspagnolLanguage.addEventHandler(MouseEvent.MOUSE_ENTERED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEspagnolLanguage.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
              }
            });
	bEspagnolLanguage.addEventHandler(MouseEvent.MOUSE_EXITED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEspagnolLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });
	bEspagnolLanguage.addEventHandler(MouseEvent.MOUSE_PRESSED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEspagnolLanguage.setPrefSize(196, 10);	
            	  bEspagnolLanguage.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
              }
            });	
	bEspagnolLanguage.addEventHandler(MouseEvent.MOUSE_RELEASED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEspagnolLanguage.setPrefSize(200, 50);	
            	  bEspagnolLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });	

	vbLanguages = new VBox();
	vbLanguages.setAlignment(Pos.CENTER);
	
	vbLanguages.setSpacing(20);
	
	hbGroupLanguages = new HBox();
	hbGroupLanguages.setAlignment(Pos.CENTER);
	hbGroupLanguages.getChildren().add(bFrenchLanguage);
	hbGroupLanguages.getChildren().add(bEnglishLanguage);
	hbGroupLanguages.getChildren().add(bEspagnolLanguage);
	
	hbGroupLanguages.setSpacing(90);
	
	vbLanguages.getChildren().add(lTitleLanguages);
	vbLanguages.getChildren().add(hbGroupLanguages);
	
	vbCenter.getChildren().add(vbLanguages);	
}


	

	private void CStyle() { // dark theme ou white theme
		lCardStyle = new Label();
		lCardStyle.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		lCardStyle.textProperty().bind(I18N.createStringBinding("txt.styleC"));

		bCSClassic = new Button();
		bCSClassic.setFont(Font.font("Gabriola", FontWeight.BOLD,30));
		bCSClassic.textProperty().bind(I18N.createStringBinding("btn.styleC"));
		bCSClassic.setPrefSize(200, 50);	
		bCSClassic.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
		bCSClassic.addEventHandler(MouseEvent.MOUSE_ENTERED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bCSClassic.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
	              }
	            });
		bCSClassic.addEventHandler(MouseEvent.MOUSE_EXITED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bCSClassic.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	              }
	            });
		bCSClassic.addEventHandler(MouseEvent.MOUSE_PRESSED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bCSClassic.setPrefSize(196, 10);	
	            	  bCSClassic.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
	              }
	            });	
		bCSClassic.addEventHandler(MouseEvent.MOUSE_RELEASED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bCSClassic.setPrefSize(200, 50);	
	            	  bCSClassic.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	              }
	            });	
		bCSSpecial = new Button();
		bCSSpecial.setFont(Font.font("Gabriola", FontWeight.BOLD,30));
		bCSSpecial.textProperty().bind(I18N.createStringBinding("btn.styleS"));
		bCSSpecial.setPrefSize(200, 50);	
		bCSSpecial.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
		bCSSpecial.addEventHandler(MouseEvent.MOUSE_ENTERED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bCSSpecial.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
	              }
	            });
		bCSSpecial.addEventHandler(MouseEvent.MOUSE_EXITED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bCSSpecial.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	              }
	            });
		bCSSpecial.addEventHandler(MouseEvent.MOUSE_PRESSED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bCSSpecial.setPrefSize(196, 10);	
	            	  bCSSpecial.setStyle("-fx-color: #904D4B; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
	              }
	            });	
		bCSSpecial.addEventHandler(MouseEvent.MOUSE_RELEASED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bCSSpecial.setPrefSize(200, 50);	
	            	  bCSSpecial.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	              }
	            });			
		
		vbCStyle = new VBox();
		vbCStyle.setAlignment(Pos.CENTER);
		
		vbCStyle.setSpacing(20);
		
		hbGroupeCStyle = new HBox();
		hbGroupeCStyle.setAlignment(Pos.CENTER);
		hbGroupeCStyle.getChildren().add(bCSClassic);
		hbGroupeCStyle.getChildren().add(bCSSpecial);
		
		hbGroupeCStyle.setSpacing(150);
		
		vbCStyle.getChildren().add(lCardStyle);
		vbCStyle.getChildren().add(hbGroupeCStyle);
		vbCenter.getChildren().addAll(vbCStyle, vbRestore);
	}

	public void EffectMusic() {
		hbEffectSounds = new HBox();
		hbEffectSounds.setAlignment(Pos.CENTER);
		
		sEffectSound = new Slider(0, 100, DataController.volumeEffect);
		sEffectSound.setPrefWidth(300);
		sEffectSound.setShowTickLabels(true);
		sEffectSound.setShowTickMarks(true);
		sEffectSound.setMajorTickUnit(20);
		sEffectSound.setMinorTickCount(5);
		sEffectSound.setBlockIncrement(10);
		
		
		tfEffectSounds = new TextField("" + DataController.volumeMusique);
		tfEffectSounds.setFont(Font.font("Gabriola", FontWeight.BOLD, 20));
		tfEffectSounds.setPrefColumnCount(2);
		tfEffectSounds.textProperty().bindBidirectional(sEffectSound.valueProperty(), new StringConverter<Number>(){
			@Override
			public String toString(Number t){
			t = t.intValue();
			return t.toString();
			}
			@Override
			public Number fromString(String s){
			int v=0;
			if (s!=null)
			if (!s.equals("")) {
			v = Integer.parseInt(s);
			if (v<0) v=0;
			if (v>100) v=100;
			}
			return v;
			}
			});
			
		
		hbEffectSounds.getChildren().addAll(lEffectSound, sEffectSound, tfEffectSounds); //lMusicOff en place 2
		
		
	}
	
	
	
	public void Music() {
		
		hbMusics = new HBox();
		hbMusics.setAlignment(Pos.CENTER);
		
	
		sMusic = new Slider(0, 100, DataController.volumeMusique);
		sMusic = new Slider();        
		sMusic.setPrefWidth(70);
		sMusic.setMaxWidth(Region.USE_PREF_SIZE);
		sMusic.setMinWidth(30);
		
		
		sMusic.setPrefWidth(300);
		sMusic.setShowTickLabels(true);
		sMusic.setShowTickMarks(true);
		sMusic.setMajorTickUnit(20);
		sMusic.setMinorTickCount(5);
		sMusic.setBlockIncrement(10);
		//sMusic.valueProperty().addListener(event -> cDialogue.definirVolumeMusic((int)sMusics.getValue()));
		
		 
		
		tfVolumeMusics = new TextField("" + DataController.volumeMusique);
		tfVolumeMusics.setFont(Font.font("Gabriola", FontWeight.BOLD, 20));
		tfVolumeMusics.setPrefColumnCount(2);
		tfVolumeMusics.textProperty().bindBidirectional(sMusic.valueProperty(), new StringConverter<Number>(){
			@Override
			public String toString(Number t){
			t = t.intValue();
			return t.toString();
			}
			@Override
			public Number fromString(String s){
			int v=0;
			if (s!=null)
			if (!s.equals("")) {
			v = Integer.parseInt(s);
			if (v<0) v=0;
			if (v>100) v=100;
			}
			return v;
			}
			});
			
		
		hbMusics.getChildren().addAll(lMusicText, sMusic, tfVolumeMusics); //lMusicOff en place 2
	
	}


	private Button getButtonLangue(Labeled n) {
		if(n.getText().equals(bFrenchLanguage.getText())) return bFrenchLanguage;
		if(n.getText().equals(bEnglishLanguage.getText())) return bEnglishLanguage;
		if(n.getText().equals(bEspagnolLanguage.getText())) return bEspagnolLanguage;
		return null;
	}
	
	
	public void definirLangue(Labeled n) {
		bFrenchLanguage.setBorder(bUnselected);
		bEnglishLanguage.setBorder(bUnselected);
		bEspagnolLanguage.setBorder(bUnselected);
		getButtonLangue(n).setBorder(bSelected);
	}



	protected void updateValues() {
	
		Media media = new Media(new File(DataController.music).toURI().toString());  
		MediaPlayer mediaPlayer = new MediaPlayer(media);  
		mediaPlayer.setAutoPlay(true);
	
	    mediaPlayer.volumeProperty().bind(sMusic.valueProperty().divide(100));
	    //mediaPlayer.setAutoPlay(true);
	    // Quand la valeur du slider change
	    sMusic.valueProperty().addListener(event -> {
	        
	        // Si 0 eteindre la music
	        if (sMusic.getValue() == 0) {
	            mediaPlayer.pause(); 
	        }
	        
	        // Attention la methode stop arrete completement la musique elle est jouer
	        // depuis le debut apres un play et pause juste pause
	        else {
	            mediaPlayer.play();
	
	        }
	        
	    });

		mediaPlayer.setOnEndOfMedia(new Runnable() {
        @Override
        public void run() {
        	mediaPlayer.seek(Duration.ZERO);;
        	mediaPlayer.play();
        }
    }); 
	 
	 
	}
	
	
	public void effectSounds() {
		
		
		Media media = new Media(new File(DataController.buttonEffect).toURI().toString());  
		MediaPlayer mediaPlayer = new MediaPlayer(media);  
		mediaPlayer.setAutoPlay(true);
	
	    mediaPlayer.volumeProperty().bind(sEffectSound.valueProperty().divide(100));
	    mediaPlayer.setAutoPlay(true);
	    // Quand la valeur du slider change
	    sEffectSound.valueProperty().addListener(event -> {
	        
	        // Si 0 eteindre la music
	        if (sEffectSound.getValue() == 0) {
	            mediaPlayer.pause(); 
	        }
	        
	        // Attention la methode stop arrete completement la musique elle est jouer
	        // depuis le debut apres un play et pause juste pause
	        else {
	            mediaPlayer.play();
	
	        }
	        
	    });

		
	}
	 
	
	
	public Settings(DialogueControllerTable cDialogue2) throws FileNotFoundException {
		
			cDialogue = cDialogue2;
			
			Image image1 = Caches.BG;
			// Creation de l'image en arriere plan/ image de fond
			BackgroundImage backgroundimage = new BackgroundImage(image1,
			BackgroundRepeat.NO_REPEAT,
			BackgroundRepeat.NO_REPEAT,
			BackgroundPosition.DEFAULT,
			new BackgroundSize(1.0, 1.0, true, true, false, false));
		
			Background background = new Background(backgroundimage);
			//On affecte ÃƒÆ’Ã‚Â  l'ÃƒÆ’Ã‚Â©cran courant l'image de fond dÃƒÆ’Ã‚Â©fini
			this.setBackground(background);
			
			widgetsCreation();
			containersCreation();
	
			placementWidgetsInContainers();
			placementContainersInPane();
			son();
			
			
			bBack.setOnAction(event -> {
				effectSounds();	
				cDialogue.showScreen(cDialogue2.getVBack());
			});
			
			bBack1.setOnAction(event -> {
				
				effectSounds();	
				cDialogue.showScreen(cDialogue2.getVBack());
			});
			
			Langues();
			
			
			CStyle();
			
			
			updateValues();
			
			
			
			bFrenchLanguage.setOnMouseClicked(event -> {
				
				I18N.setLocal(AppLanguage.FRANCAIS);
				effectSounds();	
			});
			
			bEnglishLanguage.setOnMouseClicked(event -> {
				
				I18N.setLocal(AppLanguage.ANGLAIS);
				effectSounds();
			});
			
			bEspagnolLanguage.setOnMouseClicked(event -> {
				
				I18N.setLocal(AppLanguage.ESPAGNOL);
				effectSounds();
			});
			
			
			
			
			
			//Boutton Styles de cartes
			bCSClassic.setOnMouseClicked(event -> {
				
				// ÃƒÂ  ajouter ici plus tard pour modif carte
				effectSounds();
				DataController.theme = false;
				cDialogue.getTableMenu().setTheme(false);
			});
			
			bCSSpecial.setOnMouseClicked(event -> {
				
				// ÃƒÂ  ajouter ici plus tard pour modif carte
				effectSounds();
				DataController.theme = true;
				cDialogue.getTableMenu().setTheme(true);
			});
			

			bRestore.setDefaultButton(true);
			bRestore.setOnAction(event -> {
				
				effectSounds();
				sMusic.setValue(DataController.volumeMusique);
				sEffectSound.setValue(DataController.volumeEffect);
				I18N.setLocal(AppLanguage.FRANCAIS);
				DataController.theme = false;
				cDialogue.getTableMenu().setTheme(false);
				
			});
	
			sMusic.setValue(DataController.volumeMusique);
			this.setVisible(false);
			cDialogue.saveScreen(screenName, this);
		}
	
	
	
	
}
