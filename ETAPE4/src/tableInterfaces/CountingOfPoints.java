package tableInterfaces;

import java.util.ArrayList;

import card.ObjectiveCard;
import cd.DataController.screenList;
import cd.DialogueControllerTable;
import cd.I18N;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import player.Player;

public class CountingOfPoints extends BorderPane {

	private DialogueControllerTable cDialogue = null;
	// On donne un nom a cette interface pour permettre l'identification par le
	// systeme
	private static final screenList ScreenName = screenList.COUNTINGOFPOINTS;

	// VBox qui sert de pile pour remplacer les boites et les boutons
	VBox stack = new VBox();

	// creation et initialisation des HBox des emplacements
	HBox place1 = new HBox();
	HBox place2 = new HBox();
	HBox place3 = new HBox();

	VBox place4 = new VBox();
	VBox place5 = new VBox();

	HBox place6 = new HBox();
	HBox place7 = new HBox();
	HBox place8 = new HBox();

	VBox place9 = new VBox();
	VBox place10 = new VBox();


	// creation et initialisation de la bordure
	Border outline = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
			new BorderWidths(1), new Insets(5)));

	// Image carte = new Image("C:\Users\pierr\Downloads/carteJeu.JPG");

	// creation et initialisation des VBox des places des joueurs
	VBox vbP1 = new VBox();
	VBox vbP2 = new VBox();
	VBox vbP3 = new VBox();
	VBox vbP4 = new VBox();
	VBox vbP5 = new VBox();
	VBox vbP6 = new VBox();

	VBox stackPlaces = new VBox();

	// creation et initialisation des HBox des piles des joueurs
	HBox hbDiscardAndDeckJ1 = new HBox();
	HBox hbDiscardAndDeckJ2 = new HBox();
	HBox hbDiscardAndDeckJ3 = new HBox();
	HBox hbDiscardAndDeckJ4 = new HBox();
	HBox hbDiscardAndDeckJ5 = new HBox();
	HBox hbDiscardAndDeckJ6 = new HBox();

	// creation et initialisation des piles des joueurs
	Rectangle rDeckJ1 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ1 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ2 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ2 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ3 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ3 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ4 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ4 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ5 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ5 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ6 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ6 = new Rectangle(0, 0, 100, 100);
	
	VBox vbButton1 = new VBox();
	VBox vbButton2 = new VBox();
	VBox vbButton3 = new VBox();
	VBox vbButton4 = new VBox();
	
	Button bSetting1 = new Button(); // Boutons paramètres
	Button bSetting2 = new Button();
	Button bSetting3 = new Button();
	Button bSetting4 = new Button();

	Button bRule1 = new Button(); // Boutons règles
	Button bRule2 = new Button();
	Button bRule3 = new Button();
	Button bRule4 = new Button();
	
	HBox topLeftButtons = new HBox();
	HBox topRightButtons = new HBox();
	HBox bottomLeftButtons = new HBox();
	HBox bottomRightButtons = new HBox();
	HBox topContent = new HBox();
	HBox bottomContent = new HBox();
	
	// creation et initialisation des Button des scores
	Button bScoreJ1 = new Button();
	Button bScoreJ2 = new Button();
	Button bScoreJ3 = new Button();
	Button bScoreJ4 = new Button();
	Button bScoreJ5 = new Button();
	Button bScoreJ6 = new Button();
	Button bNext = new Button();
	
	// creation et initialisation des VBox des colonnes	
	BorderPane bpColumn1 = new BorderPane();
	BorderPane bpColumn2 = new BorderPane();
	BorderPane bpColumn3 = new BorderPane();
	BorderPane bpColumn4 = new BorderPane();
	BorderPane bpColumn5 = new BorderPane();
	BorderPane bpColumn6 = new BorderPane();
	
	// creation et initialisation des VBox des cartes
	VBox vbStandardColumn1 = new VBox();
	VBox vbStandardColumn2 = new VBox();
	VBox vbStandardColumn3 = new VBox();
	VBox vbStandardColumn4 = new VBox();
	VBox vbStandardColumn5 = new VBox();
	VBox vbStandardColumn6 = new VBox();
	VBox vbDomainColumn1 = new VBox();
	VBox vbDomainColumn2 = new VBox();
	VBox vbDomainColumn3 = new VBox();
	VBox vbDomainColumn4 = new VBox();
	VBox vbDomainColumn5 = new VBox();
	VBox vbDomainColumn6 = new VBox();
	VBox vbOthersColumn1 = new VBox();
	VBox vbOthersColumn2 = new VBox();
	VBox vbOthersColumn3 = new VBox();
	VBox vbOthersColumn4 = new VBox();
	VBox vbOthersColumn5 = new VBox();
	VBox vbOthersColumn6 = new VBox();
	
	// creation et initialisation des Label principaux
	Label lTitleScore = new Label();
	Label lScore1 = new Label();
	Label lScore2 = new Label();
	Label lScore3 = new Label();
	Label lScore4 = new Label();
	Label lScore5 = new Label();
	Label lScore6 = new Label();
	
	// creation et initialisation des Label des pseudos
	Label lPseudoJ1 = new Label();
	Label lPseudoJ2 = new Label();
	Label lPseudoJ3 = new Label();
	Label lPseudoJ4 = new Label();
	Label lPseudoJ5 = new Label();
	Label lPseudoJ6 = new Label();
	
	public void setLabels() {
		lTitleScore.textProperty().bind(I18N.createStringBinding("lb.titleScore"));
		lScore1.textProperty().bind(I18N.createStringBinding("lb.score"));
		lScore2.textProperty().bind(I18N.createStringBinding("lb.score"));
		lScore3.textProperty().bind(I18N.createStringBinding("lb.score"));
		lScore4.textProperty().bind(I18N.createStringBinding("lb.score"));
		lScore5.textProperty().bind(I18N.createStringBinding("lb.score"));
	    lScore6.textProperty().bind(I18N.createStringBinding("lb.score"));

		bNext.setAlignment(Pos.CENTER);
		bNext.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bNext.setPrefSize(200, 50);
		bNext.setTranslateY(15);
		bNext.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
		bNext.textProperty().bind(I18N.createStringBinding("btn.next"));
	
	}

	// creation et initialisation des Box principales
	VBox buttons = new VBox();
	VBox leftPlaces = new VBox();
	HBox hCenter = new HBox();
	VBox center = new VBox();
	VBox rightPlaces = new VBox();
	HBox topPlaces = new HBox();
	VBox centre = new VBox();
	HBox bottomPlaces = new HBox();

	// creation et initialisation de la HBox pour mettre les colonnes
	HBox columns = new HBox();

	// creation et initialisation des BorderPane des colonnes
	BorderPane bpTopColumn1 = new BorderPane();
	BorderPane bpTopColumn2 = new BorderPane();
	BorderPane bpTopColumn3 = new BorderPane();
	BorderPane bpTopColumn4 = new BorderPane();
	BorderPane bpTopColumn5 = new BorderPane();
	BorderPane bpTopColumn6 = new BorderPane();

	// creation et initialisation des Vbox des colonnes
	VBox vbColumn1 = new VBox();
	VBox vbColumn2 = new VBox();
	VBox vbColumn3 = new VBox();
	VBox vbColumn4 = new VBox();
	VBox vbColumn5 = new VBox();
	VBox vbColumn6 = new VBox();

	// sauvegarde les places qu'ont pris les joueurs
	ArrayList<String> whoHasWhatPlace = new ArrayList<String>();

	/**
	 * placer un joueur
	 * 
	 * @param nbrElem
	 * @param position
	 * @param box
	 */
	public void setPlace(int nbrElem, int position, int box, String pseudo, String color) {
		whoHasWhatPlace.add(pseudo);
		stackPlaces.getChildren().get(0).setStyle(color);

		if (box == 1 && position == 2)
			topPlaces.setMargin(topPlaces.getChildren().get(1), new Insets(0, 0, 0, 0));
		if (box == 3 && position == 2)
			bottomPlaces.setMargin(bottomPlaces.getChildren().get(1), new Insets(0, 0, 0, 0));

		switch (box) {
		case 1:

			ChangeElementPlace(nbrElem, position, topPlaces, stackPlaces.getChildren().get(0)); // on remplace la boite
																								// de selection par la
																								// place du joueur
			if (position == 2)
				topPlaces.setMargin(topPlaces.getChildren().get(1), new Insets(0, 150, 0, 150));
			break;
		case 2:
			stackPlaces.getChildren().get(0).setRotate(90);
			stackPlaces.getChildren().get(0).setTranslateX(120);
			stackPlaces.getChildren().get(0).setTranslateY(90);
			VBox.setMargin(stackPlaces.getChildren().get(0), new Insets(0, 70, 150, 0));
			ChangeElementPlace(nbrElem, position, rightPlaces, stackPlaces.getChildren().get(0)); // on remplace la
																									// boite de
																									// selection par la
																									// place du joueur
			break;
		case 3:
			stackPlaces.getChildren().get(0).setRotate(180);
			stackPlaces.getChildren().get(0).setTranslateY(-5);
			ChangeElementPlace(nbrElem, position, bottomPlaces, stackPlaces.getChildren().get(0)); // on remplace la
																									// boite de
																									// selection par la
																									// place du joueur
			if (position == 2)
				bottomPlaces.setMargin(bottomPlaces.getChildren().get(1), new Insets(0, 150, 0, 150));
			break;
		case 4:
			stackPlaces.getChildren().get(0).setRotate(-90);
			stackPlaces.getChildren().get(0).setTranslateX(-50);
			stackPlaces.getChildren().get(0).setTranslateY(90);
			VBox.setMargin(stackPlaces.getChildren().get(0), new Insets(0, 70, 150, 0));
			ChangeElementPlace(nbrElem, position, leftPlaces, stackPlaces.getChildren().get(0)); // on remplace la boite
																									// de selection par
																									// la place du
																									// joueur
			break;
		}

		switch (whoHasWhatPlace.indexOf(pseudo) + 1) {
		case 1:
			vbP1.getChildren().remove(1);
			vbP1.getChildren().add(new Label(pseudo));
			break;
		case 2:
			vbP2.getChildren().remove(1);
			vbP2.getChildren().add(new Label(pseudo));
			break;
		case 3:
			vbP3.getChildren().remove(1);
			vbP3.getChildren().add(new Label(pseudo));
			break;
		case 4:
			vbP4.getChildren().remove(1);
			vbP4.getChildren().add(new Label(pseudo));
			break;
		case 5:
			vbP5.getChildren().remove(1);
			vbP5.getChildren().add(new Label(pseudo));
			break;
		case 6:
			vbP6.getChildren().remove(1);
			vbP6.getChildren().add(new Label(pseudo));
			break;
		}
	}

	// int nbrElem -> nombre d'elements dans la boite (2 ou 3)
	// int position -> position de l'element a remplacer (1, 2 ou 3)
	// Pane box -> boite dans laquelle on remplace
	// Node ObjetAPlacer -> nouvel element a placer
	public void ChangeElementPlace(int nbrElem, int position, Pane box, Node ObjetAPlacer) { // remplacer un �l�ment
																								// dans les boites des
																								// places
		Platform.runLater(() -> {
			if (nbrElem == 3) {
				if (position == 1) {
					stack.getChildren().addAll(box.getChildren().get(1), box.getChildren().get(2)); // on place
																									// les
																									// elements
																									// inutiles
																									// dans la
																									// pile

					box.getChildren().remove(box.getChildren().get(0)); // on supprime l'element a remplacer
					box.getChildren().addAll(ObjetAPlacer, stack.getChildren().get(0), stack.getChildren().get(1)); // ajoute
																													// l'element
																													// a
																													// placer
																													// au
																													// bon
																													// endroit
																													// en
																													// depilant
																													// les
																													// elements
																													// inutiles
				} else if (position == 2) {
					stack.getChildren().addAll(box.getChildren().get(0), box.getChildren().get(2));

					box.getChildren().remove(box.getChildren().get(0));
					box.getChildren().addAll(stack.getChildren().get(0), ObjetAPlacer, stack.getChildren().get(1));
				} else {
					stack.getChildren().addAll(box.getChildren().get(0), box.getChildren().get(1));

					box.getChildren().remove(box.getChildren().get(0));
					box.getChildren().addAll(stack.getChildren().get(0), stack.getChildren().get(1), ObjetAPlacer);
				}
			} else {
				if (position == 1) {
					stack.getChildren().add(box.getChildren().get(1));

					box.getChildren().remove(box.getChildren().get(0));
					box.getChildren().addAll(ObjetAPlacer, stack.getChildren().get(0));
				} else {
					stack.getChildren().add(box.getChildren().get(0));

					box.getChildren().remove(box.getChildren().get(0));
					box.getChildren().addAll(stack.getChildren().get(0), ObjetAPlacer);
				}
			}
		});
	}
	
	// donne la valeur aux boutons
	public void setScoreButtons(Button bouton, String type) {
		Platform.runLater(() -> {
			if(type=="standard")
				bouton.textProperty().bind(I18N.createStringBinding("btn.scoreStandard"));
			else if(type=="domain")
				bouton.textProperty().bind(I18N.createStringBinding("btn.scoreDomain"));
			else System.out.println("Type de carte non reconnu pour définir le bouton de score");
		});
	}

	/**
	 * affiche le bon nombre de colonne selon le bon nombre de joueur
	 * 
	 * @param nbrPlayer
	 */
	public void setColumns(int nbrPlayer) {
		Platform.runLater(() -> {
			center.setAlignment(Pos.CENTER);
			if(nbrPlayer >= 2) {
				vbOthersColumn1.setSpacing(-30.0);
				bpTopColumn1.setCenter(vbColumn1);
				bpTopColumn1.setBottom(lScore1);
				bpColumn1.setCenter(bpTopColumn1);
				vbOthersColumn2.setSpacing(-30.0);
				bpTopColumn2.setCenter(vbColumn2);
				bpTopColumn2.setBottom(lScore2);
				bpColumn2.setCenter(bpTopColumn2);
				bpColumn2.setMargin(bpTopColumn2, new Insets(0, 0, 0, 10));

				columns.getChildren().addAll(bpColumn1, bpColumn2);
			}
			if(nbrPlayer >= 3) {
				vbOthersColumn3.setSpacing(-30.0);
				bpTopColumn3.setCenter(vbColumn3);
				bpTopColumn3.setBottom(lScore3);
				bpColumn3.setCenter(bpTopColumn3);
				bpColumn3.setMargin(bpTopColumn3, new Insets(0, 0, 0, 10));
				columns.getChildren().add(bpColumn3);
			}
			if(nbrPlayer >= 4) {
				vbOthersColumn4.setSpacing(-30.0);
				bpTopColumn4.setCenter(vbColumn4);
				bpTopColumn4.setBottom(lScore4);
				bpColumn4.setCenter(bpTopColumn4);
				bpColumn4.setMargin(bpTopColumn4, new Insets(0, 0, 0, 10));
				columns.getChildren().add(bpColumn4);
			}
			if(nbrPlayer >= 5) {
				vbOthersColumn5.setSpacing(-30.0);
				bpTopColumn5.setCenter(vbColumn5);
				bpTopColumn5.setBottom(lScore5);
				bpColumn5.setCenter(bpTopColumn5);
				bpColumn5.setMargin(bpTopColumn5, new Insets(0, 0, 0, 10));
				columns.getChildren().add(bpColumn5);
			}
			if(nbrPlayer == 6) {
				vbOthersColumn6.setSpacing(-30.0);
				bpTopColumn6.setCenter(vbColumn6);
				bpTopColumn6.setBottom(lScore6);
				bpColumn6.setCenter(bpTopColumn6);
				bpColumn6.setMargin(bpTopColumn6, new Insets(0, 0, 0, 10));
				columns.getChildren().add(bpColumn6);
			}
		});
	}

	/**
	 * ajouter un pseudo a une colonne
	 * 
	 * @param column
	 * @param name
	 */
	public void addPseudoInColumn(int column, String name) {
		Platform.runLater(() -> {
			switch (column) {
			case 1:
				bpTopColumn1.setTop(new Label(name));
				break;
			case 2:
				bpTopColumn2.setTop(new Label(name));
				break;
			case 3:
				bpTopColumn3.setTop(new Label(name));
				break;
			case 4:
				bpTopColumn4.setTop(new Label(name));
				break;
			case 5:
				bpTopColumn5.setTop(new Label(name));
				break;
			case 6:
				bpTopColumn6.setTop(new Label(name));
				break;
			}
		});
	}

	/**
	 * ajoute une carte objectif dans une colonne
	 * 
	 * @param card
	 * @param column
	 */
	public void addObjectiveStandardCard(ObjectiveCard card, int column) {
		Platform.runLater(() -> {
			ImageView image = new ImageView();
			image = card.getPicture();

			switch (column) {
			case 1:
				if(vbStandardColumn1.getChildren().contains(image))
					vbStandardColumn1.getChildren().remove(image);
				vbStandardColumn1.getChildren().add(image);
				break;
			case 2:
				if(vbStandardColumn2.getChildren().contains(image))
					vbStandardColumn2.getChildren().remove(image);
				vbStandardColumn2.getChildren().add(image);
				break;
			case 3:
				if(vbStandardColumn3.getChildren().contains(image))
					vbStandardColumn3.getChildren().remove(image);
				vbStandardColumn3.getChildren().add(image);
				break;
			case 4:
				if(vbStandardColumn4.getChildren().contains(image))
					vbStandardColumn4.getChildren().remove(image);
				vbStandardColumn4.getChildren().add(image);
				break;
			case 5:
				if(vbStandardColumn5.getChildren().contains(image))
					vbStandardColumn5.getChildren().remove(image);
				vbStandardColumn5.getChildren().add(image);
				break;
			case 6:
				if(vbStandardColumn6.getChildren().contains(image))
					vbStandardColumn6.getChildren().remove(image);
				vbStandardColumn6.getChildren().add(image);
				break;
			}
		});
	}
	
	public void addObjectiveDomainCard(ObjectiveCard card, int column) {
		Platform.runLater(() -> {
			ImageView image = new ImageView();
			image = card.getPicture();

			switch (column) {
			case 1:
				if(vbStandardColumn1.getChildren().contains(image))
					vbStandardColumn1.getChildren().remove(image);
				vbDomainColumn1.getChildren().add(image);
				break;
			case 2:
				if(vbDomainColumn2.getChildren().contains(image))
					vbDomainColumn2.getChildren().remove(image);
				vbDomainColumn2.getChildren().add(image);
				break;
			case 3:
				if(vbDomainColumn3.getChildren().contains(image))
					vbDomainColumn3.getChildren().remove(image);
				vbDomainColumn3.getChildren().add(image);
				break;
			case 4:
				if(vbDomainColumn4.getChildren().contains(image))
					vbDomainColumn4.getChildren().remove(image);
				vbDomainColumn4.getChildren().add(image);
				break;
			case 5:
				if(vbStandardColumn5.getChildren().contains(image))
					vbStandardColumn5.getChildren().remove(image);
				vbDomainColumn5.getChildren().add(image);
				break;
			case 6:
				if(vbStandardColumn6.getChildren().contains(image))
					vbStandardColumn6.getChildren().remove(image);
				vbDomainColumn6.getChildren().add(image);
				break;
			}
		});
	}
	
	public void addObjectiveOthersCard(ObjectiveCard card, int column) {
		Platform.runLater(() -> {
			ImageView image = new ImageView();
			image = card.getPicture();

			switch (column) {
			case 1:
				vbOthersColumn1.getChildren().add(image);
				break;
			case 2:
				vbOthersColumn2.getChildren().add(image);
				break;
			case 3:
				vbOthersColumn3.getChildren().add(image);
				break;
			case 4:
				vbOthersColumn4.getChildren().add(image);
				break;
			case 5:
				vbOthersColumn5.getChildren().add(image);
				break;
			case 6:
				vbOthersColumn6.getChildren().add(image);
				break;
			}
		});
	}
	
	public void addObjectiveStandardInventory(ArrayList<ObjectiveCard> inventory, int column) {
		Platform.runLater(() -> {
			for (ObjectiveCard card : inventory)
				addObjectiveStandardCard(card, column);
		});
	}
	
	public void addObjectiveDomainInventory(ArrayList<ObjectiveCard> inventory, int column) {
		Platform.runLater(() -> {
			for (int j = 0; j < 6; j++)
				addObjectiveDomainCard(inventory.get(j), column);
			for (int j = 6; j < inventory.size(); j++)
				addObjectiveOthersCard(inventory.get(j), column);
		});
	}
	
	// configuration des boutons des scores
	private void setButtonToDomain(Button bouton, double standardScore, double domainScore, int column) {
		Platform.runLater(() -> {
			bouton.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					switch (column) {
					case 1:
						vbColumn1.getChildren().remove(vbStandardColumn1);
						vbColumn1.getChildren().addAll(vbDomainColumn1, vbOthersColumn1);
						lScore1.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScore));
						break;
					case 2:
						vbColumn2.getChildren().remove(vbStandardColumn2);
						vbColumn2.getChildren().addAll(vbDomainColumn2, vbOthersColumn2);
						lScore2.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScore));
						break;
					case 3:
						vbColumn3.getChildren().remove(vbStandardColumn3);
						vbColumn3.getChildren().addAll(vbDomainColumn3, vbOthersColumn3);
						lScore3.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScore));
						break;
					case 4:
						vbColumn4.getChildren().remove(vbStandardColumn4);
						vbColumn4.getChildren().addAll(vbDomainColumn4, vbOthersColumn4);
						lScore4.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScore));
						break;
					case 5:
						vbColumn5.getChildren().remove(vbStandardColumn5);
						vbColumn5.getChildren().addAll(vbDomainColumn5, vbOthersColumn5);
						lScore5.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScore));
						break;
					case 6:
						vbColumn6.getChildren().remove(vbStandardColumn6);
						vbColumn6.getChildren().addAll(vbDomainColumn6, vbOthersColumn6);
						lScore6.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScore));
						break;
					}
					bouton.textProperty().bind(I18N.createStringBinding("btn.scoreStandard"));
					setButtonToStandard(bouton, standardScore, domainScore, column);
				}
			});
		});
	}
	
	private void setButtonToStandard(Button bouton, double standardScore, double domainScore, int column) {
		Platform.runLater(() -> {
			bScoreJ1.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					switch (column) {
					case 1:
						vbColumn1.getChildren().removeAll(vbDomainColumn1, vbOthersColumn1);
						vbColumn1.getChildren().add(vbStandardColumn1);
						lScore5.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScore));
						break;
					case 2:
						vbColumn2.getChildren().removeAll(vbDomainColumn2, vbOthersColumn2);
						vbColumn2.getChildren().add(vbStandardColumn2);
						lScore2.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScore));
						break;
					case 3:
						vbColumn3.getChildren().removeAll(vbDomainColumn3, vbOthersColumn3);
						vbColumn3.getChildren().add(vbStandardColumn3);
						lScore3.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScore));
						break;
					case 4:
						vbColumn4.getChildren().removeAll(vbDomainColumn4, vbOthersColumn4);
						vbColumn4.getChildren().add(vbStandardColumn4);
						lScore4.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScore));
						break;
					case 5:
						vbColumn5.getChildren().removeAll(vbDomainColumn5, vbOthersColumn5);
						vbColumn5.getChildren().add(vbStandardColumn5);
						lScore5.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScore));
						break;
					case 6:
						vbColumn6.getChildren().removeAll(vbDomainColumn6, vbOthersColumn6);
						vbColumn6.getChildren().add(vbStandardColumn6);
						lScore6.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScore));
						break;
					}
					bouton.textProperty().bind(I18N.createStringBinding("btn.scoreDomain"));
					setButtonToDomain(bouton, standardScore, domainScore, column);
				}
			});
		});
	}

	// proc�dure de configuration des rectangles
	private void setRectangle(Rectangle rectangle) {
		rectangle.setFill(Color.TRANSPARENT);
		rectangle.setStroke(Color.BLACK);
	}

	// proc�dure de configuration des Hbox
	private void spRectangle(HBox box) {
		box.setSpacing(35);
		box.setPadding(new Insets(35, 35, 35, 35));
		box.setAlignment(Pos.CENTER);
	}

	// configure les piles (Discards et Decks) des joueurs
	private void configureRectangles() {
		setRectangle(rDeckJ1);
		setRectangle(rDiscardJ1);

		setRectangle(rDeckJ2);
		setRectangle(rDiscardJ2);

		setRectangle(rDeckJ3);
		setRectangle(rDiscardJ3);

		setRectangle(rDeckJ4);
		setRectangle(rDiscardJ4);

		setRectangle(rDeckJ5);
		setRectangle(rDiscardJ5);

		setRectangle(rDeckJ6);
		setRectangle(rDiscardJ6);

		spRectangle(hbDiscardAndDeckJ1);
		spRectangle(hbDiscardAndDeckJ2);
		spRectangle(hbDiscardAndDeckJ3);
		spRectangle(hbDiscardAndDeckJ4);
		spRectangle(hbDiscardAndDeckJ5);
		spRectangle(hbDiscardAndDeckJ6);
	}

	// configure les places des joueurs et leur attribut les piles
	private void configurePlacesPlayers() {
		lPseudoJ1 = new Label();
		lPseudoJ1.textProperty().bind(I18N.createStringBinding("lb.pseudoCp1"));

		lPseudoJ2 = new Label();
		lPseudoJ2.textProperty().bind(I18N.createStringBinding("lb.pseudoCp2"));

		lPseudoJ3 = new Label();
		lPseudoJ3.textProperty().bind(I18N.createStringBinding("lb.pseudoCp3"));

		lPseudoJ4 = new Label();
		lPseudoJ4.textProperty().bind(I18N.createStringBinding("lb.pseudoCp4"));

		lPseudoJ5 = new Label();
		lPseudoJ5.textProperty().bind(I18N.createStringBinding("lb.pseudoCp5"));

		lPseudoJ6 = new Label();
		lPseudoJ6.textProperty().bind(I18N.createStringBinding("lb.pseudoCp6"));
		vbP1.setBorder(outline);
		vbP1.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ1.getChildren().addAll(rDeckJ1, rDiscardJ1);
		vbP1.getChildren().addAll(hbDiscardAndDeckJ1, lPseudoJ1);

		vbP2.setBorder(outline);
		vbP2.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ2.getChildren().addAll(rDeckJ2, rDiscardJ2);
		vbP2.getChildren().addAll(hbDiscardAndDeckJ2, lPseudoJ2);

		vbP3.setBorder(outline);
		vbP3.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ3.getChildren().addAll(rDeckJ3, rDiscardJ3);
		vbP3.getChildren().addAll(hbDiscardAndDeckJ3, lPseudoJ3);

		vbP4.setBorder(outline);
		vbP4.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ4.getChildren().addAll(rDeckJ4, rDiscardJ4);
		vbP4.getChildren().addAll(hbDiscardAndDeckJ4, lPseudoJ4);

		vbP5.setBorder(outline);
		vbP5.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ5.getChildren().addAll(rDeckJ5, rDiscardJ5);
		vbP5.getChildren().addAll(hbDiscardAndDeckJ5, lPseudoJ5);

		vbP6.setBorder(outline);
		vbP6.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ6.getChildren().addAll(rDeckJ6, rDiscardJ6);
		vbP6.getChildren().addAll(hbDiscardAndDeckJ6, lPseudoJ6);

		stackPlaces.getChildren().addAll(vbP1, vbP2, vbP3, vbP4, vbP5, vbP6);
	}

	// configure les box invisibles (emplacements) pour bien placer les places des
	// joueurs
	private void configurePlacesInvisibles() {
		leftPlaces.setAlignment(Pos.CENTER_LEFT);
		center.setAlignment(Pos.CENTER);
		rightPlaces.setAlignment(Pos.CENTER_RIGHT);
		topPlaces.setAlignment(Pos.TOP_CENTER);
		bottomPlaces.setAlignment(Pos.BOTTOM_CENTER);

		place1.setPrefSize(330, 180);
		place2.setPrefSize(330, 180);
		place3.setPrefSize(330, 180);
		place4.setPrefSize(180, 330);
		place5.setPrefSize(180, 330);
		place6.setPrefSize(330, 180);
		place7.setPrefSize(330, 180);
		place8.setPrefSize(330, 180);
		place9.setPrefSize(180, 330);
		place10.setPrefSize(180, 330);

		bpTopColumn1.setStyle("-fx-border-color: #990d0d;  -fx-border-width: 2px;");
		bpTopColumn2.setStyle("-fx-border-color: #990d0d;  -fx-border-width: 3px;");
		bpTopColumn3.setStyle("-fx-border-color: #990d0d;  -fx-border-width: 2px;");
		bpTopColumn4.setStyle("-fx-border-color: #990d0d;  -fx-border-width: 2px;");
		bpTopColumn5.setStyle("-fx-border-color: #990d0d;  -fx-border-width: 2px;");
		bpTopColumn6.setStyle("-fx-border-color: #990d0d;  -fx-border-width: 2px;");


		bpTopColumn1.setMinSize(100, 0);
		bpTopColumn2.setMinSize(100, 0);
		bpTopColumn3.setMinSize(100, 0);
		bpTopColumn4.setMinSize(100, 0);
		bpTopColumn5.setMinSize(100, 0);
		bpTopColumn6.setMinSize(100, 0);
	}

	// place les elements In les boites
	private void placementWidgetsInContainers() {

		vbButton1.getChildren().add(bRule1);
		vbButton1.getChildren().add(bSetting1);
		topLeftButtons.getChildren().add(vbButton1);

		vbButton2.getChildren().add(bRule2);
		vbButton2.getChildren().add(bSetting2);
		topRightButtons.getChildren().add(vbButton2);

		vbButton3.getChildren().add(bSetting3);
		vbButton3.getChildren().add(bRule3);
		bottomLeftButtons.getChildren().add(vbButton3);

		vbButton4.getChildren().add(bSetting4);
		vbButton4.getChildren().add(bRule4);
		bottomRightButtons.getChildren().add(vbButton4);
		
		leftPlaces.getChildren().add(buttons);
		hCenter.getChildren().add(centre);
		center.getChildren().add(hCenter);

		topPlaces.getChildren().add(place1);
		topPlaces.getChildren().add(place2);
		topPlaces.getChildren().add(place3);
		rightPlaces.getChildren().add(place4);
		rightPlaces.getChildren().add(place5);
		bottomPlaces.getChildren().add(place6);
		bottomPlaces.getChildren().add(place7);
		bottomPlaces.getChildren().add(place8);
		leftPlaces.getChildren().add(place9);
		leftPlaces.getChildren().add(place10);

		hCenter.setAlignment(Pos.CENTER);
		centre.getChildren().addAll(lTitleScore, columns, bNext);
		centre.setMaxWidth(700);
		centre.setMinWidth(100);
		centre.setAlignment(Pos.TOP_CENTER);
		
		bpTopColumn1.setCenter(vbColumn1);
		bpTopColumn2.setCenter(vbColumn2);
		bpTopColumn3.setCenter(vbColumn3);
		bpTopColumn4.setCenter(vbColumn4);
		bpTopColumn5.setCenter(vbColumn5);
		bpTopColumn6.setCenter(vbColumn6);
		
		topContent.getChildren().add(topLeftButtons);
		topContent.getChildren().add(topPlaces);
		topContent.getChildren().add(topRightButtons);

		bottomContent.getChildren().add(bottomLeftButtons);
		bottomContent.getChildren().add(bottomPlaces);
		bottomContent.getChildren().add(bottomRightButtons);

		topContent.setSpacing(215);
		bottomContent.setSpacing(215);
	}
	
	public void changeSizeButton() {
		bSetting1.setPrefSize(300, 50);
		bSetting1.setPrefWidth(250);
		bSetting2.setPrefSize(300, 50);
		bSetting2.setPrefWidth(250);
		bSetting3.setPrefSize(300, 50);
		bSetting3.setPrefWidth(250);
		bSetting4.setPrefSize(300, 50);
		bSetting4.setPrefWidth(250);

		bRule1.setPrefSize(300, 50);
		bRule1.setPrefWidth(250);
		bRule2.setPrefSize(300, 50);
		bRule2.setPrefWidth(250);
		bRule3.setPrefSize(300, 50);
		bRule3.setPrefWidth(250);
		bRule4.setPrefSize(300, 50);
		bRule4.setPrefWidth(250);

		bRule3.setTranslateX(-11);
		bSetting3.setTranslateX(-11);
		bRule4.setTranslateX(11);
		bSetting4.setTranslateX(11);

	}
	
	public void RotateButton() {
		bSetting1.setRotate(180);
		bSetting2.setRotate(180);

		bRule1.setRotate(180);
		bRule2.setRotate(180);

		bRule3.setTranslateY(100);
		bSetting3.setTranslateY(100);
		bRule4.setTranslateY(100);
		bSetting4.setTranslateY(100);

	}
	
	public void changeButtonColor() {
		bRule1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bRule2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bRule3.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bRule4.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");

		bSetting1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bSetting2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bSetting3.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bSetting4.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");

		bRule1.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRule2.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRule3.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRule4.textProperty().bind(I18N.createStringBinding("btn.rules"));

		bSetting1.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bSetting2.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bSetting3.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bSetting4.textProperty().bind(I18N.createStringBinding("btn.settings"));

	}

	public void changeButtonPosition() {
		bRule1.setTranslateX(50);
		bRule2.setTranslateX(-50);
		bRule3.setTranslateX(50);
		bRule4.setTranslateX(-50);

		bSetting1.setTranslateX(50);
		bSetting2.setTranslateX(-50);
		bSetting3.setTranslateX(50);
		bSetting4.setTranslateX(-50);

		bRule1.setTranslateY(25);
		bRule2.setTranslateY(25);
		bRule3.setTranslateY(-25);
		bRule4.setTranslateY(-25);

		bSetting1.setTranslateY(50);
		bSetting2.setTranslateY(50);
	}

	// place le tout dans la boite principale
	private void placementContainersInPane() {
		this.setLeft(leftPlaces);
		this.setCenter(center);
		this.setRight(rightPlaces);
		this.setTop(topContent);
		this.setBottom(bottomContent);
	}
	
	//affiche les scores et les cartes à la fin du jeu
	public void showFinalsScores() {
		Platform.runLater(() -> {
			int playerNumber = cDialogue.getMp().getGame().getNumPlayer();
			setColumns(playerNumber);
			for(int i=0;i<playerNumber;i++) {
				switch (i) {
				case 0:
					addPseudoInColumn(1, cDialogue.getMp().getGame().getPlayers().get(i).getPseudo());
					ArrayList<ObjectiveCard> standardInventoryJ1 = cDialogue.getMp().getGame().getPlayers().get(i).getInventory();
					double standardScoreJ1 = cDialogue.getMp().getGame().getPlayers().get(i).getStandardScore();
				 	if(cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard().isEmpty()) {
						String domainInventoryJ1 = "null";
						vbColumn1.getChildren().add(vbStandardColumn1);
						addObjectiveStandardInventory(standardInventoryJ1, 1);
						lScore1.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ1));
					}else {
						ArrayList<ObjectiveCard> domainInventoryJ1 = cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard();
						double domainScoreJ1 = cDialogue.getMp().getGame().getPlayers().get(i).getDomainScore();
						bpColumn1.setBottom(bScoreJ1);
						addObjectiveStandardInventory(standardInventoryJ1, 1);
						addObjectiveDomainInventory(domainInventoryJ1, 1);
						if(standardScoreJ1>=domainScoreJ1) {
							setButtonToDomain(bScoreJ1, standardScoreJ1, domainScoreJ1, 1);
							vbColumn1.getChildren().add(vbStandardColumn1);
							lScore1.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ1));
							setScoreButtons(bScoreJ1, "domain");
						}else {
							setButtonToStandard(bScoreJ1, standardScoreJ1, domainScoreJ1, 1);
							vbColumn1.getChildren().addAll(vbDomainColumn1, vbOthersColumn1);
							lScore1.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScoreJ1));
							setScoreButtons(bScoreJ1, "standard");
					}}break;
					
				case 1:
					addPseudoInColumn(2, cDialogue.getMp().getGame().getPlayers().get(i).getPseudo());
					ArrayList<ObjectiveCard> standardInventoryJ2 = cDialogue.getMp().getGame().getPlayers().get(i).getInventory();
					double standardScoreJ2 = cDialogue.getMp().getGame().getPlayers().get(i).getStandardScore();
					if(cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard().isEmpty()) {
						String domainInventoryJ2 = "null";
						vbColumn2.getChildren().add(vbStandardColumn2);
						addObjectiveStandardInventory(standardInventoryJ2, 2);
						lScore2.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ2));
					}else {
						ArrayList<ObjectiveCard> domainInventoryJ2 = cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard();
						double domainScoreJ2 = cDialogue.getMp().getGame().getPlayers().get(i).getDomainScore();
						bpColumn2.setBottom(bScoreJ2);
						if(standardScoreJ2>=domainScoreJ2) {
							setButtonToDomain(bScoreJ2, standardScoreJ2, domainScoreJ2, 2);
							addObjectiveStandardInventory(standardInventoryJ2, 2);
							vbColumn2.getChildren().add(vbStandardColumn2);
							lScore2.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ2));
							setScoreButtons(bScoreJ2, "domain");
						}else {
							setButtonToStandard(bScoreJ2, standardScoreJ2, domainScoreJ2, 2);
							addObjectiveDomainInventory(domainInventoryJ2, 2);
							vbColumn2.getChildren().addAll(vbDomainColumn2, vbOthersColumn2);
							lScore2.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScoreJ2));
							setScoreButtons(bScoreJ2, "standard");
					}}break;
					
				case 2:
					addPseudoInColumn(3, cDialogue.getMp().getGame().getPlayers().get(i).getPseudo());
					ArrayList<ObjectiveCard> standardInventoryJ3 = cDialogue.getMp().getGame().getPlayers().get(i).getInventory();
					double standardScoreJ3 = cDialogue.getMp().getGame().getPlayers().get(i).getStandardScore();
					if(cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard().isEmpty()) {
						String domainInventoryJ3 = "null";
						vbColumn3.getChildren().add(vbStandardColumn3);
						addObjectiveStandardInventory(standardInventoryJ3, 3);
						lScore3.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ3));
					}else {
						ArrayList<ObjectiveCard> domainInventoryJ3 = cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard();
						double domainScoreJ3 = cDialogue.getMp().getGame().getPlayers().get(i).getDomainScore();
						bpColumn3.setBottom(bScoreJ3);
						if(standardScoreJ3>=domainScoreJ3) {
							setButtonToDomain(bScoreJ3, standardScoreJ3, domainScoreJ3, 3);
							addObjectiveStandardInventory(standardInventoryJ3, 3);
							vbColumn3.getChildren().add(vbStandardColumn3);
							lScore3.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ3));
							setScoreButtons(bScoreJ3, "domain");
						}else {
							setButtonToStandard(bScoreJ3, standardScoreJ3, domainScoreJ3, 4);
							addObjectiveDomainInventory(domainInventoryJ3, 3);
							vbColumn3.getChildren().addAll(vbDomainColumn3, vbOthersColumn3);
							lScore3.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScoreJ3));
							setScoreButtons(bScoreJ3, "standard");
					}}break;
					
				case 3:
					addPseudoInColumn(4, cDialogue.getMp().getGame().getPlayers().get(i).getPseudo());
					ArrayList<ObjectiveCard> standardInventoryJ4 = cDialogue.getMp().getGame().getPlayers().get(i).getInventory();
					double standardScoreJ4 = cDialogue.getMp().getGame().getPlayers().get(i).getStandardScore();
					if(cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard().isEmpty()) {
						String domainInventoryJ4 = "null";
						vbColumn4.getChildren().add(vbStandardColumn4);
						addObjectiveStandardInventory(standardInventoryJ4, 4);
						lScore4.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ4));
					}else {
						ArrayList<ObjectiveCard> domainInventoryJ4 = cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard();
						double domainScoreJ4 = cDialogue.getMp().getGame().getPlayers().get(i).getDomainScore();
						bpColumn4.setBottom(bScoreJ4);
						if(standardScoreJ4>=domainScoreJ4) {
							setButtonToDomain(bScoreJ4, standardScoreJ4, domainScoreJ4, 4);
							addObjectiveStandardInventory(standardInventoryJ4, 4);
							vbColumn4.getChildren().add(vbStandardColumn4);
							lScore4.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ4));
							setScoreButtons(bScoreJ4, "domain");
						}else {
							setButtonToStandard(bScoreJ4, standardScoreJ4, domainScoreJ4, 4);
							addObjectiveDomainInventory(domainInventoryJ4, 4);
							vbColumn4.getChildren().addAll(vbDomainColumn4, vbOthersColumn4);
							lScore4.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScoreJ4));
							setScoreButtons(bScoreJ4, "standard");
					}}break;
					
				case 4:
					addPseudoInColumn(5, cDialogue.getMp().getGame().getPlayers().get(i).getPseudo());
					ArrayList<ObjectiveCard> standardInventoryJ5 = cDialogue.getMp().getGame().getPlayers().get(i).getInventory();
					double standardScoreJ5 = cDialogue.getMp().getGame().getPlayers().get(i).getStandardScore();
					if(cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard().isEmpty()) {
						String domainInventoryJ5 = "null";
						vbColumn5.getChildren().add(vbStandardColumn5);
						addObjectiveStandardInventory(standardInventoryJ5, 5);
						lScore5.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ5));
					}else {
						ArrayList<ObjectiveCard> domainInventoryJ5 = cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard();
						double domainScoreJ5 = cDialogue.getMp().getGame().getPlayers().get(i).getDomainScore();
						bpColumn5.setBottom(bScoreJ5);
						if(standardScoreJ5>=domainScoreJ5) {
							setButtonToDomain(bScoreJ5, standardScoreJ5, domainScoreJ5, 5);
							addObjectiveStandardInventory(standardInventoryJ5, 5);
							vbColumn5.getChildren().add(vbStandardColumn5);
							lScore5.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ5));
							setScoreButtons(bScoreJ5, "domain");
						}else {
							setButtonToStandard(bScoreJ5, standardScoreJ5, domainScoreJ5, 5);
							addObjectiveDomainInventory(domainInventoryJ5, 5);
							vbColumn5.getChildren().addAll(vbDomainColumn5, vbOthersColumn5);
							lScore5.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScoreJ5));
							setScoreButtons(bScoreJ5, "standard");
					}}break;
					
				case 5:
					addPseudoInColumn(6, cDialogue.getMp().getGame().getPlayers().get(i).getPseudo());
					ArrayList<ObjectiveCard> standardInventoryJ6 = cDialogue.getMp().getGame().getPlayers().get(i).getInventory();
					double standardScoreJ6 = cDialogue.getMp().getGame().getPlayers().get(i).getStandardScore();
					if(cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard().isEmpty()) {
						String domainInventoryJ6 = "null";
						vbColumn6.getChildren().add(vbStandardColumn6);
						addObjectiveStandardInventory(standardInventoryJ6, 6);
						lScore6.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ6));
					}else {
						ArrayList<ObjectiveCard> domainInventoryJ6 = cDialogue.getMp().getGame().getPlayers().get(i).getTriedObjectiveCard();
						double domainScoreJ6 = cDialogue.getMp().getGame().getPlayers().get(i).getDomainScore();
						bpColumn6.setBottom(bScoreJ6);
						if(standardScoreJ6>=domainScoreJ6) {
							setButtonToDomain(bScoreJ6, standardScoreJ6, domainScoreJ6, 6);
							addObjectiveStandardInventory(standardInventoryJ6, 6);
							vbColumn6.getChildren().add(vbStandardColumn6);
							lScore6.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+standardScoreJ6));
							setScoreButtons(bScoreJ6, "domain");
						}else {
							setButtonToStandard(bScoreJ6, standardScoreJ6, domainScoreJ6, 6);
							addObjectiveDomainInventory(domainInventoryJ6, 6);
							vbColumn6.getChildren().addAll(vbDomainColumn6, vbOthersColumn6);
						    lScore6.textProperty().bind(I18N.createStringBinding("lb.score").concat(" "+domainScoreJ6));
							setScoreButtons(bScoreJ6, "standard");
					}}break;
				}
			}
			
			ArrayList<Player> leadboard = new ArrayList<>();
			cDialogue.getMp().getGame().setWinner(leadboard);
			cDialogue.getMp().sendEndGame(leadboard);
			
			for(Player player : leadboard) 
				cDialogue.getEndGameTable().addPlayerInClassement(player.getPseudo(), player.getScore());
		});
	}

	// Initialise l'interface avec tout les composants necessaires
	// On affecte aussi des actions aux Button pour naviguer entre les diff�rentes
	// interfaces
	public CountingOfPoints(DialogueControllerTable cDialogue2) {
		cDialogue = cDialogue2;
		this.setBackground(new Background(new BackgroundFill(Color.web("#EFE4B0"), CornerRadii.EMPTY, null)));

		// appel les proc�dures d'initialisations et de configurations
		configureRectangles();
		configurePlacesPlayers();
		configurePlacesInvisibles();
		placementWidgetsInContainers();
		placementContainersInPane();
		setLabels();
		changeSizeButton();
		RotateButton();
		changeButtonColor();
		changeButtonPosition();
		
		//pour tester setColumns(6);
		/*setColumns(6);
		
		addPseudoInColumn(1, "Pierre"); addPseudoInColumn(2, "Martin");
		addPseudoInColumn(3, "Gautier"); addPseudoInColumn(4, "Lena");
		addPseudoInColumn(5, "Eidem"); addPseudoInColumn(6, "Marco");
			 
		addObjectiveCard(new ObjectiveCard(Domain.Com, 2), 1);
		addObjectiveCard(new ObjectiveCard(Domain.Com, 2), 1);
		addObjectiveCard(new ObjectiveCard(Domain.Com, 3), 3);
		addObjectiveCard(new ObjectiveCard(Domain.Com, 1), 2);
		addObjectiveCard(new ObjectiveCard(Domain.Com, 5), 4);
		addObjectiveCard(new ObjectiveCard(Domain.Com, 2), 6);*/
		
		// recuperation du nombre de joueur puis pour chaque joueur :
		// - son pseudo
		// - son inventaire standard
		// - son score standard
		// - son inventaire domaine :
		// -- si il n'existe pas, il est marque null dans un type String
		// -- si il existe, on le recupere
		// -- puis on recupere son score domaine

		// defini l'evenement du bouton Regles
		bRule1.setOnAction(event -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.RULES);
		});
		bSetting1.setOnAction((event) -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		bRule2.setOnAction(event -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.RULES);
		});
		bSetting2.setOnAction((event) -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		bRule3.setOnAction(event -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.RULES);
		});
		bSetting3.setOnAction((event) -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		bRule4.setOnAction(event -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.RULES);
		});
		bSetting4.setOnAction((event) -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		bNext.setOnAction(event -> {
			cDialogue.showScreen(screenList.ENDGAMETABLE);
		});

		this.setVisible(false);
		cDialogue.saveScreen(ScreenName, this);
	}
}
