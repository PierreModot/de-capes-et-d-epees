package tableInterfaces;

import cd.DataController.screenList;
import cd.DialogueControllerTable;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class AppTable extends Application {
	
		private StackPane root = new StackPane();
		private Node currentScreen = null;
		DialogueControllerTable cDialogue = new DialogueControllerTable(this);
		private Scene scene = new Scene(root);
	   
		
		@Override
		public void start(Stage primaryStage) throws Exception {
			cDialogue.defineMainIhmTable(this);	
			
			TableMenu tablemenu = new TableMenu(cDialogue);
			Placement placement = new Placement(cDialogue);
			CountingOfPoints counting = new CountingOfPoints(cDialogue);
			Rules rules = new Rules(cDialogue);
			Settings settings = new Settings(cDialogue);
			GameTable game = new GameTable(cDialogue);
			EndGameTable endgame = new EndGameTable(cDialogue);
			
			GameGoalRuleTable gamegoalrule = new GameGoalRuleTable(cDialogue);
			MaterialRuleTable material = new MaterialRuleTable(cDialogue);
			UnfoldingRuleTable unfolding = new UnfoldingRuleTable(cDialogue);
			EndRoundRuleTable endround = new EndRoundRuleTable(cDialogue);
			EndGameRuleTable endgamerule = new EndGameRuleTable(cDialogue);
			DifferentCardRuleTable different1 = new DifferentCardRuleTable(cDialogue);
			DifferentCardRuleTable2 different2 = new DifferentCardRuleTable2(cDialogue);
			DifferentCardRuleTable3 different3 = new DifferentCardRuleTable3(cDialogue);
			
			//ici, on affecte a chaque interface le nom de la page, pour pouvoir ensuite etre rediriger vers la page correspondante
			cDialogue.saveScreen(screenList.TABLEMENU , tablemenu);
			cDialogue.saveScreen(screenList.PLACEMENT, placement);
			cDialogue.saveScreen(screenList.COUNTINGOFPOINTS , counting);
			cDialogue.saveScreen(screenList.RULES , rules);
			cDialogue.saveScreen(screenList.SETTINGS ,settings);
			cDialogue.saveScreen(screenList.GAMETABLE , game);
			//cDialogue.saveScreen(screenList.PLAYINGCARD , new PlayingCard(cDialogue));
			cDialogue.saveScreen(screenList.ENDGAMETABLE , endgame);
			
						cDialogue.saveScreen(screenList.GAMEGOALRULETABLE , gamegoalrule);
			cDialogue.saveScreen(screenList.MATERIALRULETABLE , material);
			cDialogue.saveScreen(screenList.UNFOLDINGRULETABLE ,unfolding);
			cDialogue.saveScreen(screenList.ENDROUNDRULETABLE , endround);
			cDialogue.saveScreen(screenList.ENDGAMERULETABLE , endgamerule);
			cDialogue.saveScreen(screenList.DIFFERENTCARDRULETABLE , different1);
			cDialogue.saveScreen(screenList.DIFFERENTCARDRULETABLE2 , different2);
			cDialogue.saveScreen(screenList.DIFFERENTCARDRULETABLE3 ,different3);

			
			// Active le mode plein écran
			primaryStage.setFullScreen(true);
			// Cache le message du plein écran
			primaryStage.setFullScreenExitHint("");
			
			// On affecte dans "root" toutes les interfacesTable
			root.getChildren().add(tablemenu);
			root.getChildren().add(placement);
			root.getChildren().add(counting);
			root.getChildren().add(rules);
			root.getChildren().add(settings);
			root.getChildren().add(game);
			//root.getChildren().add(new PlayingCard(cDialogue));
			root.getChildren().add(endgame);

			root.getChildren().add(gamegoalrule);
			root.getChildren().add(material);
			root.getChildren().add(unfolding);
			root.getChildren().add(endround);
			root.getChildren().add(endgamerule);
			root.getChildren().add(different1);
			root.getChildren().add(different2);
			root.getChildren().add(different3);

			// Pour le lancement du jeu, on affiche par defaut l'interface MenuOrdinateur
			// Ce sera donc la premiere interface que verra l'utilisateur
			cDialogue.showScreen(screenList.TABLEMENU);
			
			primaryStage.setScene(scene);
			primaryStage.show();		
		}
		
		// Permet d'afficher à l'ecran l'interface correspondante
		public void showScreen(Node n) {
			if (currentScreen != null)
				currentScreen.setVisible(false);
			n.setVisible(true);
			currentScreen = n;
		}
		
		// permet le lancement de l'interface choisi par defaut
		public static void lancement(String[] args) {
		    AppTable.launch(args);
		}		
		
		public StackPane getRoot() {
			return root;
		}
	}
