package tableInterfaces;

import java.util.ArrayList;

import cd.DataController.screenList;
import cd.DialogueControllerTable;
import cd.I18N;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import util.Level;

public class Placement extends BorderPane {

	private DialogueControllerTable cDialogue = null;
	// On donne un nom a cette interface pour permettre l'identification par le
	// systeme
	private static final screenList nameScreen = screenList.PLACEMENT;

	// VBox qui sert de pile pour remplacer les boites et les boutons
	VBox stack = new VBox();

	// variable pour bloquer les boutons
	boolean var = false;
	// variable pour passer en mode ajouter bot
	boolean addBot = false;

	// variables pour le placement des joueurs
	int nbrElem; // -> nombre d'elements dans la boite (2 ou 3)
	int position; // -> position de l'element a remplacer (1, 2 ou 3)
	int box; // -> boite dans laquelle on remplace

	// boutons des boutons places
	Button bP1;
	Button bP2;
	Button bP3;
	Button bP4;
	Button bP5;
	Button bP6;
	Button bP7;
	Button bP8;
	Button bP9;
	Button bP10;

	// definition du border
	Border border = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
			new BorderWidths(1), new Insets(5)));

	// creation du borderpane centre 1
	BorderPane bpCenter1;

	// creation des boutons du centre
	Button bSetting;
	Button bBack;
	Button bRules;
	Button baddBot;
	Button bAddPlayer;
	Button bLaunch;

	// creation de la zone de gestion joueur
	Button bAccept;
	Button bRefuse;
	VBox vbCenterLG;
	VBox vbGestion;
	HBox hbPlayerGestion;
	Label lTryConnect;
	Label lTryPseudo;

	// creation des boutons du choix de la difficulte des bots
	Button bBotE;
	Button bBotM;
	Button bBotD;

	// creation des textes du code de la partie
	Label lCodeGameTop;
	Label lCodeGameRight;
	Label lCodeGameBottom;
	Label lCodeGameLeft;

	// creation des boites des places de gauche et droite
	VBox leftPlaces;
	VBox rightPlaces;

	// creation des boites des places en haut et en bas
	HBox topPlaces;
	HBox bottomPlaces;

	// creation des boites pour placer les boutons au centre
	VBox vbRightButtons;
	VBox vbLeftButtons;

	// creation de la boite du vbMiddle qui contient les places du haut et du bas et
	// le centre
	VBox vbMiddle;

	// creation du centre 2 (le centre dans le bpCenter1)
	HBox hbCenter2;

	// creation de la VBox pour le choix du joueur qui commence et le bouton start
	// etc
	VBox vbCenter3;

	// creation de la boite ou y a le choix des joueurs connectï¿½s
	VBox vbPseudoPlayerCo;
	// creation de la boite ou y a le choix de la difficultï¿½ des bots
	VBox vbBot;

	// creation des boites qui contiennent vbPseudoPlayerCo ou vbBot
	HBox hbPlayerSelection;
	VBox vbPlayerSelection;

	// creation des croix pour annuler le placement
	Button bCross;
	Button bCrossCote;

	// choisir qui commence
	ChoiceBox<String> cbWhoStart;
	ArrayList<String> playersValues = new ArrayList<String>();

	// crï¿½ation des boutons des couleurs
	Button bRedButton;
	Button bBlueButton;
	Button bPurpleButton;
	Button bWhiteButton;
	Button bYellowButton;
	Button bGreenButton;

	// creation des boites qui contiennent le choix des couleurs disponibles
	VBox vbColorSelectionAv;

	public void chooseColor(String pseudo, boolean bot) {

		if (bot) {
			if (box == 2 || box == 4)
				vbPlayerSelection.getChildren().remove(vbBot);
			else
				hbPlayerSelection.getChildren().remove(vbBot);
		}

		else {
			if (box == 2 || box == 4)
				vbPlayerSelection.getChildren().remove(hbPlayerSelection);
			else
				hbPlayerSelection.getChildren().remove(hbPlayerSelection);
		}

		if (box == 2 || box == 4)
			vbPlayerSelection.getChildren().add(vbColorSelectionAv);
		else
			hbPlayerSelection.getChildren().add(vbColorSelectionAv);

		bRedButton.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton joueur connecte 6
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				stackPlaces.getChildren().get(0).setStyle("-fx-background-color: #b22222;"); // met la couleur rouge
				vbColorSelectionAv.getChildren().remove(bRedButton);

				if (box == 2 || box == 4)
					vbPlayerSelection.getChildren().remove(vbColorSelectionAv);
				else
					hbPlayerSelection.getChildren().remove(vbColorSelectionAv);

				placePlayer(pseudo, "-fx-background-color: #b22222;");

			}
		});

		bWhiteButton.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton joueur connecte 6
			@Override
			public void handle(ActionEvent arg0) {
				stackPlaces.getChildren().get(0).setStyle("-fx-background-color: #f8f8ff; "); // met la couleur rouge
				vbColorSelectionAv.getChildren().remove(bWhiteButton);

				placePlayer(pseudo, "-fx-background-color: #f8f8ff; ");

				if (box == 2 || box == 4)
					vbPlayerSelection.getChildren().remove(vbColorSelectionAv);
				else
					hbPlayerSelection.getChildren().remove(vbColorSelectionAv);
			}
		});

		bGreenButton.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton joueur connecte 6
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				stackPlaces.getChildren().get(0).setStyle("-fx-background-color: #32cd32; "); // met la couleur rouge
				vbColorSelectionAv.getChildren().remove(bGreenButton);

				placePlayer(pseudo, "-fx-background-color: #32cd32; ");

				if (box == 2 || box == 4)
					vbPlayerSelection.getChildren().remove(vbColorSelectionAv);
				else
					hbPlayerSelection.getChildren().remove(vbColorSelectionAv);
			}
		});

		bBlueButton.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton joueur connecte 6
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				stackPlaces.getChildren().get(0).setStyle("-fx-background-color: #1e90ff; "); // met la couleur rouge
				vbColorSelectionAv.getChildren().remove(bBlueButton);

				placePlayer(pseudo, "-fx-background-color: #1e90ff; ");

				if (box == 2 || box == 4)
					vbPlayerSelection.getChildren().remove(vbColorSelectionAv);
				else
					hbPlayerSelection.getChildren().remove(vbColorSelectionAv);
			}
		});

		bPurpleButton.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton joueur connecte 6
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				stackPlaces.getChildren().get(0).setStyle("-fx-background-color: #9370db; "); // met la couleur rouge
				vbColorSelectionAv.getChildren().remove(bPurpleButton);

				placePlayer(pseudo, "-fx-background-color: #9370db; ");

				if (box == 2 || box == 4)
					vbPlayerSelection.getChildren().remove(vbColorSelectionAv);
				else
					hbPlayerSelection.getChildren().remove(vbColorSelectionAv);
			}
		});

		bYellowButton.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton joueur connecte 6
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				stackPlaces.getChildren().get(0).setStyle("-fx-background-color: #ffd700; "); // met la couleur rouge
				vbColorSelectionAv.getChildren().remove(bYellowButton);

				placePlayer(pseudo, "-fx-background-color: #ffd700; ");

				if (box == 2 || box == 4)
					vbPlayerSelection.getChildren().remove(vbColorSelectionAv);
				else
					hbPlayerSelection.getChildren().remove(vbColorSelectionAv);
			}
		});
	}

	public void placePlayer(String pseudo, String color) {
		if (box == 1 && position == 2)
			topPlaces.setMargin(topPlaces.getChildren().get(1), new Insets(0, 0, 0, 0));
		if (box == 3 && position == 2)
			bottomPlaces.setMargin(bottomPlaces.getChildren().get(1), new Insets(0, 0, 0, 0));
		if (addBot)
			stackPseudoBot.remove(0);

		addPlayer(pseudo);
		addNameInCb(pseudo);

		/*
		 * topPlaces.setMargin(vbP1, new Insets(0,15,0,15)); topPlaces.setMargin(vbP2,
		 * new Insets(0,15,0,15)); topPlaces.setMargin(vbP3, new Insets(0,15,0,15));
		 * topPlaces.setMargin(vbP4, new Insets(0,15,0,15)); topPlaces.setMargin(vbP5,
		 * new Insets(0,15,0,15)); topPlaces.setMargin(vbP6, new Insets(0,15,0,15));
		 * 
		 * bottomPlaces.setMargin(vbP1, new Insets(0,15,0,15));
		 * bottomPlaces.setMargin(vbP2, new Insets(0,15,0,15));
		 * bottomPlaces.setMargin(vbP3, new Insets(0,15,0,15));
		 * bottomPlaces.setMargin(vbP4, new Insets(0,15,0,15));
		 * bottomPlaces.setMargin(vbP5, new Insets(0,15,0,15));
		 * bottomPlaces.setMargin(vbP6, new Insets(0,15,0,15));
		 */
		switch (box) {
		case 1:
			cDialogue.getCountingOfPoints().setPlace(nbrElem, position, box, pseudo, color);
			cDialogue.getGameTable().setPlace(nbrElem, position, box, pseudo, color);
			stackPlaces.getChildren().get(0).setTranslateY(5);
			// top, right, bottom, left

			// topPlaces.getChildren().get(0).setTranslateX(-150);
			// topPlaces.getChildren().get(2).setTranslateX(-150);

			ChangeElementPlace(nbrElem, position, topPlaces, stackPlaces.getChildren().get(0)); // on remplace la boite
																								// de selection par la
																								// place du joueur
			topPlaces.setMargin(topPlaces.getChildren().get(1), new Insets(0, 150, 0, 150));
			break;
		case 2:
			cDialogue.getCountingOfPoints().setPlace(nbrElem, position, box, pseudo, color);
			cDialogue.getGameTable().setPlace(nbrElem, position, box, pseudo, color);
			stackPlaces.getChildren().get(0).setRotate(90);
			stackPlaces.getChildren().get(0).setTranslateX(120);
			stackPlaces.getChildren().get(0).setTranslateY(90);
			VBox.setMargin(stackPlaces.getChildren().get(0), new Insets(0, 70, 150, 0));
			ChangeElementPlace(nbrElem, position, rightPlaces, stackPlaces.getChildren().get(0)); // on remplace la
																									// boite de
																									// selection par la
																									// place du joueur
			break;
		case 3:
			cDialogue.getCountingOfPoints().setPlace(nbrElem, position, box, pseudo, color);
			cDialogue.getGameTable().setPlace(nbrElem, position, box, pseudo, color);
			stackPlaces.getChildren().get(0).setRotate(180);
			stackPlaces.getChildren().get(0).setTranslateY(-5);

			// bottomPlaces.getChildren().get(0).setTranslateX(-150);
			// bottomPlaces.getChildren().get(2).setTranslateX(-150);

			ChangeElementPlace(nbrElem, position, bottomPlaces, stackPlaces.getChildren().get(0)); // on remplace la
																									// boite de
																									// selection par la
																									// place du joueur
			bottomPlaces.setMargin(bottomPlaces.getChildren().get(1), new Insets(0, 150, 0, 150));
			break;
		case 4:
			cDialogue.getCountingOfPoints().setPlace(nbrElem, position, box, pseudo, color);
			cDialogue.getGameTable().setPlace(nbrElem, position, box, pseudo, color);
			stackPlaces.getChildren().get(0).setRotate(-90);
			stackPlaces.getChildren().get(0).setTranslateX(-50);
			stackPlaces.getChildren().get(0).setTranslateY(90);
			VBox.setMargin(stackPlaces.getChildren().get(0), new Insets(0, 70, 150, 0));
			ChangeElementPlace(nbrElem, position, leftPlaces, stackPlaces.getChildren().get(0)); // on remplace la boite
																									// de selection par
																									// la place du
																									// joueur
			break;
		}

		switch (stackPlaces.getChildren().size()) {
		case 5:
			vbP1.getChildren().remove(1);
			vbP1.getChildren().add(new Label(pseudo));
			break;
		case 4:
			vbP2.getChildren().remove(1);
			vbP2.getChildren().add(new Label(pseudo));
			break;
		case 3:
			vbP3.getChildren().remove(1);
			vbP3.getChildren().add(new Label(pseudo));
			break;
		case 2:
			vbP4.getChildren().remove(1);
			vbP4.getChildren().add(new Label(pseudo));
			break;
		case 1:
			vbP5.getChildren().remove(1);
			vbP5.getChildren().add(new Label(pseudo));
			break;
		case 0:
			vbP6.getChildren().remove(1);
			vbP6.getChildren().add(new Label(pseudo));
			break;
		}

		// on supprime la vb du choix du bot ou joueur
		for (int i = vbPseudoPlayerCo.getChildren().size() - 1; i >= 0; i--) {
			Button b = (Button) vbPseudoPlayerCo.getChildren().get(i);
			if (b.getText() == pseudo) {
				vbPseudoPlayerCo.getChildren().remove(i);
				i--;
			}
		}

		var = false; // debloquer les autres boutons

	}

	/**
	 * ajoute un joueur dans la liste des joueurs qui peuvent choisir une place
	 * 
	 * @param pseudo
	 */
	public void newUnplacedPlayer(String pseudo) {
		Button bPseudo = new Button(pseudo);
		bPseudo.setPrefSize(200, 25);
		bPseudo.setAlignment(Pos.CENTER);
		vbPseudoPlayerCo.getChildren().add(bPseudo);

		bPseudo.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton joueur connecte 6
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (box == 2 || box == 4)
					vbPlayerSelection.getChildren().remove(vbPseudoPlayerCo);
				else
					hbPlayerSelection.getChildren().remove(vbPseudoPlayerCo);

				chooseColor(pseudo, false);

			}
		});

	}

	/**
	 * ajoute un nom de joueur dans la choice box
	 * 
	 * @param nom
	 */
	public void addNameInCb(String nom) {
		//playersValues.add(nom);
		//cbWhoStart = new ChoiceBox<Object>(FXCollections.observableArrayList(playersValues));
		//Label test = new Label(cbWhoStart.getItems().toString());
		//vbCenter3.getChildren().add(test);

		cbWhoStart.getItems().add(nom);
	}

	// creation des places de jeu pour les joueurs qui ont rejoint
	// creation des boites place
	VBox vbP1;
	VBox vbP2;
	VBox vbP3;
	VBox vbP4;
	VBox vbP5;
	VBox vbP6;

	// creation de la stack pour mettre les places dans l'ordre (vbP1 -> vbP2 ->
	// ...)
	VBox stackPlaces;

	// creation des boites qui contiennet les fausse et les pioches
	HBox hbDiscardAndDeckJ1;
	HBox hbDiscardAndDeckJ2;
	HBox hbDiscardAndDeckJ3;
	HBox hbDiscardAndDeckJ4;
	HBox hbDiscardAndDeckJ5;
	HBox hbDiscardAndDeckJ6;

	// creation des rectangles pour la pioche et la fausse
	Rectangle rDeckJ1;
	Rectangle rDiscardJ1;

	Rectangle rDeckJ2;
	Rectangle rDiscardJ2;

	Rectangle rDeckJ3;
	Rectangle rDiscardJ3;

	Rectangle rDeckJ4;
	Rectangle rDiscardJ4;

	Rectangle rDeckJ5;
	Rectangle rDiscardJ5;

	Rectangle rDeckJ6;
	Rectangle rDiscardJ6;

	// creation des textes des pseudos
	Label lPseudoJ1;
	Label lPseudoJ2;
	Label lPseudoJ3;
	Label lPseudoJ4;
	Label lPseudoJ5;
	Label lPseudoJ6;

	ArrayList<String> stackPseudoBot = new ArrayList<String>();

	// procedure pour mettre la couleur de fond et de border aux rectangles
	private void setRectangle(Rectangle rectangle) {
		rectangle.setFill(Color.WHITE);
		rectangle.setStroke(Color.BLACK);
	}

	// mettre la couleur de fond et de border aux rectangles prioche et fausse
	private void configureRectangles() {
		setRectangle(rDeckJ1);
		setRectangle(rDiscardJ1);

		setRectangle(rDeckJ2);
		setRectangle(rDiscardJ2);

		setRectangle(rDeckJ3);
		setRectangle(rDiscardJ3);

		setRectangle(rDeckJ4);
		setRectangle(rDiscardJ4);

		setRectangle(rDeckJ5);
		setRectangle(rDiscardJ5);

		setRectangle(rDeckJ6);
		setRectangle(rDiscardJ6);
	}

	// procedure pour espacer les rectangles dans fausse et pioche
	private void spRectangle(HBox box) {
		box.setSpacing(35);
		box.setPadding(new Insets(35, 35, 35, 35));
		box.setAlignment(Pos.CENTER);
	}

	// espacer les rectangles dans fausse et pioche
	private void addSpaceRec() {
		spRectangle(hbDiscardAndDeckJ1);
		spRectangle(hbDiscardAndDeckJ2);
		spRectangle(hbDiscardAndDeckJ3);
		spRectangle(hbDiscardAndDeckJ4);
		spRectangle(hbDiscardAndDeckJ5);
		spRectangle(hbDiscardAndDeckJ6);
	}

	// configurer les places des joueurs qui ont rejoins
	private void configureDiscardAndDeck() {
		vbP1.setBorder(border);
		vbP1.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ1.getChildren().addAll(rDeckJ1, rDiscardJ1);
		vbP1.getChildren().addAll(hbDiscardAndDeckJ1, lPseudoJ1);
		vbP1.setStyle("-fx-background-color: purple");

		vbP2.setBorder(border);
		vbP2.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ2.getChildren().addAll(rDeckJ2, rDiscardJ2);
		vbP2.getChildren().addAll(hbDiscardAndDeckJ2, lPseudoJ2);
		vbP2.setStyle("-fx-background-color: yellow");

		vbP3.setBorder(border);
		vbP3.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ3.getChildren().addAll(rDeckJ3, rDiscardJ3);
		vbP3.getChildren().addAll(hbDiscardAndDeckJ3, lPseudoJ3);
		vbP3.setStyle("-fx-background-color: grey");

		vbP4.setBorder(border);
		vbP4.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ4.getChildren().addAll(rDeckJ4, rDiscardJ4);
		vbP4.getChildren().addAll(hbDiscardAndDeckJ4, lPseudoJ4);
		vbP4.setStyle("-fx-background-color: green");

		vbP5.setBorder(border);
		vbP5.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ5.getChildren().addAll(rDeckJ5, rDiscardJ5);
		vbP5.getChildren().addAll(hbDiscardAndDeckJ5, lPseudoJ5);
		vbP5.setStyle("-fx-background-color: red");

		vbP6.setBorder(border);
		vbP6.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ6.getChildren().addAll(rDeckJ6, rDiscardJ6);
		vbP6.getChildren().addAll(hbDiscardAndDeckJ6, lPseudoJ6);
		vbP6.setStyle("-fx-background-color: blue");

		stackPseudoBot.add("BOT 1");
		stackPseudoBot.add("BOT 2");
		stackPseudoBot.add("BOT 3");
		stackPseudoBot.add("BOT 4");
		stackPseudoBot.add("BOT 5");
		stackPseudoBot.add("BOT 6");

		// mettre les places dans la pile
		stackPlaces.getChildren().addAll(vbP1, vbP2, vbP3, vbP4, vbP5, vbP6);
	}

	// On crï¿½ï¿½ les composants de la page et on les initialise (Label, Button)
	private void widgetsCreation() {

		// creation et initialisation des boutons des placesde bP1 ï¿½ bP10
		bP1 = new Button();
		bP1.setPrefSize(330, 180);
		bP1.setRotate(180);
		bP1.textProperty().bind(I18N.createStringBinding("btn.bchoose"));
		bP2 = new Button();
		bP2.setPrefSize(330, 180);
		bP2.setRotate(180);
		bP2.textProperty().bind(I18N.createStringBinding("btn.bchoose"));

		bP3 = new Button();
		bP3.setPrefSize(330, 180);
		bP3.setRotate(180);
		bP3.textProperty().bind(I18N.createStringBinding("btn.bchoose"));
		bP4 = new Button();
		bP4.setPrefSize(180, 330);
		bP4.textProperty().bind(I18N.createStringBinding("btn.bchoose"));
		bP5 = new Button();
		bP5.setPrefSize(180, 330);
		bP5.textProperty().bind(I18N.createStringBinding("btn.bchoose"));
		bP6 = new Button();
		bP6.setPrefSize(330, 180);
		bP6.textProperty().bind(I18N.createStringBinding("btn.bchoose"));
		bP7 = new Button();
		bP7.setPrefSize(330, 180);
		bP7.textProperty().bind(I18N.createStringBinding("btn.bchoose"));
		bP8 = new Button();
		bP8.setPrefSize(330, 180);
		bP8.textProperty().bind(I18N.createStringBinding("btn.bchoose"));
		bP9 = new Button();
		bP9.setPrefSize(180, 330);
		bP9.textProperty().bind(I18N.createStringBinding("btn.bchoose"));
		bP10 = new Button();
		bP10.setPrefSize(180, 330);
		bP10.textProperty().bind(I18N.createStringBinding("btn.bchoose"));
		bP1.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bP2.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bP3.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bP4.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bP5.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bP6.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bP7.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bP8.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bP9.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bP10.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");

		// creation et initialisation du Button bParameters
		bSetting = new Button();
		bSetting.setPrefSize(225, 50);
		bSetting.setStyle("-fx-font-size : 22; -fx-text-fill: #ece3dd; -fx-color: #990d0d; -fx-border-color : #cc7c7c");
		bSetting.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bSetting.setTranslateY(-100);
		bSetting.setTranslateX(-50);
		// creation et initialisation du Button bBack
		bBack = new Button();
		bBack.setPrefSize(225, 50);
		bBack.setStyle("-fx-font-size : 22; -fx-text-fill: #ece3dd; -fx-color: #990d0d; -fx-border-color : #cc7c7c");
		bBack.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack.setTranslateY(100);
		bBack.setTranslateX(-50);
		// creation et initialisation du Button bRules
		bRules = new Button("Regles du jeu");
		bRules.setPrefSize(225, 50);
		bRules.setStyle("-fx-font-size : 22; -fx-text-fill: #ece3dd; -fx-color: #990d0d; -fx-border-color : #cc7c7c");
		bRules.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRules.setTranslateY(-100);
		bRules.setTranslateX(50);
		// creation et initialisation du Button baddBot
		baddBot = new Button();
		baddBot.setPrefSize(225, 50);
		baddBot.setStyle("-fx-font-size : 22; -fx-text-fill: #ece3dd; -fx-color: #990d0d; -fx-border-color : #cc7c7c");
		baddBot.textProperty().bind(I18N.createStringBinding("btn.addBoto"));
		baddBot.setTranslateY(100);
		baddBot.setTranslateX(50);
		// creation et initialisation du Button bAddPlayer
		bAddPlayer = new Button();
		bAddPlayer.setPrefSize(225, 50);
		bAddPlayer.setStyle("-fx-font-size : 22; -fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bAddPlayer.textProperty().bind(I18N.createStringBinding("btn.addPlayer"));
		bAddPlayer.setTranslateY(100);
		bAddPlayer.setTranslateX(50);
		// creation et initialisation du Button bLaunch
		bLaunch = new Button();
		bLaunch.setPrefSize(250, 200);
		bLaunch.setAlignment(Pos.CENTER);
		bLaunch.setTranslateY(10);
		bLaunch.setStyle(
				"-fx-font-size : 28; -fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10; -fx-border-width : 3;");
		bLaunch.textProperty().bind(I18N.createStringBinding("btn.launch"));
		// creation et initialisation du Label lTryConnect
		lTryConnect = new Label();
		lTryPseudo = new Label();
		lTryConnect.textProperty().bind(I18N.createStringBinding("lb.tryconnect"));
		// creation et initialisation des HBox et VBox relatifs au centre
		hbPlayerGestion = new HBox();
		vbCenterLG = new VBox();
		vbGestion = new VBox();
		hbPlayerGestion.setAlignment(Pos.CENTER);
		vbGestion.setAlignment(Pos.CENTER);
		vbGestion.setStyle("-fx-text-fill: #f7f4e2; -fx-background-color: #d1482b; -fx-border-color : #9c3c10");
		vbGestion.setVisible(false);
		// creation et initialisation des Button bAccept et bRefuse
		bAccept = new Button();
		bRefuse = new Button();
		bAccept.setPrefSize(100, 25);
		bRefuse.setPrefSize(100, 25);
		bAccept.textProperty().bind(I18N.createStringBinding("btn.accept"));
		bRefuse.textProperty().bind(I18N.createStringBinding("btn.refuse"));
		bAccept.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bRefuse.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		vbCenterLG.getChildren().clear();
		vbCenterLG.getChildren().addAll(vbGestion);

		// creation et initialisation des boutons du choix de la difficulte des bots
		bBotE = new Button();
		bBotE.setPrefSize(200, 50);
		bBotE.setAlignment(Pos.CENTER);
		bBotE.textProperty().bind(I18N.createStringBinding("btn.simpleBot"));
		bBotM = new Button();
		bBotM.setPrefSize(200, 50);
		bBotM.setAlignment(Pos.CENTER);
		bBotM.textProperty().bind(I18N.createStringBinding("btn.midBot"));
		bBotD = new Button();
		bBotD.setPrefSize(200, 50);
		bBotD.setAlignment(Pos.CENTER);
		bBotD.textProperty().bind(I18N.createStringBinding("btn.hardBot"));
		bBotD.setDisable(true);
		// creation et initialisation des textes du code de la partie
		lCodeGameTop = new Label();
		lCodeGameRight = new Label();
		lCodeGameBottom = new Label();
		lCodeGameLeft = new Label();
		// creation et initialisation des croix pour annuler le placement
		bCross = new Button();
		bCross.setPrefSize(10, 10);
		bCross.setAlignment(Pos.TOP_RIGHT);
		bCross.textProperty().bind(I18N.createStringBinding("btn.cross"));
		bCrossCote = new Button("X");
		bCrossCote.setPrefSize(10, 10);
		bCrossCote.setAlignment(Pos.TOP_RIGHT);
		bCrossCote.textProperty().bind(I18N.createStringBinding("btn.cross"));

		// creation et initialisation des rectangles pour les 6 pioches et fausses
		rDeckJ1 = new Rectangle(0, 0, 100, 100);
		rDiscardJ1 = new Rectangle(0, 0, 100, 100);

		rDeckJ2 = new Rectangle(0, 0, 100, 100);
		rDiscardJ2 = new Rectangle(0, 0, 100, 100);

		rDeckJ3 = new Rectangle(0, 0, 100, 100);
		rDiscardJ3 = new Rectangle(0, 0, 100, 100);

		rDeckJ4 = new Rectangle(0, 0, 100, 100);
		rDiscardJ4 = new Rectangle(0, 0, 100, 100);

		rDeckJ5 = new Rectangle(0, 0, 100, 100);
		rDiscardJ5 = new Rectangle(0, 0, 100, 100);

		rDeckJ6 = new Rectangle(0, 0, 100, 100);
		rDiscardJ6 = new Rectangle(0, 0, 100, 100);

		configureRectangles();

		// creation des textes des pseudos
		lPseudoJ1 = new Label();
		lPseudoJ1.textProperty().bind(I18N.createStringBinding("lb.p1"));
		lPseudoJ2 = new Label();
		lPseudoJ2.textProperty().bind(I18N.createStringBinding("lb.p2"));
		lPseudoJ3 = new Label();
		lPseudoJ3.textProperty().bind(I18N.createStringBinding("lb.p3"));
		lPseudoJ4 = new Label();
		lPseudoJ4.textProperty().bind(I18N.createStringBinding("lb.p4"));
		lPseudoJ5 = new Label();
		lPseudoJ5.textProperty().bind(I18N.createStringBinding("lb.p5"));
		lPseudoJ6 = new Label();
		lPseudoJ6.textProperty().bind(I18N.createStringBinding("lb.p6"));

		// CREATION ET CONFIGURATION DU MENU DEROULANT DU CHOIX DU NOMBRE DE JOUEURS
		cbWhoStart = new ChoiceBox<String>();
		cbWhoStart.getItems().add("Aléatoire");
		//playersValues.add("Aléatoire");
		//cbWhoStart = new ChoiceBox<Object>(FXCollections.observableArrayList(playersValues)); // Liste observable
																								// permettant
																								// d'actualiser l'action
																								// sur fait sur le menu
																								// dï¿½roulant
																								// (rï¿½cuperer
																								// le choix)
		cbWhoStart.setValue("Aléatoire");
		cbWhoStart.setTooltip(new Tooltip("Choisir le joueur qui joue le premier")); // Mise en place d'une bulle
																						// d'informations lors du survol
																						// du menu dï¿½roulant

		cbWhoStart.setPrefSize(125, 25);
		cbWhoStart.setStyle("-fx-font-size : 18; -fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		cbWhoStart.setTranslateY(10);
		// crï¿½ation et initialisation des boutons des joueurs connectï¿½s
		bRedButton = new Button();
		bRedButton.setPrefSize(200, 25);
		bRedButton.setAlignment(Pos.CENTER);
		bRedButton.setStyle("-fx-background-color: #b22222; ");
		// bRedButton.setBorder(border);

		bBlueButton = new Button();
		bBlueButton.setPrefSize(200, 25);
		bBlueButton.setAlignment(Pos.CENTER);
		bBlueButton.setStyle("-fx-background-color: #1e90ff; ");
		// bBlueButton.setBorder(border);

		bWhiteButton = new Button();
		bWhiteButton.setPrefSize(200, 25);
		bWhiteButton.setAlignment(Pos.CENTER);
		bWhiteButton.setStyle("-fx-background-color: #f8f8ff; ");
		// bWhiteButton.setBorder(border);

		bPurpleButton = new Button();
		bPurpleButton.setPrefSize(200, 25);
		bPurpleButton.setAlignment(Pos.CENTER);
		bPurpleButton.setStyle("-fx-background-color: #4b0082; ");
		// bPurpleButton.setBorder(border);

		bYellowButton = new Button();
		bYellowButton.setPrefSize(200, 25);
		bYellowButton.setAlignment(Pos.CENTER);
		bYellowButton.setStyle("-fx-background-color: #ffd700; ");
		// bYellowButton.setBorder(border);

		bGreenButton = new Button();
		bGreenButton.setPrefSize(200, 25);
		bGreenButton.setAlignment(Pos.CENTER);
		bGreenButton.setStyle("-fx-background-color: #32cd32; ");
		// bGreenButton.setBorder(border);
	}

	// regler toutes les boites
	private void containersCreation() {
		// creation et initialisation des boites place
		vbP1 = new VBox();
		vbP2 = new VBox();
		vbP3 = new VBox();
		vbP4 = new VBox();
		vbP5 = new VBox();
		vbP6 = new VBox();

		// creation et initialisation de la stack pour mettre les places dans l'ordre
		// (vbP1 -> vbP2 -> ...)
		stackPlaces = new VBox();

		// creation et initialisation des boites qui contiennet les fausse et les
		// pioches
		hbDiscardAndDeckJ1 = new HBox();
		hbDiscardAndDeckJ2 = new HBox();
		hbDiscardAndDeckJ3 = new HBox();
		hbDiscardAndDeckJ4 = new HBox();
		hbDiscardAndDeckJ5 = new HBox();
		hbDiscardAndDeckJ6 = new HBox();

		addSpaceRec();

		// creation et initialisation de la VBox leftPlaces
		leftPlaces = new VBox();
		leftPlaces.setAlignment(Pos.CENTER_LEFT);

		// creation et initialisation de la VBox vbMiddle
		vbMiddle = new VBox();
		vbMiddle.setAlignment(Pos.CENTER);

		// creation et initialisation de la VBox placeDroite
		rightPlaces = new VBox();
		rightPlaces.setAlignment(Pos.CENTER_RIGHT);

		// creation et initialisation de la VBox vbRightButtons
		vbRightButtons = new VBox();
		vbRightButtons.setAlignment(Pos.CENTER);

		// creation et initialisation de la VBox vbPseudoPlayerCo
		vbPseudoPlayerCo = new VBox();
		vbPseudoPlayerCo.setAlignment(Pos.CENTER);
		vbPseudoPlayerCo.setSpacing(5);

		// creation et initialisation de la VBox vbBot
		vbBot = new VBox();
		vbBot.setAlignment(Pos.CENTER);
		vbBot.setSpacing(5);

		// creation et initialisation de la HBox hbPlayerSelection
		hbPlayerSelection = new HBox();
		hbPlayerSelection.setAlignment(Pos.CENTER);
		hbPlayerSelection.setPrefSize(330, 180);
		hbPlayerSelection.setBorder(border);

		// creation et initialisation de la VBox vbPlayerSelection
		vbPlayerSelection = new VBox();
		vbPlayerSelection.setAlignment(Pos.CENTER);
		vbPlayerSelection.setPrefSize(180, 330);
		vbPlayerSelection.setBorder(border);

		// creation et initialisation de la HBox topPlaces
		topPlaces = new HBox();
		topPlaces.setAlignment(Pos.TOP_CENTER);

		// creation et initialisation de la HBox bottomPlaces
		bottomPlaces = new HBox();
		bottomPlaces.setAlignment(Pos.BOTTOM_CENTER);

		// creation et initialisation de la HBox hbCenter2
		hbCenter2 = new HBox();
		hbCenter2.setAlignment(Pos.CENTER);

		// creation et initialisation de la VBox vbCenter3
		vbCenter3 = new VBox();
		vbCenter3.setAlignment(Pos.CENTER);

		// creation et initialisation du BorderPane centr1
		bpCenter1 = new BorderPane();
		bpCenter1.setMinHeight(425);
		bpCenter1.setMaxWidth(900);
		bpCenter1.setStyle("-fx-background-color: #EFE4B0; -fx-border-color : #6C3B39; -fx-border-width : 5;");

		// creation et initialisation de la VBox vbLeftButtons
		vbLeftButtons = new VBox();
		vbLeftButtons.setAlignment(Pos.CENTER);

		configureDiscardAndDeck();

		// creation et initialisation de la VBox vbColorSelection
		vbColorSelectionAv = new VBox();
		vbColorSelectionAv.setAlignment(Pos.CENTER);
		vbColorSelectionAv.setSpacing(5);
	}

	// On affecte les diffï¿½rents Label, Button, HBox, VBox dans leurs emplacements
	private void placementWidgetsInContainers() {
		leftPlaces.getChildren().addAll(bP10, bP9);
		rightPlaces.getChildren().addAll(bP4, bP5);
		topPlaces.getChildren().addAll(bP1, bP2, bP3);
		topPlaces.setMargin(bP2, new Insets(2, 150, 0, 150));
		vbMiddle.getChildren().addAll(bpCenter1);
		bottomPlaces.getChildren().addAll(bP8, bP7, bP6);
		topPlaces.setMargin(bP7, new Insets(2, 150, 0, 150));
		vbLeftButtons.getChildren().addAll(bSetting, bBack);
		hbCenter2.getChildren().addAll(vbLeftButtons, vbCenterLG, vbRightButtons);
		hbCenter2.setMargin(vbCenterLG, new Insets(0, 0, 20, 0));
		hbPlayerGestion.getChildren().addAll(bAccept, bRefuse);
		hbPlayerGestion.setMargin(bAccept, new Insets(0, 5, 0, 20));
		hbPlayerGestion.setMargin(bRefuse, new Insets(0, 20, 0, 5));
		vbGestion.getChildren().addAll(lTryPseudo, lTryConnect, hbPlayerGestion);
		vbGestion.setMargin(hbPlayerGestion, new Insets(30, 0, 15, 0));
		vbGestion.setMargin(lTryConnect, new Insets(5, 0, 0, 0));
		vbGestion.setMargin(lTryPseudo, new Insets(30, 0, 0, 0));
		vbCenter3.getChildren().addAll(hbCenter2, cbWhoStart);
		vbRightButtons.getChildren().addAll(bRules, baddBot);
		bpCenter1.setTop(lCodeGameTop);
		bpCenter1.setRight(lCodeGameRight);
		bpCenter1.setBottom(lCodeGameBottom);
		bpCenter1.setLeft(lCodeGameLeft);
		bpCenter1.setCenter(vbCenter3);
		vbBot.getChildren().addAll(bBotD, bBotM, bBotE);
		hbPlayerSelection.getChildren().add(bCross);
		vbPlayerSelection.getChildren().add(bCrossCote);
		vbColorSelectionAv.getChildren().addAll(bRedButton, bBlueButton, bYellowButton, bGreenButton, bPurpleButton,
				bWhiteButton);

	}

	private void placementContainersInPane() { // placer tout dans la boite principale
		this.setLeft(leftPlaces);
		this.setCenter(vbMiddle);
		this.setRight(rightPlaces);
		this.setTop(topPlaces);
		this.setBottom(bottomPlaces);
	}

	public void addPlacement() {
		widgetsCreation();
		containersCreation();
		placementWidgetsInContainers();
		placementContainersInPane();
	}

	// remplacer un ï¿½lï¿½ment dans les boites des places
	// int nbrElem -> nombre d'elements dans la boite (2 ou 3)
	// int position -> position de l'element a remplacer (1, 2 ou 3)
	// Pane box -> boite dans laquelle on remplace
	// Node ObjetAPlacer -> nouvel element a placer
	public void ChangeElementPlace(int nbrElem, int position, Pane box, Node ObjetAPlacer) {
		if (nbrElem == 3) {
			if (position == 1) {
				stack.getChildren().addAll(box.getChildren().get(1), box.getChildren().get(2)); // on place les autres
																								// elements dans la pile

				box.getChildren().remove(box.getChildren().get(0)); // on supprime l'element a remplacer
				box.getChildren().addAll(ObjetAPlacer, stack.getChildren().get(0), stack.getChildren().get(1)); // on
																												// ajoute
																												// l'element
																												// a
																												// placer
																												// au
																												// bon
																												// endroit
																												// en
																												// depilant
																												// les
																												// autres
																												// elements
			} else if (position == 2) {
				stack.getChildren().addAll(box.getChildren().get(0), box.getChildren().get(2));

				box.getChildren().remove(box.getChildren().get(0));
				box.getChildren().addAll(stack.getChildren().get(0), ObjetAPlacer, stack.getChildren().get(1));
			} else {
				stack.getChildren().addAll(box.getChildren().get(0), box.getChildren().get(1));

				box.getChildren().remove(box.getChildren().get(0));
				box.getChildren().addAll(stack.getChildren().get(0), stack.getChildren().get(1), ObjetAPlacer);
			}
		} else {
			if (position == 1) {
				stack.getChildren().add(box.getChildren().get(1));

				box.getChildren().remove(box.getChildren().get(0));
				box.getChildren().addAll(ObjetAPlacer, stack.getChildren().get(0));
			} else {
				stack.getChildren().add(box.getChildren().get(0));

				box.getChildren().remove(box.getChildren().get(0));
				box.getChildren().addAll(stack.getChildren().get(0), ObjetAPlacer);
			}
		}
	}

	// Il faut showGestionPlayer() quand on dï¿½tecte un nouveau joueur.
	// Quand le nombre de joueurs est complet, on lance showLaunchButton()
	public void gestionPlayer() {

		bAccept.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				vbGestion.setVisible(false);
				cDialogue.getMp().allowPlayer(lTryPseudo.getText());
			}
		});

		bRefuse.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				vbGestion.setVisible(false);
				cDialogue.getMp().denyPlayer(lTryPseudo.getText());
			}
		});

	}

	public void showGestionPlayer(String pseudo) {
		Platform.runLater(() -> {
			// lTryPseudo prend le pseudo du joueur
			vbGestion.setVisible(true);
			lTryPseudo.setText(pseudo);
		});
	}

	public void showLaunchButton() {
		Platform.runLater(() -> {
			vbCenterLG.getChildren().clear();
			vbCenterLG.getChildren().addAll(bLaunch);
		});
	}
	
	public int getNumberOfPlace() {
		switch (box) {
		case 1: 
			switch (position) {
			case 1: return 1;
			case 2: return 2;
			case 3: return 3;
			}
		case 2:
			switch (position) {
			case 1: return 4;
			case 2: return 5;
			}
		case 3:
			switch (position) {
			case 1: return 6;
			case 2: return 7;
			case 3: return 8;
			}
		case 4:
			switch (position) {
			case 1: return 9;
			case 2: return 10;
			}
		}
		return 0;
	}

	// placer les joueurs
	public void enterPlayerSet() {
		bP1.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 1
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true; // bloquer les autres boutons

						// set les variables pour placer la place au bon endroit
						nbrElem = 3;
						position = 1;
						box = 1;

						if (addBot == true) {
							hbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});

						} else {
							hbPlayerSelection.getChildren().add(vbPseudoPlayerCo);
							vbPseudoPlayerCo.setRotate(-180); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(200, 25);
						}

						ChangeElementPlace(3, 1, topPlaces, hbPlayerSelection); // remplacï¿½ le bouton cliquï¿½ par la
																				// boite
																				// qui contient la selection du joueur a
																				// ajouter

						bCross.setOnAction(new EventHandler<ActionEvent>() { // clic sur la croix
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								ChangeElementPlace(3, 1, topPlaces, bP1); // on remplace la boite de selection par le
																			// bouton

								var = false; // debloquer les autres boutons
								hbPlayerSelection.getChildren().remove(hbPlayerSelection.getChildren().get(1)); // on
																												// supprime
																												// la vb
																												// du
																												// choix
																												// du
																												// bot
																												// ou
																												// joueur
							}
						});
					}
				}
			}
		});

		bP2.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 2
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true;

						// set les variables pour placer la place au bon endroit
						nbrElem = 3;
						position = 2;
						box = 1;

						if (addBot == true) {
							hbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});

						} else {
							hbPlayerSelection.getChildren().add(vbPseudoPlayerCo);
							vbPseudoPlayerCo.setRotate(-180); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(200, 25);
						}

						ChangeElementPlace(3, 2, topPlaces, hbPlayerSelection);
						topPlaces.setMargin(topPlaces.getChildren().get(1), new Insets(2, 150, 0, 150));

						bCross.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								topPlaces.setMargin(topPlaces.getChildren().get(1), new Insets(0, 0, 0, 0));
								ChangeElementPlace(3, 2, topPlaces, bP2);

								var = false;
								hbPlayerSelection.getChildren().remove(hbPlayerSelection.getChildren().get(1));

							}
						});

					}
				}
			}
		});

		bP3.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 3
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true;

						// set les variables pour placer la place au bon endroit
						nbrElem = 3;
						position = 3;
						box = 1;

						if (addBot == true) {
							hbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});
						} else {
							hbPlayerSelection.getChildren().add(vbPseudoPlayerCo);
							vbPseudoPlayerCo.setRotate(-180); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(200, 25);
						}

						ChangeElementPlace(3, 3, topPlaces, hbPlayerSelection);

						bCross.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								ChangeElementPlace(3, 3, topPlaces, bP3);

								var = false;
								hbPlayerSelection.getChildren().remove(hbPlayerSelection.getChildren().get(1));
							}
						});

					}
				}
			}
		});

		bP4.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 4
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true;

						// set les variables pour placer la place au bon endroit
						nbrElem = 2;
						position = 1;
						box = 2;

						if (addBot == true) { // voir si on est en mode ajouter bot ou joueur
							vbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});

						} else {
							vbPlayerSelection.getChildren().add(vbPseudoPlayerCo); // voir si on est en mode ajouter bot
																					// ou joueur
							vbPseudoPlayerCo.setRotate(-90); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(25, 200);
						}

						ChangeElementPlace(2, 1, rightPlaces, vbPlayerSelection);

						bCrossCote.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								ChangeElementPlace(2, 1, rightPlaces, bP4);

								var = false;
								vbPlayerSelection.getChildren().remove(vbPlayerSelection.getChildren().get(1));
							}
						});

					}
				}
			}
		});

		bP5.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 5
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true;

						// set les variables pour placer la place au bon endroit
						nbrElem = 2;
						position = 2;
						box = 2;

						if (addBot == true) {
							vbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});

						} else {
							vbPlayerSelection.getChildren().add(vbPseudoPlayerCo);
							vbPseudoPlayerCo.setRotate(-90); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(25, 200);
						}

						ChangeElementPlace(2, 2, rightPlaces, vbPlayerSelection);

						bCrossCote.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								ChangeElementPlace(2, 2, rightPlaces, bP5);

								var = false;
								vbPlayerSelection.getChildren().remove(vbPlayerSelection.getChildren().get(1));
							}
						});

					}
				}
			}
		});

		bP6.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 6
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true;

						// set les variables pour placer la place au bon endroit
						nbrElem = 3;
						position = 3;
						box = 3;

						if (addBot == true) {
							hbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});

						} else {
							hbPlayerSelection.getChildren().add(vbPseudoPlayerCo);
							vbPseudoPlayerCo.setRotate(0); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(200, 25);
						}

						ChangeElementPlace(3, 3, bottomPlaces, hbPlayerSelection);

						bCross.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								ChangeElementPlace(3, 3, bottomPlaces, bP6);

								var = false;
								hbPlayerSelection.getChildren().remove(hbPlayerSelection.getChildren().get(1));
							}
						});

					}
				}
			}
		});

		bP7.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 7
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true;

						// set les variables pour placer la place au bon endroit
						nbrElem = 3;
						position = 2;
						box = 3;

						if (addBot == true) {
							hbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});

						} else {
							hbPlayerSelection.getChildren().add(vbPseudoPlayerCo);
							vbPseudoPlayerCo.setRotate(0); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(200, 25);
						}

						ChangeElementPlace(3, 2, bottomPlaces, hbPlayerSelection);
						bottomPlaces.setMargin(bottomPlaces.getChildren().get(1), new Insets(0, 150, 2, 150));

						bCross.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								bottomPlaces.setMargin(bottomPlaces.getChildren().get(1), new Insets(0, 0, 0, 0));
								ChangeElementPlace(3, 2, bottomPlaces, bP7);

								var = false;
								hbPlayerSelection.getChildren().remove(hbPlayerSelection.getChildren().get(1));
							}
						});

					}
				}
			}
		});

		bP8.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 8
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true;

						// set les variables pour placer la place au bon endroit
						nbrElem = 3;
						position = 1;
						box = 3;

						if (addBot == true) {
							hbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});

						} else {
							hbPlayerSelection.getChildren().add(vbPseudoPlayerCo);
							vbPseudoPlayerCo.setRotate(0); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(200, 25);
						}

						ChangeElementPlace(3, 1, bottomPlaces, hbPlayerSelection);

						bCross.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								ChangeElementPlace(3, 1, bottomPlaces, bP8);

								var = false;
								hbPlayerSelection.getChildren().remove(hbPlayerSelection.getChildren().get(1));
							}
						});
					}
				}
			}
		});

		bP9.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 9
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true;

						// set les variables pour placer la place au bon endroit
						nbrElem = 2;
						position = 2;
						box = 4;

						if (addBot == true) {
							vbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});

						} else {
							vbPlayerSelection.getChildren().add(vbPseudoPlayerCo);
							vbPseudoPlayerCo.setRotate(90); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(25, 200);
						}

						ChangeElementPlace(2, 2, leftPlaces, vbPlayerSelection);

						bCrossCote.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								ChangeElementPlace(2, 2, leftPlaces, bP9);

								var = false; // debloquer les autres boutons
								vbPlayerSelection.getChildren().remove(vbPlayerSelection.getChildren().get(1));
							}
						});

					}
				}
			}
		});

		bP10.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Place 10
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (vbPseudoPlayerCo.getChildren().size() != 0 || addBot == true) {
					if (var == false) {
						var = true;

						// set les variables pour placer la place au bon endroit
						nbrElem = 2;
						position = 1;
						box = 4;

						if (addBot == true) {
							vbPlayerSelection.getChildren().add(vbBot); // voir si on est en mode ajouter bot ou joueur

							bBotE.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Facile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Facile", true);
								}
							});
							bBotM.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Moyen
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Moyen", true);
								}
							});
							bBotD.setOnAction(new EventHandler<ActionEvent>() { // clic sur le bouton Bot Difficile
								@Override
								public void handle(ActionEvent arg0) {
									cDialogue.getSettings().effectSounds();
									chooseColor(stackPseudoBot.get(0) + " / Difficile", true);
								}
							});

						} else {
							vbPlayerSelection.getChildren().add(vbPseudoPlayerCo);
							vbPseudoPlayerCo.setRotate(90); // on met dans le bon sens
							vbPseudoPlayerCo.setPrefSize(25, 200);
						}

						ChangeElementPlace(2, 1, leftPlaces, vbPlayerSelection);

						bCrossCote.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent arg0) {
								cDialogue.getSettings().effectSounds();
								ChangeElementPlace(2, 1, leftPlaces, bP10);

								var = false; // debloquer les autres boutons
								vbPlayerSelection.getChildren().remove(vbPlayerSelection.getChildren().get(1));
							}
						});

					}
				}
			}
		});
	}

	// Initialise l'interface avec tout les composants necessaires
	// On affecte aussi des actions aux Button pour naviguer entre les diffï¿½rentes
	// interfaces
	public Placement(DialogueControllerTable cDialogue2) {
		cDialogue = cDialogue2;
		this.setBackground(new Background(new BackgroundFill(Color.web("#EFE4B0"), CornerRadii.EMPTY, null)));
		addPlacement();
		gestionPlayer();
		vbGestion.setVisible(false);
		enterPlayerSet();

		// TEST
		/*
		newUnplacedPlayer("issou");
		newUnplacedPlayer("Pierre");
		newUnplacedPlayer("Lola");
		*/

		baddBot.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (var == false) {
					vbRightButtons.getChildren().remove(baddBot);
					vbRightButtons.getChildren().add(bAddPlayer);
					addBot = true;
				}
			}

		});

		bAddPlayer.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				cDialogue.getSettings().effectSounds();
				if (var == false) {
					vbRightButtons.getChildren().remove(bAddPlayer);
					vbRightButtons.getChildren().add(baddBot);
					addBot = false;
				}
			}

		});

		// cDialogue.showScreen(screenList.GAMETABLE)
		bLaunch.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.getMp().initGame(cbWhoStart.getValue());
		});

 

		bBack.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.TABLEMENU);
		});


		bSetting.setOnAction((event) -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.editHistory(nameScreen);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		lCodeGameTop.setRotate(180);
		lCodeGameTop.setStyle("-fx-font-size : 16;");
		lCodeGameTop.setMaxWidth(Double.MAX_VALUE);
		lCodeGameTop.setAlignment(Pos.CENTER);
		lCodeGameTop.setTranslateY(5);

		lCodeGameRight.setRotate(270);
		lCodeGameRight.setStyle("-fx-font-size : 16;");
		lCodeGameRight.setMaxWidth(Double.MAX_VALUE);
		lCodeGameRight.setAlignment(Pos.CENTER);
		lCodeGameRight.setTranslateY(180);
		lCodeGameRight.setTranslateX(40);

		lCodeGameBottom.setMaxWidth(Double.MAX_VALUE);
		lCodeGameBottom.setStyle("-fx-font-size : 16;");
		lCodeGameBottom.setAlignment(Pos.CENTER);
		lCodeGameBottom.setTranslateY(-5);

		lCodeGameLeft.setRotate(90);
		lCodeGameLeft.setStyle("-fx-font-size : 16;");
		lCodeGameLeft.setMaxWidth(Double.MAX_VALUE);
		lCodeGameLeft.setAlignment(Pos.CENTER);
		lCodeGameLeft.setTranslateY(180);
		lCodeGameLeft.setTranslateX(-40);

		bRules.setOnAction((event) -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.editHistory(nameScreen);
			cDialogue.showScreen(screenList.RULES);
		});
		this.setVisible(false);
		cDialogue.saveScreen(nameScreen, this);
	}

	public void setIdp(int idp) {
		Platform.runLater(() -> {
			lCodeGameTop.setText("Code partie: " + idp);
			lCodeGameRight.setText("Code partie: " + idp);
			lCodeGameBottom.setText("Code partie: " + idp);
			lCodeGameLeft.setText("Code partie: " + idp);
		});
	}
	
	

	public void addPlayer(String pseudo) {
		String color = stackPlaces.getChildren().get(0).getStyle().split(": ")[1].split(";")[0];
		boolean bot = pseudo.indexOf("BOT") >= 0;
		Level difficulty = null;
		if (pseudo.indexOf("/ ") >= 0) {
			difficulty = Level.valueOf(pseudo.split("/ ")[1]);
		}

		if (bot)
			cDialogue.getMp().createBot(pseudo, difficulty, cDialogue.getMp().getGame().getColor(color), getNumberOfPlace());

		else
			cDialogue.getMp().installPlayer(pseudo, color, getNumberOfPlace());
	}
}
