package tableInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueControllerTable;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;


	public class EndRoundRuleTable extends BorderPane {

		private DialogueControllerTable cDialogue = null;
		//On donne un nom a cette interface pour permettre l'identification par le systeme
		private static final screenList nameScreen = screenList.ENDROUNDRULETABLE;
		
			HBox Htop;

			Label lTitle;
			
			VBox Vbottom;
			VBox Vleft;
			VBox Vright;
			
			VBox Vmiddle;

			Button bBack1;
			Button bBack2;
			
			Label lTexterr;
			Label lTexterr2;
			
			FileInputStream inp1;
			Image image1;
			ImageView imageView1;
			FileInputStream inp2;
			Image image2;
			ImageView imageView2;
			
			Label lex1;
			Label lex2;
			
			HBox Hex;
			HBox Hex1;
			HBox Hex2;
			
			// On créé les differents composants de l'interface (Label, Button)
		    private void createWidgets() throws FileNotFoundException {
				//***LABEL***//
		    	//creation et initialisation du Label lTitle
				lTitle = new Label();
				lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 200));		
				lTitle.textProperty().bind(I18N.createStringBinding("lb.titleEr"));
		    	
				
				//creation et initialisation du Button bBack1
				bBack1 = new Button();
				bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack1.setPrefSize(200, 50);
				bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack1.setTranslateY(-75);
				bBack1.setTranslateX(50);

				// creation et initialisation du Button bBack2
				bBack2 = new Button();
				bBack2.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack2.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack2.setPrefSize(200, 50);
				bBack2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack2.setTranslateY(-75);
				bBack2.setTranslateX(-50);

				lTexterr= new Label();
			    lTexterr.setAlignment(Pos.CENTER);
			    lTexterr.setFont(Font.font("Cambria", 22));
			    lTexterr.textProperty().bind(I18N.createStringBinding("text.err"));
			    
			    lTexterr2= new Label();
			    lTexterr2.setAlignment(Pos.CENTER);
			    lTexterr2.setTextAlignment(TextAlignment.CENTER);
			    lTexterr2.setFont(Font.font("Cambria", 22));
			    lTexterr2.textProperty().bind(I18N.createStringBinding("text.err2"));
			    
			    lex1= new Label();
			    lex1.setAlignment(Pos.CENTER);
			    lex1.setFont(Font.font("Cambria", 22));
			    lex1.textProperty().bind(I18N.createStringBinding("text.errex1"));
			    
			    lex2= new Label();
			    lex2.setAlignment(Pos.CENTER);
			    lex2.setFont(Font.font("Cambria", 22));
			    lex2.textProperty().bind(I18N.createStringBinding("text.errex2"));
			    
			    inp1 = new FileInputStream("./src/ressources/Rules/EndRoundEx1.png");
				
				image1  = new Image(inp1, 120, 400, false, true);
				imageView1 = new ImageView(image1);
				
				inp2 = new FileInputStream("./src/ressources/Rules/EndRoundEx2.png");
					
				image2  = new Image(inp1, 120, 400,false, true);
				imageView2 = new ImageView(image1);
				
				
			}
				
		    
			// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
		    private void createContainers() {

				// CREATION ET CONFIGURATION DE LA HBOX HAUTE/TITRE
				Htop = new HBox();
				Htop.setAlignment(Pos.CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vbottom = new VBox();
				Vbottom.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vleft = new VBox();
				Vleft.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX DROITE
				Vright = new VBox();
				Vright.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX middle
				Vmiddle = new VBox();
				Vmiddle.setAlignment(Pos.CENTER);
				Vmiddle.setSpacing(25);

				Hex = new HBox();
				Hex.setAlignment(Pos.CENTER);
				Hex.setSpacing(30);
				
				Hex1 = new HBox();
				Hex1.setAlignment(Pos.CENTER);
				
				Hex2 = new HBox();
				Hex2.setAlignment(Pos.CENTER);
		    }

			// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
		    private void placementWidgetsInContainers() {
				
				// ON AJOUTE TITRE DANS TOP
				Htop.getChildren().add(lTitle);
						
				// ON AJOUTE JOUER DANS middle
				//Vmiddle.getChildren().add(); // Il faut rajouter le texte
				
				
				// ON AJOUTE REGLES DU JEU DANS VGAUCHE
				Vleft.getChildren().add(bBack1);

				// ON AJOUTE PARAMETRE DANS VDROITE
				Vright.getChildren().add(bBack2);

				Hex1.getChildren().add(lex1);
				Hex1.getChildren().add(imageView1);
				
				Hex2.getChildren().add(lex2);
				Hex2.getChildren().add(imageView2);
				
				Hex.getChildren().add(lTexterr2);
				Hex.getChildren().add(Hex1);
				Hex.getChildren().add(Hex2);
				
				// ON AJOUTE PARAMETRE DANS VMIDDLE
				Vmiddle.getChildren().add(lTexterr);
				Vmiddle.getChildren().add(Hex);
			}

			// On définit l'emplacement des HBox et VBox dans l'interface
		    private void placementContainersInPane() {
			
				// ON ASSIGNE A CHAQUE BOX LEUR EMPLACEMENT
				this.setTop(Htop);
				this.setLeft(Vleft);
				this.setRight(Vright);
				this.setCenter(Vmiddle);
			}

			
		public EndRoundRuleTable(DialogueControllerTable cDialogue2) throws FileNotFoundException{

			cDialogue = cDialogue2;
			
			createWidgets();
		    createContainers();
		    placementWidgetsInContainers();
		    placementContainersInPane();
			
			 //
			Image image1 = Caches.BG;
			// Creation de l'image en arriere plan/ image de fond
			BackgroundImage backgroundimage = new BackgroundImage(image1,
			BackgroundRepeat.NO_REPEAT,
			BackgroundRepeat.NO_REPEAT,
			BackgroundPosition.DEFAULT,
			new BackgroundSize(1.0, 1.0, true, true, false, false));
		
			Background background = new Background(backgroundimage);
			//On affecte à l'écran courant l'image de fond défini
			this.setBackground(background);

			bBack1.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.RULES);
		});

			
			bBack2.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.RULES);
		});
			
			

			this.setVisible(false);
			cDialogue.saveScreen(nameScreen, this);

		}
			
	}
