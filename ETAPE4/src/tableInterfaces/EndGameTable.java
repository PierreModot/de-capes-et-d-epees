package tableInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList ;
import gamePart.Caches;
import cd.DialogueControllerTable;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class EndGameTable extends BorderPane {
	
	private DialogueControllerTable cDialogue = null;
	//On donne un nom a cette interface pour permettre l'identification par le systeme
	private static final screenList  ScreenName = screenList.ENDGAMETABLE ;

	 Label lTitle;

	    HBox hbTop;
	    HBox hbMiddle;

	    VBox vbMiddle;
	    VBox vbBottom;

	    Button bBack;

		Image imageTitle;
	    ImageView imageViewTitle;

		int compteur = 1;

	    FileInputStream Title;
		// On créé les differents composants de l'interface (Label, Button)
	    private void createWidgets() throws FileNotFoundException {
			
			//creation et initialisation du Label lTitre
	        lTitle = new Label();
	        lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 200));
	        //lTitle.textProperty().bind(I18N.createStringBinding("lb.winner"));
			//creation et initialisation du Label lJ1
	        
	        //lTitle.textProperty().bind(I18N.createStringBinding("txt.podium6"));
			//creation et initialisation du Button bRetour
	        bBack = new Button();
	        bBack.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
	        bBack.textProperty().bind(I18N.createStringBinding("btn.back"));
	        bBack.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
	        bBack.setPrefWidth(200);
	        bBack.setPrefSize(300, 50);
	        
	        Title = new FileInputStream(("./src/ressources/title.png"));
			
			 // regarder le chemin !!!!
			imageTitle  = new Image(Title, 1000, 470, false, true);
			imageViewTitle = new ImageView(imageTitle);

			// Background

			Image image1 = Caches.BG;
			// Creation de l'image en arriere plan/ image de fond
			BackgroundImage backgroundimage = new BackgroundImage(image1,
			BackgroundRepeat.NO_REPEAT,
			BackgroundRepeat.NO_REPEAT,
			BackgroundPosition.DEFAULT,
			new BackgroundSize(1.0, 1.0, true, true, false, false));
		
			Background background = new Background(backgroundimage);
			//On affecte à l'écran courant l'image de fond défini
			this.setBackground(background);


	    }

		public String place(int number) {
	    	switch (number){
	    	case 1:
	    		return "1er";
	    	case 2:
	    		return "2ème";
	    	case 3:
	    		return "3ème";
	    	case 4:
	    		return "4ème";
	    	case 5:
	    		return "5ème";
	    	case 6:
	    		return "6ème";
	    	default:
	    		return null;
	    	}
	    }
	    
	    public void addPlayerInClassement(String pseudo, int points) {
	    	Label lJoueur = new Label(compteur + " - " + pseudo + " " + place(compteur) + " - " + points + " points");
	        lJoueur.setFont(Font.font("Gabriola", FontWeight.BOLD, 45));
	        //lTitle.textProperty().bind(I18N.createStringBinding("txt.podium4"));
	        
	        vbMiddle.getChildren().add(lJoueur);
	        
	        if (compteur == 1) {
	        	lTitle.textProperty().bind(I18N.createStringBinding("lb.victory").concat(" "+pseudo));
	        }
	        
	        compteur++;
	    }

		// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
	    private void createContainers() {

			//creation et initialisation de la HBox hbTop
	        hbTop = new HBox();
	        hbTop.setAlignment(Pos.CENTER);

			//creation et initialisation de la HBox hbMiddle
	        hbMiddle = new HBox();
	        hbMiddle.setAlignment(Pos.CENTER);

			//creation et initialisation de la VBox vbMiddle
	        vbMiddle = new VBox();
	        vbMiddle.setAlignment(Pos.CENTER);

			//creation et initialisation de la VBox vbBottom
	        vbBottom = new VBox();
	        vbBottom.setAlignment(Pos.BOTTOM_CENTER);
	    }

		// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
	    private void placementWidgetsInContainers() {
	        hbTop.getChildren().add(lTitle);
	        vbBottom.getChildren().add(bBack);
	        
	    }

		// On définit l'emplacement des HBox et VBox dans l'interface
	    private void placementContainersInPane() {
	        this.setTop(hbTop);
	        this.setCenter(vbMiddle);
	        this.setBottom(vbBottom);
	    }

		// Initialise l'interface avec tout les composants necessaires
    	// On affecte aussi des actions aux Button pour naviguer entre les différentes interfaces
	    public EndGameTable(DialogueControllerTable cd)  {
	    	cDialogue = cd;
			this.setBackground(new Background(new BackgroundFill(Color.WHITE,CornerRadii.EMPTY,null)));

			try {
				createWidgets();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        createContainers();
	        placementWidgetsInContainers();
	        placementContainersInPane();

			//defini l'evenement du bouton bBack
	        bBack.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.TABLEMENU);
		});


	        this.setVisible(false);
	        cDialogue.saveScreen(ScreenName, this);
	}	
}
