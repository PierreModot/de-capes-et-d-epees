package tableInterfaces;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import card.InfluenceCard;
import card.ObjectiveCard;
import cd.DataController.screenList;
import cd.DialogueControllerTable;
import cd.I18N;
import gamePart.Columns;
import gamePart.Game;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import player.Player;
import util.Domain;
import util.Role;

public class GameTable extends BorderPane {

	private DialogueControllerTable cDialogue = null;
	private static final screenList ScreenName = screenList.GAMETABLE;

	VBox stack = new VBox(); // VBox qui sert de pile pour remplacer les boites et les boutons

	HBox place1 = new HBox();
	HBox place2 = new HBox();
	HBox place3 = new HBox();
	VBox place4 = new VBox();
	VBox place5 = new VBox();
	HBox place6 = new HBox();
	HBox place7 = new HBox();
	HBox place8 = new HBox();
	VBox place9 = new VBox();
	VBox place10 = new VBox();

	Border outline = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
			new BorderWidths(1), new Insets(5)));

	VBox vbP1 = new VBox();
	VBox vbP2 = new VBox();
	VBox vbP3 = new VBox();
	VBox vbP4 = new VBox();
	VBox vbP5 = new VBox();
	VBox vbP6 = new VBox();

	VBox stackPlaces = new VBox();

	HBox hbDiscardAndDeckJ1 = new HBox();
	HBox hbDiscardAndDeckJ2 = new HBox();
	HBox hbDiscardAndDeckJ3 = new HBox();
	HBox hbDiscardAndDeckJ4 = new HBox();
	HBox hbDiscardAndDeckJ5 = new HBox();
	HBox hbDiscardAndDeckJ6 = new HBox();

	Rectangle rDeckJ1 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ1 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ2 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ2 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ3 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ3 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ4 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ4 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ5 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ5 = new Rectangle(0, 0, 100, 100);

	Rectangle rDeckJ6 = new Rectangle(0, 0, 100, 100);
	Rectangle rDiscardJ6 = new Rectangle(0, 0, 100, 100);

	Label lPseudoJ1 = new Label();
	Label lPseudoJ2 = new Label();
	Label lPseudoJ3 = new Label();
	Label lPseudoJ4 = new Label();
	Label lPseudoJ5 = new Label();
	Label lPseudoJ6 = new Label();

	VBox vbButton1 = new VBox();
	VBox vbButton2 = new VBox();
	VBox vbButton3 = new VBox();
	VBox vbButton4 = new VBox();
	VBox leftPlaces = new VBox();
	VBox rightPlaces = new VBox();
	HBox topPlaces = new HBox();
	VBox centre = new VBox();
	HBox bottomPlaces = new HBox();
	HBox topLeftButtons = new HBox();
	HBox topRightButtons = new HBox();
	HBox bottomLeftButtons = new HBox();
	HBox bottomRightButtons = new HBox();
	HBox topContent = new HBox();
	HBox bottomContent = new HBox();

	HBox hbObjectiveCards = new HBox(); // boite qui contient les cartes objectif
	HBox hbcolumnsCardsInfluence = new HBox(); // boite qui contient les colonnes des cartes influence

	VBox column1 = new VBox(); // columns des cartes influence
	VBox column2 = new VBox();
	VBox column3 = new VBox();
	VBox column4 = new VBox();
	VBox column5 = new VBox();
	VBox column6 = new VBox();

	VBox firstInfluenceCardsC1 = new VBox(); // boites pour les plus anciennes cartes de la colonne
	VBox firstInfluenceCardsC2 = new VBox();
	VBox firstInfluenceCardsC3 = new VBox();
	VBox firstInfluenceCardsC4 = new VBox();
	VBox firstInfluenceCardsC5 = new VBox();
	VBox firstInfluenceCardsC6 = new VBox();

	VBox lastestInfluenceCardsC1 = new VBox(); // boites pour les 2 cartes les plus rescentes de la colonne
	VBox lastestInfluenceCardsC2 = new VBox();
	VBox lastestInfluenceCardsC3 = new VBox();
	VBox lastestInfluenceCardsC4 = new VBox();
	VBox lastestInfluenceCardsC5 = new VBox();
	VBox lastestInfluenceCardsC6 = new VBox();

	Button bSetting1 = new Button(); // Boutons paramètres
	Button bSetting2 = new Button();
	Button bSetting3 = new Button();
	Button bSetting4 = new Button();

	Button bRule1 = new Button(); // Boutons règles
	Button bRule2 = new Button();
	Button bRule3 = new Button();
	Button bRule4 = new Button();

	// sauvegarde les places qu'ont pris les joueurs
	ArrayList<String> whoHasWhatPlace = new ArrayList<String>();

	Label lNewRound = new Label();


	/**
	 * affiche une carte retournée sur le deck et la fausse si le parametre est a
	 * true
	 *
	 * @param deck
	 * @param discard
	 * @param box
	 * @param color
	 */
	public void showDeckAndDiscard(boolean deck, boolean discard, String pseudo, util.Color color) {
		Platform.runLater(() -> {
			int box = whoHasWhatPlace.indexOf(pseudo);
			switch (box + 1) {
			case 1:
				if (deck) {
					InfluenceCard card = new InfluenceCard(Role.Al, color ,false);

					ImageView image = card.getBackCard();

					stack.getChildren().add(hbDiscardAndDeckJ1.getChildren().get(1));
					hbDiscardAndDeckJ1.getChildren().remove(0);
					hbDiscardAndDeckJ1.getChildren().addAll(image, stack.getChildren().get(0));
				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					stack.getChildren().add(hbDiscardAndDeckJ1.getChildren().get(1));
					hbDiscardAndDeckJ1.getChildren().remove(0);
					hbDiscardAndDeckJ1.getChildren().addAll(rectangle, stack.getChildren().get(0));
				}

				if (discard) {
					/*InfluenceCard img;
					if (InfluenceCard.getTheme()) {
						img = new InfluenceCard(Role.Ro, color ,false);// new InfluenceCard(Role.Al, color,
																				// false);
					} else {
						img = new InfluenceCard(Role.Ro, color ,false);
					}*/
					
					InfluenceCard card;
					Player player = cDialogue.getMp().getGame().getPlayerByColor(color);
					
					if(player != null) {
						card = new InfluenceCard(player.getDiscard().get(player.getDiscard().size() - 1).encode(), false);
						ImageView image = card.getFrontCard();
						hbDiscardAndDeckJ1.getChildren().set(1, image);
					}

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);
					
					hbDiscardAndDeckJ1.getChildren().set(1,rectangle);
				}
				break;
			case 2:
				if (deck) {
					InfluenceCard card;
					card = new InfluenceCard(Role.Al, color ,false);// new InfluenceCard(Role.Al, color,

					ImageView image = card.getBackCard();

					stack.getChildren().add(hbDiscardAndDeckJ2.getChildren().get(1));
					hbDiscardAndDeckJ2.getChildren().remove(0);
					hbDiscardAndDeckJ2.getChildren().addAll(image, stack.getChildren().get(0));

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					stack.getChildren().add(hbDiscardAndDeckJ2.getChildren().get(1));
					hbDiscardAndDeckJ2.getChildren().remove(0);
					hbDiscardAndDeckJ2.getChildren().addAll(rectangle, stack.getChildren().get(0));
				}

				if (discard) {
				
					InfluenceCard card;
					Player player = cDialogue.getMp().getGame().getPlayerByColor(color);
					
					if(player != null) {
						card = new InfluenceCard(player.getDiscard().get(player.getDiscard().size() - 1).encode(), false);
						ImageView image = card.getFrontCard();
						hbDiscardAndDeckJ2.getChildren().set(1, image);
					}
					
				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					hbDiscardAndDeckJ2.getChildren().set(1, rectangle);
				}
				break;
			case 3:
				if (deck) {
					InfluenceCard card;
					card = new InfluenceCard(Role.Al, color ,false);// new InfluenceCard(Role.Al, color,

					ImageView image = card.getBackCard();

					stack.getChildren().add(hbDiscardAndDeckJ3.getChildren().get(1));
					hbDiscardAndDeckJ3.getChildren().remove(0);
					hbDiscardAndDeckJ3.getChildren().addAll(image, stack.getChildren().get(0));

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					stack.getChildren().add(hbDiscardAndDeckJ3.getChildren().get(1));
					hbDiscardAndDeckJ3.getChildren().remove(0);
					hbDiscardAndDeckJ3.getChildren().addAll(rectangle, stack.getChildren().get(0));
				}

				if (discard) {
					InfluenceCard card;
					Player player = cDialogue.getMp().getGame().getPlayerByColor(color);
					
					if(player != null) {
						card = new InfluenceCard(player.getDiscard().get(player.getDiscard().size() - 1).encode(), false);
						ImageView image = card.getFrontCard();
						hbDiscardAndDeckJ3.getChildren().set(1, image);
					}

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					hbDiscardAndDeckJ3.getChildren().set(1, rectangle);
				}
				break;
			case 4:
				if (deck) {
					InfluenceCard card;
					card = new InfluenceCard(Role.Al, color ,false);// new InfluenceCard(Role.Al, color,
		

					ImageView image = card.getBackCard();

					stack.getChildren().add(hbDiscardAndDeckJ4.getChildren().get(1));
					hbDiscardAndDeckJ4.getChildren().remove(0);
					hbDiscardAndDeckJ4.getChildren().addAll(image, stack.getChildren().get(0));

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					stack.getChildren().add(hbDiscardAndDeckJ4.getChildren().get(1));
					hbDiscardAndDeckJ4.getChildren().remove(0);
					hbDiscardAndDeckJ4.getChildren().addAll(rectangle, stack.getChildren().get(0));
				}

				if (discard) {
					InfluenceCard card;
					Player player = cDialogue.getMp().getGame().getPlayerByColor(color);
					
					if(player != null) {
						card = new InfluenceCard(player.getDiscard().get(player.getDiscard().size() - 1).encode(), false);
						ImageView image = card.getFrontCard();
						hbDiscardAndDeckJ4.getChildren().set(1, image);
					}

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					hbDiscardAndDeckJ4.getChildren().set(1, rectangle);
				}
				break;
			case 5:
				if (deck) {
					InfluenceCard card;
					card = new InfluenceCard(Role.Al, color ,false);// new InfluenceCard(Role.Al, color,
		
					ImageView image = card.getBackCard();

					stack.getChildren().add(hbDiscardAndDeckJ5.getChildren().get(1));
					hbDiscardAndDeckJ5.getChildren().remove(0);
					hbDiscardAndDeckJ5.getChildren().addAll(image, stack.getChildren().get(0));

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					stack.getChildren().add(hbDiscardAndDeckJ5.getChildren().get(1));
					hbDiscardAndDeckJ5.getChildren().remove(0);
					hbDiscardAndDeckJ5.getChildren().addAll(rectangle, stack.getChildren().get(0));
				}

				if (discard) {
					InfluenceCard card;
					Player player = cDialogue.getMp().getGame().getPlayerByColor(color);
					
					if(player != null) {
						card = new InfluenceCard(player.getDiscard().get(player.getDiscard().size() - 1).encode(), false);
						ImageView image = card.getFrontCard();
						hbDiscardAndDeckJ5.getChildren().set(1, image);
					}

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					hbDiscardAndDeckJ5.getChildren().set(1, rectangle);
				}
				break;
			case 6:
				if (deck) {
					InfluenceCard card;
					card = new InfluenceCard(Role.Al, color ,false);// new InfluenceCard(Role.Al, color,
				
					ImageView image = card.getBackCard();

					stack.getChildren().add(hbDiscardAndDeckJ6.getChildren().get(1));
					hbDiscardAndDeckJ6.getChildren().remove(0);
					hbDiscardAndDeckJ6.getChildren().addAll(image, stack.getChildren().get(0));

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					stack.getChildren().add(hbDiscardAndDeckJ6.getChildren().get(1));
					hbDiscardAndDeckJ6.getChildren().remove(0);
					hbDiscardAndDeckJ6.getChildren().addAll(rectangle, stack.getChildren().get(0));
				}

				if (discard) {
					InfluenceCard card;
					Player player = cDialogue.getMp().getGame().getPlayerByColor(color);
					
					if(player != null) {
						card = new InfluenceCard(player.getDiscard().get(player.getDiscard().size() - 1).encode(), false);
						ImageView image = card.getFrontCard();
						hbDiscardAndDeckJ6.getChildren().set(1, image);
					}

				} else {
					Rectangle rectangle = new Rectangle(0, 0, 100, 100);
					setRectangle(rectangle);

					hbDiscardAndDeckJ6.getChildren().set(1, rectangle);
				}
				break;
			}
		});
	}

	/**
	 * affiche "objectif atteins" au dessus d'une colonne
	 *
	 * @param column
	 */
	public void objectiveAchieved(int column) {
		Platform.runLater(() -> {
			VBox pile = new VBox();
			VBox vbox = new VBox();
			ImageView image = new ImageView();
			int nbColumns = hbObjectiveCards.getChildren().size();

			for (int i = nbColumns - column; i > 0; i--) {
				pile.getChildren().add(hbObjectiveCards.getChildren().get(hbObjectiveCards.getChildren().size() - 1));
			}

			vbox = (VBox) hbObjectiveCards.getChildren().get(hbObjectiveCards.getChildren().size() - 1);
			image = (ImageView) vbox.getChildren().get(0);
			vbox.getChildren().remove(0);

			Label lObjectiveAchieved = new Label();
			lObjectiveAchieved.setFont(Font.font("Gabriola", FontWeight.BOLD, 22));
			lObjectiveAchieved.textProperty().bind(I18N.createStringBinding("lb.objectiveAchieved"));
			vbox.getChildren().addAll(lObjectiveAchieved, image);

			hbObjectiveCards.getChildren().remove(hbObjectiveCards.getChildren().size() - 1);
			hbObjectiveCards.getChildren().add(vbox);

			for (int i = nbColumns - column; i > 0; i--) {
				hbObjectiveCards.getChildren().add(pile.getChildren().get(pile.getChildren().size() - 1));
			}
		});
	}

	/**
	 * affiche le nombre de colonne donné en paramettre
	 *
	 * @param nbrPlayer
	 */
	public void setColumns(int nbrPlayer) {
		Platform.runLater(() -> {
			switch (nbrPlayer) {
			case 2:
				hbcolumnsCardsInfluence.getChildren().addAll(column1, column2);
				break;
			case 3:
				hbcolumnsCardsInfluence.getChildren().addAll(column1, column2, column3);
				break;
			case 4:
				hbcolumnsCardsInfluence.getChildren().addAll(column1, column2, column3, column4);
				break;
			case 5:
				hbcolumnsCardsInfluence.getChildren().addAll(column1, column2, column3, column4, column5);
				break;
			case 6:
				hbcolumnsCardsInfluence.getChildren().addAll(column1, column2, column3, column4, column5, column6);
				break;
			}
		});
	}

	/**
	 * joue une carte face cachée et retourne la précédente
	 * 
	 * @param card
	 * @param column
	 */
	public void cardPlayedWithReturn(InfluenceCard card, int column) {
		Platform.runLater(() -> {
			ImageView image = card.getBackCard();
			switch (column) {
			case 1:
				lastestInfluenceCardsC1.getChildren().add(image);
				tightenCards(1);
				if (column1.getChildren().size() >= 2)
					returnCard(1);
				break;
			case 2:
				lastestInfluenceCardsC2.getChildren().add(image);
				tightenCards(2);
				if (column2.getChildren().size() >= 2)
					returnCard(2);
				break;
			case 3:
				lastestInfluenceCardsC3.getChildren().add(image);
				tightenCards(3);
				if (column3.getChildren().size() >= 2)
					returnCard(3);
				break;
			case 4:
				lastestInfluenceCardsC4.getChildren().add(image);
				tightenCards(4);
				if (column4.getChildren().size() >= 2)
					returnCard(4);
				break;
			case 5:
				lastestInfluenceCardsC5.getChildren().add(image);
				tightenCards(5);
				if (column5.getChildren().size() >= 2)
					returnCard(5);
				break;
			case 6:
				lastestInfluenceCardsC6.getChildren().add(image);
				tightenCards(6);
				if (column6.getChildren().size() >= 2)
					returnCard(6);
				break;
			}
		});
	}

	/**
	 * joue une carte face cachée
	 * 
	 * @param card
	 * @param column
	 */
	public void cardPlayed(InfluenceCard card, int column) {
		Platform.runLater(() -> {
			ImageView img = card.getFrontCard();
			img.setFitHeight(100);
			img.setFitWidth(100);
			switch (column) {
			case 1:
				if (card.isVisible())
					lastestInfluenceCardsC1.getChildren().add(card.getFrontCard());
				else
					lastestInfluenceCardsC1.getChildren().add(card.getBackCard());
				tightenCards(1);
				break;
			case 2:
				if (card.isVisible())
					lastestInfluenceCardsC2.getChildren().add(card.getFrontCard());
				else
					lastestInfluenceCardsC2.getChildren().add(card.getBackCard());
				tightenCards(2);
				break;
			case 3:
				if (card.isVisible())
					lastestInfluenceCardsC3.getChildren().add(card.getFrontCard());
				else
					lastestInfluenceCardsC3.getChildren().add(card.getBackCard());
				tightenCards(3);
				break;
			case 4:
				if (card.isVisible())
					lastestInfluenceCardsC4.getChildren().add(card.getFrontCard());
				else
					lastestInfluenceCardsC4.getChildren().add(card.getBackCard());
				tightenCards(4);
				break;
			case 5:
				if (card.isVisible())
					lastestInfluenceCardsC5.getChildren().add(card.getFrontCard());
				else
					lastestInfluenceCardsC5.getChildren().add(card.getBackCard());
				tightenCards(5);
				break;
			case 6:
				if (card.isVisible())
					lastestInfluenceCardsC6.getChildren().add(card.getFrontCard());
				else
					lastestInfluenceCardsC6.getChildren().add(card.getBackCard());
				tightenCards(6);
				break;
			}
		});
	}

	public void refreshCard() {
		Platform.runLater(() -> {

			clearAllCards();

			for (Columns column : cDialogue.getMp().getGame().getColumns()) {
				column.getOCard().objectif();
				addObjectiveCard(column.getOCard());
			}

			for (int i = 0; i < cDialogue.getMp().getGame().getColumns().size(); i++) {
				Columns column = cDialogue.getMp().getGame().getColumns().get(i);

				for (InfluenceCard card : column.getContent()) {
					card.setDomain();
					cardPlayed(card, i + 1);
				}

				if (column.getIsRealized())
					objectiveAchieved(i + 1);

			}
			cDialogue.getSettings().effectSounds();
		});
	}

	/**
	 * retourne l'avant dernière carte de la colonne
	 * 
	 * @param card
	 * @param column
	 */
	public void returnCard(int column) {
		Platform.runLater(() -> {
			Node lastCard = new ImageView();
			int posLastCard;
			InfluenceCard beforeLastCard = null;
			switch (column) {
			case 1:
				beforeLastCard = cDialogue.getMp().getGame().getEndCard(column - 1,
						lastestInfluenceCardsC1.getChildren().size() - 2);
				break;
			case 2:
				beforeLastCard = cDialogue.getMp().getGame().getEndCard(column - 1,
						lastestInfluenceCardsC2.getChildren().size() - 2);
				break;
			case 3:
				posLastCard = lastestInfluenceCardsC3.getChildren().size() - 2;
				beforeLastCard = cDialogue.getMp().getGame().getEndCard(column - 1,
						lastestInfluenceCardsC3.getChildren().size() - 2);
				break;
			case 4:
				beforeLastCard = cDialogue.getMp().getGame().getEndCard(column - 1,
						lastestInfluenceCardsC4.getChildren().size() - 2);
				break;
			case 5:
				beforeLastCard = cDialogue.getMp().getGame().getEndCard(column - 1,
						lastestInfluenceCardsC5.getChildren().size() - 2);
				break;
			case 6:
				beforeLastCard = cDialogue.getMp().getGame().getEndCard(column - 1,
						lastestInfluenceCardsC6.getChildren().size() - 2);
				break;
			}
			if (beforeLastCard != null) {
				switch (column) {
				case 1:
					posLastCard = lastestInfluenceCardsC1.getChildren().size() - 2;
					lastCard = lastestInfluenceCardsC1.getChildren().get(posLastCard + 1);
					if (posLastCard >= 0) {
						lastestInfluenceCardsC1.getChildren().remove(posLastCard);
						lastestInfluenceCardsC1.getChildren().remove(posLastCard);
						lastestInfluenceCardsC1.getChildren().addAll(beforeLastCard.getFrontCard(), lastCard);
					}
					break;
				case 2:
					posLastCard = lastestInfluenceCardsC2.getChildren().size() - 2;
					lastCard = lastestInfluenceCardsC2.getChildren().get(posLastCard + 1);
					if (posLastCard >= 0) {
						lastestInfluenceCardsC2.getChildren().remove(posLastCard);
						lastestInfluenceCardsC2.getChildren().remove(posLastCard);
						lastestInfluenceCardsC2.getChildren().addAll(beforeLastCard.getFrontCard(), lastCard);
					}
					break;
				case 3:
					posLastCard = lastestInfluenceCardsC3.getChildren().size() - 2;
					lastCard = lastestInfluenceCardsC3.getChildren().get(posLastCard + 1);
					if (posLastCard >= 0) {
						lastestInfluenceCardsC3.getChildren().remove(posLastCard);
						lastestInfluenceCardsC3.getChildren().remove(posLastCard);
						lastestInfluenceCardsC3.getChildren().addAll(beforeLastCard.getFrontCard(), lastCard);
					}
					break;
				case 4:
					posLastCard = lastestInfluenceCardsC4.getChildren().size() - 2;
					lastCard = lastestInfluenceCardsC4.getChildren().get(posLastCard + 1);
					if (posLastCard >= 0) {
						lastestInfluenceCardsC4.getChildren().remove(posLastCard);
						lastestInfluenceCardsC4.getChildren().remove(posLastCard);
						lastestInfluenceCardsC4.getChildren().addAll(beforeLastCard.getFrontCard(), lastCard);
					}
					break;
				case 5:
					posLastCard = lastestInfluenceCardsC5.getChildren().size() - 2;
					lastCard = lastestInfluenceCardsC5.getChildren().get(posLastCard + 1);
					if (posLastCard >= 0) {
						lastestInfluenceCardsC5.getChildren().remove(posLastCard);
						lastestInfluenceCardsC5.getChildren().remove(posLastCard);
						lastestInfluenceCardsC5.getChildren().addAll(beforeLastCard.getFrontCard(), lastCard);
					}
					break;
				case 6:
					posLastCard = lastestInfluenceCardsC6.getChildren().size() - 2;
					lastCard = lastestInfluenceCardsC6.getChildren().get(posLastCard + 1);
					if (posLastCard >= 0) {
						lastestInfluenceCardsC6.getChildren().remove(posLastCard);
						lastestInfluenceCardsC6.getChildren().remove(posLastCard);
						lastestInfluenceCardsC6.getChildren().addAll(beforeLastCard.getFrontCard(), lastCard);
					}
					break;
				}
			}
		});
	}

	/**
	 * ajoute une carte objectif a la colonne la plus a droite
	 *
	 * @param card
	 */
	public void addObjectiveCard(ObjectiveCard card) {
		Platform.runLater(() -> {
			VBox vbox = new VBox();
			vbox.setMinHeight(90);
			vbox.setMinWidth(150);
			
			vbox.setAlignment(Pos.BOTTOM_CENTER);

			ImageView image = card.getPicture();
			image.setFitHeight(50);
			image.setFitWidth(100);
			vbox.getChildren().add(image);

			hbObjectiveCards.getChildren().add(vbox);
		});
	}

	/**
	 * resserre les cartes pour n'en garder que 3 entièrement visibles
	 *
	 * @param column
	 */
	private void tightenCards(int column) { // resserrer les cartes
		switch (column) {
		case 1:
			if (lastestInfluenceCardsC1.getChildren().size() > 2) {
				firstInfluenceCardsC1.getChildren().add(lastestInfluenceCardsC1.getChildren().get(0));
				if (firstInfluenceCardsC1.getChildren().size() > 6) {
					firstInfluenceCardsC1.setSpacing(firstInfluenceCardsC1.getSpacing() - 4);
				} else
					firstInfluenceCardsC1.setSpacing(-60);
			}
			break;
		case 2:
			if (lastestInfluenceCardsC2.getChildren().size() > 2) {
				firstInfluenceCardsC2.getChildren().add(lastestInfluenceCardsC2.getChildren().get(0));
				if (firstInfluenceCardsC2.getChildren().size() > 6) {
					firstInfluenceCardsC2.setSpacing(firstInfluenceCardsC2.getSpacing() - 4);
				} else
					firstInfluenceCardsC2.setSpacing(-60);
			}
			break;
		case 3:

			if (lastestInfluenceCardsC3.getChildren().size() > 2) {
				firstInfluenceCardsC3.getChildren().add(lastestInfluenceCardsC3.getChildren().get(0));
				if (firstInfluenceCardsC3.getChildren().size() > 6) {
					firstInfluenceCardsC3.setSpacing(firstInfluenceCardsC3.getSpacing() - 4);
				} else
					firstInfluenceCardsC3.setSpacing(-60);
			}
			break;
		case 4:
			if (lastestInfluenceCardsC4.getChildren().size() > 2) {
				firstInfluenceCardsC4.getChildren().add(lastestInfluenceCardsC4.getChildren().get(0));
				if (firstInfluenceCardsC4.getChildren().size() > 6) {
					firstInfluenceCardsC4.setSpacing(firstInfluenceCardsC4.getSpacing() - 4);
				} else
					firstInfluenceCardsC4.setSpacing(-60);
			}
			break;
		case 5:
			if (lastestInfluenceCardsC5.getChildren().size() > 2) {
				firstInfluenceCardsC5.getChildren().add(lastestInfluenceCardsC5.getChildren().get(0));
				if (firstInfluenceCardsC5.getChildren().size() > 6) {
					firstInfluenceCardsC5.setSpacing(firstInfluenceCardsC5.getSpacing() - 4);
				} else
					firstInfluenceCardsC5.setSpacing(-60);
			}
			break;
		case 6:
			if (lastestInfluenceCardsC6.getChildren().size() > 2) {
				firstInfluenceCardsC6.getChildren().add(lastestInfluenceCardsC6.getChildren().get(0));
				if (firstInfluenceCardsC6.getChildren().size() > 6) {
					firstInfluenceCardsC6.setSpacing(firstInfluenceCardsC6.getSpacing() - 4);
				} else
					firstInfluenceCardsC6.setSpacing(-60);
			}
			break;
		}

	}

	/**
	 * deplace la carte explorateur de sa colonne vers une autre
	 *
	 * @param column
	 * @param destinationColumn
	 */
	private void moveExplorer(int column, int destinationColumn) {
		Platform.runLater(() -> {
			Node explorerCard = null;
			switch (column) {
			case 1:
				explorerCard = lastestInfluenceCardsC1.getChildren().get(0);
				lastestInfluenceCardsC1.getChildren().remove(0);
				break;
			case 2:
				explorerCard = lastestInfluenceCardsC2.getChildren().get(0);
				lastestInfluenceCardsC2.getChildren().remove(0);
				break;
			case 3:
				explorerCard = lastestInfluenceCardsC3.getChildren().get(0);
				lastestInfluenceCardsC3.getChildren().remove(0);
				break;
			case 4:
				explorerCard = lastestInfluenceCardsC4.getChildren().get(0);
				lastestInfluenceCardsC4.getChildren().remove(0);
				break;
			case 5:
				explorerCard = lastestInfluenceCardsC5.getChildren().get(0);
				lastestInfluenceCardsC5.getChildren().remove(0);
				break;
			case 6:
				explorerCard = lastestInfluenceCardsC6.getChildren().get(0);
				lastestInfluenceCardsC6.getChildren().remove(0);
				break;
			}
			switch (destinationColumn) {
			case 1:
				lastestInfluenceCardsC1.getChildren().add(explorerCard);
				break;
			case 2:
				lastestInfluenceCardsC2.getChildren().add(explorerCard);
				break;
			case 3:
				lastestInfluenceCardsC3.getChildren().add(explorerCard);
				break;
			case 4:
				lastestInfluenceCardsC4.getChildren().add(explorerCard);
				break;
			case 5:
				lastestInfluenceCardsC5.getChildren().add(explorerCard);
				break;
			case 6:
				lastestInfluenceCardsC6.getChildren().add(explorerCard);
				break;
			}
		});
	}

	private void setRectangle(Rectangle rectangle) {
		rectangle.setFill(Color.WHITE);
		rectangle.setStroke(Color.BLACK);
	}

	private void spRectangle(HBox box) {
		box.setSpacing(35);
		box.setPadding(new Insets(35, 35, 35, 35));
		box.setAlignment(Pos.CENTER);
	}

	private void ConfigureRectangles() { // configurer les rectangles des places
		setRectangle(rDeckJ1);
		setRectangle(rDiscardJ1);

		setRectangle(rDeckJ2);
		setRectangle(rDiscardJ2);

		setRectangle(rDeckJ3);
		setRectangle(rDiscardJ3);

		setRectangle(rDeckJ4);
		setRectangle(rDiscardJ4);

		setRectangle(rDeckJ5);
		setRectangle(rDiscardJ5);

		setRectangle(rDeckJ6);
		setRectangle(rDiscardJ6);

		spRectangle(hbDiscardAndDeckJ1);
		spRectangle(hbDiscardAndDeckJ2);
		spRectangle(hbDiscardAndDeckJ3);
		spRectangle(hbDiscardAndDeckJ4);
		spRectangle(hbDiscardAndDeckJ5);
		spRectangle(hbDiscardAndDeckJ6);

	}

	private void ConfigurePlayerPlaces() { // configurer les places des joueurs
		vbP1.setBorder(outline);
		vbP1.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ1.getChildren().addAll(rDeckJ1, rDiscardJ1);
		vbP1.getChildren().addAll(hbDiscardAndDeckJ1, lPseudoJ1);

		vbP2.setBorder(outline);
		vbP2.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ2.getChildren().addAll(rDeckJ2, rDiscardJ2);
		vbP2.getChildren().addAll(hbDiscardAndDeckJ2, lPseudoJ2);

		vbP3.setBorder(outline);
		vbP3.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ3.getChildren().addAll(rDeckJ3, rDiscardJ3);
		vbP3.getChildren().addAll(hbDiscardAndDeckJ3, lPseudoJ3);

		vbP4.setBorder(outline);
		vbP4.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ4.getChildren().addAll(rDeckJ4, rDiscardJ4);
		vbP4.getChildren().addAll(hbDiscardAndDeckJ4, lPseudoJ4);

		vbP5.setBorder(outline);
		vbP5.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ5.getChildren().addAll(rDeckJ5, rDiscardJ5);
		vbP5.getChildren().addAll(hbDiscardAndDeckJ5, lPseudoJ5);

		vbP6.setBorder(outline);
		vbP6.setAlignment(Pos.CENTER);
		hbDiscardAndDeckJ6.getChildren().addAll(rDeckJ6, rDiscardJ6);
		vbP6.getChildren().addAll(hbDiscardAndDeckJ6, lPseudoJ6);

		stackPlaces.getChildren().addAll(vbP1, vbP2, vbP3, vbP4, vbP5, vbP6);
		lPseudoJ1.textProperty().bind(I18N.createStringBinding("lb.p1"));
		lPseudoJ2.textProperty().bind(I18N.createStringBinding("lb.p2"));
		lPseudoJ3.textProperty().bind(I18N.createStringBinding("lb.p3"));
		lPseudoJ4.textProperty().bind(I18N.createStringBinding("lb.p4"));
		lPseudoJ5.textProperty().bind(I18N.createStringBinding("lb.p5"));
		lPseudoJ6.textProperty().bind(I18N.createStringBinding("lb.p6"));
	}

	public void newRound(int round) {
		Platform.runLater(() -> {
			centre.getChildren().removeAll(hbObjectiveCards, hbcolumnsCardsInfluence);
			lNewRound.textProperty().bind(I18N.createStringBinding("lb.newRound").concat(" " + round));
			centre.getChildren().add(lNewRound);
		});
	}

	public void showGame() {
		Platform.runLater(() -> {
			centre.getChildren().remove(lNewRound);
			centre.getChildren().addAll(hbObjectiveCards, hbcolumnsCardsInfluence);
		});
	}

	private void configureCardsArea() { // configurer les colonnes pour les Cards
		centre.getChildren().addAll(hbObjectiveCards, hbcolumnsCardsInfluence);

		hbObjectiveCards.setAlignment(Pos.TOP_CENTER);
		hbcolumnsCardsInfluence.setAlignment(Pos.TOP_CENTER);

		centre.setAlignment(Pos.TOP_CENTER);
		// hbcolumnsCardsInfluence.getChildren().addAll(column1, column2, column3,
		// column4, column5, column6);
		column1.getChildren().addAll(firstInfluenceCardsC1, lastestInfluenceCardsC1);
		column2.getChildren().addAll(firstInfluenceCardsC2, lastestInfluenceCardsC2);
		column3.getChildren().addAll(firstInfluenceCardsC3, lastestInfluenceCardsC3);
		column4.getChildren().addAll(firstInfluenceCardsC4, lastestInfluenceCardsC4);
		column5.getChildren().addAll(firstInfluenceCardsC5, lastestInfluenceCardsC5);
		column6.getChildren().addAll(firstInfluenceCardsC6, lastestInfluenceCardsC6);

		column1.setMinSize(150, 0);
		column2.setMinSize(150, 0);
		column3.setMinSize(150, 0);
		column4.setMinSize(150, 0);
		column5.setMinSize(150, 0);
		column6.setMinSize(150, 0);

		column1.setAlignment(Pos.TOP_CENTER);
		column2.setAlignment(Pos.TOP_CENTER);
		column3.setAlignment(Pos.TOP_CENTER);
		column4.setAlignment(Pos.TOP_CENTER);
		column5.setAlignment(Pos.TOP_CENTER);
		column6.setAlignment(Pos.TOP_CENTER);

		firstInfluenceCardsC1.setAlignment(Pos.TOP_CENTER);
		firstInfluenceCardsC2.setAlignment(Pos.TOP_CENTER);
		firstInfluenceCardsC3.setAlignment(Pos.TOP_CENTER);
		firstInfluenceCardsC4.setAlignment(Pos.TOP_CENTER);
		firstInfluenceCardsC5.setAlignment(Pos.TOP_CENTER);
		firstInfluenceCardsC6.setAlignment(Pos.TOP_CENTER);

		lastestInfluenceCardsC1.setAlignment(Pos.TOP_CENTER);
		lastestInfluenceCardsC2.setAlignment(Pos.TOP_CENTER);
		lastestInfluenceCardsC3.setAlignment(Pos.TOP_CENTER);
		lastestInfluenceCardsC4.setAlignment(Pos.TOP_CENTER);
		lastestInfluenceCardsC5.setAlignment(Pos.TOP_CENTER);
		lastestInfluenceCardsC6.setAlignment(Pos.TOP_CENTER);

		hbObjectiveCards.setSpacing(50);
		hbcolumnsCardsInfluence.setSpacing(50);

	}

	private void ConfigureInvisiblesPlaces() { // configurer les box invisibles pour bien placer les places des joueurs
		leftPlaces.setAlignment(Pos.CENTER_LEFT);
		rightPlaces.setAlignment(Pos.CENTER_RIGHT);
		topPlaces.setAlignment(Pos.TOP_CENTER);
		bottomPlaces.setAlignment(Pos.BOTTOM_CENTER);
		topContent.setAlignment(Pos.CENTER);
		bottomContent.setAlignment(Pos.BOTTOM_CENTER);

		place1.setPrefSize(330, 180);
		place2.setPrefSize(330, 180);
		place3.setPrefSize(330, 180);
		place4.setPrefSize(180, 330);
		place5.setPrefSize(180, 330);
		place6.setPrefSize(330, 180);
		place7.setPrefSize(330, 180);
		place8.setPrefSize(330, 180);
		place9.setPrefSize(180, 330);
		place10.setPrefSize(180, 330);

	}

	private void placementWidgetsInContainers() { // mettre les elements dans les boites
		vbButton1.getChildren().add(bRule1);
		vbButton1.getChildren().add(bSetting1);
		topLeftButtons.getChildren().add(vbButton1);

		vbButton2.getChildren().add(bRule2);
		vbButton2.getChildren().add(bSetting2);
		topRightButtons.getChildren().add(vbButton2);

		vbButton3.getChildren().add(bSetting3);
		vbButton3.getChildren().add(bRule3);
		bottomLeftButtons.getChildren().add(vbButton3);

		vbButton4.getChildren().add(bSetting4);
		vbButton4.getChildren().add(bRule4);
		bottomRightButtons.getChildren().add(vbButton4);

		topPlaces.getChildren().add(place1);
		topPlaces.getChildren().add(place2);
		topPlaces.getChildren().add(place3);
		rightPlaces.getChildren().add(place4);
		rightPlaces.getChildren().add(place5);
		bottomPlaces.getChildren().add(place6);
		bottomPlaces.getChildren().add(place7);
		bottomPlaces.getChildren().add(place8);
		leftPlaces.getChildren().add(place9);
		leftPlaces.getChildren().add(place10);

		topContent.getChildren().add(topLeftButtons);
		topContent.getChildren().add(topPlaces);
		topContent.getChildren().add(topRightButtons);

		bottomContent.getChildren().add(bottomLeftButtons);
		bottomContent.getChildren().add(bottomPlaces);
		bottomContent.getChildren().add(bottomRightButtons);

		topContent.setSpacing(215);
		bottomContent.setSpacing(215);

	}

	private void placementContainersInPane() { // placer tout dans la boite principale
		this.setLeft(leftPlaces);
		this.setCenter(centre);
		this.setRight(rightPlaces);
		this.setTop(topContent);
		this.setBottom(bottomContent);
	}

	public void changeSizeButton() {
		bSetting1.setPrefSize(300, 50);
		bSetting1.setPrefWidth(250);
		bSetting2.setPrefSize(300, 50);
		bSetting2.setPrefWidth(250);
		bSetting3.setPrefSize(300, 50);
		bSetting3.setPrefWidth(250);
		bSetting4.setPrefSize(300, 50);
		bSetting4.setPrefWidth(250);

		bRule1.setPrefSize(300, 50);
		bRule1.setPrefWidth(250);
		bRule2.setPrefSize(300, 50);
		bRule2.setPrefWidth(250);
		bRule3.setPrefSize(300, 50);
		bRule3.setPrefWidth(250);
		bRule4.setPrefSize(300, 50);
		bRule4.setPrefWidth(250);

		bRule3.setTranslateX(-11);
		bSetting3.setTranslateX(-11);
		bRule4.setTranslateX(11);
		bSetting4.setTranslateX(11);

	}

	public void RotateButton() {
		bSetting1.setRotate(180);
		bSetting2.setRotate(180);

		bRule1.setRotate(180);
		bRule2.setRotate(180);

		bRule3.setTranslateY(100);
		bSetting3.setTranslateY(100);
		bRule4.setTranslateY(100);
		bSetting4.setTranslateY(100);

	}

	public void changeButtonColor() {
		bRule1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bRule2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bRule3.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bRule4.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");

		bSetting1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bSetting2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bSetting3.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bSetting4.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");

		bRule1.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRule2.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRule3.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRule4.textProperty().bind(I18N.createStringBinding("btn.rules"));

		bSetting1.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bSetting2.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bSetting3.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bSetting4.textProperty().bind(I18N.createStringBinding("btn.settings"));

	}

	public void changeButtonPosition() {
		bRule1.setTranslateX(50);
		bRule2.setTranslateX(-50);
		bRule3.setTranslateX(50);
		bRule4.setTranslateX(-50);

		bSetting1.setTranslateX(50);
		bSetting2.setTranslateX(-50);
		bSetting3.setTranslateX(50);
		bSetting4.setTranslateX(-50);

		bRule1.setTranslateY(25);
		bRule2.setTranslateY(25);
		bRule3.setTranslateY(-25);
		bRule4.setTranslateY(-25);

		bSetting1.setTranslateY(50);
		bSetting2.setTranslateY(50);
	}

	public void changeLabelStyle() {

		lNewRound.setFont(Font.font("Gabriola", FontWeight.BOLD, 45));
		}

	/**
	 * placer un joueur
	 *
	 * @param nbrElem
	 * @param position
	 * @param box
	 */
	public void setPlace(int nbrElem, int position, int box, String pseudo, String color) {
		whoHasWhatPlace.add(pseudo);
		stackPlaces.getChildren().get(0).setStyle(color);

		if (box == 1 && position == 2)
			topPlaces.setMargin(topPlaces.getChildren().get(1), new Insets(0, 0, 0, 0));
		if (box == 3 && position == 2)
			bottomPlaces.setMargin(bottomPlaces.getChildren().get(1), new Insets(0, 0, 0, 0));

		switch (box) {
		case 1:

			ChangeElementPlace(nbrElem, position, topPlaces, stackPlaces.getChildren().get(0)); // on remplace la boite
																								// de selection par la
																								// place du joueur
			if (position == 2)
				topPlaces.setMargin(topPlaces.getChildren().get(1), new Insets(0, 150, 0, 150));
			break;
		case 2:
			stackPlaces.getChildren().get(0).setRotate(90);
			stackPlaces.getChildren().get(0).setTranslateX(120);
			stackPlaces.getChildren().get(0).setTranslateY(90);
			VBox.setMargin(stackPlaces.getChildren().get(0), new Insets(0, 70, 150, 0));
			ChangeElementPlace(nbrElem, position, rightPlaces, stackPlaces.getChildren().get(0)); // on remplace la
																									// boite de
																									// selection par la
																									// place du joueur

			break;
		case 3:
			stackPlaces.getChildren().get(0).setRotate(180);
			stackPlaces.getChildren().get(0).setTranslateY(-5);
			ChangeElementPlace(nbrElem, position, bottomPlaces, stackPlaces.getChildren().get(0)); // on remplace la
																									// boite de
																									// selection par la
																									// place du joueur
			if (position == 2)
				bottomPlaces.setMargin(bottomPlaces.getChildren().get(1), new Insets(0, 150, 0, 150));
			break;

		case 4:
			stackPlaces.getChildren().get(0).setRotate(-90);
			stackPlaces.getChildren().get(0).setTranslateX(-50);
			stackPlaces.getChildren().get(0).setTranslateY(90);
			VBox.setMargin(stackPlaces.getChildren().get(0), new Insets(0, 70, 150, 0));
			ChangeElementPlace(nbrElem, position, leftPlaces, stackPlaces.getChildren().get(0)); // on remplace la boite
																									// de selection par
																									// la place du
																									// joueur
			break;

		}

		switch (whoHasWhatPlace.indexOf(pseudo) + 1) {
		case 1:
			vbP1.getChildren().remove(1);
			vbP1.getChildren().add(new Label(pseudo));
			break;
		case 2:
			vbP2.getChildren().remove(1);
			vbP2.getChildren().add(new Label(pseudo));
			break;
		case 3:
			vbP3.getChildren().remove(1);
			vbP3.getChildren().add(new Label(pseudo));
			break;
		case 4:
			vbP4.getChildren().remove(1);
			vbP4.getChildren().add(new Label(pseudo));
			break;
		case 5:
			vbP5.getChildren().remove(1);
			vbP5.getChildren().add(new Label(pseudo));
			break;
		case 6:
			vbP6.getChildren().remove(1);
			vbP6.getChildren().add(new Label(pseudo));
			break;
		}
	}

	/**
	 * retire la dernière carte influence d'une colonne
	 *
	 * @param column
	 */
	public void removeInfluenceCard(int column) {
		switch (column) {
		case 1:
			lastestInfluenceCardsC1.getChildren().remove(lastestInfluenceCardsC1.getChildren().size() - 1);
			break;
		case 2:
			lastestInfluenceCardsC2.getChildren().remove(lastestInfluenceCardsC2.getChildren().size() - 1);
			// UntightenCards(column1);
			break;
		case 3:
			lastestInfluenceCardsC3.getChildren().remove(lastestInfluenceCardsC3.getChildren().size() - 1);
			// UntightenCards(column1);
			break;
		case 4:
			lastestInfluenceCardsC4.getChildren().remove(lastestInfluenceCardsC4.getChildren().size() - 1);
			// UntightenCards(column1);
			break;
		case 5:
			lastestInfluenceCardsC5.getChildren().remove(lastestInfluenceCardsC5.getChildren().size() - 1);
			// UntightenCards(column1);
			break;
		case 6:
			lastestInfluenceCardsC6.getChildren().remove(lastestInfluenceCardsC6.getChildren().size() - 1);
			// UntightenCards(column1);
			break;
		}
	}

	public void clearAllCards() {
		Platform.runLater(() -> {
			for (int i = hbObjectiveCards.getChildren().size(); i > 0; i--) {
				hbObjectiveCards.getChildren().remove(0);
			}

			for (int i = lastestInfluenceCardsC1.getChildren().size(); i > 0; i--) {
				lastestInfluenceCardsC1.getChildren().remove(0);
			}
			for (int i = firstInfluenceCardsC1.getChildren().size(); i > 0; i--) {
				firstInfluenceCardsC1.getChildren().remove(0);
			}

			for (int i = lastestInfluenceCardsC2.getChildren().size(); i > 0; i--) {
				lastestInfluenceCardsC2.getChildren().remove(0);
			}
			for (int i = firstInfluenceCardsC2.getChildren().size(); i > 0; i--) {
				firstInfluenceCardsC2.getChildren().remove(0);
			}

			for (int i = lastestInfluenceCardsC3.getChildren().size(); i > 0; i--) {
				lastestInfluenceCardsC3.getChildren().remove(0);
			}
			for (int i = firstInfluenceCardsC3.getChildren().size(); i > 0; i--) {
				firstInfluenceCardsC3.getChildren().remove(0);
			}
			// lastestInfluenceCardsC3.getChildren().remove(0);

			for (int i = lastestInfluenceCardsC4.getChildren().size(); i > 0; i--) {
				lastestInfluenceCardsC4.getChildren().remove(0);
			}
			for (int i = firstInfluenceCardsC4.getChildren().size(); i > 0; i--) {
				firstInfluenceCardsC4.getChildren().remove(0);
			}

			for (int i = lastestInfluenceCardsC5.getChildren().size(); i > 0; i--) {
				lastestInfluenceCardsC5.getChildren().remove(0);
			}
			for (int i = firstInfluenceCardsC5.getChildren().size(); i > 0; i--) {
				firstInfluenceCardsC5.getChildren().remove(0);
			}

			for (int i = lastestInfluenceCardsC6.getChildren().size(); i > 0; i--) {
				lastestInfluenceCardsC6.getChildren().remove(0);
			}
			for (int i = firstInfluenceCardsC6.getChildren().size(); i > 0; i--) {
				firstInfluenceCardsC6.getChildren().remove(0);
			}
			firstInfluenceCardsC1.setSpacing(0);
			firstInfluenceCardsC2.setSpacing(0);
			firstInfluenceCardsC3.setSpacing(0);
			firstInfluenceCardsC4.setSpacing(0);
			firstInfluenceCardsC5.setSpacing(0);
			firstInfluenceCardsC6.setSpacing(0);
		});

	}

	// int nbrElem -> nombre d'elements dans la boite (2 ou 3)
	// int position -> position de l'element a remplacer (1, 2 ou 3)
	// Pane box -> boite dans laquelle on remplace
	// Node ObjetAPlacer -> nouvel element a placer
	public void ChangeElementPlace(int nbrElem, int position, Pane box, Node ObjetAPlacer) { // remplacer un élément
																								// dans les boites des
																								// places
		Platform.runLater(() -> {
			if (nbrElem == 3) {
				if (position == 1) {
					stack.getChildren().addAll(box.getChildren().get(1), box.getChildren().get(2)); // on place
																									// les
																									// elements
																									// inutiles
																									// dans la
																									// pile

					box.getChildren().remove(box.getChildren().get(0)); // on supprime l'element a remplacer
					box.getChildren().addAll(ObjetAPlacer, stack.getChildren().get(0), stack.getChildren().get(1)); // ajoute
																													// l'element
																													// a
																													// placer
																													// au
																													// bon
																													// endroit
																													// en
																													// depilant
																													// les
																													// elements
																													// inutiles
				} else if (position == 2) {
					stack.getChildren().addAll(box.getChildren().get(0), box.getChildren().get(2));

					box.getChildren().remove(box.getChildren().get(0));
					box.getChildren().addAll(stack.getChildren().get(0), ObjetAPlacer, stack.getChildren().get(1));
				} else {
					stack.getChildren().addAll(box.getChildren().get(0), box.getChildren().get(1));

					box.getChildren().remove(box.getChildren().get(0));
					box.getChildren().addAll(stack.getChildren().get(0), stack.getChildren().get(1), ObjetAPlacer);
				}
			} else {
				if (position == 1) {
					stack.getChildren().add(box.getChildren().get(1));

					box.getChildren().remove(box.getChildren().get(0));
					box.getChildren().addAll(ObjetAPlacer, stack.getChildren().get(0));
				} else {
					stack.getChildren().add(box.getChildren().get(0));

					box.getChildren().remove(box.getChildren().get(0));
					box.getChildren().addAll(stack.getChildren().get(0), ObjetAPlacer);
				}
			}
		});
	}

	public GameTable(DialogueControllerTable cDialogue2) {
		cDialogue = cDialogue2;
		this.setBackground(new Background(new BackgroundFill(Color.web("#EFE4B0"), CornerRadii.EMPTY, null)));

		ConfigureRectangles();
		ConfigurePlayerPlaces();
		ConfigureInvisiblesPlaces();
		placementWidgetsInContainers();
		placementContainersInPane();
		configureCardsArea();
		changeSizeButton();
		RotateButton();
		changeButtonColor();
		changeButtonPosition();
		changeLabelStyle();

		// ----------- pour tester --------------//
		// defini l'evenement du bouton Regles
		bRule1.setOnAction(event -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.RULES);
		});
		bSetting1.setOnAction((event) -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		bRule2.setOnAction(event -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.RULES);
		});
		bSetting2.setOnAction((event) -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		bRule3.setOnAction(event -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.RULES);
		});
		bSetting3.setOnAction((event) -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		bRule4.setOnAction(event -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.RULES);
		});
		bSetting4.setOnAction((event) -> {
			cDialogue.editHistory(ScreenName);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		this.setVisible(false);
		cDialogue.saveScreen(ScreenName, this);

	}

}
