package tableInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import cd.DialogueControllerTable;
import cd.I18N;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class TableMenu extends BorderPane {

	private DialogueControllerTable cDialogue = null;
	// On donne un nom a cette interface pour permettre l'identification par le
	// systeme
	private static final screenList nameScreen = screenList.TABLEMENU;

	HBox Htop;

	Label nbPlayers;
	Label nbPlayersInformations;

	VBox Vbottom;
	VBox Vleft;
	VBox Vright;

	VBox Vmiddle;
	HBox HVmiddle;

	Button bplay;
	Button bSettings;
	Button bRules;
	Button bleave;

	FileInputStream inpTitle;
	Image imageTitle;
	ImageView imageViewTitle;

	ChoiceBox<Object> choiceBox;
	String nbPlayersValues[] = { "2", "3", "4", "5", "6" };
	
	private Image image1;
	private Image image2;

	// On crÃ©Ã© les differents composants de l'interface (Label, Button)
	private void createWidgets() throws FileNotFoundException {
		// ***LABEL***//

		// CREATION ET CONFIGURATION DU LABEL NBJOUEURS
		nbPlayers = new Label();
		nbPlayers.setFont(Font.font("Gabriola", FontWeight.BOLD, 25));
		nbPlayers.textProperty().bind(I18N.createStringBinding("lb.nbplayer"));
		// CREATION ET CONFIGURATION DU LABEL NBJOUEURS
		nbPlayersInformations = new Label();
		nbPlayersInformations.setFont(Font.font("Gabriola", FontWeight.BOLD, 25));
		nbPlayers.textProperty().bind(I18N.createStringBinding("lb.nbPlayInformation"));
		// CREATION ET CONFIGURATION DU BOUTON JOUER
		bplay = new Button();
		bplay.setFont(Font.font("Gabriola", FontWeight.BOLD, 80));
		bplay.setAlignment(Pos.CENTER);
		bplay.setPrefWidth(130);
		bplay.setPrefSize(600, 100);
		bplay.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bplay.textProperty().bind(I18N.createStringBinding("btn.createGame"));
		// CREATION ET CONFIGURATION DU BOUTON PARAMETRES
		bSettings = new Button();
		bSettings.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bSettings.setPrefSize(200, 50);
		bSettings.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bSettings.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bSettings.setTranslateX(-50);
		// CREATION ET CONFIGURATION DU BOUTON REGLES DU JEU
		bRules = new Button();
		bRules.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bRules.setPrefSize(200, 50);
		bRules.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bRules.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRules.setTranslateX(50);
		// CREATION ET CONFIGURATION DU BOUTON leave
		bleave = new Button();
		bleave.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bleave.setPrefSize(200, 50);
		bleave.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bleave.textProperty().bind(I18N.createStringBinding("btn.leave"));
		bleave.setTranslateY(-38);

		// creation et initialisation de l'image Titre

		// inpTitle = new FileInputStream(("./src/ressources/title.png"));

		// regarder le chemin !!!!
		// imageTitle = new Image(inpTitle, 1000, 470, false, true);
		// imageViewTitle = new ImageView(imageTitle);

		// CREATION ET CONFIGURATION DU MENU DEROULANT DU CHOIX DU NOMBRE DE JOUEURS
		choiceBox = new ChoiceBox<Object>(FXCollections.observableArrayList(nbPlayersValues)); // Liste observable
																								// permettant
																								// d'actualiser l'action
																								// sur fait sur le menu
																								// dÃ©roulant
		choiceBox.setValue("2");
		choiceBox.setTooltip(new Tooltip("Choisir le nombre de joueurs")); // Mise en place d'une bulle d'informations
																			// lors du survol du menu dÃ©roulant
		choiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() { // MÃ©thode
																											// pemettant
																											// de mettre
																											// Ã  jours
																											// le label
																											// en
																											// traduisant
																											// le label
																											// en nombre
			public void changed(ObservableValue ov, Number value, Number new_value) {
				nbPlayersInformations.setText(nbPlayersValues[new_value.intValue()] + " joueurs selectionnés");
			}
		});

	}

	// On crÃ©Ã© les diffÃ©rents emplacements de l'interface (HBox, VBox), cela
	// permet de structurer la page
	private void createContainers() {

		// CREATION ET CONFIGURATION DE LA HBOX HAUTE/TITRE
		Htop = new HBox();
		Htop.setAlignment(Pos.CENTER);

		// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
		Vbottom = new VBox();
		Vbottom.setAlignment(Pos.BOTTOM_CENTER);

		// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
		Vleft = new VBox();
		Vleft.setAlignment(Pos.BOTTOM_CENTER);

		// CREATION ET CONFIGURATION DE LA VBOX DROITE
		Vright = new VBox();
		Vright.setAlignment(Pos.BOTTOM_CENTER);

		// CREATION ET CONFIGURATION DE LA VBOX middle
		Vmiddle = new VBox();
		Vmiddle.setAlignment(Pos.CENTER);
		Vmiddle.setMargin(choiceBox, new Insets(10));

		// CREATION HBOX pour choix du nb joueurs
		HVmiddle = new HBox();
		HVmiddle.setAlignment(Pos.CENTER);
		HVmiddle.setMargin(choiceBox, new Insets(10));

	}

	// On affecte les diffÃ©rents Label, Button dans leurs emplacements respectifs
	// (HBox, VBox)
	private void placementWidgetsInContainers() {

		// ON AJOUTE TITRE DANS TOP
		// Htop.getChildren().add(imageViewTitle);

		// ON AJOUTE JOUER DANS middle
		Vmiddle.getChildren().add(bplay);
		Vmiddle.getChildren().add(HVmiddle);

		HVmiddle.getChildren().add(nbPlayers);
		HVmiddle.getChildren().add(choiceBox);
		this.setMargin(Vmiddle, new Insets(400, 0, 0, 0));

		Vmiddle.getChildren().add(nbPlayersInformations);

		// ON AJOUTE leave DANS BOTTOM
		Vbottom.getChildren().add(bleave);

		// ON AJOUTE REGLES DU JEU DANS VGAUCHE
		Vleft.getChildren().add(bRules);
		Vleft.setMargin(bRules, new Insets(0, 0, 0, 10));

		// ON AJOUTE PARAMETRE DANS VDROITE
		Vright.getChildren().add(bSettings);
		Vright.setMargin(bSettings, new Insets(0, 10, 0, 0));

	}

	// On dÃ©finit l'emplacement des HBox et VBox dans l'interface
	private void placementContainersInPane() {

		// ON ASSIGNE A CHAQUE BOX LEUR EMPLACEMENT
		this.setTop(Htop);
		this.setBottom(Vbottom);
		this.setLeft(Vleft);
		this.setRight(Vright);
		this.setCenter(Vmiddle);
	}

	private void initBackground() {
		try {
			FileInputStream inpBackground = new FileInputStream("./src/ressources/background_menu.png");
			FileInputStream inpBackgroundSpecial = new FileInputStream("./src/ressources/background_special.jpg");
			// Creation de l'image
			image1 = new Image(inpBackground);
			image2 = new Image(inpBackgroundSpecial);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		
	public void setTheme(boolean b) {
		if (b) {
			BackgroundImage backgroundimage = new BackgroundImage(image2, BackgroundRepeat.NO_REPEAT,
					BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
					new BackgroundSize(1.0, 1.0, true, true, false, false));
			Background background = new Background(backgroundimage);
			this.setBackground(background);
		} else {
			BackgroundImage backgroundimage = new BackgroundImage(image1, BackgroundRepeat.NO_REPEAT,
					BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
					new BackgroundSize(1.0, 1.0, true, true, false, false));
			Background background = new Background(backgroundimage);
			this.setBackground(background);
		}
	}

	public TableMenu(DialogueControllerTable cDialogue2) throws FileNotFoundException {

		cDialogue = cDialogue2;

		createWidgets();
		initBackground();
		createContainers();
		placementWidgetsInContainers();
		placementContainersInPane();
		setTheme(false);

		//
		/*FileInputStream inpBackground = new FileInputStream("./src/ressources/background_menu.jpg");
		FileInputStream inpBackgroundSpecial = new FileInputStream("./src/ressources/background_speicial.jpg");
		// Creation de l'image
		Image image1 = new Image(inpBackground);
		Image image2 = new Image(inpBackgroundSpecial);
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1, BackgroundRepeat.NO_REPEAT,
				BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				new BackgroundSize(1.0, 1.0, true, true, false, false));
		Background background = new Background(backgroundimage);
		// On affecte Ã  l'Ã©cran courant l'image de fond dÃ©fini
		this.setBackground(background);*/

		bSettings.setOnAction((event) -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.editHistory(nameScreen);
			cDialogue.showScreen(screenList.SETTINGS);
		});

		bRules.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.RULES);
		});

		// cDialogue.showScreen(screenList.PLACEMENT)
		bplay.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.getMp().createGame(Integer.parseInt((String) choiceBox.getValue()));
		});

		bleave.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			Platform.exit();
		});

		this.setVisible(false);
		cDialogue.saveScreen(nameScreen, this);

	}

}
