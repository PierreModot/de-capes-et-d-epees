package tableInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueControllerTable;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;


	public class UnfoldingRuleTable extends BorderPane {

		private DialogueControllerTable cDialogue = null;
		//On donne un nom a cette interface pour permettre l'identification par le systeme
		private static final screenList nameScreen = screenList.UNFOLDINGRULETABLE;
		
			HBox Htop;

			Label lTitle;
		
			VBox Vbottom;
			VBox Vleft;
			VBox Vright;
			
			VBox Vmiddle;

			Button bBack1;
			Button bBack2;
			
			Label lu1;
			Label lu2;
			Label lu3;
			Label lu4;
			Label lu5;
			Label lu6;
			Label lu7;
			Label lu8;
			
			Label li1;
			Label li2;
			
			FileInputStream inp1;
			Image i1;
			ImageView iv1;
			
			VBox v1;
			VBox v2;
			VBox v3;

			// On créé les differents composants de l'interface (Label, Button)
		    private void createWidgets() throws FileNotFoundException {
				//***LABEL***//
		    	//creation et initialisation du Label lTitle
				lTitle = new Label();
				lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 200));		
				lTitle.textProperty().bind(I18N.createStringBinding("lb.titleUr"));
				
				//creation et initialisation du Button bBack1
				bBack1 = new Button();
				bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack1.setPrefSize(200, 50);
				bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack1.setTranslateY(-75);
				bBack1.setTranslateX(50);

				// creation et initialisation du Button bBack2
				bBack2 = new Button();
				bBack2.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack2.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack2.setPrefSize(200, 50);
				bBack2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack2.setTranslateY(-75);
				bBack2.setTranslateX(-50);
				
				lu1= new Label();
				lu1.setAlignment(Pos.CENTER);
				lu1.setFont(Font.font("Cambria", FontWeight.EXTRA_BOLD,20));
				lu1.setTextAlignment(TextAlignment.CENTER);
				lu1.textProperty().bind(I18N.createStringBinding("lb.u1"));

				lu2= new Label();
				lu2.setAlignment(Pos.CENTER);
				lu2.setFont(Font.font("Cambria", 20));
				lu2.setTextAlignment(TextAlignment.CENTER);
				lu2.textProperty().bind(I18N.createStringBinding("lb.u2"));
				
				lu3= new Label();
				lu3.setAlignment(Pos.CENTER);
				lu3.setFont(Font.font("Cambria", 20));
				lu3.textProperty().bind(I18N.createStringBinding("lb.u3"));
				
				
				lu4= new Label();
				lu4.setAlignment(Pos.CENTER);
				lu4.setFont(Font.font("Cambria", FontWeight.BOLD, 20));
			    lu4.textProperty().bind(I18N.createStringBinding("lb.u4"));
			    
			    
			    lu5= new Label();
			    lu5.setAlignment(Pos.CENTER);
			    lu5.setFont(Font.font("Cambria", 20));
			    lu5.textProperty().bind(I18N.createStringBinding("lb.u5"));
				
			    lu6= new Label();
			    lu6.setAlignment(Pos.CENTER);
			    lu6.setFont(Font.font("Cambria", 20));
			    lu6.textProperty().bind(I18N.createStringBinding("lb.u6"));
				
			    lu7= new Label();
			    lu7.setAlignment(Pos.CENTER);
			    lu7.setFont(Font.font("Cambria", 20));
			    lu7.textProperty().bind(I18N.createStringBinding("lb.u7"));
				
			    lu8= new Label();
			    lu8.setAlignment(Pos.CENTER);
				lu8.setFont(Font.font("Cambria", 20));
				lu8.textProperty().bind(I18N.createStringBinding("lb.u8"));
				
				inp1 = new FileInputStream("./src/ressources/Rules/ur.jpg");			
				i1  = new Image(inp1, 800, 350, false, true);
				iv1 = new ImageView(i1);
				
			}
				
		    
			// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
		    private void createContainers() {

				// CREATION ET CONFIGURATION DE LA HBOX HAUTE/TITRE
				Htop = new HBox();
				Htop.setAlignment(Pos.CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vbottom = new VBox();
				Vbottom.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vleft = new VBox();
				Vleft.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX DROITE
				Vright = new VBox();
				Vright.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX middle
				Vmiddle = new VBox();
				Vmiddle.setAlignment(Pos.TOP_CENTER);

				
				v1 = new VBox();
				v1.setAlignment(Pos.TOP_CENTER);
				v1.setSpacing(13);
				
				v2 = new VBox();
				v2.setAlignment(Pos.CENTER);
				v2.setSpacing(5);
				
				v3 = new VBox();
				v3.setAlignment(Pos.CENTER_LEFT);
				v3.setSpacing(7);
		    }

			// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
		    private void placementWidgetsInContainers() {
				
				// ON AJOUTE TITRE DANS TOP
				Htop.getChildren().add(lTitle);
						
				// ON AJOUTE JOUER DANS middle
				//Vmiddle.getChildren().add(); // Il faut rajouter le texte
				
				
				// ON AJOUTE REGLES DU JEU DANS VGAUCHE
				Vleft.getChildren().add(bBack1);

				// ON AJOUTE PARAMETRE DANS VDROITE
				Vright.getChildren().add(bBack2);

				v2.getChildren().add(lu2);
				v2.getChildren().add(iv1);
				v2.getChildren().add(lu3);
				
				v3.getChildren().add(lu4);			
				v3.getChildren().add(lu5);
				v3.getChildren().add(lu6);			
				v3.getChildren().add(lu7);
				v3.getChildren().add(lu8);
				
				v1.getChildren().add(lu1);			
				v1.getChildren().add(v2);
				v1.getChildren().add(v3);
				
				Vmiddle.getChildren().add(v1);
			}

			// On définit l'emplacement des HBox et VBox dans l'interface
		    private void placementContainersInPane() {
			
				// ON ASSIGNE A CHAQUE BOX LEUR EMPLACEMENT
				this.setTop(Htop);
				this.setLeft(Vleft);
				this.setRight(Vright);
				this.setCenter(Vmiddle);
			}

			
		public UnfoldingRuleTable(DialogueControllerTable cDialogue2) throws FileNotFoundException{

			cDialogue = cDialogue2;
			
			createWidgets();
		    createContainers();
		    placementWidgetsInContainers();
		    placementContainersInPane();
			
			 //
			Image image1 = Caches.BG;
			// Creation de l'image en arriere plan/ image de fond
			BackgroundImage backgroundimage = new BackgroundImage(image1,
			BackgroundRepeat.NO_REPEAT,
			BackgroundRepeat.NO_REPEAT,
			BackgroundPosition.DEFAULT,
			new BackgroundSize(1.0, 1.0, true, true, false, false));
		
			Background background = new Background(backgroundimage);
			//On affecte à l'écran courant l'image de fond défini
			this.setBackground(background);

			bBack1.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.RULES);
		});

			
			bBack2.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.RULES);
		});
			

			this.setVisible(false);
			cDialogue.saveScreen(nameScreen, this);

		}
			
	}





