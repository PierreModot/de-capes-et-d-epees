package tableInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueControllerTable;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


	public class DifferentCardRuleTable extends BorderPane {

		private DialogueControllerTable cDialogue = null;
		//On donne un nom a cette interface pour permettre l'identification par le systeme
		private static final screenList nameScreen = screenList.DIFFERENTCARDRULETABLE;
		
			HBox Htop;

			Label lTitle;
		
			VBox Vbottom;
			VBox Vleft;
			VBox Vright;
			
			VBox Vmiddle;
			VBox VmiddleBot;

			Button bBack1;
			Button bBack2;
			
			Button bnext;
			
			//1er partie
			Label ldcr;
			Label ld1;
			Label ld12;
			Label lt1;
			
			FileInputStream inp1;
			Image i1;
			ImageView iv1;
			FileInputStream inp12;
			Image i12;
			ImageView iv12;
			FileInputStream inp13;
			Image i13;
			ImageView iv13;
			FileInputStream inp14;
			Image i14;
			ImageView iv14;
			
			VBox v11;
			VBox v12;
			HBox h1;
			HBox h11;
			HBox h12;
			HBox h13;
			HBox h14;
			
			Label lking;
			Label lqueen;
			Label ljuliette;
			
			//2eme partie
			Label lt2;
			Label ld2;
			Label lexplorer;
			Label lmurderer;
			Label lstorm;
			Label ltraitor;
			Label lic;
			
			FileInputStream inp2;
			Image i2;
			ImageView iv2;
			FileInputStream inp21;
			Image i21;
			ImageView iv21;
			FileInputStream inp22;
			Image i22;
			ImageView iv22;
			FileInputStream inp23;
			Image i23;
			ImageView iv23;
			FileInputStream inp24;
			Image i24;
			ImageView iv24;
			FileInputStream inp25;
			Image i25;
			ImageView iv25;

			VBox v2;
			HBox h2;
			HBox h21;
			HBox h22;
			HBox h23;
			HBox h24;
			HBox h25;
			
			
			
			// On créé les differents composants de l'interface (Label, Button)
		    private void createWidgets() throws FileNotFoundException {
				//***LABEL***//
		    	//creation et initialisation du Label lTitle
				lTitle = new Label();
				lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 200));
				lTitle.textProperty().bind(I18N.createStringBinding("lb.titleDcr"));
		    	
				
				//creation et initialisation du Button bBack1
				bBack1 = new Button();
				bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack1.setPrefSize(200, 50);
				bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack1.setTranslateY(-75);
				bBack1.setTranslateX(50);

				// creation et initialisation du Button bBack2
				bBack2 = new Button();
				bBack2.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack2.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack2.setPrefSize(200, 50);
				bBack2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack2.setTranslateY(-75);
				bBack2.setTranslateX(-50);

				
				bnext = new Button(">");
				bnext.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
				bnext.setPrefSize(30, 30);
				bnext.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
					
				ldcr= new Label();
			    ldcr.setAlignment(Pos.CENTER);
			    ldcr.setFont(Font.font("Cambria", 20));
			    ldcr.textProperty().bind(I18N.createStringBinding("text.dcr"));
			    
			    //création et initialisation de tous les elements de la 1er partie
			    ld1= new Label();
			    ld1.setAlignment(Pos.CENTER);
			    ld1.setFont(Font.font("Cambria", FontWeight.BOLD, 20));
			    ld1.textProperty().bind(I18N.createStringBinding("text.d1"));
			    
			    ld12= new Label();
			    ld12.setAlignment(Pos.CENTER);
			    ld12.setFont(Font.font("Cambria", 20));
			    ld12.textProperty().bind(I18N.createStringBinding("text.d12"));
			    
			    lt1 = new Label();
			    lt1.setAlignment(Pos.CENTER_LEFT);
				lt1.setFont(Font.font("Cambria", FontWeight.BOLD, 20));		
			    lt1.setTextFill(Color.RED);
				lt1.textProperty().bind(I18N.createStringBinding("lb.t1"));
				
				inp1 = new FileInputStream("./src/ressources/Rules/king.png");			
				i1  = new Image(inp1, 80, 80, false, true);
				iv1 = new ImageView(i1);
				
				inp12 = new FileInputStream("./src/ressources/Rules/queen.png");	
				i12  = new Image(inp12, 80, 80 ,false, true);
				iv12 = new ImageView(i12);
				
				inp13 = new FileInputStream("./src/ressources/Rules/juliette.png");				
				i13  = new Image(inp13, 80, 80,false, true);
				iv13 = new ImageView(i13);
				
				inp14 = new FileInputStream("./src/ressources/Rules/dcr1.png");				
				i14  = new Image(inp14, 160, 160,false, true);
				iv14 = new ImageView(i14);
				
				lking= new Label();
				lking.setAlignment(Pos.CENTER);
				lking.setFont(Font.font("Cambria", 20));
				lking.textProperty().bind(I18N.createStringBinding("lb.king"));
			    
				lqueen= new Label();
				lqueen.setAlignment(Pos.CENTER);
				lqueen.setFont(Font.font("Cambria", 20));
			    lqueen.textProperty().bind(I18N.createStringBinding("lb.queen"));
			    
			    ljuliette= new Label();
			    ljuliette.setAlignment(Pos.CENTER);
			    ljuliette.setFont(Font.font("Cambria", 20));
			    ljuliette.textProperty().bind(I18N.createStringBinding("lb.juliette"));
			    
			    //création et initialisation de tous les elements de la 1er partie
			    lt2 = new Label();
			    lt2.setAlignment(Pos.CENTER_LEFT);
				lt2.setFont(Font.font("Cambria", FontWeight.BOLD, 20));		
			    lt2.setTextFill(Color.RED);
				lt2.textProperty().bind(I18N.createStringBinding("lb.t2"));
				
				ld2 = new Label();
				ld2.setAlignment(Pos.CENTER);
				ld2.setFont(Font.font("Cambria", 20));
				ld2.textProperty().bind(I18N.createStringBinding("text.d2"));
				    
				lexplorer= new Label();
				lexplorer.setAlignment(Pos.CENTER);
				lexplorer.setFont(Font.font("Cambria", 20));
				lexplorer.textProperty().bind(I18N.createStringBinding("lb.explorer"));
				
				lmurderer= new Label();
				lmurderer.setAlignment(Pos.CENTER);
				lmurderer.setFont(Font.font("Cambria", 20));
				lmurderer.textProperty().bind(I18N.createStringBinding("lb.murderer"));
				
				/*
				lstorm= new Label();
				lstorm.setAlignment(Pos.CENTER);
				lstorm.setFont(Font.font("Cambria", 20));
				lstorm.textProperty().bind(I18N.createStringBinding("lb.storm"));
				
				ltraitor= new Label();
				ltraitor.setAlignment(Pos.CENTER);
				ltraitor.setFont(Font.font("Cambria", 20));
				ltraitor.textProperty().bind(I18N.createStringBinding("lb.traitor"));
				
				lic= new Label();
				lic.setAlignment(Pos.CENTER);
				lic.setFont(Font.font("Cambria", 20));
				lic.textProperty().bind(I18N.createStringBinding("lb.ic"));			
				*/
				
				inp2 = new FileInputStream("./src/ressources/Rules/dcr2.png");			
				i2  = new Image(inp2, 80, 80, false, true);
				iv2 = new ImageView(i2);
				
				inp21 = new FileInputStream("./src/ressources/Rules/explorer.png");			
				i21  = new Image(inp21, 80, 80, false, true);
				iv21 = new ImageView(i21);
				
				inp22 = new FileInputStream("./src/ressources/Rules/murderer.png");			
				i22  = new Image(inp22, 80, 80, false, true);
				iv22 = new ImageView(i22);
				
				/*
				inp23 = new FileInputStream("./src/ressources/Rules/storm.png");			
				i23  = new Image(inp23, 80, 80, false, true);
				iv23 = new ImageView(i23);
				
				inp24 = new FileInputStream("./src/ressources/Rules/traitor.png");			
				i24  = new Image(inp24, 80, 80, false, true);
				iv24 = new ImageView(i24);
				
				inp25 = new FileInputStream("./src/ressources/Rules/ic.png");			
				i25  = new Image(inp25, 80, 80, false, true);
				iv25 = new ImageView(i25);
				*/
				
			}
				
		    
			// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
		    private void createContainers() {

				// CREATION ET CONFIGURATION DE LA HBOX HAUTE/TITRE
				Htop = new HBox();
				Htop.setAlignment(Pos.CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vbottom = new VBox();
				Vbottom.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vleft = new VBox();
				Vleft.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX DROITE
				Vright = new VBox();
				Vright.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX middle
				Vmiddle = new VBox();
				Vmiddle.setAlignment(Pos.TOP_LEFT);
				Vmiddle.setSpacing(15);
				
				VmiddleBot = new VBox();
				VmiddleBot.setAlignment(Pos.BOTTOM_CENTER);
				
				//1er partie
				v11 = new VBox();
				v11.setAlignment(Pos.CENTER);
				
				v12 = new VBox();
				v12.setAlignment(Pos.CENTER_LEFT);
				
				h1 = new HBox();
				h1.setAlignment(Pos.CENTER);
				h1.setSpacing(70);
				
				h11 = new HBox();
				h11.setAlignment(Pos.CENTER);
				h11.setSpacing(10);
				
				h12 = new HBox();
				h12.setAlignment(Pos.CENTER);
				h12.setSpacing(10);
				
				h13 = new HBox();
				h13.setAlignment(Pos.CENTER);
				h13.setSpacing(10);
				
				h14 = new HBox();
				h14.setAlignment(Pos.CENTER);
				h14.setSpacing(10);
				
				//2eme partie
				v2 = new VBox();
				v2.setAlignment(Pos.CENTER_LEFT);
				v2.setSpacing(15);
				
				h2 = new HBox();
				h2.setAlignment(Pos.CENTER_LEFT);
				h2.setSpacing(10);
				
				h21 = new HBox();
				h21.setAlignment(Pos.CENTER_LEFT);
				h21.setSpacing(10);
				
				h22 = new HBox();
				h22.setAlignment(Pos.CENTER_LEFT);
				h22.setSpacing(10);
				
				/*
				h23 = new HBox();
				h23.setAlignment(Pos.CENTER_LEFT);
				h23.setSpacing(10);
				
				h24 = new HBox();
				h24.setAlignment(Pos.CENTER_LEFT);
				h24.setSpacing(10);
				
				h25 = new HBox();
				h25.setAlignment(Pos.CENTER_LEFT);
				h25.setSpacing(10);
				*/
		    }

			// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
		    private void placementWidgetsInContainers() {
				
				// ON AJOUTE TITRE DANS TOP
				Htop.getChildren().add(lTitle);
								
				// ON AJOUTE REGLES DU JEU DANS VGAUCHE
				Vleft.getChildren().add(bBack1);

				// ON AJOUTE PARAMETRE DANS VDROITE
				Vright.getChildren().add(bBack2);
				
				// ON AJOUTE PARAMETRE DANS 1ER PARTIE
				h11.getChildren().add(iv1);
				h11.getChildren().add(lking);
				
				h12.getChildren().add(iv12);
				h12.getChildren().add(lqueen);
				
				h13.getChildren().add(iv13);
				h13.getChildren().add(ljuliette);
				
				v11.getChildren().add(h11);
				v11.getChildren().add(h12);
				v11.getChildren().add(h13);
				
				h14.getChildren().add(iv14);
				h14.getChildren().add(ld12);
				
				v12.getChildren().add(ld1);
				v12.getChildren().add(h14);
				
				h1.getChildren().add(v11);
				h1.getChildren().add(v12);
				
				// ON AJOUTE PARAMETRE DANS 2EME PARTIE
				h2.getChildren().add(ld2);
				h2.getChildren().add(iv2);
				
				h21.getChildren().add(iv21);
				h21.getChildren().add(lexplorer);
				
				h22.getChildren().add(iv22);
				h22.getChildren().add(lmurderer);
				
				/*
				h23.getChildren().add(iv23);
				h23.getChildren().add(lstorm);
				
				h24.getChildren().add(iv24);
				h24.getChildren().add(ltraitor);
				
				h25.getChildren().add(iv25);
				h25.getChildren().add(lic);
				*/
				
				v2.getChildren().add(h2);
				v2.getChildren().add(h21);
				v2.getChildren().add(h22);
				//v2.getChildren().add(h23);
				//v2.getChildren().add(h24);
				//v2.getChildren().add(h25);
				
				// ON AJOUTE PARAMETRE DANS VMIDDLE
				Vmiddle.getChildren().add(ldcr);
				Vmiddle.getChildren().add(lt1);
				Vmiddle.getChildren().add(h1);
				Vmiddle.getChildren().add(lt2);
				Vmiddle.getChildren().add(v2);
				VmiddleBot.getChildren().add(bnext);
				Vmiddle.getChildren().add(VmiddleBot);

			}

			// On définit l'emplacement des HBox et VBox dans l'interface
		    private void placementContainersInPane() {
			
				// ON ASSIGNE A CHAQUE BOX LEUR EMPLACEMENT
				this.setTop(Htop);
				this.setLeft(Vleft);
				this.setRight(Vright);
				this.setCenter(Vmiddle);
			}

			
		public DifferentCardRuleTable(DialogueControllerTable cDialogue2) throws FileNotFoundException{

			cDialogue = cDialogue2;
			
			createWidgets();
		    createContainers();
		    placementWidgetsInContainers();
		    placementContainersInPane();
			
			 //
			Image image1 = Caches.BG;
			// Creation de l'image en arriere plan/ image de fond
			BackgroundImage backgroundimage = new BackgroundImage(image1,
			BackgroundRepeat.NO_REPEAT,
			BackgroundRepeat.NO_REPEAT,
			BackgroundPosition.DEFAULT,
			new BackgroundSize(1.0, 1.0, true, true, false, false));
		
			Background background = new Background(backgroundimage);
			//On affecte à l'écran courant l'image de fond défini
			this.setBackground(background);

			bBack1.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.RULES);
		});



			bBack2.setOnAction(event -> {
			cDialogue.getSettings().effectSounds();
			cDialogue.showScreen(screenList.RULES);
		});
			
			bnext.setOnAction(event -> {
				cDialogue.getSettings().effectSounds();
				cDialogue.showScreen(screenList.DIFFERENTCARDRULETABLE2);
		});

			this.setVisible(false);
			cDialogue.saveScreen(nameScreen, this);

		}
			
	}
