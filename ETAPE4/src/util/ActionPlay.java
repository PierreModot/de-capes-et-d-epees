package util;

public class ActionPlay {
    private Role card;
    private int column;
    private Color player;
    
    public Role getCard(){return card;}
    public int getColumn(){return column;}
    public Color getPlayer(){return player;}

    public void setCard(Role newValue){card=newValue;}
    public void setColumn(int newValue){column=newValue;}
    public void setPlayer(Color newValue){player=newValue;}

    public ActionPlay(Role card,int column,Color player){
        this.card=card;
        this.column=column;
        this.player=player;
    }

}
