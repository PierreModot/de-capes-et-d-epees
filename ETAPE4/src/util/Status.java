package util;

public enum Status {
    ATTENTE,	//En creation de partie, tant qu'il n'y a pas tous les joueurs
    ANNULEE,	//Quand un joueur quitte la partie, cela arrete la partie
    COMPLETE,	//Lorsque tous les joueurs sont presents, la partie pourra commencer
    TERMINEE,	//Lorsqu'une partie est terminee depuis au moins 1 minute, apres, la partie se supprime
    PAUSE;		//Pour mettre en Pause une partie
}