package util;

import java.nio.channels.GatheringByteChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import card.InfluenceCard;
import gamePart.Columns;
import gamePart.Game;
import gamePart.MP;

public class GameState {
    private GameState parent;
    private ArrayList<GameState> childs=null;
    private int numerator=0;
    private int denominator=0;
    private ArrayList<Columns> state;
    private ActionPlay action=null;
    private ArrayList<InfluenceCard> currentHand;
    private Color playerColor;
    
    public GameState getParent(){return parent;}
    public ArrayList<GameState> getChilds(){return childs;}
    public int getNumerator(){return numerator;}
    public int getDenominator(){return denominator;}
    public ArrayList<Columns> getState(){return state;}
    public ActionPlay getAction(){return action;}

    public void setParent(GameState newValue){parent=newValue;}
    public void setChilds(ArrayList<GameState> newValue){childs=newValue;}
    public void setNumerator(int newValue){numerator=newValue;}
    public void setDenominator(int newValue){denominator=newValue;}
    public void setState(ArrayList<Columns> newValue){state=newValue;}
    public void setAction(ActionPlay newValue){action=newValue;}

    public GameState(Game game,ArrayList<InfluenceCard> hand, Color color){
        parent=null;
        state=game.getColumns();
        currentHand=hand;
        playerColor=color;
    }

    public GameState(GameState parent,ArrayList<Columns> state,ActionPlay action,ArrayList<InfluenceCard> currentHand,Color playerColor){
        this.parent=parent;
        this.state=state;
        this.action=action;
        this.currentHand=currentHand;
        this.playerColor=playerColor;
    }

    public GameState(GameState parent,ArrayList<GameState> childs,int numerator,int denominator,ArrayList<Columns> state,ActionPlay action,ArrayList<InfluenceCard> currentHand,Color playerColor){
        this.parent=parent;
        this.childs=childs;
        this.numerator=numerator;
        this.denominator=denominator;
        this.state=state;
        this.action=action;
        this.currentHand=currentHand;
        this.playerColor=playerColor;
    }

    public GameState(GameState dad){
        parent=dad;
    }

    public double score(){
        return (numerator*1.0)/denominator;
    }

    public boolean expand(){
    	
        if(!fullyExtended()){//Si pas tout les enfants ont ete cree
            int card=0;
            int column=0;
            boolean quit = false;
            do{
                Role character = currentHand.get(card).getRole();
                do{

                    for(GameState child : childs){
                        ActionPlay action = child.getAction();
                        if((action.getCard() != currentHand.get(card).getRole() || action.getColumn() != column) && quit==false){
                            quit = true;
                        }
                    }
                
                }while(!quit);

                card++;
            }while(!quit);
            
            if(quit){
                ArrayList<InfluenceCard>childHand=currentHand;
                childHand.remove(card);
                ArrayList<Columns>childState=state;
                childState.get(column).addInfluenceCard(new InfluenceCard(currentHand.get(card)));
                
                childs.add(new GameState(this,childState,new ActionPlay(currentHand.get(card).getRole(), column, playerColor),childHand,playerColor));
                childs.get(childs.size()-1).simulate();
                return true;
            }else{
                return false;
            }


            
        }else {//Si tout les child ont ete cree
            double bestScore=-1;
            GameState childToExplore=null;
            
            if(this.getChilds()!=null) {
            	for(GameState game:childs){
	            	if(!( fullyExtended() && game.getChilds()==null )) {
	            		if(game.score()>bestScore){
		                    childToExplore=game;
		                    bestScore=game.score();
		                }else if (game.score() == bestScore && childToExplore!=null && game.getDenominator()<childToExplore.getDenominator()){
		                    childToExplore=game;
		                    bestScore=game.score();
		                }
	            	}
	                
	            }
            }
            
            return childToExplore.expand();
        }
    }

    public boolean fullyExtended(){
    	boolean leaf=true;
    	for(Columns column : state) {
    		if(column.getIsRealized())leaf=false;
    	}
    	if(!leaf) {
		int columnPlayable=0;
        boolean end=true;
        for (Columns column:state){
            boolean found=false;
            for(InfluenceCard card:column.getContent()){
                if (card.getRole()==Role.Te)found=true;
            }
            if(!column.getIsRealized()) end=false;
            if(!found)columnPlayable++;
        }
        int size=currentHand.size();
        if (size<=0) size=1;
        return childs.size()>=size*columnPlayable;
    	}else {
        return true;
        }
        
    }

    public boolean evaluate(ArrayList<InfluenceCard> hand,ArrayList<InfluenceCard> deck){

        int i=0;
        currentHand=hand;
        boolean execution=true;

        while(i<100 && execution==true){ //au moins 60 boucles
            execution = this.expand();
            i++;
        }
        return execution;
    }

    public GameState bestChild(){
        double bestChildScore=0.0;
        GameState bestChild=null;
        for(GameState stateTest : childs){
            double score=stateTest.score()+2*Math.sqrt(2*((-Math.log(1-getDenominator()))/getDenominator())/((-Math.log(1-getDenominator()))/getDenominator()));

            if(score>bestChildScore){
                bestChildScore=score;
                bestChild=stateTest;
            }
        }
        return bestChild;
    }

    public void simulate(){
        //Cree La simulation
        ArrayList<Columns> simulation=this.getState();
        int timeSimulation=0;
        int player=1;
        int nbPlayer=state.size();
        Random rdm = new Random();

        ArrayList<Role> allCards=new ArrayList<>();
        allCards.add(Role.Ro);allCards.add(Role.Re);allCards.add(Role.Ju);allCards.add(Role.Al);allCards.add(Role.Md);
        allCards.add(Role.Se);allCards.add(Role.Ma);allCards.add(Role.Ca);allCards.add(Role.Tb);allCards.add(Role.Ex);
        allCards.add(Role.As);allCards.add(Role.Te);allCards.add(Role.Tr);allCards.add(Role.Ci);allCards.add(Role.Tm);
        allCards.add(Role.Mg);allCards.add(Role.So);allCards.add(Role.Pr);allCards.add(Role.Ec);allCards.add(Role.Er);
        allCards.add(Role.Pg);allCards.add(Role.Dr);allCards.add(Role.RO);allCards.add(Role.Me);allCards.add(Role.SO);
        ArrayList<Role> currentDeck = allCards;
        for(Columns column:state){
            for(InfluenceCard card : column.getContent()){
                if (card.getColor()==playerColor){currentDeck.remove(card.getRole());}
            }
        }
        for(InfluenceCard card : currentHand){
            if (card.getColor()==playerColor){currentDeck.remove(card.getRole());}
        }

        while (timeSimulation<nbPlayer*6 && !fullyExtended()){
            //Creation de la simulation
            Color cardColor;
            Role roleToPlay;
            if (player==0){
                roleToPlay = currentDeck.get(rdm.nextInt(currentDeck.size()));
                cardColor=playerColor;
            }else{
                roleToPlay = allCards.get(rdm.nextInt(allCards.size()));
                do{
                    cardColor=Color.values()[rdm.nextInt(nbPlayer)];
                }while(cardColor==playerColor);
            }
            int columnToPlay;
            do{
                columnToPlay = rdm.nextInt(simulation.size());
            }while(simulation.get(columnToPlay).getIsPlacable());

            simulation.get(columnToPlay).addInfluenceCard(new InfluenceCard(roleToPlay, cardColor, true));
            
            


            player=(player+1)%nbPlayer;
            timeSimulation++;
        }
        //Evalue la simulation
        int score=0;
        for(Columns column : state){
            HashMap<Color,Integer> leader = column.leaderboard();

            Color bestRank=null;
            int bestScore=0;
            for(Color rank:leader.keySet()){

                if(bestScore>leader.get(rank)){
                    bestRank=rank;
                    bestScore=leader.get(rank);
                }else if(score==leader.get(rank)){
                    Color found=null;
                    int i=0;
                    while (found!=rank){
                        found=column.getContent().get(i).getColor();
                        i++;
                    }
                    bestRank=found;
                }
            }
            if(bestRank==playerColor){score+=column.getOCard().getPoint();}  
        }
        backUp(score);
    }

    public void backUp(int num){
        numerator+=num;
        denominator++;
        if (parent!=null) parent.backUp(num);
    }

    public GameState duplicate(){
        return new GameState(parent,childs,numerator,denominator,state,action,currentHand,playerColor);
    }
}
