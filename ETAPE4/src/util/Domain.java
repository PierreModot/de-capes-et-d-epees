package util;

public enum Domain {
    Alc,	//Alchimie
    Agr,	//Agriculture
    Cbt,	//Combat
    Com,	//Commerce
    Mus,	//Musique
    Rel;	//Religion
}
