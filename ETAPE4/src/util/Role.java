package util;

public enum Role {
    Ro,	//Roi
    Re,	//Reine
    Ju,	//Juliette
    Al,	//Alchimiste
    Md,	//Maitre d'armes
    Se,	//Seigneur
    Ma, //Marchand
    Ca,	//Cardinal
    Tb, //Troubadour
    Ex,	//Explorateur
    As,	//Assassin
    Te, //Tempete
    Tr,	//Traitre 
    Ci,	//Cape d'invisibilite
    Tm,	//Les Trois Mousquetaires
    Mg,	//Magicien
    So,	//Sorciere
    Pr,	//Prince
    Ec,	//Ecuyer
    Er,	//Ermite
    Pg,	//Petit Geant
    Dr, //Dragon
    RO,	//Romeo
    Me,	//Mendiant
    SO;	//Sosie
}
