package cd;

import java.util.EnumMap;

import cd.DataController.screenList;
import gamePart.MP;
import javafx.scene.Node;
import tableInterfaces.AppTable;
import tableInterfaces.CountingOfPoints;
import tableInterfaces.EndGameTable;
import tableInterfaces.GameTable;
import tableInterfaces.Placement;
import tableInterfaces.Settings;
import tableInterfaces.TableMenu;

public class DialogueControllerTable {

	private AppTable mainIhm = null;
	private EnumMap<screenList, Node> screenLists = new EnumMap<>(screenList.class);
	private screenList vBack = screenList.TABLEMENU;
	private MP mp = new MP(true);

	/**
	 * 
	 * @param appTable
	 */
	public DialogueControllerTable(AppTable appTable) {
		this.mainIhm = appTable;
		mp.setControllerTable(this);
	}

	/**
	 * Definit la table d'IHM principale
	 * 
	 * @param app
	 */
	public void defineMainIhmTable(AppTable app) {
		mainIhm = app;
	}

	/**
	 * Sauvegarde l'ecran
	 * 
	 * @param s
	 * @param n
	 */
	public void saveScreen(screenList s, Node n) {
		screenLists.put(s, n);
	}

	/**
	 * Affiche l'ecran
	 * 
	 * @param s
	 */
	public void showScreen(screenList s) {
		if (screenLists.containsKey(s))
			mainIhm.showScreen(screenLists.get(s));
	}

	/**
	 * Modifie l'historique
	 * 
	 * @param s
	 */
	public void editHistory(screenList s) {
		vBack = s;
	}

	public AppTable getMainIhm() {
		return mainIhm;
	}

	public screenList getVBack() {
		return vBack;
	}

	public void setVBack(screenList vBack) {
		this.vBack = vBack;
	}

	public MP getMp() {
		return mp;
	}

	public void setIdp(int idp) {
		Placement placement = (Placement) mainIhm.getRoot().getChildren().get(1);
		placement.setIdp(idp);
	}

	public GameTable getGameTable() {
		return (GameTable) mainIhm.getRoot().getChildren().get(5);
	}
	
	public EndGameTable getEndGameTable() {
		return (EndGameTable) mainIhm.getRoot().getChildren().get(6);
	}
	
	public Placement getPlacement() {
		return (Placement) mainIhm.getRoot().getChildren().get(1);
	}
	
	public CountingOfPoints getCountingOfPoints() {
		return (CountingOfPoints) mainIhm.getRoot().getChildren().get(2);
	}
	
	public TableMenu getTableMenu() {
		return (TableMenu) mainIhm.getRoot().getChildren().get(0);
	}
	
	public Settings getSettings() {
        return (Settings) mainIhm.getRoot().getChildren().get(4);
    }
	
	public void resetPlacement() {
		
		Placement placement = new Placement(this);
		CountingOfPoints counting = new CountingOfPoints(this);
		GameTable game = new GameTable(this);
		EndGameTable endgame = new EndGameTable(this);
		
		mainIhm.getRoot().getChildren().set(1,placement);
		screenLists.replace(screenList.PLACEMENT, placement);
		
		mainIhm.getRoot().getChildren().set(2,counting);
		screenLists.replace(screenList.COUNTINGOFPOINTS, counting);
		
		mainIhm.getRoot().getChildren().set(6,endgame);
		screenLists.replace(screenList.ENDGAMETABLE, endgame);
		
		mainIhm.getRoot().getChildren().set(5,game);
		screenLists.replace(screenList.GAMETABLE, game);
	}

}
