package cd;

import java.util.Locale;

//import java.util.Locale;

public class DataController {

	// Les différentes valeurs/interfaces que peut prendre EcranApplication
	// Si on souhaite rajouter une interface dans notre jeu, il faudra donc y
	// rajouter ici le nom de cette interface
	public enum screenList {
		JOINGAME, GAME, HANDCARD, TABLEMENU, SETTINGS, RULES, COMPUTERMENU, CARDDETAILS, PLAY, GAMETABLE, PLACEMENT,
		COUNTINGOFPOINTS, ENDGAMECOMPUTER, LASTCARD, COMPUTERRULES, COMPUTERSETTINGS, PLAYINGCARD, ENDGAMETABLE,
		WAITING, GAMEGOALRULETABLE, MATERIALRULETABLE, UNFOLDINGRULETABLE, ENDROUNDRULETABLE, ENDGAMERULETABLE,
		DIFFERENTCARDRULETABLE, DIFFERENTCARDRULETABLE2, GAMEGOALRULE, MATERIALRULE, UNFOLDINGRULE, ENDROUNDRULE, ENDGAMERULE,
		DIFFERENTCARDRULE, DENYDEMAND, DIFFERENTCARDRULETABLE3, DIFFERENTCARDRULE2, DIFFERENTCARDRULE3
	}
// chemin vers les images
/// CARTES INFLUENCE NORMALES
	
	public static final String imagePathIC = "ressources/InfluenceCards.jpg";
	public static final String imagePathICB = "ressources/Influence_Cards_Blue.png";
	public static final String imagePathICG = "ressources/Influence_Cards_Green.png";
	public static final String imagePathICP = "ressources/Influence_Cards_Purple.png";
	public static final String imagePathICW = "ressources/Influence_Cards_White.png";
	public static final String imagePathICY = "ressources/Influence_Cards_Yellow.png";
	
/// CARTES INFLUENCE SPECIALES
	
	public static final String imagePathspeIC = "ressources/SpecialInfluenceCards.png";
	public static final String imagePathspeICB = "ressources/SpecialInfluenceCards_Blue.png";
	public static final String imagePathspeICP = "ressources/SpecialInfluenceCards_Purple.png";
	public static final String imagePathspeICR = "ressources/SpecialInfluenceCards_Green.png";
	public static final String imagePathspeICY = "ressources/SpecialInfluenceCards_Yellow.png";
	public static final String imagePathspeICG = "ressources/SpecialInfluenceCards_Green.png";
	
/// CARTES MELANGES NORMALES
	
	public static final String imagePathC = "ressources/Cards.jpg";
	public static final String imagePathCB = "ressources/Cards_Blue.png";
	public static final String imagePathCG = "ressources/Cards_Green.png";
	public static final String imagePathCP = "ressources/Cards_Purple.png";
	public static final String imagePathCW = "ressources/Cards_White.png";
	public static final String imagePathCY = "ressources/Cards_Yellow.png";
	
/// CARTES MELANGES SPECIALES
	
	public static final String imagePathspeC = "ressources/SpecialCards.png";
	public static final String imagePathspeCB = "ressources/SpecialCards_Blue.png";
	public static final String imagePathspeCG = "ressources/SpecialCards_Green.png";
	public static final String imagePathspeCP = "ressources/SpecialCards_Purple.png";
	public static final String imagePathspeCR = "ressources/SpecialCards_Green.png";
	public static final String imagePathspeCY = "ressources/SpecialCards_Yellow.png";
	
/// CARTES OBJECTIF NORMALES
	
	public static final String imagePathOC = "ressources/ObjectifCards.jpg";
	
/// CARTES OBJECTIF SPECIALES
	
	public static final String imagePathspeOC = "ressources/ObjectifCardsSpe.png";

	
	public static final int volumeEffect = 40;
	public static final int volumeMusique = /*20*/0;
	public static final String music = "src\\ressources\\Music\\medievalSong.mp3";
	public static final String buttonEffect = "src\\ressources\\Music\\buttonEffect.wav";
	
	public static final Locale localeEN = new Locale("en", "UK");
	public static final Locale localeFR = new Locale("fr", "FR");
	public static final Locale localeES = new Locale("es", "ES");
	public static final String urlFichiersTrad = "Ressources.Textes.messages";
	
	public static boolean theme = false;


}
