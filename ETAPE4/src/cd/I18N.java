package cd;

import java.util.Locale;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/*
 * Cette classe a été inspirée de la l'exemple suivant :
 * https://javafxpedia.com/en/tutorial/5434/internationalization-in-javafx
 */

public class I18N {

    public enum AppLanguage {
        FRANCAIS,
        ANGLAIS,
        ESPAGNOL; 
    }
    
    public enum AppTheme {
       LIGHT,
       DARK;
    }

    public enum AppCard {
        CLASSIQUE,
        SPECIAL; 
    }
    
    private static ObjectProperty<Locale> currentLocal;

    private static Locale localFR = new Locale("fr", "FR");
    private static Locale localEN = new Locale("en", "US");
    private static Locale localES = new Locale("es", "ES");

    private static Locale localSP = new Locale("sp", "SP");
    private static Locale localCL = new Locale("cl", "cl");
    
    private static Locale localDK = new Locale("dk", "DK");
    private static Locale localLG = new Locale("lg", "LG");
    
    private static String baseNameBundle = "ressources/Languages/messages";
    
    static {
        
        currentLocal = new SimpleObjectProperty<>(Locale.getDefault()); 
        currentLocal.addListener((observable, oldValue, newValue) -> Locale.setDefault(newValue));
    }

    public static String get(String key) {
        
        ResourceBundle res = ResourceBundle.getBundle(baseNameBundle, currentLocal.get());
        if(res.containsKey(key))
            return res.getString(key);
        return "";
    }

    public static StringBinding createStringBinding(String key) {
        return Bindings.createStringBinding(() -> get(key), currentLocal);
    }

    public static void setLocal(AppLanguage AppLanguages) {
        currentLocal.set(obtenirLocal(AppLanguages));
    }
    
    private static Locale obtenirLocal(AppLanguage AppLanguages) {
        switch (AppLanguages) {
        case FRANCAIS:
            return localFR;
        case ANGLAIS :
            return localEN; 
        case ESPAGNOL :
            return localES;
        default:
            return localFR;
        }
    }
   
    public static void setLocal(AppCard classique) {
        currentLocal.set(obtenirLocal(classique));
    }
    private static Locale obtenirLocal(AppCard AppCard) {
        switch (AppCard) {
        case CLASSIQUE:
            return localCL;
        case SPECIAL :
            return localSP; 
        default:
            return localCL;
        }
    }
    
    public static void setLocal(AppTheme AppTheme) {
        currentLocal.set(obtenirLocal(AppTheme));
    }
    
    private static Locale obtenirLocal(AppTheme AppTheme) {
        switch (AppTheme) {
        case DARK:
            return localDK;
        case LIGHT :
            return localLG; 
        default:
            return localLG;
        }
    }
}
