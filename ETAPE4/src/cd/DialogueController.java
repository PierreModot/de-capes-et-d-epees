package cd;

import java.util.EnumMap;

import card.InfluenceCard;
import cd.DataController.screenList;
import computerInterfaces.App;
import computerInterfaces.CardDetails;
import computerInterfaces.ComputerSettings;
import computerInterfaces.EndGameComputer;
import computerInterfaces.HandCard;
import gamePart.MP;
import javafx.scene.Node;

public class DialogueController {

	private App mainIhm = null;
	private EnumMap<screenList, Node> screenLists = new EnumMap<>(screenList.class);
	private screenList vBack = screenList.COMPUTERMENU;
	private MP mp = new MP(false);

	public MP getMp() {
		return mp;
	}

	public void setMp(MP mp) {
		this.mp = mp;
	}

	/**
	 * 
	 * @param app
	 */
	public DialogueController(App app) {
		this.mainIhm = app;
		mp.setControllerPlayer(this);
	}

	/**
	 * Cree une methode pour refresh les cartes
	 * 
	 * @param list
	 */
	public void printCard(InfluenceCard card) {
		getHandCard().changeACard(card, 0);
	}

	/**
	 * Definie l'interface principale
	 * 
	 * @param ihm
	 */
	public void defindMainIHM(App ihm) {
		mainIhm = ihm;
	}

	/**
	 * Sauvegarde l'ecran
	 * 
	 * @param s
	 * @param n
	 */
	public void saveScreen(screenList s, Node n) {
		screenLists.put(s, n);
	}

	/**
	 * Affiche l'ecran
	 * 
	 * @param s
	 */
	public void showScreen(screenList s) {
		if (screenLists.containsKey(s))
			mainIhm.showScreen(screenLists.get(s));
	}

	/**
	 * Modifie l'historique
	 * 
	 * @param s
	 */
	public void editHistory(screenList s) {
		vBack = s;
	}

	public App getMainIhm() {
		return mainIhm;
	}

	public screenList getVBack() {
		return vBack;
	}

	public void setVBack(screenList vBack) {
		this.vBack = vBack;
	}
	
	public HandCard getHandCard() {
		return (HandCard) mainIhm.getRoot().getChildren().get(2);
	}

	public ComputerSettings getComputerSettings() {
        return (ComputerSettings) mainIhm.getRoot().getChildren().get(7);
    }
	
	public EndGameComputer getEndGameComputer() {
        return (EndGameComputer) mainIhm.getRoot().getChildren().get(4);
    }
	
	public CardDetails getCardDetails() {
		return (CardDetails) mainIhm.getRoot().getChildren().get(3);
	}
	
}
