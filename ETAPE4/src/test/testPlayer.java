package test;

import gamePart.MP;
import util.Color;
import util.Level;

public class testPlayer {

	public static void main(String[] argv) {

		int delay = 200;

		MP mainProgram = new MP(true);
		MP.javafx = false;

		try {
			Thread.sleep(delay);
			mainProgram.createGame(2);
			Thread.sleep(delay);
			mainProgram.createBot("BOT 1", Level.Facile, Color.Bla, 1);
			Thread.sleep(delay);
			mainProgram.createBot("BOT 2", Level.Facile, Color.Ble, 2);
			Thread.sleep(delay);
			System.out.println("-> Lancement de la partie " + mainProgram.getGame().getId());
			Thread.sleep(delay);
			mainProgram.initGame("");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
