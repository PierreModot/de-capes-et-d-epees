package test;

import gamePart.MP;

public class testTable {
	
	public static void main(String[] argv) {
		MP mainProgram = new MP(true);
		
		mainProgram.getGame().setNumPlayer(5);
		mainProgram.getGame().setNumHuman(3);
		mainProgram.getGame().setNumBot(2);
		
		mainProgram.getGame().setId(8);
		
		try {
			Thread.sleep(5000);
			mainProgram.createGame(5);
			mainProgram.updateGame();
			mainProgram.initGame("");
			mainProgram.send3Cards();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
