package gamePart;

import util.Role;

import java.util.ArrayList;
import java.util.HashMap;

import util.Color;
import util.Domain;
import card.*;

public class Columns {

	/*-------------Attributs-----------------*/

	private static int counter = 1; // compteur pour pouvoir identifier les colonnes � partir de l'int emplacement

	private final int location;
	private boolean state; // Si la carte Objectif de la colonne courant est realise ou non
	private ArrayList<InfluenceCard> content; // Le contenu de la colonne courante
	private ObjectiveCard oCard; // Carte Objectif de la colonne courante
	private boolean isPlacable;

	/*-------------Constructeurs-----------------*/

	/**
	 * constructeur
	 * 
	 * @param oCard
	 */
	public Columns() {

		this.location = counter;

		this.state = false;
		this.content = new ArrayList<>();
		this.isPlacable = true;

		counter++;
	}

	/*-------------Getters/Setters-----------------*/

	public boolean getIsRealized() {
		return state;
	}

	public void setIsRealized(boolean isRealized) {
		this.state = isRealized;
	}

	public ArrayList<InfluenceCard> getContent() {
		return content;
	}

	public void setContent(ArrayList<InfluenceCard> content) {
		this.content = content;
	}

	public ObjectiveCard getOCard() {
		return oCard;
	}

	public void setOCard(ObjectiveCard oCard) {
		this.oCard = oCard;
	}

	public void setIsPlacable(boolean isPlacable) {
		this.isPlacable = isPlacable;
	}

	public boolean getIsPlacable() {
		return isPlacable;
	}
	
	

	/*-------------Methodes-----------------*/

	/**
	 * ajouter une carte et change le statut de la colonne de la colonne si elle
	 * nest plus utilisable
	 * 
	 * @param InfluenceCard
	 *
	 */

	public void addInfluenceCard(InfluenceCard InfluenceCard) {
		content.add(InfluenceCard);

		if (content.size() == oCard.getPoint()) {
			state = true;
		}

	}
	
	/**
	 * permet un classemet de chaque couleur qui
	 */
	public HashMap<Color,Integer> leaderboard(){return leaderboard(new InfluenceCard());}
	/**
	 * permet un classemet de chaque couleur qui
	 */
	public HashMap<Color,Integer> leaderboard(InfluenceCard cardSup){
		HashMap<Color,Integer> leaderboardFinal = new HashMap<>();
		HashMap<Color,Integer> leaderboard = new HashMap<>();
		//detection des cartes a effet de groupe
		Boolean mousquetaire = false;
		Boolean mendiant = false;
		int magicien =0;
		int sorciere =0;
		ArrayList<Color> dragon = new ArrayList<>();
		ArrayList<Color> RomeoJuliette = new ArrayList<>();
		HashMap<Color,Integer> EcuyerPrince = new HashMap<>();

		for(int i=0;i<=content.size();i++){// On recupere les cartes qui ont un effet a la fin de la manche, sauf le sosie
			InfluenceCard card;
			if(i==content.size()) card=cardSup;
			else card = content.get(i-1);
			
			if(cardSup.getColor()==null)card=null;
			if(card!=null) {
				switch(card.getRole()){
					case Tm:
						mousquetaire=true;
					break;
					case Dr:
						dragon.add(card.getColor());
					break;
					case Ju:
						RomeoJuliette.add(card.getColor());
					break;
					case Pr:
					case Ec:
						if(EcuyerPrince.containsKey(card.getColor()))EcuyerPrince.put(card.getColor(), i);
						else EcuyerPrince.put(card.getColor(), 0);
					break;
					case Mg:
						magicien++;
					break;
					case So:
						sorciere++;
					break;
					case Me:
						mendiant=true;
					break;
					default: 
					break;
				}
				
				
			}
			
		}
		Color winnerColor=null;//Variable qui indique une victoire absolue dans une colonne (Ecuyer+Prince)

		if(!mousquetaire){//Application des effets
			
			
			if(EcuyerPrince.size()!=0 && magicien!=1 && sorciere!=1){
				
				int bestposition = 0;
				for (Color color : Color.values()){
					if(EcuyerPrince.containsKey(color) && EcuyerPrince.get(color)!=0){
						if(bestposition>EcuyerPrince.get(color)){
							winnerColor=color;
							bestposition=EcuyerPrince.get(color);
						}
					}
				}
				if (winnerColor!=null)leaderboard.put(winnerColor, 100);
			}
			for(int i=0;i<=content.size();i++){
				InfluenceCard card;
				if(i==content.size()) card=cardSup;
				else card = content.get(i);
				int value=0;
				if(card==cardSup && cardSup.getColor()==null)card=null;
				if(card!=null) {
					switch(card.getRole()){
						case Al:
						if(this.getOCard().getDomain().equals(Domain.Alc)) value=12;
						else value=8;
						break;
						case Md:
							if(this.getOCard().getDomain().equals(Domain.Com)) value=12;
							else value=8;
						break;
						case Se:
							if(this.getOCard().getDomain().equals(Domain.Cbt)) value=12;
							else value=8;
						break;
						case Ma:
							if(this.getOCard().getDomain().equals(Domain.Com)) value=12;
							else value=8;
						break;
						case Ca:
							if(this.getOCard().getDomain().equals(Domain.Com)) value=12;
							else value=8;
						break;
						case Tb:
							if(this.getOCard().getDomain().equals(Domain.Mus)) value=12;
							else value=8;
						break;
						case Er:
							value=12-content.size()-1;
						break;
						case Pg:
							value=2+content.size()*3;
						break;
						case RO:
							if(RomeoJuliette.contains(card.getColor())) value=15;
							else value = 5;
						break;
						case Ex:
							if(i<content.size()){if (content.get(i+1)!=null) value=0;}
							else value = 13;
						break;
						default:
							if (card!=null) value=(int) card.getValue();
						break;
					}
				
				
				

					//annulation de valeur
					if(value>=10 && magicien==1) value=0;
					if(value<=9 && sorciere==1 && card.getRole()!=Role.So)value=0;
	
					//malus dragon
					value-=2*dragon.size();
					if(dragon.contains(card.getColor()))value+=2;
					if(card.getRole()==Role.Dr)value+=2;
	
					//Mis a jour de la valeur du sosie de la case precedente
					if (i>0 && content.get(i-1).getRole()==Role.SO){
						 if(leaderboard.containsKey(content.get(i-1).getColor())) value+=leaderboard.get(content.get(i-1).getColor());
							leaderboard.put(content.get(i-1).getColor(),value);
					}
				

					//Ajout de la valeur au score du joueur
				
					if(value<0)value=0;
						if(leaderboard.containsKey(card.getColor())) value+=leaderboard.get(card.getColor());
						leaderboard.put(card.getColor(),value);
				}
				
			}
			

		}else{//Calcul sans effet
			for(int i=0;i<=content.size();i++){
				InfluenceCard card;
				if(i==content.size()) 
					if(cardSup.getColor()!=null) {card=cardSup;} else{card=null;}
				else card = content.get(i);
				int value=0;
				switch(card.getRole()){
					case Al:
					if(this.getOCard().getDomain().equals(Domain.Alc)) value=12;
					else value=8;
				break;
				case Md:
					if(this.getOCard().getDomain().equals(Domain.Com)) value=12;
					else value=8;
				break;
				case Se:
					if(this.getOCard().getDomain().equals(Domain.Cbt)) value=12;
					else value=8;
				break;
				case Ma:
					if(this.getOCard().getDomain().equals(Domain.Com)) value=12;
					else value=8;
				break;
				case Ca:
					if(this.getOCard().getDomain().equals(Domain.Com)) value=12;
					else value=8;
				break;
				case Tb:
					if(this.getOCard().getDomain().equals(Domain.Mus)) value=12;
					else value=8;
				break;
				default:
					if (card!=null) value=(int) card.getValue();
				}

				if(value<0)value=0;
				if(leaderboard.containsKey(card.getColor())) value+=leaderboard.get(card.getColor());
				leaderboard.put(card.getColor(),value);
			}
		}
		
		if(mendiant) {
			for(Color color : leaderboard.keySet()) {
				leaderboard.put(color, -leaderboard.get(color));
			}
		}

		//Creation du leaderboard
		for(int i=0;i<leaderboard.size();i++){
			Color best= null;
			int max=0;
			for(Color paint : Color.values()){
				
				if (leaderboard.containsKey(paint) && leaderboard.get(paint)>max){
					best=paint;
					max=leaderboard.get(paint);
				}   
			}
			if(i==0 && winnerColor!=null)
			if(best != winnerColor){leaderboardFinal.put(best,leaderboard.get(best));}
			leaderboard.remove(best);
		}
		
		return leaderboardFinal;
	}


}
