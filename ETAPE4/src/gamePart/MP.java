package gamePart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import card.InfluenceCard;
import card.ObjectiveCard;
import cd.DataController.screenList;
import cd.DialogueController;
import cd.DialogueControllerTable;
import network.Network;
import network.Packet;
import player.Bot;
import player.Player;
import util.Color;
import util.Level;
import util.Status;

public class MP {

	private boolean table;
	private Network network;
	private Game game;

	private DialogueController controllerPlayer;
	private DialogueControllerTable controllerTable;

	private HashMap<String, MP> bots = new HashMap<>();
	private Bot bot;

	public static boolean javafx = true;

	public MP(boolean table) {
		this.table = table;
		game = new Game(this);
		this.network = new Network(this, table);
	}

	public boolean isTable() {
		return table;
	}

	public Network getNetwork() {
		return network;
	}

	public Game getGame() {
		return game;
	}

	public void createGame(int nbPlayer) {
		game = new Game(this);
		controllerTable.resetPlacement();

		game.initGame();
		network.initTable();

		game.setNumPlayer(nbPlayer);
		game.setNumBot(nbPlayer);
		game.setNumHuman(nbPlayer);

		Packet packet = new Packet();

		packet.add("ACP");
		packet.add("P" + game.getId());
		packet.add(network.getServer().getIp());
		packet.add(network.getServer().getPort());
		packet.add(game.getName());
		packet.add(game.getNumPlayer());
		packet.add(game.getNumHuman());
		packet.add(game.getNumBot());
		packet.add(game.getStatus().name());

		network.sendUDP(packet);
		network.getServer().search();
	}

	public void updateGame() {
		Packet packet = new Packet();

		packet.add("AMP");
		packet.add("P" + game.getId());
		packet.add(network.getServer().getIp());
		packet.add(network.getServer().getPort());
		packet.add(game.getName());
		packet.add(game.getNumPlayer());
		packet.add(game.getNumHuman());
		packet.add(game.getNumBot());
		packet.add(game.getNumHumanConnected());
		packet.add(game.getNumBotConnected());
		packet.add(game.getStatus().name());

		network.sendUDP(packet);
	}

	public void joinGame(String pseudo, String type, int gameId) {
		network.connectClient(gameId);

		Packet packet = new Packet();

		packet.add("DCP");
		packet.add(pseudo);
		packet.add(type);
		packet.add(gameId);

		network.sendTCP(packet);
	}

	public void denyPlayer(int gameId, Player player) {
		Packet packet = new Packet();

		packet.add("RCP");
		packet.add(gameId);

		network.sendTCP(packet, player.getId());
	}

	public void acceptPlayer(Player player) {
		Packet packet = new Packet();

		packet.add("ADP");
		packet.add(game.getId());
		packet.add(player.getId());

		network.sendTCP(packet, player.getId());
	}

	public void disconnectPlayer(Player player) {
		Packet packet = new Packet();

		packet.add("ADJ");
		packet.add(game.getId());

		network.sendTCP(packet, player.getId());
	}

	public void initGame(String start) {
		if (game.getNumPlayer() == game.getNumBotConnected() + game.getNumHumanConnected()) {
			if (MP.javafx) {
				getControllerTable().showScreen(screenList.GAMETABLE);
				getControllerTable().getGameTable().setColumns(game.getNumPlayer());
			}

			game.setStatus(Status.COMPLETE);
			updateGame();

			Packet packet = new Packet();

			packet.add("ILP");

			String players = "";
			String colors = "";

			boolean find = false;
			
			game.sortPlayers();

			for (int i = 0; i < game.getPlayers().size(); i++) {
				Player player = game.getPlayers().get(i);

				if (player.getPseudo().equals(start)) {
					game.getPlayers().remove(i);
					game.getPlayers().add(0, player);
					find = true;
				}
			}

			if (!find) {
				Random rdm = new Random();

				Player player = game.getPlayers().get(rdm.nextInt(game.getPlayers().size() - 1));
				game.getPlayers().remove(player);
				game.getPlayers().add(0, player);
			}

			for (Player player : game.getPlayers()) {
				players += player.getPseudo() + ",";
				colors += player.getColor().name() + ",";
				if (MP.javafx)
					getControllerTable().getGameTable().showDeckAndDiscard(true, false, player.getPseudo(),
							player.getColor());
			}

			packet.add(players.substring(0, players.length() - 1));
			packet.add(colors.substring(0, colors.length() - 1));
			packet.add(game.getId());

			game.sendAll(packet);

			send3Cards();
			initRound();
		}
	}

	public void send3Cards() {

		for (Player player : game.getPlayers()) {

			player.initDeck();

			player.drawCard();
			player.drawCard();
			player.drawCard();

			Packet packet = new Packet();

			packet.add("RTC");

			String card = "";

			for (InfluenceCard iC : player.getHand())
				card += iC.encode() + ",";

			packet.add(card.substring(0, card.length() - 1));
			packet.add(game.getId());

			network.sendTCP(packet, player.getId());

		}
	}

	public void initRound() {
		for (Columns column : game.getColumns()) {
			for (InfluenceCard card : column.getContent())
				card.setExecuted(false);
		}
		game.createColumns();

		if (MP.javafx)
			getControllerTable().getGameTable().clearAllCards();
		game.initDeckObjective();
		game.setCurrentPlayer(game.getPlayers().get(0));

		sendObjectiveCard();
		try {
			Thread.sleep(100);
			initTurn();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sendObjectiveCard() {
		Packet packet = new Packet();

		packet.add("ILM");

		String objectivecards = "";

		for (int i = 0; i < game.getNumPlayer(); i++) {
			ObjectiveCard card = game.getObjectiveCards().get(i);
			String code = card.encode();
			if (objectivecards.indexOf(code) >= 0) {
				if (objectivecards.indexOf(code + "A") >= 0)
					code += "B";
				else
					code += "A";
			}
			if (MP.javafx)
				getControllerTable().getGameTable().addObjectiveCard(card);
			game.getColumns().get(i).setOCard(card);
			objectivecards += code + ",";
		}

		packet.add(objectivecards.substring(0, objectivecards.length() - 1));
		packet.add(game.getId());
		packet.add(game.getRound());

		game.sendAll(packet);
	}

	public void initTurn() {
		Packet packet = new Packet();

		packet.add("IDT");
		packet.add(game.getCurrentPlayer().getColor().name());
		packet.add(game.getId());
		packet.add(game.getRound());

		game.sendAll(packet);

	}

	public void playInfluenceCard(InfluenceCard iC, int column) {
		for (int i = 0; i < game.getPlayer().getHand().size(); i++) {
			InfluenceCard iChand = game.getPlayer().getHand().get(i);

			if (iChand.getRole().equals(iC.getRole())) {
				Packet packet = new Packet();

				packet.add("JCI");
				packet.add(iC.encode());
				packet.add(column);
				packet.add(game.getId());
				packet.add(game.getRound());
				packet.add(game.getPlayer().getId());

				network.sendTCP(packet);
				game.setLastCardPlayed(i);
			}
		}
	}

	public void informCardPlayed(int column, String effect) {
		Packet packet = new Packet();

		packet.add("ICJ");
		packet.add(game.getCurrentPlayer().getColor().name());
		packet.add(column);
		packet.add(effect);
		packet.add(game.getId());
		packet.add(game.getRound());

		game.sendAll(packet);
	}

	public void fillPlayerHand(InfluenceCard card, Player player) {
		Packet packet = new Packet();

		packet.add("RMJ");
		packet.add(card.encode());
		packet.add(game.getId());
		packet.add(game.getRound());

		network.sendTCP(packet, player.getId());
	}

	public void updateDiscard(Player player) {
		Packet packet = new Packet();

		packet.add("RRJ");
		packet.add(game.getCurrentPlayer().getColor().name());
		packet.add(game.getId());
		packet.add(game.getRound());

		game.sendAll(packet);
	}

	public void endRound(int round) {
		try {

			game.setRound(game.getRound() + 1);
			Packet packet = new Packet();

			packet.add("FDM");
			packet.add("NUL");
			packet.add(game.getId());
			packet.add(game.getRound());

			game.sendAll(packet);

			for (int i = 0; i < game.getColumns().size(); i++) {
				Player player = game.getColumnWinner(i);

				ArrayList<Integer> Me = new ArrayList<>();
				Player winnerEffect = game.applyEffect(i, Me);

				if (Me.size() > 0)
					player = game.getMeWinner(i);

				if (winnerEffect != null)
					winnerEffect.getInventory().add(game.getColumns().get(i).getOCard());

				else if (player != null)
					player.getInventory().add(game.getColumns().get(i).getOCard());

			}

			for (Columns column : game.getColumns()) {
				for (InfluenceCard card : column.getContent()) {
					for (Player player : game.getPlayers()) {
						if (player.getColor().equals(card.getColor()))
							player.addDiscard(card);
					}
				}
			}

			Thread.sleep(1000);

			if (MP.javafx) {
				this.getControllerTable().getGameTable().newRound(round);
				Thread.sleep(1000);
				this.controllerTable.getGameTable().showGame();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void createBot(String pseudo, Level difficulty, Color color, int priority) {
		MP botProgram = new MP(false);
		bots.put(pseudo, botProgram);

		Bot bot = new Bot(difficulty);
		bot.setColor(color);
		bot.setId("J" + network.getServer().getPid());

		botProgram.setBot(bot);
		botProgram.joinGame(pseudo, "BOT", game.getId());
		botProgram.getNetwork().setServerList(network.getServerList());
		bot.setPriority(priority);
	}

	public void setControllerPlayer(DialogueController controllerPlayer) {
		this.controllerPlayer = controllerPlayer;
	}

	public void setControllerTable(DialogueControllerTable controllerTable) {
		this.controllerTable = controllerTable;
	}

	public DialogueController getControllerPlayer() {
		return controllerPlayer;
	}

	public DialogueControllerTable getControllerTable() {
		return controllerTable;
	}

	public Bot getBot() {
		return bot;
	}

	public void setBot(Bot bot) {
		this.bot = bot;
	}

	public HashMap<String, MP> getBots() {
		return bots;
	}

	public void allowPlayer(String pseudo) {
		for (int i = 0; i < game.getUserRequest().size(); i++) {
			Player player = game.getUserRequest().get(i);
			if (player.getPseudo().equals(pseudo)) {
				acceptPlayer(player);
				game.getPlayers().add(player);

				player.setAccepted(true);
				player.setPriority(game.getPlayers().size());

				updateGame();

				if (!player.getType().equals("BOT") && MP.javafx)
					getControllerTable().getPlacement().newUnplacedPlayer(player.getPseudo());

				game.getUserRequest().remove(i);

				if (!player.getType().equals("BOT"))
					game.setNumHumanConnected(game.getNumHumanConnected() + 1);
			}
		}
	}

	public void denyPlayer(String pseudo) {
		for (int i = 0; i < game.getUserRequest().size(); i++) {
			Player player = game.getUserRequest().get(i);
			if (player.getPseudo() == pseudo) {
				this.denyPlayer(game.getId(), player);
				game.getUserRequest().remove(i);
			}
		}
	}

	public void sendEndGame(ArrayList<Player> leadboard) {

		Player winner = leadboard.get(0);

		if (winner != null) {
			Packet packet = new Packet();
			packet.add("FDP");
			packet.add(winner.getColor().name());

			String players = "";
			String scores = "";

			for (Player player : leadboard) {
				players += player.getPseudo() + ",";
				scores += player.getScore() + ",";
			}

			packet.add(players.substring(0, players.length() - 1));
			packet.add(scores.substring(0, scores.length() - 1));
			packet.add(game.getId());

			for (Player player : leadboard) {
				network.sendTCP(packet, player.getId());
			}
		}

	}

	public void askColumnTr(Player player, int column) {
		game.setPause(true);
		game.getTrCard().put(player.getId(), column);

		Packet packet = new Packet();
		packet.add("ECT");
		packet.add(game.getColumns().get(column).getOCard().encode());
		packet.add(column);
		packet.add(game.getId());
		packet.add(game.getRound());

		network.sendTCP(packet, player.getId());
	}

	public void setColumnTr(int column) {
		Packet packet = new Packet();

		packet.add("JCT");
		packet.add(column);
		packet.add(game.getId());
		packet.add(game.getRound());
		packet.add(game.getPlayer().getId());

		network.sendTCP(packet);
	}

	public void askColumnCa(Player player, int column) {
		game.setPause(true);
		game.getCaCard().put(player.getId(), column);

		Packet packet = new Packet();
		packet.add("CCI");
		packet.add(column);
		packet.add(game.getId());
		packet.add(game.getRound());

		network.sendTCP(packet, player.getId());
	}

	public void setColumnCa(InfluenceCard card) {
		Packet packet = new Packet();

		packet.add("JCC");
		packet.add(card.encode());
		packet.add(game.getId());
		packet.add(game.getRound());
		packet.add(game.getPlayer().getId());

		network.sendTCP(packet);
		game.setLastCardPlayed(game.getPlayer().getIdCardFromHand(card));
	}

	public void installPlayer(String pseudo, String color, int position) {
		game.setColor(pseudo, color);
		Player player = game.getPlayerByColor(game.getColor(color));
		
		if(player != null)
			player.setPriority(position);

		if (game.getNumHumanConnected() + game.getNumBotConnected() >= game.getNumPlayer() && MP.javafx)
			getControllerTable().getPlacement().showLaunchButton();
	}

}
