package gamePart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import card.InfluenceCard;
import card.ObjectiveCard;
import cd.DataController.screenList;
import network.Packet;
import player.Player;
import util.Color;
import util.Domain;
import util.Role;
import util.Status;

public class Game {

	private static HashMap<util.Color, HashMap<util.Role, InfluenceCard>> allCards;
	private static HashMap<util.Color, HashMap<util.Role, InfluenceCard>> allCardsSpe;

	private static HashMap<util.Domain, ObjectiveCard> objCard;
	private static HashMap<util.Domain, ObjectiveCard> objSpeCard;

	private MP mainProgram;
	private String name;
	private int id = -1;

	private int numPlayer = 0;
	private int numHuman = 0;
	private int numBot = 0;
	private int numHumanConnected = 0;
	private int numBotConnected = 0;

	private Status status;
	private Player player;

	private ArrayList<Player> players = new ArrayList<>();
	private ArrayList<ObjectiveCard> objectiveCards = new ArrayList<>();
	private ArrayList<Columns> columns = new ArrayList<>();

	private int round = 1;
	private Player currentPlayer;

	private int lastCardPlayed;
	private ArrayList<Player> userRequest = new ArrayList<>();
	private HashMap<String, Integer> trCard = new HashMap<>();
	private HashMap<String, Integer> caCard = new HashMap<>();

	private boolean pause = false;

	boolean wait = false;

	public Game(MP mainProgram) {
		this.mainProgram = mainProgram;
		initGame();
		status = Status.ATTENTE;
		player = new Player();
		allCards = setAllCards();
		objCard = setObjCard();
	}

	public void initGame() {
		id = (int) (Math.random() * 1000);
		name = "Game_" + id;
	}

	public static HashMap<Color, HashMap<Role, InfluenceCard>> setAllCards() {
		HashMap<util.Color, HashMap<util.Role, InfluenceCard>> _allCards = new HashMap<>();
		for (util.Color color : util.Color.values()) {
			HashMap<Role, InfluenceCard> hash2 = new HashMap<Role, InfluenceCard>();
			for (util.Role role : util.Role.values()) {
				InfluenceCard c = new InfluenceCard(role, color, false);
				hash2.put(role, c);
			}
			_allCards.put(color, hash2);
		}
		return _allCards;
	}

	public static HashMap<util.Domain, ObjectiveCard> getObjCard() {
		return objCard;
	}

	public static HashMap<util.Domain, ObjectiveCard> setObjCard() {
		HashMap<util.Domain, ObjectiveCard> _objCard = new HashMap<>();
		for (util.Domain domain : util.Domain.values()) {
			for (int i = 1; i < 6; i++) {
				_objCard.put(domain, new ObjectiveCard(domain, i));
				if (i == 3) {
					_objCard.put(domain, new ObjectiveCard(domain, i));
				}
			}
		}
		return _objCard;
	}

	public static HashMap<util.Domain, ObjectiveCard> getObjSpeCard() {
		return objSpeCard;
	}

	public int getId() {
		return id;
	}

	public static HashMap<Color, HashMap<Role, InfluenceCard>> getAllsCard() {
		return allCards;
	}

	public static HashMap<Color, HashMap<Role, InfluenceCard>> getAllsCardSpe() {
		return allCardsSpe;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;

	}

	public int getNumPlayer() {
		return numPlayer;
	}

	public void setNumPlayer(int numPlayer) {
		this.numPlayer = numPlayer;
	}

	public int getNumHuman() {
		return numHuman;
	}

	public void setNumHuman(int numHuman) {
		this.numHuman = numHuman;
	}

	public int getNumBot() {
		return numBot;
	}

	public void setNumBot(int numBot) {
		this.numBot = numBot;
	}

	public boolean isWait() {
		return wait;
	}

	public void setWait(boolean wait) {
		this.wait = wait;
	}

	public int getNumHumanConnected() {
		return numHumanConnected;
	}

	public void setNumHumanConnected(int numHumanConnected) {
		this.numHumanConnected = numHumanConnected;
	}

	public int getNumBotConnected() {
		return numBotConnected;
	}

	public void setNumBotConnected(int numBotConnected) {
		this.numBotConnected = numBotConnected;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public ArrayList<Columns> getColumns() {
		return columns;
	}

	public void createColumns() {
		columns = new ArrayList<>();
		for (int i = 0; i < players.size(); i++)
			columns.add(new Columns());
	}

	public void sendAll(Packet packet) {
		String message = packet.encodeMessage();
		packet.clearMessage();

		for (Player player : players)
			mainProgram.getNetwork().sendTCPMessage(message, player.getId());

	}

	public void initDeckObjective() {

		for (Domain domain : Domain.values()) {
			for (int i = numPlayer == 2 ? 2 : 1; i < 6; i++) {
				objectiveCards.add(new ObjectiveCard(domain, i));
			}

			objectiveCards.add(new ObjectiveCard(domain, 3));
		}

		Collections.shuffle(objectiveCards);

		int tour = numPlayer == 2 ? 30 - numPlayer * 6 : 36 - numPlayer * 6;

		for (int i = 0; i < tour; i++)
			objectiveCards.remove(objectiveCards.size() - 1);

	}

	public ArrayList<ObjectiveCard> getObjectiveCards() {
		return objectiveCards;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public Player getPlayerById(String id) {
		for (Player player : players) {
			if (player.getId().equals(id))
				return player;
		}
		return null;
	}

	public InfluenceCard getCardFlipped(int column) {
		return getEndCard(column - 1, 2);
	}

	public void setColor(String pseudo, String clr) {
		Color color = getColor(clr);

		if (color != null) {
			for (Player player : players) {
				if (player.getPseudo().equals(pseudo))
					player.setColor(color);
			}
		}
	}

	public Color getColor(String clr) {
		Color color = null;

		if (clr.equals("#9370db"))
			color = Color.Vio;
		else if (clr.equals("#ffd700"))
			color = Color.Jau;
		else if (clr.equals("#f8f8ff"))
			color = Color.Bla;
		else if (clr.equals("#b22222"))
			color = Color.Rou;
		else if (clr.equals("#32cd32"))
			color = Color.Ver;
		else if (clr.equals("#1e90ff"))
			color = Color.Ble;
		return color;
	}

	public int getLastCardPlayed() {
		return lastCardPlayed;
	}

	public void setLastCardPlayed(int lastCardPlayed) {
		this.lastCardPlayed = lastCardPlayed;
	}

	public InfluenceCard getLastCard(int column) {
		return getEndCard(column, 1);
	}

	public InfluenceCard getEndCard(int column, int index) {
		if (index >= 0 && columns.get(column).getContent().size() - index >= 0
				&& index < columns.get(column).getContent().size())
			return columns.get(column).getContent().get(columns.get(column).getContent().size() - index - 1);

		return null;
	}

	public InfluenceCard getStartCard(int column, int index) {
		if (index < columns.get(column).getContent().size() && index >= 0)
			return columns.get(column).getContent().get(index);

		return null;
	}

	public Player getPlayerByColor(Color color) {
		for (Player player : players) {
			if (player.getColor().equals(color))
				return player;
		}
		return null;
	}

	/**
	 * Applique les effets des cartes a effet immédiat
	 * 
	 * @param column
	 */
	public InfluenceCard useDirectlyEffect(int column) {

		try {

			InfluenceCard prevCard = getEndCard(column, 1);
			InfluenceCard playCard = columns.get(column).getContent().get(columns.get(column).getContent().size() - 1);

			if (prevCard != null) {
				prevCard.setVisible(true);
				Player prev_player = getPlayerByColor(prevCard.getColor());
				Player player = getPlayerByColor(playCard.getColor());

				switch (prevCard.getRole()) {

				case As:
					if (!prevCard.getExecuted()) {
						mainProgram.getControllerTable().getGameTable().refreshCard();
						Thread.sleep(1000);
						columns.get(column).getContent().remove(playCard);

						if (player != null)
							player.addDiscard(playCard);

						prevCard.setExecuted(true);
					}
					break;

				case Ex:
					int next = (column + 1) % players.size();
					boolean find = false;
					int nbColumnTest = 1;
					int i = 1;

					while (!find) {
						next = (column + i) % players.size();
						if (nbColumnTest == columns.size() * 2) {
							next = column;
							find = true;
						} else if (columns.get(next).getIsPlacable())
							find = true;
						else {
							nbColumnTest++;
							i++;
						}
					}

					if (next != column) {

						if (mainProgram.getControllerTable() != null)
							mainProgram.getControllerTable().getGameTable().refreshCard();

						Thread.sleep(1000);

						if (!columns.get(next).getContent().isEmpty()) {
							columns.get(next).getContent().get(columns.get(next).getContent().size() - 1)
									.setVisible(true);
						}

						columns.get(column).getContent().remove(prevCard);
						columns.get(next).getContent().add(prevCard);

						if (!columns.get(next).getContent().isEmpty())
							useDirectlyEffect(next);

						prevCard.setVisible(false);
					}
					break;

				case Te:
					if (!prevCard.getExecuted()) {
						columns.get(column).setIsPlacable(false);
						columns.get(column).setIsRealized(true);
						mainProgram.getControllerTable().getGameTable().objectiveAchieved(column + 1);
						prevCard.setExecuted(true);
					}
					break;

				case Tr:
					if (!prevCard.getExecuted()) {
						mainProgram.askColumnTr(prev_player, column);
						prevCard.setExecuted(true);
					}
					break;

				case Ci:
					if (!prevCard.getExecuted()) {
						mainProgram.askColumnCa(prev_player, column);
						prevCard.setExecuted(true);
					}
					break;
				case Al:
				case Md:
				case Se:
				case Ma:
				case Ca:
				case Tb:
					if (prevCard.getSymbol() != null
							&& prevCard.getSymbol().equals(getColumns().get(column).getOCard().getDomain())
							&& !prevCard.getExecuted()) {
						prevCard.setValue(12);
						prevCard.setExecuted(true);
					}
					break;

				default:
					break;

				}

			}

			mainProgram.informCardPlayed(column, prevCard != null ? prevCard.encode() : "NUL");

			mainProgram.getControllerTable().getGameTable().refreshCard();
			if (!getPause()) {
				endTurn(column);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void setHiddenCardChoosen(InfluenceCard influenceCard, int column) {
		/*
		 * if (getEndCard(column, 2).getRole().equals(Role.Ci)) { getEndCard(column,
		 * 2).setHiddenCard(influenceCard); }
		 */
	}

	/**
	 * Methode pour echanger les cartes objective de place
	 * 
	 * @param startCard
	 * @param destinationCard
	 */
	public void exchangeObjectiveCard(int startCard, int destinationCard) {

		if (destinationCard == -1)
			return;
		/*
		 * ObjectiveCard startObjectiveCard = objectiveCards.get(startCard);
		 * ObjectiveCard destinationObjectiveCard = objectiveCards.get(destinationCard);
		 * 
		 * objectiveCards.set(startCard, destinationObjectiveCard);
		 * objectiveCards.set(destinationCard, startObjectiveCard);
		 */

	}

	/**
	 * Finit la partie en affichant le score des gens
	 */
	public void end() {

		if (MP.javafx) {
			mainProgram.getControllerTable().showScreen(screenList.COUNTINGOFPOINTS);
			mainProgram.getControllerTable().getCountingOfPoints().showFinalsScores();
		}

	}

	public void setWinner(ArrayList<Player> players) {

		Player winner = getGameWinner();

		if (winner != null) {
			players.add(winner);
			for (int i = 0; i < this.players.size(); i++) {
				if (this.players.get(i).getColor().equals(winner.getColor()))
					this.players.remove(i);
			}
			setWinner(players);
		}

	}

	public Player getGameWinner() {
		Player winner = null;
		int score = 0;

		for (Player player : players) {
			if (player.getScore() > score) {
				winner = player;
				score = player.getScore();
			} else if (player.getScore() == score && winner != null && score != 0) {
				for (int i = 5; i > 0; i--) {
					if (player.getOCardValue(i) > winner.getOCardValue(i)) {
						player = winner;
						break;
					} else if (player.getOCardValue(i) < winner.getOCardValue(i))
						break;
				}
			}
		}

		return winner;
	}

	/**
	 * Return le score d'un joueur dans une colonne
	 * 
	 * @param player
	 * @param n      = numero de la colonne
	 * @return int
	 */
	public int getScoreColumnPlayer(Player player, int column) {

		int score = 0;
		ArrayList<InfluenceCard> list = getColumns().get(column).getContent();

		for (InfluenceCard iC : list) {
			if (iC.getColor().equals(player.getColor())) {
				score += iC.getValue();
				if (iC.getHiddenCard() != null)
					score += iC.getHiddenCard().getValue();
			}
		}
		return score;
	}

	/**
	 * Renvoie le player qui a gagne la colonne
	 *
	 * @param n = numero de la colonne
	 * @return Player
	 */
	public Player getColumnWinner(int n) {

		Player winner = null;
		int score = -1;
		ArrayList<InfluenceCard> list = getColumns().get(n).getContent();

		for (Player player : getPlayers()) {
			if (getScoreColumnPlayer(player, n) > score) {
				winner = player;
				score = getScoreColumnPlayer(player, n);
			} else if (getScoreColumnPlayer(player, n) == score && score != 0) {
				for (InfluenceCard iC2 : list) {
					if (getPlayerFromACard(iC2).equals(player)) {
						winner = player;
						break;
					}
				}
			}
		}

		return winner;
	}

	public Player getMeWinner(int n) {

		Player winner = null;
		int score = 999999;
		ArrayList<InfluenceCard> list = getColumns().get(n).getContent();

		for (Player player : getPlayers()) {
			int scorePlayer = getScoreColumnPlayer(player, n);
			if (scorePlayer > 0 && scorePlayer < score) {
				winner = player;
				score = getScoreColumnPlayer(player, n);
			} else if (getScoreColumnPlayer(player, n) == score && score != 0) {
				for (InfluenceCard iC2 : list) {
					if (getPlayerFromACard(iC2).equals(player)) {
						winner = player;
						break;
					}
				}
			}
		}

		return winner;
	}

	/**
	 * Retrouve le joueur d'une carte
	 * 
	 * @param iC
	 * @return Player
	 */
	public Player getPlayerFromACard(InfluenceCard iC) {
		for (Player player : getPlayers()) {
			if (player.getColor().equals(iC.getColor())) {
				return player;
			}
		}
		return null;
	}

	/**
	 * Applique les effets pour les cartes de fin de manche
	 */
	public Player applyEffect(int i, ArrayList<Integer> me) {

		ArrayList<InfluenceCard> cardList = getColumns().get(i).getContent();

		boolean hasTm = isTm(i);

		try {
			if (!hasTm) {

				if (isOneRole(i, Role.Mg)) {
					for (int j = 0; j < cardList.size(); j++) {
						InfluenceCard iC = cardList.get(j);
						if (iC.getValue() >= 10) {
							cardList.remove(j);
							if (mainProgram.getControllerTable() != null) {
								mainProgram.getControllerTable().getGameTable().refreshCard();
								Thread.sleep(100);

							}
						}
					}
					mainProgram.getControllerTable().getGameTable().refreshCard();
				}

				if (isOneRole(i, Role.So)) {
					for (int j = 0; j < cardList.size(); j++) {
						InfluenceCard iC = cardList.get(j);
						if (iC.getValue() <= 9 && !iC.getRole().equals(Role.So)) {
							cardList.remove(j);
							if (mainProgram.getControllerTable() != null) {
								mainProgram.getControllerTable().getGameTable().refreshCard();
								Thread.sleep(100);
							}
						}
					}
					mainProgram.getControllerTable().getGameTable().refreshCard();
				}

				Player winner = isPrinceAndSquire(i);

				if (winner == null) {

					for (InfluenceCard iC : cardList) {
						if (getLastCard(i) != null && !getLastCard(i).encode().equals(iC.encode())) {
							executeEffect(iC, cardList, me);
							if (iC.getHiddenCard() != null)
								executeEffect(iC.getHiddenCard(), cardList, me);
						}
					}

					for (int j = cardList.size() - 1; j >= 0; j--) {
						if (cardList.get(j).getRole().equals(Role.SO) && j != cardList.size() - 1) {
							cardList.get(j).setValue(cardList.get(j + 1).getValue());
						}
					}
				} else
					return winner;

			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void executeEffect(InfluenceCard iC, ArrayList<InfluenceCard> cardList, ArrayList<Integer> me) {
		if (!iC.equals(cardList.get(cardList.size() - 1))) {
			switch (iC.getRole()) {

			case Er:
				iC.setValue(Math.max(iC.getValue() - (cardList.size() - 1), 0));
				break;

			case Pg:
				iC.setValue(iC.getValue() + (cardList.size() - 1) * 3);
				break;

			case Dr:
				for (InfluenceCard iC2 : cardList) {
					if (!iC2.getColor().equals(iC.getColor())) {
						iC2.setValue(Math.max(iC2.getValue() - 2, 0));
					}
				}
				break;

			case RO:
				for (InfluenceCard iC2 : cardList) {
					if (iC2.getRole().equals(Role.Ju) && iC2.getColor().equals(iC.getColor())) {
						iC.setValue(15);
					}
				}
				break;

			case Me:
				// TODO faire la carte mendiant break;
				me.add(1);
				break;

			case Ci:
				iC = iC.getHiddenCard();
				break;

			default:
				break;

			}

		}
	}

	/**
	 * Detecte s'il y a une carte Prince et Ecuyer dans une meme colonne appartenant
	 * au meme joueur en prenant en compte le joueur le plus proche de la
	 * carteObjectif s'il y a deux joueurs ayant le Prince et l'Ecuyer dans une meme
	 * colonne
	 * 
	 * @param n = numero de la colonne
	 * @return Player
	 */
	public Player isPrinceAndSquire(int n) {

		ArrayList<InfluenceCard> list = getColumns().get(n).getContent();

		Color color;

		for (InfluenceCard iC : list) {
			if (iC.getRole().equals(Role.Ec) || iC.getRole().equals(Role.Pr) && !list.get(list.size() - 1).equals(iC)) {
				if (iC.getRole().equals(Role.Ec)) {
					color = iC.getColor();

					for (InfluenceCard iC2 : list) {
						if (iC2.getRole().equals(Role.Pr) && iC2.getColor().equals(color)
								&& !list.get(list.size() - 1).equals(iC2)) {
							return getPlayerFromACard(iC);
						}
					}

				} else if (iC.getRole().equals(Role.Pr)) {
					color = iC.getColor();

					for (InfluenceCard iC2 : list) {
						if (iC2.getRole().equals(Role.Ec) && iC2.getColor().equals(color)
								&& !list.get(list.size() - 1).equals(iC2)) {
							return getPlayerFromACard(iC);
						}
					}
				}

			}

		}
		return null;
	}

	public boolean isTm(int column) {

		for (InfluenceCard card : columns.get(column).getContent()) {
			if (card.getRole().equals(Role.Tm))
				return true;
		}

		return false;
	}

	/**
	 * Detecte si il y a une carte dans une colonne en prenant son role
	 * 
	 * @param n    = numero de la colonne
	 * @param role = role de l'InfluenceCard
	 * @return boolean
	 */
	public boolean isRole(int n, Role role) {

		ArrayList<InfluenceCard> list = getColumns().get(n).getContent();

		for (InfluenceCard iC : list) {
			if (iC.getRole().equals(role) && !list.get(list.size() - 1).equals(iC)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Detecte si il y a une carte ET seulement une d'un role dans une colonne
	 * 
	 * @param n
	 * @param role
	 * @return
	 */
	public boolean isOneRole(int n, Role role) {

		ArrayList<InfluenceCard> list = getColumns().get(n).getContent();
		int i = 0;

		for (InfluenceCard iC : list) {
			if (iC.getRole().equals(role) && !list.get(list.size() - 1).equals(iC)) {
				i++;
			}
			if (iC.getHiddenCard() != null) {
				if (iC.getHiddenCard().getRole().equals(role)
						&& !list.get(list.size() - 1).equals(iC.getHiddenCard())) {
					i++;
				}
			}
		}
		return i == 1;
	}

	public ArrayList<Player> getUserRequest() {
		return userRequest;
	}

	public boolean getPause() {
		return pause;
	}

	public void setPause(boolean pause) {
		this.pause = pause;
	}

	public HashMap<String, Integer> getTrCard() {
		return trCard;
	}

	public HashMap<String, Integer> getCaCard() {
		return caCard;
	}

	public void endTurn(int column) {
		try {
			if (getColumns().get(column).getOCard().getPoint() <= getColumns().get(column).getContent().size()) {
				if (MP.javafx && !getColumns().get(column).getIsRealized())
					mainProgram.getControllerTable().getGameTable().objectiveAchieved(column + 1);
				getColumns().get(column).setIsRealized(true);
			}

			int nbPlacable = mainProgram.getGame().getColumns().size();

			for (Columns columns : mainProgram.getGame().getColumns()) {
				if (columns.getIsRealized())
					nbPlacable--;
			}

			if (MP.javafx)
				Thread.sleep(200);

			if (nbPlacable <= 0) {

				for (Columns _column : columns) {
					for (InfluenceCard card : _column.getContent()) {
						if (!card.isVisible()) {
							card.setVisible(true);
							if (mainProgram.getControllerTable() != null) {
								mainProgram.getControllerTable().getGameTable().refreshCard();
								Thread.sleep(100);
							}
						}
					}
				}
				Thread.sleep(1500);
				mainProgram.endRound(mainProgram.getGame().getRound());

				if (mainProgram.getGame().getRound() <= 6)
					mainProgram.initRound();
				else {
					mainProgram.getGame().end();
					if (!MP.javafx) {
						System.out.println("-> Partie " + mainProgram.getGame().getId() + " terminé!");
						for (Player player : mainProgram.getGame().getPlayers()) {
							System.out.println(
									"\t" + player.getPseudo() + " nbObjectivCard: " + player.getInventory().size());
						}
					} else {
						System.out.println("fini!");
					}

				}

			} else
				mainProgram.initTurn();
		} catch (

		InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sortPlayers() {
		for (int i = 0; i < players.size(); i++) {
			// A1 : tab[0..i-1] est trié
			int indiceMin = i;
			for (int j = i + 1; j < players.size(); j++) {
				// A2 : tab[indiceMin] <= tab[k] pour tout i <= k <j
				if (players.get(indiceMin).getPriority() > players.get(j).getPriority())
					indiceMin = j;
			}
			
			Player player = players.get(i);
			players.set(i, players.get(indiceMin));
			players.set(indiceMin, player);
			
			players.get(i).setPriority(i + 1);
		}
	}
}
