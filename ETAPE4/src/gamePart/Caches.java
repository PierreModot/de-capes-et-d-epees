package gamePart;

import javafx.scene.image.Image;

public class Caches {
	
	/// NORMAL CARDS
		public static Image IC = new Image(cd.DataController.imagePathIC);
		public static Image ICB = new Image(cd.DataController.imagePathICB);
		public static Image ICG = new Image(cd.DataController.imagePathICG);
		public static Image ICP = new Image(cd.DataController.imagePathICP);
		public static Image ICW = new Image(cd.DataController.imagePathICW);
		public static Image ICY = new Image(cd.DataController.imagePathICY);

		public static Image C = new Image(cd.DataController.imagePathC);
		public static Image CB = new Image(cd.DataController.imagePathCB);
		public static Image CG = new Image(cd.DataController.imagePathCG);
		public static Image CP = new Image(cd.DataController.imagePathCP);
		public static Image CW = new Image(cd.DataController.imagePathCW);
		public static Image CY = new Image(cd.DataController.imagePathCY);

	/// SPECIAL CARDS
		public static Image speIC = new Image(cd.DataController.imagePathspeIC);
		public static Image speICB = new Image(cd.DataController.imagePathspeICB);
		public static Image speICG = new Image(cd.DataController.imagePathspeICG);
		public static Image speICP = new Image(cd.DataController.imagePathspeICP);
		public static Image speICR = new Image(cd.DataController.imagePathspeICR);
		public static Image speICY = new Image(cd.DataController.imagePathspeICY);

		public static Image speC = new Image(cd.DataController.imagePathspeC);
		public static Image speCB = new Image(cd.DataController.imagePathspeCB);
		public static Image speCG = new Image(cd.DataController.imagePathspeCG);
		public static Image speCP = new Image(cd.DataController.imagePathspeCP);
		public static Image speCR = new Image(cd.DataController.imagePathspeCR);
		public static Image speCY = new Image(cd.DataController.imagePathspeCY);
		
		public static Image OC = new Image(cd.DataController.imagePathOC);
		public static Image OCS = new Image(cd.DataController.imagePathspeOC);
		
		public static Image BG  = new Image("ressources/background.png");

}
