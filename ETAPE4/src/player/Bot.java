package player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import card.InfluenceCard;
import gamePart.Columns;
import gamePart.MP;
import util.ActionPlay;
import util.Color;
import util.GameState;
import util.Level;
import util.Role;

public class Bot extends Player {

	/*-------------Attributs-----------------*/

	private Level difficulty; // Level du BOT
	private boolean logs = false; // Affichage ou non des logs du Bot.
	private GameState MctsTree=null;

	/*-------------Constructeurs-----------------*/

	/**
	 * Permet de creer un Bot
	 * @param difficulty   Niveau de difficulte du Bot cree
	 */
	public Bot(Level difficulty) {
		this.difficulty = difficulty;

//	joueurs.add(prio,this); // Ajout du Bot a un emplacement selon sa prio dans l'arrayList joueurs qui constitue l'ensemble des joueurs

	}
	/*-------------Getters/Setters-----------------*/

	public Level getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Level difficulty) {
		this.difficulty = difficulty;
	}

	public boolean getlogs() {
		return logs;
	}

	public void setlogs(boolean truefalse) {
		logs = truefalse;
	}

	/* Methodes */
	/**
	 * Permet d'activer / desactiver les logs que renvoit ce Bot.
	 */
	public void switchLogs() {
		if (logs)
			System.out.println("Desactivation des logs pour le bot " + getId());
		else
			System.out.println("Activation des logs pour le bot " + getId());
		setlogs(!(getlogs()));
	}

	/**
	 * Demande au Bot de jouer son tour. Le Bot va choisir en fonction de sa
	 * difficulte l'IA a utiliser.
	 * 
	 * @param game Partie dans laquelle le Bot doit jouer son tour
	 */
	public void Play(MP game) {
		if (logs)
			System.out.println("Le bot " + getId() + " va jouer au Level " + difficulty);
		if (difficulty == Level.Moyen) {
			mediumAi(game);
		} else if (difficulty == Level.Difficile) {
			hardAi(game);
		} else {
			easyAi(game);
		}
	}

	/**
	 * Demande a l'IA facile de jouer une carte
	 * 
	 * @param game Partie dans laquelle le Bot doit jouer son tour
	 */
	private void easyAi(MP game) {
		int playerNumber = game.getGame().getNumPlayer();
		Random rdm = new Random();

		// -- choix de la Columns --
		int column = rdm.nextInt(playerNumber);

		// -- choix de la carte --
		if (logs)
			System.out.println("On choisit une carte aleatoire dans sa main");
		int card = rdm.nextInt(2);

		InfluenceCard cardToPlay = getHand().get(card);

		playCard(cardToPlay, column, game);
	}

	/**
	 * Demande a l'IA moyen de jouer une carte
	 * 
	 * @param game Partie dans laquelle le Bot doit jouer son tour
	 */

	/**
	 * Permet de Jouer une carte dans une Columns.
	 * 
	 * @param card Carte a jouer
	 * @param pos  Numero de Columns dans laquelle mettre la carte
	 * @param game Partie dans laquelle se deroule le jeu
	 */
	public void playCard(InfluenceCard card, int pos, MP game) {
		if (logs) System.out.println("Le Bot joue la carte " + card + " dans la Columns " + pos + " dans la partie " + game);
		game.playInfluenceCard(card, pos);
	}

	/* Fonctions */
	/**
	 * Cherche une carte dans la main du joueur, cherche une carte choisi sur le plateau. Si les deux sont presente alors joue la carte dans la main dans la colonne ou se trouve l'autre carte. (Possibilte de filtre sur les cartes de la meme couleur)
	 * @param game Jeux dans lequel jouer.
	 * @param needed Carte a trouver dans la main.
	 * @param toFind Carte a trouver sur le plateau.
	 * @param same Est ce que la carte a chercher doit etre de la meme couleur.
	 * @return Retourne vrais si une carte a ete jouee, sinon faux.
	 */
	private boolean instantSynergy(MP game,Role needed, Role toFind,Boolean same){
		return instantSynergy(game, game.getGame().getColumns(), needed, toFind, same);
	}


	/**
	 * Cherche une carte dans la main du joueur, cherche une carte choisi sur le plateau. Si les deux sont presente alors joue la carte dans la main dans la colonne ou se trouve l'autre carte. (Possibilte de filtre sur les cartes de la meme couleur).
	 * Si la carte recherche est le Sosie, alors la recherche de la carte ne traite que la derniere carte joue de chaque colonne.
	 * @param game Jeux dans lequel jouer.
	 * @param columns Colonne ou chercher.
	 * @param needed Carte a trouver dans la main.
	 * @param toFind Carte a trouver sur le plateau.
	 * @param same Est ce que la carte a chercher doit etre de la meme couleur.
	 * @return Retourne vrais si une carte a ete jouee, sinon faux.
	 */
	private boolean instantSynergy(MP game,ArrayList<Columns> columns,Role needed, Role toFind,Boolean same){
		boolean found=false;

		if(toFind==Role.SO){
			for(InfluenceCard inHand: this.getHand()){
				for (Columns column : game.getGame().getColumns()){
					InfluenceCard lastcard=null;
					if(column.getContent().size()!=0) lastcard=column.getContent().get(column.getContent().size()-1);
					if(lastcard!=null
					&& lastcard.getRole()==toFind // Est ce que le role recherche est le bon
					&& (lastcard.getColor().equals(this.getColor())||lastcard.isVisible()) // Est ce que on peut connaitre la carte
					&& ((!same) || lastcard.getColor()==this.getColor()) // Est ce que la carte doit etre de la meme couleur
					){playCard(inHand, game.getGame().getColumns().indexOf(column),game);found=true;}
				}
			}
		}
		else {
			// Regarde chaque carte de la main du joueur
			for(InfluenceCard inHand: this.getHand()){
				if(inHand.getRole()==needed){

					// Regarde chaque colonne presente dans la partie
					for (Columns column:columns){
						
						int index=0;
						Boolean Go = false;
						Boolean possible=true;
						// Regarde chaque carte de la colonne
						while((!found) && index < column.getContent().size() && possible){
							InfluenceCard card = column.getContent().get(index);
							if(card.getRole()==Role.Te || card.getRole()==Role.Tm)possible=false;

							// Si la carte est trouve
							if(card.getRole()==toFind // Est ce que le role recherche est le bon
							&& (card.getColor().equals(this.getColor())||card.isVisible()) // Est ce que on peut connaitre la carte
							&& ((!same) || card.getColor()==this.getColor()) // Est ce que la carte doit etre de la meme couleur
							)Go=true;

							index++;
						}
						// Si il n'y a pas de tempete et que l'on a trouve la carte recherche
						if(Go && possible) {playCard(inHand, game.getGame().getColumns().indexOf(column),game);found=true;}
					}
				}
				
			}
		}
		
		return found;
		
	}

	// ----------------------- TODO A terminer -----------------------

	private void mediumAi(MP game) {

		Boolean AlreadyPlayed=false;//variable indiquand si le bot a deja joue ou non
		ArrayList<Columns> unSortedObj = new ArrayList<>(); // liste completes des objectifs non trie
		ArrayList<Columns> targetlist = new ArrayList<>(); /// liste trie des objectifs ou l'on peut jouer
		
		for(Columns column : game.getGame().getColumns()) unSortedObj.add(column);

		for(int i=0;i<game.getGame().getColumns().size();i++){// Creation de la liste trie des objectifs
			Columns addcolumn = unSortedObj.get(0);
			int max = unSortedObj.get(0).getOCard().getPoint();
			for(Columns column : unSortedObj){ // Parcours des colonne pas encore trie
				if(max<column.getOCard().getPoint() && column.getIsPlacable()){
					max=column.getOCard().getPoint();
					addcolumn=column;
				}
			}
			if(addcolumn.getIsPlacable()){targetlist.add(addcolumn);}
			unSortedObj.remove(addcolumn);
			
		}

		//Traitement => Algo de niveaux moyen du bot
		if(!AlreadyPlayed)AlreadyPlayed = evaluateMedium(game,targetlist);

		//Si aucun decision n'est prise
		if(!AlreadyPlayed)easyAi(game);

		//Fin de l'execution du Bot
	}





		/**
		 * 
		 * @param game
		 * @return retourne si une carte a deja ete jouee
		 */
		public Boolean evaluateMedium(MP game){
		return evaluateMedium(game,game.getGame().getColumns());
		}

		/**
		 * 
		 * @param game Jeux dans lequel il faut jouer
		 * @param objectifs liste des colonnes dans lequel effectue le traitement
		 * @return retourne si une carte a deja ete jouee
		 */
		private Boolean evaluateMedium(MP game,ArrayList<Columns> objectifs){
			
			//Si peut jouer une carte qui realise une synergie: la joue
			Boolean AlreadyPlayed=this.instantSynergy(game, Role.RO, Role.Ju,true);
			if (!AlreadyPlayed){AlreadyPlayed=this.instantSynergy(game, objectifs, Role.Ju, Role.RO,true);}
			if (!AlreadyPlayed){AlreadyPlayed=this.instantSynergy(game, objectifs, Role.Pr, Role.Ec,true);}
			if (!AlreadyPlayed){AlreadyPlayed=this.instantSynergy(game, objectifs, Role.Ec, Role.Pr,true);}
			if (!AlreadyPlayed){AlreadyPlayed=this.instantSynergy(game, objectifs, Role.So, Role.Mg,false);}
			if (!AlreadyPlayed){AlreadyPlayed=this.instantSynergy(game, objectifs, Role.Mg, Role.So,true);}
			if (!AlreadyPlayed){AlreadyPlayed=this.instantSynergy(game, objectifs, Role.Ro, Role.SO,true);}
			
			if(!AlreadyPlayed){ // Joue l'ecuyer dans une colonne ou le bot n'a pas encore joue et ou se trouve un mendiant
				for(InfluenceCard inHand : this.getHand()){
					if(inHand.getRole()==Role.Ec){
						for(Columns column : objectifs){
							int found = 0;
							for(InfluenceCard card : column.getContent()){
								if(card.getRole()==Role.Me && (card.isVisible()||card.getColor()==this.getColor())){found++;} // Si on trouve un mendiant
								if((card.getRole()==Role.Me || card.getRole()==Role.Tm) && (card.isVisible()||card.getColor()==this.getColor()))found+=10;// Si on trouve une carte de notre couleur
							}
							if(found==1){
								AlreadyPlayed=true;
								playCard(inHand, game.getGame().getColumns().indexOf(column),game);
							}
	
						}
					}
					
				}
			}

			if (!AlreadyPlayed){
				for(Columns column : objectifs){
					
					if(!AlreadyPlayed){
						Boolean mendiantCapture = null;
						for(InfluenceCard cardToTest:column.getContent()){ //Regarde si dans la colonne se trouve le mendiant
									if(cardToTest.getRole()==Role.Me && mendiantCapture==null) mendiantCapture=true;
									if(cardToTest.getRole()==Role.Tm) mendiantCapture=false;
								}
						
					
						for(InfluenceCard card : this.getHand()){ //Joue pour gagner une colonne
							

							if (!AlreadyPlayed){ //Si le bot n'a pas joue
								
								column.leaderboard();
								HashMap<Color,Integer> leaderboard = column.leaderboard();//leaderboard actuel de la colonne
								HashMap<Color,Integer> newleaderboard = column.leaderboard(card);//leaderboard si on pose la carte en plus
								
								if(leaderboard.size()!=0 && leaderboard.keySet().toArray()[leaderboard.size()-1]==this.getColor() && mendiantCapture!=true && card.getRole()==Role.Me){ //Si le bot est dernier et peux jouer le mendiant, le joue
									AlreadyPlayed=true;
									playCard(card, game.getGame().getColumns().indexOf(column),game);
								}
								else if(leaderboard.size()>0 && leaderboard.keySet().toArray()[0]!=this.getColor() && mendiantCapture!=true){ //Si on peut jouer dans la colonne et le bot n'est pas celuis qui va remporter la colonne
									if(newleaderboard.keySet().toArray()[0]==this.getColor()){AlreadyPlayed=true;playCard(card, game.getGame().getColumns().indexOf(column),game);}
								}
							}
						}
						
						
					}
				}
			



			}
			
			
			
			
			return AlreadyPlayed;
		}
		
	

		

		
	

	/**
	 * Pour le moment, execute le script de l'IA moyen TODO Demande a l'IA difficile de jouer une carte.
	 * @param game Partie dans laquelle le Bot doit jouer son tour
	 */
	private void hardAi(MP game) {
		Boolean AlreadyPlayed = false;

		AlreadyPlayed=MctsSearch(game);
		
		if (!AlreadyPlayed) mediumAi(game);
	}

	

	public Boolean MctsSearch(MP game){
		Boolean AlreadyPlayed = false;

		//Creation de l'arbre
		if(MctsTree==null) MctsTree = new GameState(game.getGame(),this.getHand(),this.getColor());

		//Agrandissement de l arbre
		boolean execution = MctsTree.evaluate(this.getHand(),this.getDeck());

		if(execution){

			//Choix du meilleur fils
			ActionPlay action = MctsTree.bestChild().getAction();
			int found=0;
			InfluenceCard cardToPlay;

			do{
				cardToPlay = this.getHand().get(found);
				found++;
			}while(cardToPlay.getRole()!=action.getCard() && found>=3);
			
			int columnToPlay = action.getColumn();

			//Mis a jour de l'arbre
			MctsTree=MctsTree.bestChild();
			MctsTree.setParent(null);

			//Joue l'action
			playCard(cardToPlay, columnToPlay, game);

		}

		
		return AlreadyPlayed;
	}
	
	public Bot duplicate() {
		Bot bot = new Bot(difficulty);
		bot.setPseudo(getPseudo());
		bot.setColor(getColor());
		bot.setId(getId());
		bot.setType("BOT");
		bot.setPriority(getPriority());
		
		return bot;
	}

}
