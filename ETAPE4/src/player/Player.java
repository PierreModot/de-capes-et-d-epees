package player;

import java.util.ArrayList;
import java.util.Collections;

import card.InfluenceCard;
import card.ObjectiveCard;
import util.Color;
import util.Domain;
import util.Role;

public class Player {

	private String id = "";
	private String type = "";
	private String pseudo = "";
	private Color color;
	private boolean accepted = false;

	private ArrayList<InfluenceCard> hand = new ArrayList<>();
	private ArrayList<InfluenceCard> deck = new ArrayList<>();
	private ArrayList<InfluenceCard> discard = new ArrayList<>();

	private ArrayList<ObjectiveCard> inventory = new ArrayList<>();

	private int priority = 0;

	public ArrayList<ObjectiveCard> getInventory() {
		return inventory;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean getAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public ArrayList<InfluenceCard> getHand() {
		return hand;
	}

	public void setHand(ArrayList<InfluenceCard> hand) {
		this.hand = hand;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * Initialise le deck en donnant au joueur toutes les cartes dans son deck
	 */
	public void initDeck() {
		for (Role role : Role.values())
			deck.add(new InfluenceCard(role, color, false));

		Collections.shuffle(deck);
	}

	public ArrayList<InfluenceCard> getDeck() {
		return deck;
	}

	public ArrayList<InfluenceCard> getDiscard() {
		return discard;
	}

	/**
	 * Pioche une carte
	 */
	public void drawCard() {
		hand.add(deck.get(0));
		deck.remove(0);
	}

	public boolean hasDeck() {
		return !deck.isEmpty();
	}

	public boolean hasDiscard() {
		return !discard.isEmpty();
	}

	public InfluenceCard drawCard(InfluenceCard card) {
		int id = getIdCardFromHand(card);

		if (deck.isEmpty())
			addDiscardToDeck();

		
		if (id < 0)
			id = 0;

		hand.set(id, deck.get(0));
		deck.remove(0);

		return hand.get(id);
	}

	public int getIdCardFromHand(InfluenceCard card) {
		for (int i = 0; i < hand.size(); i++) {
			if (hand.get(i).getRole().equals(card.getRole()))
				return i;
		}
		return -1;
	}

	public void addDiscard(InfluenceCard card) {
		discard.add(card);
	}

	/**
	 * Verifie si le joueur est un BOT
	 * 
	 * @return boolean
	 */
	public boolean isBot() {
		return type.equals("BOT");
	}

	/**
	 * Met les cartes de la defausse dans le deck
	 */
	public void addDiscardToDeck() {
		ArrayList<String> caches = new ArrayList<>();

		while (!discard.isEmpty()) {
			InfluenceCard card = discard.get(0);
			if (!caches.contains(card.encode())) {
				deck.add(discard.get(0));
				caches.add(card.encode());
			}
			discard.remove(0);
		}
	}

	/**
	 * Verifie que le joueur possede au moins une carte du domaine mit en parametre
	 * 
	 * @param domain
	 * @return
	 */
	public boolean isDomain(Domain domain) {
		for (ObjectiveCard cObjectif : inventory) {
			if (domain.equals(cObjectif.getDomain())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Verifie que le joueur possede tous les domaines dans son seck
	 * 
	 * @return boolean
	 */
	public boolean isAllDomain() {
		for (Domain domain : Domain.values()) {
			if (!isDomain(domain)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Recupere la plus grande valeur que possede un joueur dans un domaine precis
	 * 
	 * @param domain
	 * @return int
	 */
	public int getBigger(Domain domain) {
		int valueMax = 0;

		for (ObjectiveCard cObjectif : inventory) {
			if (domain.equals(cObjectif.getDomain()) && cObjectif.getPoint() > valueMax) {
				valueMax = cObjectif.getPoint();
			}
		}
		return valueMax;
	}

	/**
	 * Recupere la carte ayant plus grande valeur que possede un joueur dans un
	 * domaine precis
	 * 
	 * @param domain
	 * @return int
	 */
	public ObjectiveCard getBiggerCard(Domain domain) {
		ObjectiveCard objectiveCard = new ObjectiveCard(domain, 0);

		for (ObjectiveCard cObjectif : inventory) {
			if (domain.equals(cObjectif.getDomain()) && cObjectif.getPoint() > objectiveCard.getPoint()) {
				objectiveCard = cObjectif;
			}
		}
		return objectiveCard.getPoint() == 0 ? null : objectiveCard;
	}

	/**
	 * Methode de calcul du score par rapport a l'inventaire qui contient les cartes
	 * objectifs qu il a gagne durant les manches lors dune partie
	 * 
	 * @return int
	 */
	public int getScore() {

		return isAllDomain() ? getDomainScore() : getStandardScore();
	}

	/**
	 * Methode de calcul qui prend la plus grosse carte de chaque domaine, multiplie
	 * le resultat par 2 et soustrait par rapport aux nombres de cartes non utilises
	 * pour le calcul
	 * 
	 * @return int
	 */
	public int getDomainScore() {
		int score = 0;

		for (Domain domain : Domain.values()) {
			score += getBigger(domain);
		}

		return score * 2 - (inventory.size() - 6);
	}

	/**
	 * Methode de calcul classique qui compte la valeur de chaque carte pour obtenir
	 * le score
	 * 
	 * @return int
	 */
	public int getStandardScore() {
		int score = 0;

		for (ObjectiveCard cObjectif : inventory) {
			score += cObjectif.getPoint();
		}

		return score;
	}

	/**
	 * Return le nombre de carte objectif d'une certaine valeur que possede un
	 * joueur
	 * 
	 * @return int
	 */
	public int getOCardValue(int value) {
		int i = 0;

		for (ObjectiveCard objectiveCard : inventory) {
			if (objectiveCard.getPoint() == value)
				i++;

		}
		return i;
	}

	/**
	 * Renvoie l'inventaire d'un joueur avec les meilleurs cartes de chaque domaine
	 * dans l'ordre puis le reste si le joueur ne possede pas les 6 domaines cela
	 * renvoie une ArrayList vite;
	 * 
	 * @return ArrayList<ObjectiveCard>
	 */
	public ArrayList<ObjectiveCard> getTriedObjectiveCard() {
		if (!isAllDomain())
			return new ArrayList<>();

		ArrayList<ObjectiveCard> objectiveCards = new ArrayList<>();

		for (Domain domain : Domain.values()) {
			objectiveCards.add(getBiggerCard(domain));
		}

		for (ObjectiveCard objectiveCard : inventory) {
			for (Domain domain : Domain.values()) {
				if (objectiveCard.getDomain().equals(domain) && !objectiveCard.equals(getBiggerCard(domain))) {
					objectiveCards.add(objectiveCard);
				}
			}
		}

		return new ArrayList<ObjectiveCard>(objectiveCards);
	}
}
