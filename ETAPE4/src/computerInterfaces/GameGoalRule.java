package computerInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


	public class GameGoalRule extends BorderPane {

		private DialogueController cDialogue = null;
		//On donne un nom a cette interface pour permettre l'identification par le systeme
		private static final screenList nameScreen = screenList.GAMEGOALRULE;
		
			HBox Htop;

			Label lTitle;
		
			VBox Vbottom;
			VBox Vleft;
			VBox Vright;
			
			VBox Vmiddle;

			Button bBack1;
			Button bBack2;

			Label lTextggr;
			Label lSetup;
			Label lTextGs;


			// On créé les differents composants de l'interface (Label, Button)
		    private void createWidgets() {
				//***LABEL***//
		    	//creation et initialisation du Label lTitle
				lTitle = new Label();
				lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 200));		
				lTitle.textProperty().bind(I18N.createStringBinding("lb.GameGrt"));
						
				//creation et initialisation du Button bBack1
				bBack1 = new Button();
				bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack1.setPrefSize(200, 50);
				bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack1.setTranslateY(-75);
				bBack1.setTranslateX(50);

				// creation et initialisation du Button bBack2
				bBack2 = new Button();
				bBack2.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack2.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack2.setPrefSize(200, 50);
				bBack2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack2.setTranslateY(-75);
				bBack2.setTranslateX(-50);
				
				lTextggr= new Label();
			    lTextggr.setAlignment(Pos.CENTER);
			    lTextggr.setFont(Font.font("Cambria", 22));
			    lTextggr.textProperty().bind(I18N.createStringBinding("text.ggr"));
			    
			    lSetup = new Label();
				lSetup.setFont(Font.font("Cambria", FontWeight.BOLD, 60));		
				lSetup.textProperty().bind(I18N.createStringBinding("lb.SetupGameRule"));
				
				lTextGs= new Label();
				lTextGs.setAlignment(Pos.CENTER);
				lTextGs.setFont(Font.font("Cambria", 22));
				lTextGs.textProperty().bind(I18N.createStringBinding("text.gs"));
				bBack2.setPrefSize(300, 80);

			}
				
		    
			// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
		    private void createContainers() {

				// CREATION ET CONFIGURATION DE LA HBOX HAUTE/TITRE
				Htop = new HBox();
				Htop.setAlignment(Pos.CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vbottom = new VBox();
				Vbottom.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vleft = new VBox();
				Vleft.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX DROITE
				Vright = new VBox();
				Vright.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX middle
				Vmiddle = new VBox();
				Vmiddle.setAlignment(Pos.CENTER);

		    }

			// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
		    private void placementWidgetsInContainers() {
				
				// ON AJOUTE TITRE DANS TOP
				Htop.getChildren().add(lTitle);
						
				// ON AJOUTE JOUER DANS middle
				//Vmiddle.getChildren().add(); // Il faut rajouter le texte
				
				
				// ON AJOUTE REGLES DU JEU DANS VGAUCHE
				Vleft.getChildren().add(bBack1);

				// ON AJOUTE PARAMETRE DANS VDROITE
				Vright.getChildren().add(bBack2);

				// ON AJOUTE PARAMETRE DANS VDROITE
				Vmiddle.getChildren().add(lTextggr);
				Vmiddle.getChildren().add(lSetup);
				Vmiddle.getChildren().add(lTextGs);


			}

			// On définit l'emplacement des HBox et VBox dans l'interface
		    private void placementContainersInPane() {
			
				// ON ASSIGNE A CHAQUE BOX LEUR EMPLACEMENT
				this.setTop(Htop);
				this.setLeft(Vleft);
				this.setRight(Vright);
				this.setCenter(Vmiddle);
			}

			
		public GameGoalRule(DialogueController cDialogue2) throws FileNotFoundException{

			cDialogue = cDialogue2;
			
			createWidgets();
		    createContainers();
		    placementWidgetsInContainers();
		    placementContainersInPane();

			//Creation de l'image
			Image image1 = Caches.BG;
			// Creation de l'image en arriere plan/ image de fond
			BackgroundImage backgroundimage = new BackgroundImage(image1,
			BackgroundRepeat.NO_REPEAT,
			BackgroundRepeat.NO_REPEAT,
			BackgroundPosition.DEFAULT,
			new BackgroundSize(1.0, 1.0, true, true, false, false));
		
			Background background = new Background(backgroundimage);
			//On affecte à l'écran courant l'image de fond défini
			this.setBackground(background);

			bBack1.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.COMPUTERRULES);
		});

			
			bBack2.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.COMPUTERRULES);
		});
			
			

			this.setVisible(false);
			cDialogue.saveScreen(nameScreen, this);

		}
			
	}



