package computerInterfaces;

import card.InfluenceCard;
import card.ObjectiveCard;
import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import java.io.FileInputStream;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import java.io.FileNotFoundException;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.image.Image;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import util.Domain;
import util.Role;

public class HandCard extends BorderPane {

	private DialogueController cDialogue = null;
	// On donne un nom a cette interface pour permettre l'identification par le
	// systeme
	private static final screenList screenName = screenList.HANDCARD;

	// creation d'une bordure (Border)
	Border outline = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
			new BorderWidths(1), new Insets(5)));

	// variable pour sauvegarder la position de la carte qui vient d'Ãªtre jouÃ©e
	int cardPlayed;

	// variable pour savoir si le joueur a le droit de jouer une carte ou non
	boolean canPlay = true;

	// variable de retour pour la mÃ©thode exchangeObjectiveCard
	int column = 0;

	InfluenceCard lastCardReturned;

	boolean returnedCard = false;

	// cartes dans la main
	InfluenceCard cardLeft;
	InfluenceCard cardCenter;
	InfluenceCard cardRight;

	Label lRound;
	Label lFormer;
	Label lNameCard;
	Label lColumn;
	Label lUsername;

	BorderPane bpTop;
	HBox hbCenter;
	HBox hbRight;
	HBox hbLeft;
	HBox hbBottom;

	VBox vbCenter;
	VBox vbRight;
	VBox vbLeft;

	VBox vbCenterC;
	VBox vbRightC;
	VBox vbLeftC;

	HBox hbChoiceButtons;
	VBox vbChoices;
	VBox vb1;
	HBox hb1;

	Button bCardLeft;
	Button bCardCenter;
	Button bCardRight;

	Button bDetailsCLeft;
	Button bDetailsCCenter;
	Button bDetailsCRight;

	Button bLastCarteUsed;

	Button bPlayCard;

	HBox hbCross;
	Button bCross;

	Button bParameters;
	Button bRules;

	Button bPlayColumn1;
	Button bPlayColumn2;
	Button bPlayColumn3;
	Button bPlayColumn4;
	Button bPlayColumn5;
	Button bPlayColumn6;

	VBox vbPlayColumn1;
	VBox vbPlayColumn2;
	VBox vbPlayColumn3;
	VBox vbPlayColumn4;
	VBox vbPlayColumn5;
	VBox vbPlayColumn6;

	boolean button1 = true;
	boolean button2 = true;
	boolean button3 = true;
	boolean button4 = true;
	boolean button5 = true;
	boolean button6 = true;

	Button bDontPlay;

	String PlayedCard;

	TextArea Formertxt;

	/**
	 * permet ou interdit au joueur de jouer une carte
	 * 
	 * @param bool
	 */
	public void canPlay(boolean bool) {
		canPlay = bool;
		setOpacityAnimation(bCardLeft, bool ? 1 : .4);
		setOpacityAnimation(bCardCenter, bool ? 1 : .4);
		setOpacityAnimation(bCardRight, bool ? 1 : .4);
	}

	public void setTurnText(String pseudo) {
		Platform.runLater(() -> {
			lRound.textProperty().bind(I18N.createStringBinding("lb.roundHd").concat(" " + pseudo));
		});
	}

	public void disableButton(int column) {
		Platform.runLater(() -> {
			switch (column) {
			case 1:
				button1 = false;
				bPlayColumn1.setDisable(true);
				break;
			case 2:
				button2 = false;
				bPlayColumn2.setDisable(true);
				break;
			case 3:
				button3 = false;
				bPlayColumn3.setDisable(true);
				break;
			case 4:
				button4 = false;
				bPlayColumn4.setDisable(true);
				break;
			case 5:
				button5 = false;
				bPlayColumn5.setDisable(true);
				break;
			case 6:
				button6 = false;
				bPlayColumn6.setDisable(true);
				break;

			}
		});
	}

	public void enableButton() {
		Platform.runLater(() -> {
			button1 = true;
			button2 = true;
			button3 = true;
			button4 = true;
			button5 = true;
			button6 = true;
			bPlayColumn1.setDisable(false);
			bPlayColumn2.setDisable(false);
			bPlayColumn3.setDisable(false);
			bPlayColumn4.setDisable(false);
			bPlayColumn5.setDisable(false);
			bPlayColumn6.setDisable(false);
		});
	}

	public void setColumnButtons(int nbrPlayer) {
		Platform.runLater(() -> {
			switch (nbrPlayer) {
			case 2:
				hbChoiceButtons.getChildren().addAll(vbPlayColumn1, vbPlayColumn2);
				break;
			case 3:
				hbChoiceButtons.getChildren().addAll(vbPlayColumn1, vbPlayColumn2, vbPlayColumn3);
				break;
			case 4:
				hbChoiceButtons.getChildren().addAll(vbPlayColumn1, vbPlayColumn2, vbPlayColumn3, vbPlayColumn4);
				break;
			case 5:
				hbChoiceButtons.getChildren().addAll(vbPlayColumn1, vbPlayColumn2, vbPlayColumn3, vbPlayColumn4,
						vbPlayColumn5);
				break;
			case 6:
				hbChoiceButtons.getChildren().addAll(vbPlayColumn1, vbPlayColumn2, vbPlayColumn3, vbPlayColumn4,
						vbPlayColumn5, vbPlayColumn6);
				break;
			}
		});
	}

	/**
	 * affiche une carte dans un bouton
	 * 
	 * @param card
	 * @param position
	 */
	public void changeACard(InfluenceCard card, int position) {
		Platform.runLater(() -> {
			ImageView image = card.getFrontCard();
			image.setFitHeight(200);
			image.setFitWidth(200);

			switch (position) {
			case 1:
				bCardLeft.setGraphic(image);
				cardLeft = new InfluenceCard(card);

				bDetailsCLeft.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent arg0) {
						cDialogue.getCardDetails().detailsCard(new InfluenceCard(cardLeft.encode(), true));
						// changeACard(cardLeft, 1);
					}
				});
				break;
			case 2:
				bCardCenter.setGraphic(image);
				cardCenter = new InfluenceCard(card);

				bDetailsCCenter.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent arg0) {
						cDialogue.getCardDetails().detailsCard(new InfluenceCard(cardCenter.encode(), true));
						// changeACard(cardLeft, 2);
					}
				});
				break;
			case 3:
				bCardRight.setGraphic(image);
				cardRight = new InfluenceCard(card);

				bDetailsCRight.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent arg0) {
						cDialogue.getCardDetails().detailsCard(new InfluenceCard(cardRight.encode(), true));
						// changeACard(cardLeft, 3);
					}
				});
				break;
			}
		});
	}

	/**
	 * remplace la carte jouÃ©e par une autre
	 * 
	 * @param card
	 */
	public void changePlayedCard(InfluenceCard card) {
		switch (cardPlayed) {
		case 1:
			changeACard(card, 1);
			break;
		case 2:
			changeACard(card, 2);
			break;
		case 3:
			changeACard(card, 3);
			break;
		}
	}

	/**
	 * Fait choisir au joueur une carte a cacher sous la cape d'invisibilité
	 * 
	 * @return
	 */
	public void hideACard(int column) {
		Platform.runLater(() -> {
			Label l = new Label("Quelle carte souhaitez vous cacher derrière la cape d'invisibilité ?");
			l.setFont(Font.font("Gabriola", FontWeight.BOLD, 50));
			l.setTranslateY(-150);

			if (hbBottom.getChildren().size() != 0) {
				hbBottom.getChildren().add(l);
			} else {
				hbBottom.getChildren().remove(vbChoices);
				hbBottom.getChildren().add(l);
			}

			this.column = column;
			// clic sur la carte 1
			bCardLeft.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().setHiddenCardChoosen(cardLeft, column);
					setButtons();
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnCa(cardLeft);
					hbBottom.getChildren().remove(0);
				}
			});

			// clic sur la carte 2
			bCardCenter.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().setHiddenCardChoosen(cardCenter, column);
					setButtons();
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnCa(cardCenter);
					hbBottom.getChildren().remove(0);
				}
			});

			// clic sur la carte 3
			bCardRight.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().setHiddenCardChoosen(cardRight, column);
					setButtons();
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnCa(cardRight);
					hbBottom.getChildren().remove(0);
				}
			});

			// clic sur le bouton ne rien faire
			bDontPlay.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().setHiddenCardChoosen(null, column);
					setButtons();
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnCa(null);
					hbBottom.getChildren().remove(0);
				}
			});
		});
	}

	/**
	 * fait choisir au joueur une carte objectif a echanger
	 * 
	 * @return
	 */
	public void exchangeObjectiveCard(int column) {
		Platform.runLater(() -> {
			vbCenter.getChildren().remove(hbCenter);
			Label l = new Label(
					"Avec quelle carte objectif voulez vous echanger la carte de la colonne " + column + " ?");

			l.setFont(Font.font("Gabriola", FontWeight.BOLD, 50));
			vbCenter.getChildren().add(l);

			hbBottom.getChildren().add(vbChoices);

			this.column = column;

			// clic sur la colonne 1
			bPlayColumn1.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().exchangeObjectiveCard(column - 1, 0);
					hbBottom.getChildren().remove(vbChoices);
					vbCenter.getChildren().remove(vbCenter.getChildren().get(0));
					vbCenter.getChildren().add(hbCenter);
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnTr(0);
				}
			});

			// clic sur la colonne 2
			bPlayColumn2.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().exchangeObjectiveCard(column - 1, 1);
					hbBottom.getChildren().remove(vbChoices);
					vbCenter.getChildren().remove(vbCenter.getChildren().get(0));
					vbCenter.getChildren().add(hbCenter);
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnTr(1);
				}
			});

			bPlayColumn3.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() { // clic sur la
																									// colonne 3
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().exchangeObjectiveCard(column - 1, 2);
					hbBottom.getChildren().remove(vbChoices);
					vbCenter.getChildren().remove(vbCenter.getChildren().get(0));
					vbCenter.getChildren().add(hbCenter);
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnTr(2);
				}
			});

			bPlayColumn4.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 4
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().exchangeObjectiveCard(column - 1, 3);
					hbBottom.getChildren().remove(vbChoices);
					vbCenter.getChildren().remove(vbCenter.getChildren().get(0));
					vbCenter.getChildren().add(hbCenter);
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnTr(3);
				}
			});

			bPlayColumn5.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 5
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().exchangeObjectiveCard(column - 1, 4);
					hbBottom.getChildren().remove(vbChoices);
					vbCenter.getChildren().remove(vbCenter.getChildren().get(0));
					vbCenter.getChildren().add(hbCenter);
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnTr(4);
				}
			});

			bPlayColumn6.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 6
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().exchangeObjectiveCard(column - 1, 5);
					hbBottom.getChildren().remove(vbChoices);
					vbCenter.getChildren().remove(vbCenter.getChildren().get(0));
					vbCenter.getChildren().add(hbCenter);
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnTr(5);
				}
			});

			bDontPlay.setOnAction(new EventHandler<ActionEvent>() { // clic sur la croix
				@Override
				public void handle(ActionEvent arg0) {
					cDialogue.getMp().getGame().exchangeObjectiveCard(column, -1);
					hbBottom.getChildren().add(vbChoices);
					vbCenter.getChildren().remove(vbCenter.getChildren().get(0));
					vbCenter.getChildren().add(hbCenter);
					cDialogue.getMp().getGame().setWait(false);
					cDialogue.getMp().setColumnTr(column - 1);
				}
			});
		});
	}

	// On crÃ©Ã© les composants de la page et on les initialise (Label, Button)
	private void creationWidgets() {

		// creation et initialisation du Label lRound
		lRound = new Label();
		lRound.setFont(Font.font("Gabriola", FontWeight.BOLD, 100));
		lRound.textProperty().bind(I18N.createStringBinding("lb.roundHd"));
		// creation et initialisation du Label lFormer

		lFormer = new Label();
		lFormer.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		lFormer.textProperty().bind(I18N.createStringBinding("txt.lastCardPlay"));

		// creation et initialisation du Label lNameCard

		lNameCard = new Label();
		lNameCard.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		// lNameCard.textProperty().bind(I18N.createStringBinding("txt.cardName"));
		// creation et initialisation du Label lColumn
		lColumn = new Label();
		lColumn.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		// lColumn.textProperty().bind(I18N.createStringBinding("txt.column"));
		// creation et initialisation du Label lUsername
		lUsername = new Label();
		lUsername.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		// lUsername.textProperty().bind(I18N.createStringBinding("txt.by"));
		// creation et initialisation du Button bParameters
		bParameters = new Button("ParamÃ¨tres");
		bParameters.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		bParameters.setPrefSize(200, 80);
		bParameters.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bParameters
				.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bParameters.setTranslateX(-50);
		bParameters.setTranslateY(50);

		// creation et initialisation du Button bRules
		bRules = new Button("RÃ¨gles");
		bRules.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		bRules.setPrefSize(200, 80);
		bRules.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bRules.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRules.setTranslateY(25);
		bRules.setTranslateX(-50);
		// creation et initialisation du Button bLastCarteUseds
		bLastCarteUsed = new Button();
		bLastCarteUsed.setPrefSize(200, 200);
		bLastCarteUsed.setTranslateX(50);
		// creation et initialisation du Button bCartesGauche
		bCardLeft = new Button();
		bCardLeft.setPrefSize(200, 200);

		// creation et initialisation du Button bCardCenter
		bCardCenter = new Button();
		bCardCenter.setPrefSize(200, 200);

		// creation et initialisation du Button bCardRight
		bCardRight = new Button();
		bCardRight.setPrefSize(200, 200);

		bDetailsCLeft = new Button();
		bDetailsCLeft.textProperty().bind(I18N.createStringBinding("btn.detailsC"));
		bDetailsCLeft.setPrefSize(100, 30);
		bDetailsCLeft.setTranslateX(50);
		bDetailsCLeft.setTranslateY(10);

		bDetailsCCenter = new Button();
		bDetailsCCenter.textProperty().bind(I18N.createStringBinding("btn.detailsC"));
		bDetailsCCenter.setPrefSize(100, 30);
		bDetailsCCenter.setTranslateY(10);

		bDetailsCRight = new Button();
		bDetailsCRight.textProperty().bind(I18N.createStringBinding("btn.detailsC"));
		bDetailsCRight.setPrefSize(100, 30);
		bDetailsCRight.setTranslateX(-50);
		bDetailsCRight.setTranslateY(10);

		// creation et initialisation du Button bPlayCard
		bPlayCard = new Button();
		// bPlayCard.setFont(Font.font("Gabriola", FontWeight.BOLD, 80));
		bPlayCard.setPrefWidth(200);
		bPlayCard.setPrefSize(100, 25);
		bPlayCard.textProperty().bind(I18N.createStringBinding("btn.playCard"));

		// creation et emplacement cross
		hbCross = new HBox();
		hbCross.setTranslateY(-10);
		hbCross.setAlignment(Pos.CENTER);
		// creation et initialisation du Button bCross
		bCross = new Button();
		bCross.setPrefSize(50, 50);
		bCross.textProperty().bind(I18N.createStringBinding("btn.cross"));

		// creation et initialisation du Button bCross

		bPlayColumn1 = new Button();
		// bPlayColumn1.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bPlayColumn1.setPrefSize(200, 50);
		bPlayColumn1.textProperty().bind(I18N.createStringBinding("btn.playC1"));

		// creation et initialisation du Button bPlayColumn2
		bPlayColumn2 = new Button();

		// bPlayColumn2.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bPlayColumn2.setPrefSize(200, 50);
		bPlayColumn2.textProperty().bind(I18N.createStringBinding("btn.playC2"));

		// creation et initialisation du Button bPlayColumn3
		bPlayColumn3 = new Button();
		bPlayColumn3.textProperty().bind(I18N.createStringBinding("btn.playC3"));
		// bPlayColumn3.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bPlayColumn3.setPrefSize(200, 50);

		// creation et initialisation du Button bPlayColumn4
		bPlayColumn4 = new Button();
		// bPlayColumn4.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bPlayColumn4.setPrefSize(200, 50);
		bPlayColumn4.textProperty().bind(I18N.createStringBinding("btn.playC4"));

		// creation et initialisation du Button bPlayColumn5
		bPlayColumn5 = new Button();
		// bPlayColumn5.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bPlayColumn5.setPrefSize(200, 50);
		bPlayColumn5.textProperty().bind(I18N.createStringBinding("btn.playC5"));

		// creation et initialisation du Button bPlayColumn6
		bPlayColumn6 = new Button();
		// bPlayColumn6.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bPlayColumn6.setPrefSize(200, 50);
		bPlayColumn6.textProperty().bind(I18N.createStringBinding("btn.playC6"));

		// creation et initialisation du Button bDontPlay
		bDontPlay = new Button();
		bDontPlay.setPrefWidth(200);
		bDontPlay.setPrefSize(100, 30);
		bDontPlay.textProperty().bind(I18N.createStringBinding("btn.dontPlay"));

		PlayedCard = new String();
		// ???

	}

	// On crÃ©Ã© les diffÃ©rents emplacements de l'interface (HBox, VBox), cela
	// permet de structurer la page
	private void creationContainers() {

		// Creation et emplacement de la HBox haute
		bpTop = new BorderPane();

		// Creation et emplacement de la HBox gauche
		hbLeft = new HBox();
		hbLeft.setAlignment(Pos.TOP_LEFT);
		hbLeft.setTranslateX(50);
		hbLeft.setTranslateY(25);
		hbLeft.setPrefSize(500, 50);

		// Creation et emplacement de la HBox centre
		hbCenter = new HBox();
		hbCenter.setAlignment(Pos.CENTER);
		hbCenter.setSpacing(250);

		// Creation et emplacement de la HBox droite
		hbRight = new HBox();
		hbRight.setAlignment(Pos.TOP_RIGHT);

		hb1 = new HBox();
		hb1.setAlignment(Pos.TOP_LEFT);
		hb1.setPrefSize(487, 213);
		hb1.setTranslateX(50);
		hb1.setTranslateY(25);
		hb1.setBorder(outline);

		// Creation et emplacement de la VBox droite
		vbRight = new VBox();
		vbRight.setAlignment(Pos.TOP_RIGHT);

		vb1 = new VBox();
		vb1.setAlignment(Pos.TOP_LEFT);
		// Creation et emplacement de la VBox centre
		vbCenter = new VBox();
		vbCenter.setAlignment(Pos.TOP_CENTER);
		vbCenter.setSpacing(250);
		vbCenter.setTranslateY(75);

		// Creation et emplacement de la VBox gauche
		vbLeft = new VBox();
		vbLeft.setAlignment(Pos.TOP_LEFT);

		// Creation et emplacement de la VBox gaucheC
		vbLeftC = new VBox();
		vbLeftC.setAlignment(Pos.CENTER_LEFT);

		// Creation et emplacement de la VBox centreC
		vbCenterC = new VBox();
		vbCenterC.setAlignment(Pos.CENTER);

		// Creation et emplacement de la VBox droiteC
		vbRightC = new VBox();
		vbRightC.setAlignment(Pos.CENTER_RIGHT);

		// Creation et emplacement de la VBox hbChoiceButtons
		hbChoiceButtons = new HBox();
		vbChoices = new VBox();
		vbChoices.setTranslateY(-10);

		hbBottom = new HBox();
		hbBottom.setAlignment(Pos.CENTER);

		vbPlayColumn1 = new VBox();
		vbPlayColumn1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd;");
		vbPlayColumn1.setPadding(new Insets(10, 50, 50, 50));
		vbPlayColumn2 = new VBox();
		vbPlayColumn2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd;");
		vbPlayColumn2.setPadding(new Insets(10, 50, 50, 50));
		vbPlayColumn3 = new VBox();
		vbPlayColumn3.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd;");
		vbPlayColumn3.setPadding(new Insets(10, 50, 50, 50));
		vbPlayColumn4 = new VBox();
		vbPlayColumn4.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd;");
		vbPlayColumn4.setPadding(new Insets(10, 50, 50, 50));
		vbPlayColumn5 = new VBox();
		vbPlayColumn5.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd;");
		vbPlayColumn5.setPadding(new Insets(10, 50, 50, 50));
		vbPlayColumn6 = new VBox();
		vbPlayColumn6.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd;");
		vbPlayColumn6.setPadding(new Insets(10, 50, 50, 50));
	}

	// On affecte les diffÃ©rents Label, Button, HBox, VBox dans leurs emplacements
	// respectifs (HBox, VBox)
	private void placementWidgetsInContainers() {
		vbRight.getChildren().addAll(bRules, bParameters);
		vbLeftC.getChildren().addAll(bCardLeft, bDetailsCLeft);
		vbCenterC.getChildren().addAll(bCardCenter, bDetailsCCenter);
		vbRightC.getChildren().addAll(bCardRight, bDetailsCRight);
		hbCenter.getChildren().addAll(vbLeftC, vbCenterC, vbRightC);
		vbCenter.getChildren().add(hbCenter);
		vb1.getChildren().addAll(lFormer, lNameCard, lColumn, lUsername);
		hb1.getChildren().addAll(vb1, bLastCarteUsed);
		vbLeft.getChildren().add(hb1);
		bpTop.setLeft(vbLeft);
		bpTop.setCenter(lRound);
		bpTop.setRight(vbRight);
		hbCross.getChildren().add(bCross);
		vbChoices.getChildren().addAll(hbCross, hbChoiceButtons);
		vbPlayColumn1.getChildren().add(bPlayColumn1);
		vbPlayColumn2.getChildren().add(bPlayColumn2);
		vbPlayColumn3.getChildren().add(bPlayColumn3);
		vbPlayColumn4.getChildren().add(bPlayColumn4);
		vbPlayColumn5.getChildren().add(bPlayColumn5);
		vbPlayColumn6.getChildren().add(bPlayColumn6);
	}

	// On dÃ©finit l'emplacement des HBox et VBox dans l'interface
	private void placementWidgetsInPane() {
		// this.setRight(vbRight);
		this.setCenter(vbCenter);
		// this.setLeft(vbLeft);
		this.setBottom(hbBottom);
		this.setTop(bpTop);
	}
	// Initialise l'interface avec tout les composants necessaires
	// On affecte aussi des actions aux Button pour naviguer entre les diffÃ©rentes
	// interfaces

	private void addAllWidgets() {
		creationWidgets();
		creationContainers();
		placementWidgetsInContainers();
		placementWidgetsInPane();
		// setColumnButtons(cDialogue.getMp().getGame().getNumPlayer());
		// setColumnButtons(6);
		setButtons();
	}

	private void setButtons() {
		// clic sur la carte de gauche
		bCardLeft.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				if (canPlay == true) {
					if (hbBottom.getChildren().contains(vbChoices))
						hbBottom.getChildren().remove(vbChoices);

					setOpacityAnimation(bCardLeft, 1);
					setOpacityAnimation(bCardCenter, .5);
					setOpacityAnimation(bCardRight, .5);

					hbBottom.getChildren().add(vbChoices);

					bPlayColumn1.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent arg0) {
							if (button1) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 1;
								cDialogue.getMp().playInfluenceCard(cardLeft, 0);

								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					// clic sur la colonne 2
					bPlayColumn2.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button2) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 1;
								cDialogue.getMp().playInfluenceCard(cardLeft, 1);

								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn3.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() { // clic
																											// sur
																											// la
																											// colonne
																											// 3
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button3) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 1;
								cDialogue.getMp().playInfluenceCard(cardLeft, 2);

								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn4.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 4
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button4) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 1;
								cDialogue.getMp().playInfluenceCard(cardLeft, 3);

								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn5.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 5
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button5) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 1;
								cDialogue.getMp().playInfluenceCard(cardLeft, 4);

								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn6.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 6
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button6) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 1;
								cDialogue.getMp().playInfluenceCard(cardLeft, 5);

								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bCross.setOnAction(new EventHandler<ActionEvent>() { // clic sur la croix
						@Override
						public void handle(ActionEvent arg0) {
							hbBottom.getChildren().remove(vbChoices);

							setOpacityAnimation(bCardLeft, 1);
							setOpacityAnimation(bCardCenter, 1);
							setOpacityAnimation(bCardRight, 1);
						}
					});

				}

				bCross.setOnAction(new EventHandler<ActionEvent>() { // clic sur la croix
					@Override
					public void handle(ActionEvent arg0) {
						hbBottom.getChildren().remove(vbChoices);
						setOpacityAnimation(bCardLeft, 1);
						setOpacityAnimation(bCardCenter, 1);
						setOpacityAnimation(bCardRight, 1);
					}
				});

			}
		});

		bCardCenter.setOnAction(new EventHandler<ActionEvent>() { // clic sur la carte de gauche
			@Override
			public void handle(ActionEvent arg0) {
				if (canPlay == true) {
					if (hbBottom.getChildren().contains(vbChoices))
						hbBottom.getChildren().remove(vbChoices);

					setOpacityAnimation(bCardLeft, .5);
					setOpacityAnimation(bCardCenter, 1);
					setOpacityAnimation(bCardRight, .5);

					hbBottom.getChildren().add(vbChoices);

					bPlayColumn1.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent arg0) {
							if (button1) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 2;
								cDialogue.getMp().playInfluenceCard(cardCenter, 0);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					// clic sur la colonne 2
					bPlayColumn2.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button2) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 2;
								cDialogue.getMp().playInfluenceCard(cardCenter, 1);

								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn3.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() { // clic
																											// sur
																											// la
																											// colonne
																											// 3
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button3) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 2;
								cDialogue.getMp().playInfluenceCard(cardCenter, 2);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn4.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 4
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button4) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 2;
								cDialogue.getMp().playInfluenceCard(cardCenter, 3);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn5.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 5
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button5) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 2;
								cDialogue.getMp().playInfluenceCard(cardCenter, 4);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn6.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 6
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button6) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 2;
								cDialogue.getMp().playInfluenceCard(cardCenter, 5);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bCross.setOnAction(new EventHandler<ActionEvent>() { // clic sur la croix
						@Override
						public void handle(ActionEvent arg0) {
							hbBottom.getChildren().remove(vbChoices);
							setOpacityAnimation(bCardLeft, 1);
							setOpacityAnimation(bCardCenter, 1);
							setOpacityAnimation(bCardRight, 1);
						}
					});

				}

				bCross.setOnAction(new EventHandler<ActionEvent>() { // clic sur la croix
					@Override
					public void handle(ActionEvent arg0) {
						hbBottom.getChildren().remove(vbChoices);
						setOpacityAnimation(bCardLeft, 1);
						setOpacityAnimation(bCardCenter, 1);
						setOpacityAnimation(bCardRight, 1);
					}
				});

			}
		});

		bCardRight.setOnAction(new EventHandler<ActionEvent>() { // clic sur la carte de gauche
			@Override
			public void handle(ActionEvent arg0) {
				if (canPlay == true) {
					if (hbBottom.getChildren().contains(vbChoices))
						hbBottom.getChildren().remove(vbChoices);

					setOpacityAnimation(bCardLeft, .5);
					setOpacityAnimation(bCardCenter, .5);
					setOpacityAnimation(bCardRight, 1);

					hbBottom.getChildren().add(vbChoices);

					bPlayColumn1.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent arg0) {
							if (button1) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 1;
								cDialogue.getMp().playInfluenceCard(cardRight, 0);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					// clic sur la colonne 2
					bPlayColumn2.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button2) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 3;
								cDialogue.getMp().playInfluenceCard(cardRight, 1);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn3.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() { // clic
																											// sur
																											// la
																											// colonne
																											// 3
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button3) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 3;
								cDialogue.getMp().playInfluenceCard(cardRight, 2);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn4.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 4
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button4) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 3;
								cDialogue.getMp().playInfluenceCard(cardRight, 3);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn5.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 5
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button5) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 3;
								cDialogue.getMp().playInfluenceCard(cardRight, 4);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bPlayColumn6.setOnAction(new EventHandler<ActionEvent>() { // clic sur la colonne 6
						@Override
						public void handle(ActionEvent arg0) {
							// return le coup
							if (button6) {
								hbBottom.getChildren().remove(vbChoices);
								cardPlayed = 3;
								cDialogue.getMp().playInfluenceCard(cardRight, 5);
								setOpacityAnimation(bCardLeft, 1);
								setOpacityAnimation(bCardCenter, 1);
								setOpacityAnimation(bCardRight, 1);
							}
						}
					});

					bCross.setOnAction(new EventHandler<ActionEvent>() { // clic sur la croix
						@Override
						public void handle(ActionEvent arg0) {
							hbBottom.getChildren().remove(vbChoices);
							setOpacityAnimation(bCardLeft, 1);
							setOpacityAnimation(bCardCenter, 1);
							setOpacityAnimation(bCardRight, 1);
						}
					});

				}

				bCross.setOnAction(new EventHandler<ActionEvent>() { // clic sur la croix
					@Override
					public void handle(ActionEvent arg0) {
						hbBottom.getChildren().remove(vbChoices);
						setOpacityAnimation(bCardLeft, 1);
						setOpacityAnimation(bCardCenter, 1);
						setOpacityAnimation(bCardRight, 1);
					}
				});

			}
		});
	}

	public void addObjectiveCardInColumn(ObjectiveCard card, int column) {
		Platform.runLater(() -> {
			ImageView image = card.getPicture();
			image.setFitHeight(50);
			image.setFitWidth(100);

			switch (column) {
			case 1:
				if (vbPlayColumn1.getChildren().size() == 1)
					vbPlayColumn1.getChildren().add(image);
				else {
					vbPlayColumn1.getChildren().remove(vbPlayColumn1.getChildren().get(1));
					vbPlayColumn1.getChildren().add(image);
				}
				break;
			case 2:
				if (vbPlayColumn2.getChildren().size() == 1)
					vbPlayColumn2.getChildren().add(image);
				else {
					vbPlayColumn2.getChildren().remove(vbPlayColumn2.getChildren().get(1));
					vbPlayColumn2.getChildren().add(image);
				}
				break;
			case 3:
				if (vbPlayColumn3.getChildren().size() == 1)
					vbPlayColumn3.getChildren().add(image);
				else {
					vbPlayColumn3.getChildren().remove(vbPlayColumn3.getChildren().get(1));
					vbPlayColumn3.getChildren().add(image);
				}
				break;
			case 4:
				if (vbPlayColumn4.getChildren().size() == 1)
					vbPlayColumn4.getChildren().add(image);
				else {
					vbPlayColumn4.getChildren().remove(vbPlayColumn4.getChildren().get(1));
					vbPlayColumn4.getChildren().add(image);
				}
				break;
			case 5:
				if (vbPlayColumn5.getChildren().size() == 1)
					vbPlayColumn5.getChildren().add(image);
				else {
					vbPlayColumn5.getChildren().remove(vbPlayColumn5.getChildren().get(1));
					vbPlayColumn5.getChildren().add(image);
				}
				break;
			case 6:
				if (vbPlayColumn6.getChildren().size() == 1)
					vbPlayColumn6.getChildren().add(image);
				else {
					vbPlayColumn6.getChildren().remove(vbPlayColumn6.getChildren().get(1));
					vbPlayColumn6.getChildren().add(image);
				}
				break;
			}
		});
	}

	public void updateInfoCardPlayed(String pseudo, int colonne, InfluenceCard c) {
		Platform.runLater(() -> {
			returnedCard = true;
			lastCardReturned = c;

			Label newColumn = new Label();
			newColumn.textProperty().bind(I18N.createStringBinding("txt.column"));
			Label newColumn2 = new Label(newColumn.getText() + ": " + column);
			lColumn.setText(newColumn2.textProperty().get());

			newColumn = new Label();
			newColumn.textProperty().bind(I18N.createStringBinding("txt.by"));
			newColumn2 = new Label(newColumn.getText() + " " + pseudo);
			lUsername.setText(newColumn2.textProperty().get());

			switch (c.getRole()) {

			case Ro: // Roi
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.king"));
				break;

			case Re: // Reine
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.queen"));
				break;

			case Ju: // Juliette
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.juliette"));
				break;

			case Al: // Alchimiste
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.alchimiste"));
				break;

			case Md: // Maitre d'armes
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.weaponMaster"));
				break;

			case Se: // Seigneur
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.lord"));
				break;

			case Ma: // Marchand
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.merchant"));
				break;

			case Ca: // Cardinal
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.cardinal"));
				break;

			case Tb: // Troubadour
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.toubador"));
				break;

			case Ex: // Explorateur
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.explorator"));
				break;

			case As: // Assassin
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.assasin"));
				break;

			case Te: // Tempete
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.tempete"));
				break;

			case Tr: // Traitre
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.traitor"));
				break;

			case Ci: // Cape d'invisibilite
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.cloakInvisibility"));
				break;

			case Tm: // Les Trois Mousquetaires
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.ThreeMusketeers"));
				break;

			case Mg: // Magicien
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.wizard"));
				break;

			case So: // Sorciere
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.witch"));
				break;

			case Pr: // Prince
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.prince"));
				break;

			case Ec: // Ecuyer
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.Squire"));
				break;

			case Er: // Ermite
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.hermit"));
				break;

			case Pg: // Petit Geant
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.litleGiant"));
				break;

			case Dr: // Dragon
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.dragon"));
				break;

			case RO: // Romeo
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.romeo"));
				break;

			case Me: // Mendiant
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.beggar"));
				break;

			case SO: // Sosie
				lNameCard.textProperty().bind(I18N.createStringBinding("cd.double"));
				break;

			}

			ImageView image = new ImageView();
			image = c.getFrontCard();
			image.setFitHeight(200);
			image.setFitWidth(200);
			bLastCarteUsed.setGraphic(image);
		});
	}

	public InfluenceCard getLastCardReturned() {
		return lastCardReturned;
	}

	public HandCard(DialogueController cd) {
		cDialogue = cd;

		// Creation de l'image
		Image image1 = Caches.BG;
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1, BackgroundRepeat.NO_REPEAT,
				BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				new BackgroundSize(1.0, 1.0, true, true, false, false));

		Background background = new Background(backgroundimage);
		// On affecte ÃƒÂ  l'ÃƒÂ©cran courant l'image de fond dÃƒÂ©fini
		this.setBackground(background);

		addAllWidgets();

		/*
		 * changeACard(new InfluenceCard(Role.Ec, util.Color.Ble, true), 1);
		 * changeACard(new InfluenceCard(Role.Ro, util.Color.Ble, true), 2);
		 * changeACard(new InfluenceCard(Role.Dr, util.Color.Ble, true), 3);
		 * 
		 * addObjectiveCardInColumn(new ObjectiveCard(Domain.Rel, 2), 1);
		 * addObjectiveCardInColumn(new ObjectiveCard(Domain.Rel, 2), 2);
		 * addObjectiveCardInColumn(new ObjectiveCard(Domain.Rel, 2), 3);
		 * addObjectiveCardInColumn(new ObjectiveCard(Domain.Rel, 2), 4);
		 * addObjectiveCardInColumn(new ObjectiveCard(Domain.Rel, 2), 5);
		 * addObjectiveCardInColumn(new ObjectiveCard(Domain.Rel, 2), 6);
		 * 
		 */
		// hbChoiceButtons.setBorder(outline);

		bRules.setOnAction((event) -> {
			cDialogue.editHistory(screenName);
			cDialogue.showScreen(screenList.COMPUTERRULES);
		});
		bParameters.setOnAction((event) -> {
			cDialogue.editHistory(screenName);
			cDialogue.showScreen(screenList.COMPUTERSETTINGS);
		});
		bLastCarteUsed.setOnAction(event -> {
			if (returnedCard) {
				cDialogue.showScreen(screenList.CARDDETAILS);
				cDialogue.getCardDetails().detailsCard(new InfluenceCard(lastCardReturned.encode(), true));
			}
		});

		this.setVisible(false);
		cDialogue.saveScreen(screenName, this);
	}

	public void setOpacityAnimation(Node data, double opacity) {
		data.setOpacity(opacity);
	}
}
