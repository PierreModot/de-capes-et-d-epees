package computerInterfaces;

import cd.DataController.screenList;
import cd.DialogueController;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class App extends Application {
	
	private StackPane root = new StackPane();
	private Node currentScreen = null;
	DialogueController cDialogue = new DialogueController(this);
	private Scene scene = new Scene(root);
	   
		
	@Override
	public void start(Stage primaryStage) throws Exception {	
		cDialogue.defindMainIHM(this);	
		
		ComputerMenu computer = new ComputerMenu(cDialogue);
		JoinGame join = new JoinGame(cDialogue);
		HandCard hand = new HandCard(cDialogue);
		CardDetails details = new CardDetails(cDialogue);
		EndGameComputer endgame = new EndGameComputer(cDialogue);
		LastCard last = new LastCard(cDialogue);
		ComputerRules rules = new ComputerRules(cDialogue);
		ComputerSettings settings = new ComputerSettings(cDialogue);
		Waiting waiting = new Waiting(cDialogue);
		GameGoalRule goal = new GameGoalRule(cDialogue);
		MaterialRule material = new MaterialRule(cDialogue);
		UnfoldingRule unfolding = new UnfoldingRule(cDialogue);
		EndRoundRule endround = new EndRoundRule(cDialogue);
		EndGameRule endgamerule = new EndGameRule(cDialogue);
		DifferentCardRule different = new DifferentCardRule(cDialogue);
		DifferentCardRule2 different2 = new DifferentCardRule2(cDialogue);
		DifferentCardRule3 different3 = new DifferentCardRule3(cDialogue);
		DenyDemand deny = new DenyDemand(cDialogue);
		
		//ici, on affecte a chaque interface le nom de la page, pour pouvoir ensuite etre rediriger vers la page correspondante
		cDialogue.saveScreen(screenList.COMPUTERMENU, computer);
		cDialogue.saveScreen(screenList.JOINGAME,join);
		cDialogue.saveScreen(screenList.HANDCARD, hand);
		cDialogue.saveScreen(screenList.CARDDETAILS, details);
		cDialogue.saveScreen(screenList.ENDGAMECOMPUTER, endgame);
		cDialogue.saveScreen(screenList.LASTCARD, last);
		cDialogue.saveScreen(screenList.COMPUTERRULES, rules);
		cDialogue.saveScreen(screenList.COMPUTERSETTINGS, settings);
		cDialogue.saveScreen(screenList.WAITING, waiting);
		cDialogue.saveScreen(screenList.GAMEGOALRULE , goal);
		cDialogue.saveScreen(screenList.MATERIALRULE , material);
		cDialogue.saveScreen(screenList.UNFOLDINGRULE , unfolding);
		cDialogue.saveScreen(screenList.ENDROUNDRULE , endround);
		cDialogue.saveScreen(screenList.ENDGAMERULE , endgamerule);
		cDialogue.saveScreen(screenList.DIFFERENTCARDRULE , different);
		cDialogue.saveScreen(screenList.DIFFERENTCARDRULE2 ,different2);
		cDialogue.saveScreen(screenList.DIFFERENTCARDRULE3 , different3);
		cDialogue.saveScreen(screenList.DENYDEMAND , deny);
	
		// Active le mode plein ecran
		primaryStage.setFullScreen(true);
		// Cache le message du plein ecran
		primaryStage.setFullScreenExitHint("");
		
		// On affecte dans "root" toutes les interfacesOrdi
		root.getChildren().add(computer);
		root.getChildren().add(join);
		root.getChildren().add(hand);
		root.getChildren().add(details);
		root.getChildren().add(endgame);
		root.getChildren().add(last);
		root.getChildren().add(rules);
		root.getChildren().add(settings);
		root.getChildren().add(waiting);
		root.getChildren().add(goal);
		root.getChildren().add(material);
		root.getChildren().add(unfolding);
		root.getChildren().add(endround);
		root.getChildren().add(endgamerule);
		root.getChildren().add(different);
		root.getChildren().add(different2);
		root.getChildren().add(different3);
		root.getChildren().add(deny);
		
		// Pour le lancement du jeu, on affiche par defaut l'interface MenuOrdinateur
		// Ce sera donc la premiere interface que verra l'utilisateur
		cDialogue.showScreen(screenList.COMPUTERMENU);
		
		primaryStage.setScene(scene);
		primaryStage.show();		
	}
		
	public Object getCurrentScreen() {
	    return currentScreen.getClass();
    }

	public void setCurrentScreen(Node currentScreen) {
		this.currentScreen = currentScreen;
	}

	// Permet d'afficher à l'ecran l'interface correspondante
	public void showScreen(Node n) {
		if (currentScreen != null)
			currentScreen.setVisible(false);
		n.setVisible(true);
		currentScreen = n;
	}
		
	// permet le lancement de l'interface choisi par defaut
	public static void lancement(String[] args) {
	    App.launch(args);
	}		
		
	public StackPane getRoot() {
		return root;
	}
}
