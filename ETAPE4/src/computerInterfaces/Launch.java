package computerInterfaces;

// Ici, on va pouvoir lancer l'InterfaceOrdi avec App
// Par defaut, la premiere interface à se lancer est MenuOrdinateur

public class Launch {
	public static void main(String[] args) {
	    App.lancement(args);
	}
}
