package computerInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Waiting extends BorderPane {
	
	private DialogueController cDialogue = null;
	//On donne un nom a cette interface pour permettre l'identification par le systeme
	private static final screenList nameScreen = screenList.WAITING;
	Label lTitle;
	Label MessageA;
	
	HBox hbTop;
	HBox hbCentre;

	VBox vbCentre;
	VBox vbBottom;
	
	
	private void creationWidgets() {
		lTitle = new Label();
		lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 120));
		lTitle.textProperty().bind(I18N.createStringBinding("lb.title"));
		lTitle.setPadding(new Insets(35));
		
		MessageA = new Label();
		MessageA.setFont(Font.font("Gabriola", FontWeight.BOLD, 80));
		MessageA.textProperty().bind(I18N.createStringBinding("lb.messWait"));
		
		

	}
	// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
	private void creationContainers() {

		// Creation et emplacement de la HBox haute
		hbTop = new HBox();
		hbTop.setAlignment(Pos.CENTER);
	
		// Creation et emplacement de la HBOX centre
		hbCentre = new HBox();
		hbCentre.setAlignment(Pos.CENTER);
		
		// Creation et emplacement de la VBOX centre
		vbCentre = new VBox();
		vbCentre.setAlignment(Pos.CENTER);	
		
		// Creation et emplacement de la VBOX basse
		vbBottom = new VBox();
		vbBottom.setAlignment(Pos.BOTTOM_CENTER);
	}	
	
	// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
	private void placementWidgetInContainers() {
		hbTop.getChildren().add(lTitle);
		vbCentre.getChildren().addAll(MessageA);
	}	

	// On définit l'emplacement des HBox et VBox dans l'interface
	private void placementContainersInPane() {
		this.setTop(hbTop);	
		this.setCenter(vbCentre);
		this.setBottom(vbBottom);
	}

	
	
	// Initialise l'interface avec tout les composants necessaires
	// On affecte aussi des actions aux Button pour naviguer entre les différentes interfaces
	public Waiting(DialogueController cd) throws FileNotFoundException {
		cDialogue = cd;
		
		//Creation de l'image
		Image image1 = Caches.BG;
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1,
		BackgroundRepeat.NO_REPEAT,
		BackgroundRepeat.NO_REPEAT,
		BackgroundPosition.DEFAULT,
		new BackgroundSize(1.0, 1.0, true, true, false, false));
	
		Background background = new Background(backgroundimage);
		//On affecte ÃƒÂ  l'ÃƒÂ©cran courant l'image de fond dÃƒÂ©fini
		this.setBackground(background);
		
		
		//this.setBackground(new Background(new BackgroundFill(Color.WHITE,CornerRadii.EMPTY,null)));
		creationWidgets();
		creationContainers();
		placementWidgetInContainers();
		placementContainersInPane();
		

		this.setVisible(false);
		cDialogue.saveScreen(nameScreen, this);
	}
	
	

}
