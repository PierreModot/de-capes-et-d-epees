package computerInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import cd.DialogueController;
import cd.I18N;
import gamePart.Caches;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ComputerMenu extends BorderPane{

	private DialogueController cDialogue = null;
	//On donne un nom a cette interface pour permettre l'identification par le systeme
	private static final screenList screenName = screenList.COMPUTERMENU;

	HBox hbTop;

	Label lPseudo;

	VBox vbBottom;
	VBox vbLeft;
	VBox vbRight;
	VBox vbCenter;
	VBox vbPseudo;

	Button bJoinGame;
	Button bSettings;
	Button bRules;
	Button bLeave;

	FileInputStream inpTitle;
	Image imageTitle;
	ImageView imageViewTitle;

	TextField tfPseudoText;

	// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
	private void createContainers() {

		//***HBOX***//
		//creation et initialisation de la HBox hbTop
		hbTop = new HBox();
		hbTop.setAlignment(Pos.CENTER);
		
		//***VBOX***//
		//creation et initialisation de la VBox vbBottom
		vbBottom = new VBox();
		vbBottom.setAlignment(Pos.BOTTOM_CENTER);
			
		//creation et initialisation de la VBox vbLeft
		vbLeft = new VBox();
		vbLeft.setAlignment(Pos.BOTTOM_CENTER);

		//creation et initialisation de la VBox vbRight
		vbRight = new VBox();
		vbRight.setAlignment(Pos.BOTTOM_CENTER);

		//creation et initialisation de la VBox vbCenter
		vbCenter = new VBox();
		vbCenter.setAlignment(Pos.CENTER);
		vbCenter.setSpacing(100);
			
		//creation et initialisation de la VBox vbPseudo
		vbPseudo = new VBox();
		vbPseudo.setAlignment(Pos.CENTER);
	}

	// On créé les différents composants de l'interface (Label, Button)
	private void createWidgets() throws FileNotFoundException {		
		//***BOUTONS***//			
		//creation et initialisation du Button bJoinGame
		bJoinGame = new Button();
		bJoinGame.setFont(Font.font("Gabriola", FontWeight.BOLD, 80));
		bJoinGame.setAlignment(Pos.CENTER);
		bJoinGame.textProperty().bind(I18N.createStringBinding("btn.joinGame"));
		bJoinGame.setPrefWidth(130);
		bJoinGame.setPrefSize(600, 100);
		bJoinGame.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bJoinGame.setTranslateY(-30);

		//creation et initialisation du Button bSettings
		bSettings = new Button();
		bSettings.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bSettings.textProperty().bind(I18N.createStringBinding("btn.settings"));
		bSettings.setPrefSize(200, 50);
		bSettings.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bSettings.setTranslateX(-50);
			
		//creation et initialisation du Button bRules
		bRules = new Button();
		bRules.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bRules.textProperty().bind(I18N.createStringBinding("btn.rules"));
		bRules.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bRules.setPrefSize(200, 50);
		bRules.setTranslateX(50);
		
		//creation et initialisation du Button 
		bLeave = new Button();
		bLeave.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bLeave.textProperty().bind(I18N.createStringBinding("btn.leave"));
		bLeave.setPrefSize(300, 50);
		bLeave.setPrefWidth(200);
		bLeave.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bLeave.setTranslateY(-38);
		
		//creation et initialisation du TextField tfPseudoText 
		tfPseudoText = new TextField();
		tfPseudoText.setPromptText("Rentrer votre pseudo");
		tfPseudoText.setFont(Font.font("Gabriola", FontWeight.BOLD,40));
		tfPseudoText.setMaxWidth(700);
		tfPseudoText.setAlignment(Pos.CENTER);

		//***LABEL***//		
		//creation et initialisation de l'image Titre	
		inpTitle = new FileInputStream(("./src/ressources/title.png"));
			
		imageTitle  = new Image(inpTitle, 1000, 470, false, true);
		imageViewTitle = new ImageView(imageTitle);
			
		//creation et initialisation du Label lPseudo
		lPseudo = new Label();
		lPseudo.setFont(Font.font("Gabriola", FontWeight.BOLD, 80));
		lPseudo.textProperty().bind(I18N.createStringBinding("lb.pseudo"));
	}
	
	// On affecte les différents Label, Button, HBox, VBox dans leurs emplacements respectifs (HBox, VBox)
	private void placementWidgetsInContainers() {	
		hbTop.getChildren().add(imageViewTitle);
		vbPseudo.getChildren().addAll(lPseudo, tfPseudoText);
		vbCenter.getChildren().addAll(vbPseudo, bJoinGame);		
		vbBottom.getChildren().add(bLeave);
		vbLeft.getChildren().add(bRules);
		vbRight.getChildren().add(bSettings);
	}

	// On définit l'emplacement des HBox et VBox dans l'interface
	private void placementContainersInPane() {
		
		this.setTop(hbTop);
		this.setBottom(vbBottom);
		this.setLeft(vbLeft);
		this.setRight(vbRight);
		this.setCenter(vbCenter);
	}

	public ComputerMenu(DialogueController cDialogue2) throws FileNotFoundException {

		cDialogue = cDialogue2;
		createWidgets();
		createContainers();
		placementWidgetsInContainers();
		placementContainersInPane();

		Image image1 = Caches.BG;
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1,
		BackgroundRepeat.NO_REPEAT,
		BackgroundRepeat.NO_REPEAT,
		BackgroundPosition.DEFAULT,
		new BackgroundSize(1.0, 1.0, true, true, false, false));
	
		Background background = new Background(backgroundimage);
		//On affecte à l'écran courant l'image de fond défini
		this.setBackground(background);

		bSettings.setOnAction((event) -> {
		cDialogue.getComputerSettings().effectSounds();
		cDialogue.editHistory(screenName);
		cDialogue.showScreen(screenList.COMPUTERSETTINGS);});
	
		bRules.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.COMPUTERRULES);
		});
		
		bJoinGame.setOnAction((event) -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.getMp().getGame().getPlayer().setPseudo(tfPseudoText.getText());
			cDialogue.showScreen(screenList.JOINGAME);
		});

		bLeave.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			Platform.exit();
		});

		this.setVisible(false);
		cDialogue.saveScreen(screenName, this);
	}
		
}

