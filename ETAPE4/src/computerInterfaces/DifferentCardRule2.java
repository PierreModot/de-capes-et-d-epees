package computerInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


	public class DifferentCardRule2 extends BorderPane {

		private DialogueController cDialogue = null;
		//On donne un nom a cette interface pour permettre l'identification par le systeme
		private static final screenList nameScreen = screenList.DIFFERENTCARDRULE2;
		
			HBox Htop;

			Label lTitle;
		
			VBox Vbottom;
			VBox Vleft;
			VBox Vright;
			
			VBox Vmiddle;
			HBox HmiddleBot;

			Button bBack1;
			Button bBack2;
			
			Button bnext;
			Button bback;
			
			//2eme partie
			Label lstorm;
			Label ltraitor;
			Label lic;
			
			FileInputStream inp23;
			Image i23;
			ImageView iv23;
			FileInputStream inp24;
			Image i24;
			ImageView iv24;
			FileInputStream inp25;
			Image i25;
			ImageView iv25;

			VBox v2;
			HBox h23;
			HBox h24;
			HBox h25;
			
			//3eme partie
			Label lt3;
			Label ld3;
			
			Label lthreem;
			Label lwizard;
			
			FileInputStream inp3;
			Image i3;
			ImageView iv3;
			FileInputStream inp31;
			Image i31;
			ImageView iv31;
			
			VBox v3;
			HBox h31;
			HBox h32;
			
			Label vide;
			
			// On créé les differents composants de l'interface (Label, Button)
		    private void createWidgets() throws FileNotFoundException {
				//***LABEL***//
		    	//creation et initialisation du Label lTitle
				lTitle = new Label();
				lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 200));
				lTitle.textProperty().bind(I18N.createStringBinding("lb.titleDcr"));
		    	
				
				//creation et initialisation du Button bBack1
				bBack1 = new Button();
				bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack1.setPrefSize(200, 50);
				bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack1.setTranslateY(-75);
				bBack1.setTranslateX(50);

				// creation et initialisation du Button bBack2
				bBack2 = new Button();
				bBack2.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack2.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack2.setPrefSize(200, 50);
				bBack2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack2.setTranslateY(-75);
				bBack2.setTranslateX(-50);

				
				bnext = new Button(">");
				bnext.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
				bnext.setPrefSize(30, 30);
				bnext.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
				
				bback = new Button("<");
				bback.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
				bback.setPrefSize(30, 30);
				bback.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
			    
			    //création et initialisation de tous les elements de la 2eme partie
				
				lstorm= new Label();
				lstorm.setAlignment(Pos.CENTER);
				lstorm.setFont(Font.font("Cambria", 20));
				lstorm.textProperty().bind(I18N.createStringBinding("lb.storm"));
				
				ltraitor= new Label();
				ltraitor.setAlignment(Pos.CENTER);
				ltraitor.setFont(Font.font("Cambria", 20));
				ltraitor.textProperty().bind(I18N.createStringBinding("lb.traitor"));
				
				lic= new Label();
				lic.setAlignment(Pos.CENTER);
				lic.setFont(Font.font("Cambria", 20));
				lic.textProperty().bind(I18N.createStringBinding("lb.ic"));
				
				inp23 = new FileInputStream("./src/ressources/Rules/storm.png");			
				i23  = new Image(inp23, 80, 80, false, true);
				iv23 = new ImageView(i23);
				
				inp24 = new FileInputStream("./src/ressources/Rules/traitor.png");			
				i24  = new Image(inp24, 80, 80, false, true);
				iv24 = new ImageView(i24);
				
				inp25 = new FileInputStream("./src/ressources/Rules/ic.png");			
				i25  = new Image(inp25, 80, 80, false, true);
				iv25 = new ImageView(i25);
				
				 //création et initialisation de tous les elements de la 2eme partie
				
				ld3= new Label();
			    ld3.setAlignment(Pos.CENTER);
			    ld3.setFont(Font.font("Cambria", FontWeight.BOLD, 20));
			    ld3.textProperty().bind(I18N.createStringBinding("text.d3"));
			    
			    lt3 = new Label();
			    lt3.setAlignment(Pos.CENTER_LEFT);
				lt3.setFont(Font.font("Cambria", FontWeight.BOLD, 20));		
			    lt3.setTextFill(Color.RED);
				lt3.textProperty().bind(I18N.createStringBinding("lb.t3"));
			    
				lthreem= new Label();
				lthreem.setAlignment(Pos.CENTER);
				lthreem.setFont(Font.font("Cambria", 20));
				lthreem.textProperty().bind(I18N.createStringBinding("lb.threem"));
				
				lwizard= new Label();
				lwizard.setAlignment(Pos.CENTER);
				lwizard.setFont(Font.font("Cambria", 20));
				lwizard.textProperty().bind(I18N.createStringBinding("lb.wizard"));
				
				inp3 = new FileInputStream("./src/ressources/cover.png");			
				i3  = new Image(inp3, 80, 80, false, true);
				iv3 = new ImageView(i3);
				
				inp31 = new FileInputStream("./src/ressources/cover.png");			
				i31 = new Image(inp31, 80, 80, false, true);
				iv31 = new ImageView(i31);
				
				vide = new Label();
			}
				
		    
			// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
		    private void createContainers() {

				// CREATION ET CONFIGURATION DE LA HBOX HAUTE/TITRE
				Htop = new HBox();
				Htop.setAlignment(Pos.CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vbottom = new VBox();
				Vbottom.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vleft = new VBox();
				Vleft.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX DROITE
				Vright = new VBox();
				Vright.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX middle
				Vmiddle = new VBox();
				Vmiddle.setAlignment(Pos.TOP_LEFT);
				Vmiddle.setSpacing(15);
				
				HmiddleBot = new HBox();
				HmiddleBot.setAlignment(Pos.BOTTOM_CENTER);
				
				//2eme partie
				v2 = new VBox();
				v2.setAlignment(Pos.CENTER_LEFT);
				v2.setSpacing(15);
				
				h23 = new HBox();
				h23.setAlignment(Pos.CENTER_LEFT);
				h23.setSpacing(10);
				
				h24 = new HBox();
				h24.setAlignment(Pos.CENTER_LEFT);
				h24.setSpacing(10);
				
				h25 = new HBox();
				h25.setAlignment(Pos.CENTER_LEFT);
				h25.setSpacing(10);
				
				//3eme partie
				v3 = new VBox();
				v3.setAlignment(Pos.CENTER_LEFT);
				v3.setSpacing(15);
				
				h31 = new HBox();
				h31.setAlignment(Pos.CENTER_LEFT);
				h31.setSpacing(10);
				
				h32 = new HBox();
				h32.setAlignment(Pos.CENTER_LEFT);
				h32.setSpacing(10);
				
				
		    }

			// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
		    private void placementWidgetsInContainers() {
				
				// ON AJOUTE TITRE DANS TOP
				Htop.getChildren().add(lTitle);
								
				// ON AJOUTE REGLES DU JEU DANS VGAUCHE
				Vleft.getChildren().add(bBack1);

				// ON AJOUTE PARAMETRE DANS VDROITE
				Vright.getChildren().add(bBack2);
				
				//2eme partie
				h23.getChildren().add(iv23);
				h23.getChildren().add(lstorm);
				
				h24.getChildren().add(iv24);
				h24.getChildren().add(ltraitor);
				
				h25.getChildren().add(iv25);
				h25.getChildren().add(lic);
				
				v2.getChildren().add(h23);
				v2.getChildren().add(h24);
				v2.getChildren().add(h25);
				
				//3eme partie
				h31.getChildren().add(iv3);
				h31.getChildren().add(lthreem);
				
				h32.getChildren().add(iv31);
				h32.getChildren().add(lwizard);
				
				v3.getChildren().add(ld3);
				v3.getChildren().add(h31);
				v3.getChildren().add(h32);
				v3.getChildren().add(vide);
				
				// ON AJOUTE PARAMETRE DANS VMIDDLE
				Vmiddle.getChildren().add(v2);
				Vmiddle.getChildren().add(lt3);
				Vmiddle.getChildren().add(v3);
				
				HmiddleBot.getChildren().add(bback);
				HmiddleBot.getChildren().add(bnext);
				Vmiddle.getChildren().add(HmiddleBot);

			}

			// On définit l'emplacement des HBox et VBox dans l'interface
		    private void placementContainersInPane() {
			
				// ON ASSIGNE A CHAQUE BOX LEUR EMPLACEMENT
				this.setTop(Htop);
				this.setBottom(Vbottom);
				this.setLeft(Vleft);
				this.setRight(Vright);
				this.setCenter(Vmiddle);
			}

			
		public DifferentCardRule2(DialogueController cDialogue2) throws FileNotFoundException{

			cDialogue = cDialogue2;
			
			createWidgets();
		    createContainers();
		    placementWidgetsInContainers();
		    placementContainersInPane();
			
			//Creation de l'image
			Image image1 = Caches.BG;
			// Creation de l'image en arriere plan/ image de fond
			BackgroundImage backgroundimage = new BackgroundImage(image1,
			BackgroundRepeat.NO_REPEAT,
			BackgroundRepeat.NO_REPEAT,
			BackgroundPosition.DEFAULT,
			new BackgroundSize(1.0, 1.0, true, true, false, false));
		
			Background background = new Background(backgroundimage);
			//On affecte à l'écran courant l'image de fond défini
			this.setBackground(background);

			bBack1.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.COMPUTERRULES);
		});



			bBack2.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.COMPUTERRULES);
		});
			
			
			
			bback.setOnAction(event -> {
				cDialogue.getComputerSettings().effectSounds();
				cDialogue.showScreen(screenList.DIFFERENTCARDRULE);
		});
			
			bnext.setOnAction(event -> {
				cDialogue.getComputerSettings().effectSounds();
				cDialogue.showScreen(screenList.DIFFERENTCARDRULE3);
		});

			this.setVisible(false);
			cDialogue.saveScreen(nameScreen, this);

		}
			
	}





