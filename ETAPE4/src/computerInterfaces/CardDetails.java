package computerInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import card.InfluenceCard;
import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import util.Color;
import util.Role;


public class CardDetails extends BorderPane {
	
	private DialogueController cDialogue = null;
	//On donne un nom a cette interface pour permettre l'identification par le systeme
	private static final screenList screenName = screenList.CARDDETAILS;
	
	// dÃ©clarations des titres
	Label title;
	Label empty;
	Label resum;
	Label titleCard;
	
	// dÃ©clarations buttons
	Button bBack;
	Button bBack1;
	
	//creation des cartes
	Rectangle card;
	
	// texte de decriptions des cartes
	Text descriptionCard;
	
	HBox hbCenter;
	VBox vbCard;
	VBox vbRight;
	VBox vbCenter;
	VBox vbLeft;
	VBox vbTop;

	// MÃ©thode permettant d'attribuer la description associÃ© Ã  la carte choisi
	public void detailsCard(InfluenceCard influenceCard) {
		cDialogue.showScreen(screenList.CARDDETAILS);
		ImageView image = influenceCard.getFrontCard();
		image.setFitHeight(500);
		image.setFitWidth(500);
		vbCard.getChildren().remove(0);
		vbCard.getChildren().add(image);
		
		
		titleCard.textProperty().set(influenceCard.getTitle().getText());
		descriptionCard.textProperty().set(influenceCard.getDetails().getText());
		}
	
	// On crÃ©Ã© les diffÃ©rents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
	private void createContainers() {

		//creation et initialisation du HBox hbCenter
		hbCenter = new HBox();
		hbCenter.setAlignment(Pos.CENTER);
		hbCenter.setSpacing(150);
		hbCenter.setPrefSize(1000, 500);
		
		//creation et initialisation du VBox vbRight
		vbRight = new VBox();
		vbRight.setAlignment(Pos.BOTTOM_RIGHT);

		//creation et initialisation du VBox vbRight
		vbCenter = new VBox();
		vbCenter.setAlignment(Pos.CENTER);
		vbCenter.setSpacing(80);
		vbCenter.setTranslateX(100);
		//vbCenter.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");

		//creation et initialisation du VBox vbLeft
		vbLeft = new VBox();
		vbLeft.setAlignment(Pos.BOTTOM_LEFT);

		
		// CrÃ©ation et initialisation du VBox vbTop
		vbTop = new VBox();
		vbTop.setAlignment(Pos.TOP_CENTER);	
		
		// CrÃ©ation et initialisation du HBox hCard
		vbCard = new VBox(0);
		vbCard.setAlignment(Pos.CENTER);	
		vbCard.setTranslateX(200);	
	}

	private void createWidgets() {
		//creation et initialisation du Button back back1
		bBack = new Button();
		bBack.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bBack.setPrefSize(225, 50);
		bBack.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bBack.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack.setTranslateY(-100);
		bBack.setTranslateX(-50);
		
		bBack1 = new Button();
		bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bBack1.setPrefSize(225, 50);
		bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack1.setTranslateY(-100);
		bBack1.setTranslateX(50);
		
		//creation et initialisation du Rectangle card	
		card = new Rectangle();
		card.setWidth(600);
		card.setHeight(600);
		card.setArcWidth(20);
		card.setArcHeight(20);
							
		//creation et initialisation du Text descriptionCard 
		descriptionCard = new Text();
		descriptionCard.setFont(Font.font("Gabriola", 30));
		descriptionCard.setTextAlignment(TextAlignment.CENTER);
		descriptionCard.setTranslateY(-100);
		//descriptionCard.textProperty().bind(I18N.createStringBinding("btn.descripCard"));

		// CrÃ©ation et initialisation du label titleCard
		titleCard = new Label();
		titleCard.setFont(Font.font("Gabriola", 40));
		titleCard.setTextAlignment(TextAlignment.CENTER);
		titleCard.setTranslateY(-50);
			
		// CrÃ©ation et initialisation du Label title
		title = new Label("Description de la carte");
		title.setFont(Font.font("Gabriola", 60));
		title.setTextAlignment(TextAlignment.CENTER);
		title.setTranslateY(100);
	}

	// On affecte les diffÃ©rents Label, Button, HBox, VBox, Text, Rectangle dans leurs emplacements respectifs (HBox, VBox)
	private void placementWidgetsInContainers() {
		vbTop.getChildren().add(title);
		vbCenter.getChildren().addAll(titleCard, descriptionCard);
		vbCard.getChildren().add(card);
		hbCenter.getChildren().addAll(vbCenter, vbCard);
		vbRight.getChildren().add(bBack);
		vbLeft.getChildren().add(bBack1);
	}
		
	// On dÃ©finit l'emplacement des HBox et VBox dans l'interface
	private void placementContainersInPane() {
		this.setTop(vbTop);
		this.setCenter(hbCenter);	
		this.setRight(vbRight);
		this.setLeft(vbLeft);
	}

	// Initialise l'interface avec tout les composants necessaires
	public CardDetails(DialogueController cd) throws FileNotFoundException {	
		cDialogue = cd;

		//Creation de l'image
		Image image1 = Caches.BG;
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1,
		BackgroundRepeat.NO_REPEAT,
		BackgroundRepeat.NO_REPEAT,
		BackgroundPosition.DEFAULT,
		new BackgroundSize(1.0, 1.0, true, true, false, false));
	
		Background background = new Background(backgroundimage);
		//On affecte ÃƒÆ’Ã‚Â  l'ÃƒÆ’Ã‚Â©cran courant l'image de fond dÃƒÆ’Ã‚Â©fini
		this.setBackground(background);
		
		createWidgets();
		createContainers();
		placementWidgetsInContainers();
		placementContainersInPane();

		InfluenceCard I = new InfluenceCard(Role.Ro, Color.Ble, true);
		detailsCard(I);
			
		// On affecte des actions aux Button pour naviguer entre les diffÃ©rentes interfaces et effectuer des actions sur l'interface courante
		bBack.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.HANDCARD);
		});			

		bBack1.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.HANDCARD);
		});	
		
		this.setVisible(false);
		cDialogue.saveScreen(screenName, this);
	}
}
