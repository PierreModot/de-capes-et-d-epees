package computerInterfaces;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import java.io.FileNotFoundException;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import java.io.FileInputStream;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;


public class LastCard extends BorderPane {
	
	private DialogueController cDialogue = null;
	//On donne un nom a cette interface pour permettre l'identification par le systeme
	private static final screenList screenName = screenList.LASTCARD;
	
	VBox vbDroite;
	VBox vb;
	HBox hbCenter;
	VBox vbCenter;

	Label lTitle;
	Label lEmpty;
	Label lSum;
	Button bCross;
	Rectangle rCard;
	Text cardDescription;
	
	// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
	private void creationContainers() {
		
		//creation et initialisation de la VBox vbDroite	
		vbDroite = new VBox();
		vbDroite.setAlignment(Pos.TOP_RIGHT);
		
		//creation et initialisation de la VBox vb
		vb = new VBox();
		vb.setAlignment(Pos.TOP_CENTER);
		vb.setSpacing(60);

		//creation et initialisation de la HBox hbCenter
		hbCenter = new HBox();
		hbCenter.setAlignment(Pos.CENTER);
		hbCenter.setSpacing(50);

		//creation et initialisation de la VBox vbCenter
		vbCenter = new VBox();
		vbCenter.setAlignment(Pos.BASELINE_CENTER);
	}
		

	// On créé les différents composants de l'interface (Label, Button)
	private void creationWidgets() {

		//creation et initialisation du Label lTitle 
		lTitle = new Label();
		lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 100));
		lTitle.textProperty().bind(I18N.createStringBinding("lb.titleLc"));
		//creation et initialisation du Rectangle rCard 
		rCard = new Rectangle(0, 0, 500, 500);
			
		//creation et initialisation du Label lSum 
		lSum = new Label();
		lSum.setFont(Font.font("Gabriola", 40));
		lSum.textProperty().bind(I18N.createStringBinding("lb.sum"));
				
		//creation et initialisation du Label lEmpty 
		lEmpty = new Label();
				
		//creation et initialisation du Text cardDescription 
		cardDescription = new Text();
		cardDescription.setFont(Font.font("Gabriola", 40));
		cardDescription.textProperty().bind(I18N.createStringBinding("txt.cardDescrip"));

		//creation et initialisation du Button bCross
		bCross = new Button();
		bCross.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		bCross.textProperty().bind(I18N.createStringBinding("btn.cross"));
		bCross.setPrefWidth(10);
		bCross.setPrefSize(20, 20);

	}
		
	// On affecte les différents Label, Button, HBox, VBox, Text dans leurs emplacements respectifs (HBox, VBox)
	private void placementWidgetsInContainers() {
		vbCenter.getChildren().add(lSum);
		vbCenter.getChildren().add(lEmpty);
		vbCenter.getChildren().add(cardDescription);
		hbCenter.getChildren().add(vbCenter);
		hbCenter.getChildren().add(rCard);
		vbDroite.getChildren().add(bCross);
		vb.getChildren().add(lTitle);
		vb.getChildren().add(hbCenter);
	}

	// On définit l'emplacement des VBox dans l'interface
	private void placementContainersInPane() {

		this.setCenter(vb);
		this.setRight(vbDroite);
	}

	// Initialise l'interface avec tout les composants necessaires
	// On affecte aussi des actions aux Button pour naviguer entre les différentes interfaces et effectuer des actions dans l'interface courante
	public LastCard(DialogueController cd) {
		
		cDialogue = cd;


		// Creation de l'image
		Image image1 = Caches.BG;
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1, BackgroundRepeat.NO_REPEAT,
				BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				new BackgroundSize(1.0, 1.0, true, true, false, false));

		Background background = new Background(backgroundimage);
		// On affecte ÃƒÂ  l'ÃƒÂ©cran courant l'image de fond dÃƒÂ©fini
		this.setBackground(background);

		creationWidgets();
		creationContainers();
		placementWidgetsInContainers();
		placementContainersInPane();

		bCross.setOnAction(event -> {
				cDialogue.getComputerSettings().effectSounds();
				cDialogue.showScreen(screenList.HANDCARD);
			});
				
		this.setVisible(false);
		cDialogue.saveScreen(screenName, this);
	}
}
