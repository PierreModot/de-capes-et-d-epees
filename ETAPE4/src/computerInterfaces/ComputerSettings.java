package computerInterfaces;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import card.InfluenceCard;
import card.ObjectiveCard;
import cd.DataController;
import cd.DataController.screenList;
import cd.DialogueController;
import cd.I18N;
import cd.I18N.AppLanguage;
import gamePart.Caches;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.StringConverter;

public class ComputerSettings extends BorderPane {
	
	private DialogueController cDialogue = null;
	private static final screenList screenName = screenList.COMPUTERSETTINGS;
	
	//declaration Label
	Label lTitle;
	Label lTitleLanguage;
	Label lCardStyle;
	
	//declaration Button de Bottom
	Button bBack;
	Button bBack1;
	
	//declaration Vbox
	VBox vbLeft;
	VBox vbRight;
	VBox vbCenter;
	VBox vbLanguages;
	VBox vbCStyle;
	
	//declaration Hbox
	HBox hbTop;
	HBox hbCenter;
	HBox hbBottomRight;
	HBox hbBottomLeft;
	HBox hbGroupLanguages;
	HBox hbGroupeCStyle;
	
	
	//declaration TextField
	TextField tfPVolumeMusics;
	TextField tfVolumeMusics;
	
	//declaration Button choix de langue
	Button bFrenchLanguage;
	Button bEnglishLanguage;
	Button bEspagnolLanguage;
	
	
	
	//Boutton Styles de cartes
	Button bCSClassic;
	Button bCSSpecial;
	
	//Button restaurer paramètre par defaut
	Button bRestore;
	VBox vbRestore;
	

	Slider sEffectSound;
	HBox hbEffectSounds;
	VBox vbSongs;
	Label lTitleSong;
	Label lEffectSound;
	
	private Border bSelected = new Border(new BorderStroke(Color.AQUA, 
			BorderStrokeStyle.SOLID,
			new CornerRadii(5),
			new BorderWidths(3),
			new Insets(0)
			));
	
	private Border bUnselected = new Border(new BorderStroke(Color.AQUA, 
			BorderStrokeStyle.NONE,
			new CornerRadii(5),
			new BorderWidths(3),
			new Insets(0)
			));
	
	private void widgetsCreation() {
		lTitle = new Label ();
		lTitle.textProperty().bind(I18N.createStringBinding("texte.parametre"));
		lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 120));

		lEffectSound = new Label();
		lEffectSound.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		lEffectSound.setPrefWidth(300);
		lEffectSound.textProperty().bind(I18N.createStringBinding("lb.sound")); 
		
		bBack = new Button();
		bBack.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bBack.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack.setPrefSize(200, 50);
		bBack.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bBack.setTranslateY(-75);
		bBack.setTranslateX(50);
				
		bBack1 = new Button();
		bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
		bBack1.setPrefSize(200, 50);
		bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bBack1.setTranslateY(-75);
		bBack1.setTranslateX(-50);
		
		bRestore = new Button();
		bRestore.setFont(Font.font("Gabriola", FontWeight.BOLD, 30)); 
		bRestore.textProperty().bind(I18N.createStringBinding("btn.restore"));
		bRestore.setPrefSize(300, 50);	
		bRestore.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
		bRestore.addEventHandler(MouseEvent.MOUSE_ENTERED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bRestore.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
	              }
	            });

		bRestore.addEventHandler(MouseEvent.MOUSE_EXITED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bRestore.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	              }
	            });

		bRestore.addEventHandler(MouseEvent.MOUSE_PRESSED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bRestore.setPrefSize(296, 46);	
	            	  bRestore.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
	              }
	            });	

		bRestore.addEventHandler(MouseEvent.MOUSE_RELEASED,
	            new EventHandler<MouseEvent>() {
	              @Override
	              public void handle(MouseEvent e) {
	            	  bRestore.setPrefSize(300, 50);	
	            	  bRestore.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
	              }
	            });	
	}
	
	private void containersCreation() {
		hbTop = new HBox();
		hbTop.setAlignment(Pos.CENTER);
		
		vbLeft = new VBox();
		vbLeft.setAlignment(Pos.BOTTOM_LEFT);
		
		vbCenter = new VBox();
		vbCenter.setAlignment(Pos.TOP_CENTER);
		vbCenter.setSpacing(50);
		
		vbRight = new VBox();
		vbRight.setAlignment(Pos.BOTTOM_RIGHT);
		
		hbCenter = new HBox();
		hbCenter.setAlignment(Pos.CENTER);
		
		hbBottomRight = new HBox();
		hbBottomRight.setAlignment(Pos.BOTTOM_RIGHT);
		
		hbBottomLeft = new HBox();
		hbBottomLeft.setAlignment(Pos.BOTTOM_LEFT);
		
		vbRestore =  new VBox();
		vbRestore.setAlignment(Pos.CENTER);
		
		//lMusicOff = new Label();
		//lMusicOff.setGraphic(new ImageView(ControleurDonnees.VOLUMEON));
		//lMusicOff.setOnMouseClicked(event -> cDialogue.definirVolumeMusic(-1));
	}
	
	private void placementWidgetsInContainers() {
		hbTop.getChildren().add(lTitle);
		hbBottomLeft.getChildren().add(bBack);
		hbBottomRight.getChildren().add(bBack1);
		hbCenter.getChildren().add(vbCenter);
		vbRestore.getChildren().add(bRestore);
	}
	
	private void placementContainersInPane() {
		this.setTop(hbTop);
		this.setRight(vbRight);
		this.setLeft(vbLeft);		
		this.setCenter(hbCenter);
		this.setRight(hbBottomRight);
		this.setLeft(hbBottomLeft);
	}
	


	private void Langues() {
		lTitleLanguage = new Label ();
		lTitleLanguage.textProperty().bind(I18N.createStringBinding("lb.changeLanguage"));
		lTitleLanguage.setFont(Font.font("Gabriola", FontWeight.BOLD,90)); 
	
		bFrenchLanguage = new Button();
		bFrenchLanguage.setFont(Font.font("Gabriola", FontWeight.BOLD, 30)); 
		bFrenchLanguage.textProperty().bind(I18N.createStringBinding("btn.fr"));
		bFrenchLanguage.setPrefSize(200, 50);	
		bFrenchLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");

		bFrenchLanguage.addEventHandler(MouseEvent.MOUSE_ENTERED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
          		bFrenchLanguage.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
              }
            });

		bFrenchLanguage.addEventHandler(MouseEvent.MOUSE_EXITED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bFrenchLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });

		bFrenchLanguage.addEventHandler(MouseEvent.MOUSE_PRESSED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            		bFrenchLanguage.setPrefSize(196, 10);	
            		bFrenchLanguage.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
              }
            });	

		bFrenchLanguage.addEventHandler(MouseEvent.MOUSE_RELEASED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            		bFrenchLanguage.setPrefSize(200, 50);	
            		bFrenchLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });			
	
		bEnglishLanguage = new Button();
		bEnglishLanguage.setFont(Font.font("Gabriola", FontWeight.BOLD, 30));
		bEnglishLanguage.textProperty().bind(I18N.createStringBinding("btn.en"));
		bEnglishLanguage.setPrefSize(200, 50);	
		bEnglishLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
		bEnglishLanguage.addEventHandler(MouseEvent.MOUSE_ENTERED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEnglishLanguage.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
              }
            });

		bEnglishLanguage.addEventHandler(MouseEvent.MOUSE_EXITED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEnglishLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });

		bEnglishLanguage.addEventHandler(MouseEvent.MOUSE_PRESSED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEnglishLanguage.setPrefSize(196, 10);	
            	  bEnglishLanguage.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
              }
            });	

		bEnglishLanguage.addEventHandler(MouseEvent.MOUSE_RELEASED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEnglishLanguage.setPrefSize(200, 50);	
            	  bEnglishLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });
	
		bEspagnolLanguage = new Button();
		bEspagnolLanguage.setFont(Font.font("Gabriola", FontWeight.BOLD, 30)); 
		bEspagnolLanguage.textProperty().bind(I18N.createStringBinding("btn.es"));
		bEspagnolLanguage.setPrefSize(200, 50);	
		bEspagnolLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
		bEspagnolLanguage.addEventHandler(MouseEvent.MOUSE_ENTERED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEspagnolLanguage.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
              }
            });

		bEspagnolLanguage.addEventHandler(MouseEvent.MOUSE_EXITED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEspagnolLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });

		bEspagnolLanguage.addEventHandler(MouseEvent.MOUSE_PRESSED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEspagnolLanguage.setPrefSize(196, 10);	
            	  bEspagnolLanguage.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
              }
            });	

		bEspagnolLanguage.addEventHandler(MouseEvent.MOUSE_RELEASED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bEspagnolLanguage.setPrefSize(200, 50);	
            	  bEspagnolLanguage.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });	

		vbLanguages = new VBox();
		vbLanguages.setAlignment(Pos.CENTER);	
		vbLanguages.setSpacing(20);
	
		hbGroupLanguages = new HBox();
		hbGroupLanguages.setAlignment(Pos.CENTER);
		hbGroupLanguages.getChildren().add(bFrenchLanguage);
		hbGroupLanguages.getChildren().add(bEnglishLanguage);
		hbGroupLanguages.getChildren().add(bEspagnolLanguage);
		hbGroupLanguages.setSpacing(90);
	
		vbLanguages.getChildren().add(lTitleLanguage);
		vbLanguages.getChildren().add(hbGroupLanguages);
	
		vbCenter.getChildren().add(vbLanguages);
	}


	

	private void CStyle() { // dark theme ou white theme
		lCardStyle = new Label();
		lCardStyle.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		lCardStyle.textProperty().bind(I18N.createStringBinding("txt.styleC"));
	
		bCSClassic = new Button();
		bCSClassic.setFont(Font.font("Gabriola", FontWeight.BOLD,30));
		bCSClassic.textProperty().bind(I18N.createStringBinding("btn.styleC"));
		bCSClassic.setPrefSize(200, 50);	
		bCSClassic.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
		bCSClassic.addEventHandler(MouseEvent.MOUSE_ENTERED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bCSClassic.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
              }
            });

		bCSClassic.addEventHandler(MouseEvent.MOUSE_EXITED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bCSClassic.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });

		bCSClassic.addEventHandler(MouseEvent.MOUSE_PRESSED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bCSClassic.setPrefSize(196, 10);	
            	  bCSClassic.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
              }
            });	

		bCSClassic.addEventHandler(MouseEvent.MOUSE_RELEASED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bCSClassic.setPrefSize(200, 50);	
            	  bCSClassic.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });	

		bCSSpecial = new Button();
		bCSSpecial.setFont(Font.font("Gabriola", FontWeight.BOLD,30));
		bCSSpecial.textProperty().bind(I18N.createStringBinding("btn.styleS"));
		bCSSpecial.setPrefSize(200, 50);	
		bCSSpecial.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
		bCSSpecial.addEventHandler(MouseEvent.MOUSE_ENTERED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bCSSpecial.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 1px;");
              }
            });
		bCSSpecial.addEventHandler(MouseEvent.MOUSE_EXITED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bCSSpecial.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });
		bCSSpecial.addEventHandler(MouseEvent.MOUSE_PRESSED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bCSSpecial.setPrefSize(196, 10);	
            	  bCSSpecial.setStyle("-fx-color: #976766; -fx-base: #e8d0a2; -fx-border-color: #a31c12; -fx-border-width: 2px;");
              }
            });	
		bCSSpecial.addEventHandler(MouseEvent.MOUSE_RELEASED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
            	  bCSSpecial.setPrefSize(200, 50);	
            	  bCSSpecial.setStyle("-fx-color: #643433; -fx-base: #e8d0a2; -fx-border-color: #AB9282;");
              }
            });	

		vbCStyle = new VBox();
		vbCStyle.setAlignment(Pos.CENTER);
		vbCStyle.setSpacing(20);
	
		hbGroupeCStyle = new HBox();
		hbGroupeCStyle.setAlignment(Pos.CENTER);
		hbGroupeCStyle.getChildren().add(bCSClassic);
		hbGroupeCStyle.getChildren().add(bCSSpecial);
		hbGroupeCStyle.setSpacing(150);
	
		vbCStyle.getChildren().add(lCardStyle);
		vbCStyle.getChildren().add(hbGroupeCStyle);
		vbCenter.getChildren().addAll(vbCStyle, vbRestore);
	}



	private Button getButtonLangue(Labeled n) {
		if(n.getText().equals(bFrenchLanguage.getText())) return bFrenchLanguage;
		if(n.getText().equals(bEnglishLanguage.getText())) return bEnglishLanguage;
		if(n.getText().equals(bEspagnolLanguage.getText())) return bEspagnolLanguage;
		return null;
	}

	public void definirLangue(Labeled n) {
		bFrenchLanguage.setBorder(bUnselected);
		bEnglishLanguage.setBorder(bUnselected);
		bEspagnolLanguage.setBorder(bUnselected);
		getButtonLangue(n).setBorder(bSelected);
	}

	public void son() {
		lTitleSong = new Label();
		lTitleSong.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		lTitleSong.textProperty().bind(I18N.createStringBinding("lb.change"));
	
		EffectMusic();

		vbSongs = new VBox();
		vbSongs.setAlignment(Pos.CENTER);
		vbSongs.setSpacing(20);

		vbSongs.getChildren().add(lTitleSong);
		vbSongs.getChildren().add(hbEffectSounds);
		vbCenter.getChildren().add(vbSongs);
		
	}


	public void EffectMusic() {
		hbEffectSounds = new HBox();
		hbEffectSounds.setAlignment(Pos.CENTER);
	
		sEffectSound = new Slider(0, 100, DataController.volumeEffect);
		sEffectSound.setPrefWidth(300);
		sEffectSound.setShowTickLabels(true);
		sEffectSound.setShowTickMarks(true);
		sEffectSound.setMajorTickUnit(20);
		sEffectSound.setMinorTickCount(5);
		sEffectSound.setBlockIncrement(10);
	
		tfPVolumeMusics = new TextField("" + DataController.volumeMusique);
		tfPVolumeMusics.setFont(Font.font("Gabriola", FontWeight.BOLD, 20));
		tfPVolumeMusics.setPrefColumnCount(2);
		tfPVolumeMusics.textProperty().bindBidirectional(sEffectSound.valueProperty(), new StringConverter<Number>(){
			@Override
			public String toString(Number t){
				t = t.intValue();
				return t.toString();
			}
			@Override
			public Number fromString(String s){
				int v=0;
				if (s!=null)
				if (!s.equals("")) {
					v = Integer.parseInt(s);
					if (v<0) 
						v=0;
					if (v>100) 
						v=100;
				}
				return v;
			}
		});	
		
		hbEffectSounds.getChildren().addAll(lEffectSound, sEffectSound, tfPVolumeMusics); //lMusicOff en place 2
	
	}

	public void effectSounds() {
	
		Media media = new Media(new File(DataController.buttonEffect).toURI().toString());  
		MediaPlayer mediaPlayer = new MediaPlayer(media);  
		mediaPlayer.setAutoPlay(true);

    	mediaPlayer.volumeProperty().bind(sEffectSound.valueProperty().divide(100));
    	mediaPlayer.setAutoPlay(true);
    	// Quand la valeur du slider change
    	sEffectSound.valueProperty().addListener(event -> {
        
        	// Si 0 eteindre la music
        	if (sEffectSound.getValue() == 0) {
	            mediaPlayer.pause(); 
    	    }
        
        	// Attention la methode stop arrete completement la musique elle est jouer
        	// depuis le debut apres un play et pause juste pause
        	else {
	            mediaPlayer.play();
        	}
		 });
	}

	
	public ComputerSettings(DialogueController cDialogue2) throws FileNotFoundException {
		
		cDialogue = cDialogue2;

		//Creation de l'image
		Image image1 = Caches.BG;
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1,
		BackgroundRepeat.NO_REPEAT,
		BackgroundRepeat.NO_REPEAT,
		BackgroundPosition.DEFAULT,
		new BackgroundSize(1.0, 1.0, true, true, false, false));
	
		Background background = new Background(backgroundimage);
		//On affecte ÃƒÂ  l'ÃƒÂ©cran courant l'image de fond dÃƒÂ©fini
		this.setBackground(background);
		
		widgetsCreation();
		containersCreation();

		placementWidgetsInContainers();
		placementContainersInPane();
		son();
		
		bBack.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(cDialogue2.getVBack());
		});
		
		bBack1.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(cDialogue2.getVBack());
		});

		//sMusic.valueProperty().addListener(event -> cDialogue.definirVolumeMusique((int)sMusic.getValue()));
		
		Langues();
		//definirLangue(new Label (bFrenchLanguage.getText()));
		
		//definirTheme(new Label (bThemeFrancais.getText()));
		
		CStyle();
		
		//lMusiquesOff.setOnMouseClicked(event -> cDialogue.definirVolumeMusique(-1));;
		//sMusiques.valueProperty().addListener(event -> cDialogue.definirVolumeMusique((int)sMusiques.getValue()));
		
		//lEffetsOff.setOnMouseClicked(event -> cDialogue.definirVolumeMusique(-1));;
		//sEffets.valueProperty().addListener(event -> cDialogue.definirVolumeMusique((int)sEffets.getValue()));
		
	
		bFrenchLanguage.setOnMouseClicked(event -> {
			I18N.setLocal(AppLanguage.FRANCAIS);
			effectSounds();	
		});
		
		bEnglishLanguage.setOnMouseClicked(event -> {
			I18N.setLocal(AppLanguage.ANGLAIS);
			effectSounds();
		});
		
		bEspagnolLanguage.setOnMouseClicked(event -> {
			I18N.setLocal(AppLanguage.ESPAGNOL);
			effectSounds();
		});
		
		
		//Boutton Styles de cartes
		bCSClassic.setOnMouseClicked(event -> {
			// Ã  ajouter ici plus tard pour modif carte
			effectSounds();
			DataController.theme = false;
		});
		
		bCSSpecial.setOnMouseClicked(event -> {
			// Ã  ajouter ici plus tard pour modif carte
			effectSounds();
			DataController.theme = true;
		});
	
		bRestore.setDefaultButton(true);
		bRestore.setOnAction(event -> {
			effectSounds();
			DataController.theme = false;
			sEffectSound.setValue(DataController.volumeEffect);
			I18N.setLocal(AppLanguage.FRANCAIS);
			//I18N.setLocal(AppCard.CLASSIQUE);
		});

		this.setVisible(false);
		cDialogue.saveScreen(screenName, this);
	}
}
