package computerInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class DenyDemand extends BorderPane {
	
	private DialogueController cDialogue = null;
	
	private static final screenList nameScreen = screenList.DENYDEMAND;
		
	Label ldeny;
	Button bBack;
	
	FileInputStream sad;
	
	Image img;
	ImageView imgV;
	
	
	HBox Htop;
	HBox Hbottom;
	HBox Hcenter;
	
	// On créé les differents composants de l'interface (Label, Button)
    private void createWidgets() throws FileNotFoundException {
    	
    	//Label "Vous avez été refusé"
    	ldeny = new Label();
    	ldeny.setFont(Font.font("Gabriola", FontWeight.EXTRA_BOLD , 110));
    	ldeny.textProperty().bind(I18N.createStringBinding("txt.deny"));
    	ldeny.setTranslateY(100);
    	
    	bBack = new Button();
    	bBack.textProperty().bind(I18N.createStringBinding("btn.back"));
    	bBack.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bBack.setPrefSize(300, 50);
		bBack.setPrefWidth(200);
		bBack.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
    	bBack.setTranslateY(-90);
    	
    	
    	sad = new FileInputStream(("./src/ressources/sad.png"));
    	
    	img  = new Image(sad, 1000, 470, false, true);
		imgV = new ImageView(img);
    	
    }
    
    private void createContainers() {
    	
    	// CREATION ET CONFIGURATION DE LA HBOX HAUTE/TITRE
		Htop = new HBox();
		Htop.setAlignment(Pos.CENTER);

		// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
		Hbottom = new HBox();
		Hbottom.setAlignment(Pos.BOTTOM_CENTER);
		
		// CREATION ET CONFIGURATION DE LA VBOX CENTRE
		Hcenter = new HBox();
		Hcenter.setAlignment(Pos.CENTER);
		
    }
    
    private void placementWidgetsInContainers() {
    	
    	Htop.getChildren().add(ldeny);
    	Hbottom.getChildren().add(bBack);
    	Hcenter.getChildren().add(imgV);
    }
    
    private void placementContainersInPane() {
    	this.setTop(Htop);
    	this.setBottom(Hbottom);
    	this.setCenter(imgV);
    }
    
	public DenyDemand(DialogueController cDialogue2) throws FileNotFoundException {
		
		cDialogue=cDialogue2;
		
		createWidgets();
		createContainers();
		placementWidgetsInContainers();
		placementContainersInPane();
		
		//Creation de l'image
		Image image1 = Caches.BG;
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1,
		BackgroundRepeat.NO_REPEAT,
		BackgroundRepeat.NO_REPEAT,
		BackgroundPosition.DEFAULT,
		new BackgroundSize(1.0, 1.0, true, true, false, false));
	
		Background background = new Background(backgroundimage);
		//On affecte Ã  l'Ã©cran courant l'image de fond dÃ©fini
		this.setBackground(background);
	
		bBack.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.COMPUTERMENU);
		});
			
		this.setVisible(false);
	    cDialogue.saveScreen(nameScreen, this);
    	}
	}
	

