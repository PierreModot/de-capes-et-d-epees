package computerInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


public class ComputerRules extends BorderPane {
	
	private DialogueController cDialogue = null;
	//On donne un nom a cette interface pour permettre l'identification par le systeme
	private static final screenList screenName = screenList.COMPUTERRULES;
	
	// Creation et emplacement d'une Border outline
	Border outline = new Border
					(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1), 
					new Insets(5)));
	
	Label lTitle;
	HBox hbUp;
	HBox hbDownRight;
	HBox hbDownLeft;
	HBox hbCenter;
	VBox vbCenterL;
	VBox vbCenterC;
	VBox vbCenterR;
	Button bBack1;
	Button bBack2;
	Button bGoal;
	Button bMaterial;
	Button bUnfolding;
	Button bEndRound;
	Button bEndGame;
	Button bDifferentCard;

	Image imageTitle;
	ImageView imageViewTitle;

	FileInputStream Title;
	
	// On créé les differents composants de l'interface (Label, Button)
	private void creationWidgets() throws FileNotFoundException {

		//creation et initialisation du Label lTitle
		lTitle = new Label();
		lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 200));
		lTitle.textProperty().bind(I18N.createStringBinding("lb.rules"));

		//creation et initialisation du Button bBack1
		bBack1 = new Button();
		bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
		bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack1.setPrefSize(200, 50);
		bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bBack1.setTranslateY(-75);
		bBack1.setTranslateX(-50);

		// creation et initialisation du Button bBack2
		bBack2 = new Button();
		bBack2.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
		bBack2.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack2.setPrefSize(200, 50);
		bBack2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
		bBack2.setTranslateY(-75);
		bBack2.setTranslateX(50);

		// création des Button des différentes pages de règles
		bGoal = new Button();
		bGoal.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bGoal.textProperty().bind(I18N.createStringBinding("btn.goal"));
		bGoal.setPrefWidth(200);
		bGoal.setPrefSize(380, 250);
		bGoal.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");

		bMaterial = new Button();
		bMaterial.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bMaterial.textProperty().bind(I18N.createStringBinding("btn.material"));
		bMaterial.setPrefWidth(200);
		bMaterial.setPrefSize(380, 250);
		bMaterial.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");

		bUnfolding = new Button();
		bUnfolding.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bUnfolding.textProperty().bind(I18N.createStringBinding("btn.unfolding"));
		bUnfolding.setPrefWidth(200);
		bUnfolding.setPrefSize(380, 250);
		bUnfolding.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");

		bEndRound = new Button();
		bEndRound.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bEndRound.textProperty().bind(I18N.createStringBinding("btn.endRound"));
		bEndRound.setPrefWidth(200);
		bEndRound.setPrefSize(380, 250);
		bEndRound.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");

		bEndGame = new Button();
		bEndGame.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bEndGame.textProperty().bind(I18N.createStringBinding("btn.endGame"));
		bEndGame.setPrefWidth(200);
		bEndGame.setPrefSize(380, 250);
		bEndGame.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");

		bDifferentCard = new Button();
		bDifferentCard.setFont(Font.font("Gabriola", FontWeight.BOLD, 60));
		bDifferentCard.textProperty().bind(I18N.createStringBinding("btn.differendCard"));
		bDifferentCard.setPrefWidth(200);
		bDifferentCard.setPrefSize(380, 250);
		bDifferentCard.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		
		Title = new FileInputStream(("./src/ressources/title.png"));
		
		imageTitle  = new Image(Title, 1000, 470, false, true);
		imageViewTitle = new ImageView(imageTitle);

		// fond d'écran
		//Creation de l'image
		Image image1 = Caches.BG;
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1,
		BackgroundRepeat.NO_REPEAT,
		BackgroundRepeat.NO_REPEAT,
		BackgroundPosition.DEFAULT,
		new BackgroundSize(1.0, 1.0, true, true, false, false));
	
		Background background = new Background(backgroundimage);
		//On affecte à l'écran courant l'image de fond défini
		this.setBackground(background);
	}
	
	// On créé les différents emplacements de l'interface (HBox), cela permet de structurer la page
	private void creationContainers() {

		//creation et initialisation de la HBox hbUp
		hbUp = new HBox();
		hbUp.setAlignment(Pos.TOP_CENTER);
		
		//creation et initialisation de la HBox hbDownRight
		hbDownRight = new HBox();
		hbDownRight.setAlignment(Pos.BOTTOM_RIGHT);
		
		//creation et initialisation de la HBox hbDownLeft
		hbDownLeft = new HBox();
		hbDownLeft.setAlignment(Pos.BOTTOM_LEFT);
		
		//creation et initialisation de la HBox hbCenter
		hbCenter = new HBox();
		hbCenter.setAlignment(Pos.CENTER);
		hbCenter.setSpacing(30);
		
		//creation et initialisation des vbox du centre
		vbCenterL = new VBox();
		vbCenterL.setAlignment(Pos.CENTER);
		vbCenterL.setSpacing(20);
		
		vbCenterC = new VBox();
		vbCenterC.setAlignment(Pos.CENTER);
		vbCenterC.setSpacing(20);
		
		vbCenterR = new VBox();
		vbCenterR.setAlignment(Pos.CENTER);
		vbCenterR.setSpacing(20);
	}
	
	// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox)
	private void placementWidgetsInContainers() {
		hbUp.getChildren().add(lTitle);
		hbDownRight.getChildren().add(bBack1);
		hbDownLeft.getChildren().add(bBack2);
		
		vbCenterL.getChildren().add(bGoal);
		vbCenterL.getChildren().add(bEndRound);
		
		vbCenterC.getChildren().add(bMaterial);
		vbCenterC.getChildren().add(bEndGame);

		vbCenterR.getChildren().add(bUnfolding);
		vbCenterR.getChildren().add(bDifferentCard);
		
		hbCenter.getChildren().add(vbCenterL);
		hbCenter.getChildren().add(vbCenterC);
		hbCenter.getChildren().add(vbCenterR);
	}
	
	// On définit l'emplacement des HBox dans l'interface
	private void placementContainersInPane() {
		this.setTop(hbUp);
		this.setRight(hbDownRight);
		this.setLeft(hbDownLeft);
		this.setCenter(hbCenter);
	}
	
	// Initialise l'interface avec tout les composants necessaires
	// On affecte aussi des actions aux Button pour naviguer entre les différentes interfaces
	public ComputerRules(DialogueController cDialogue2) throws FileNotFoundException {
		cDialogue = cDialogue2;
		this.setBackground(new Background(new BackgroundFill(Color.WHITE,CornerRadii.EMPTY,null)));
		creationWidgets();
		creationContainers();
		placementWidgetsInContainers();
		placementContainersInPane();
		
		bBack1.setOnAction(event -> {
            cDialogue.getComputerSettings().effectSounds();
            cDialogue.showScreen(cDialogue2.getVBack());
        });       
        
        bBack2.setOnAction(event -> {
            cDialogue.getComputerSettings().effectSounds();
            cDialogue.showScreen(cDialogue2.getVBack());
        });        

        bGoal.setOnAction(event -> {
            cDialogue.getComputerSettings().effectSounds();
            cDialogue.showScreen(screenList.GAMEGOALRULE);
        });        
        
        bMaterial.setOnAction(event -> {
            cDialogue.getComputerSettings().effectSounds();
            cDialogue.showScreen(screenList.MATERIALRULE);
        });        
        
        bUnfolding.setOnAction(event -> {
            cDialogue.getComputerSettings().effectSounds();
            cDialogue.showScreen(screenList.UNFOLDINGRULE);
        });
        
        bEndRound.setOnAction(event -> {
            cDialogue.getComputerSettings().effectSounds();
            cDialogue.showScreen(screenList.ENDROUNDRULE);
        });        
        
        bEndGame.setOnAction(event -> {
            cDialogue.getComputerSettings().effectSounds();
            cDialogue.showScreen(screenList.ENDGAMERULE);
        });      
        
        bDifferentCard.setOnAction(event -> {
            cDialogue.getComputerSettings().effectSounds();
            cDialogue.showScreen(screenList.DIFFERENTCARDRULE);
        });

		this.setVisible(false);
		cDialogue.saveScreen(screenName, this);
	}
}
