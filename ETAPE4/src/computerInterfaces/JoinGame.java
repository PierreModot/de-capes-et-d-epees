package computerInterfaces;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class JoinGame extends BorderPane {
	
	private DialogueController cDialogue = null;
	//On donne un nom a cette interface pour permettre l'identification par le systeme
	private static final screenList screenName = screenList.JOINGAME;

	FileInputStream lTitle;
	Label lCode;
	
	HBox hbUp;
	HBox hbCenter;

	VBox vbCenter;
	VBox vbDown;
	VBox vbPlayer;
	VBox vbIdParty;
	
	TextField tfCode;
	
	Button bBack;
	Button bPlay;
	
	Image imageTitle;
	ImageView imageViewTitle;
	
	// On créé les differents composants de l'interface (Label, Button)
	private void creationWidgets() {
		
		//creation et initialisation du Button bBack
		bBack = new Button();
		bBack.setFont(Font.font("Gabriola", FontWeight.BOLD, 40));
		bBack.textProperty().bind(I18N.createStringBinding("btn.back"));
		bBack.setPrefSize(200, 50);
		bBack.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c");
		bBack.setTranslateY(-38);
		
		//creation et initialisation du Button bJoue
		bPlay = new Button();
		bPlay.setFont(Font.font("Gabriola", FontWeight.BOLD, 80)); 
		bPlay.textProperty().bind(I18N.createStringBinding("btn.joinGame"));
		bPlay.setPrefWidth(200);
		bPlay.setPrefSize(600, 100);
		bPlay.setStyle("-fx-text-fill: #e8d0a2; -fx-background-color: #643433; -fx-border-color : #9c3c10");
		bPlay.setTranslateY(-30);
	}

	// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
	private void creationContainers() throws FileNotFoundException {

		//creation et initialisation de la HBox hbUp
		hbUp = new HBox();
		hbUp.setAlignment(Pos.CENTER);

		//creation et initialisation de la HBox hbCenter
		hbCenter = new HBox();
		hbCenter.setAlignment(Pos.CENTER);
		
		//creation et initialisation de la VBox vbCenter
		vbCenter = new VBox();
		vbCenter.setAlignment(Pos.CENTER);	
		//defini l'espacement de la VBox vbCenter
		vbCenter.setSpacing(100);
		
		//creation et initialisation de la VBox vbDown
		vbDown = new VBox();
		vbDown.setAlignment(Pos.BOTTOM_CENTER);
		
		//creation et initialisation de l'image Titre
		
		
		lTitle = new FileInputStream(("./src/ressources/title.png"));
			
		 // regarder le chemin !!!!
		imageTitle  = new Image(lTitle, 1000, 470, false, true);
		imageViewTitle = new ImageView(imageTitle);
	}	
	
	// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
	private void placementWidgetsInContainers() {
		hbUp.getChildren().add(imageViewTitle);
		vbDown.getChildren().add(bBack);
	}	

	// On définit l'emplacement des HBox et VBox dans l'interface
	private void placementContainersInPane() {
		this.setTop(hbUp);	
		this.setCenter(vbCenter);
		this.setBottom(vbDown);
	}	

	// On souhaite rejoindre une partie à l'aide d'un code
	private void enteredCode() {

		//creation et initialisation de la VBox vbPlayer
		vbPlayer = new VBox();
		vbPlayer.setAlignment(Pos.CENTER);
		
		
		//creation et initialisation du Label lCode
		lCode = new Label();
		lCode.setFont(Font.font("Gabriola", FontWeight.BOLD, 80));
		lCode.textProperty().bind(I18N.createStringBinding("lb.code"));
		
		//creation et initialisation du TextField tfCode
		tfCode = new TextField();
		tfCode.setPromptText("Rentrez le code de la partie");
		tfCode.setFont(Font.font("Gabriola", FontWeight.BOLD,40));
		tfCode.setMaxWidth(700);
		tfCode.setAlignment(Pos.CENTER);
		
		// On affecte les différents Label, Button dans leurs emplacements respectifs (VBox)
		vbPlayer.getChildren().addAll(lCode, tfCode);
		vbCenter.getChildren().addAll(vbPlayer, bPlay);
		
	}
	
	// Initialise l'interface avec tout les composants necessaires
	// On affecte aussi des actions aux Button pour naviguer entre les différentes interfaces
	public JoinGame(DialogueController cd) throws FileNotFoundException {
		cDialogue = cd;
		this.setBackground(new Background(new BackgroundFill(Color.WHITE,CornerRadii.EMPTY,null)));
		creationWidgets();
		creationContainers();
		placementWidgetsInContainers();
		placementContainersInPane();
		enteredCode();
		
		Image image1 = Caches.BG;
		// Creation de l'image en arriere plan/ image de fond
		BackgroundImage backgroundimage = new BackgroundImage(image1,
		BackgroundRepeat.NO_REPEAT,
		BackgroundRepeat.NO_REPEAT,
		BackgroundPosition.DEFAULT,
		new BackgroundSize(1.0, 1.0, true, true, false, false));
	
		Background background = new Background(backgroundimage);
		//On affecte à l'écran courant l'image de fond défini
		this.setBackground(background);
		
		// cDialogue.showScreen(screenList.HANDCARD)
		//bPlay.setOnAction(event -> cDialogue.getMp().joinGame("Joueur 1", Integer.parseInt(tfCode.getText())));
		bPlay.setOnAction((event) -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.getMp().joinGame(cDialogue.getMp().getGame().getPlayer().getPseudo(), "JR", Integer.parseInt(tfCode.getText()));
			// cDialogue.showScreen(screenList.HANDCARD);
		});
		bBack.setOnAction(event -> {
				cDialogue.getComputerSettings().effectSounds();
				cDialogue.showScreen(screenList.COMPUTERMENU);
			});
		
		this.setVisible(false);
		cDialogue.saveScreen(screenName, this);
	}	
}
