package computerInterfaces;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cd.DataController.screenList;
import gamePart.Caches;
import cd.DialogueController;
import cd.I18N;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


	public class MaterialRule extends BorderPane {

		private DialogueController cDialogue = null;
		//On donne un nom a cette interface pour permettre l'identification par le systeme
		private static final screenList nameScreen = screenList.MATERIALRULE;
		
			HBox Htop;

			Label lTitle;
		
			VBox Vbottom;
			VBox Vleft;
			VBox Vright;	
			VBox Vmiddle;

			Button bBack1;
			Button bBack2;
			
			FileInputStream inp1;
			Image image1;
			ImageView imageView1;
			
			FileInputStream inp2;
			Image image2;
			ImageView imageView2;
			
			VBox Vi1;
			VBox Vi2;
			HBox Hi1;
			HBox Hi12;
			HBox Hi2;
			
			Label lpc;
			Label lv;
			Label lbcwc;
			Label ln;
			Label lsa;
			Label ltcbc;
			Label lvide;
			Label lvide1;
			Label lvide2;
			
			Label lalchemy;
			Label lfight;
			Label lfarming;
			Label ltrade;
			Label lreligion;
			Label lmusic;
			
			Label ll1;
			Label ll2;
			
			VBox vl1;
			VBox vl2;

			// On créé les differents composants de l'interface (Label, Button)
		    private void createWidgets() throws FileNotFoundException {
				//***LABEL***//
		    	//creation et initialisation du Label lTitle
				lTitle = new Label();
				lTitle.setFont(Font.font("Gabriola", FontWeight.BOLD, 200));		
				lTitle.textProperty().bind(I18N.createStringBinding("lb.titleMrt"));
				
				//creation et initialisation du Button bBack1
				bBack1 = new Button();
				bBack1.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack1.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack1.setPrefSize(200, 50);
				bBack1.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack1.setTranslateY(-75);
				bBack1.setTranslateX(50);

				// creation et initialisation du Button bBack2
				bBack2 = new Button();
				bBack2.setFont(Font.font("Gabriola", FontWeight.BOLD, 40)); 
				bBack2.textProperty().bind(I18N.createStringBinding("btn.back"));
				bBack2.setPrefSize(200, 50);
				bBack2.setStyle("-fx-color: #990d0d; -fx-base: #ece3dd; -fx-border-color : #cc7c7c; -fx-border-width : 2;");
				bBack2.setTranslateY(-75);
				bBack2.setTranslateX(-50);
				
				
				//creation et initialisation de l'image 1
				inp1 = new FileInputStream("./src/ressources/Rules/Material1.png");
				
				image1  = new Image(inp1, 1000, 250, false, true);
				imageView1 = new ImageView(image1);
				
				//creation et initialisation des labels pour l'image 2
				inp2 = new FileInputStream("./src/ressources/Rules/Material2.png");
					
				image2  = new Image(inp2, 1000, 200, false, true);
				imageView2 = new ImageView(image2);
				
				
				//creation et initialisation des labels pour l'image 2
				
				lpc = new Label();
				lpc.setFont(Font.font("Cambria",  20));		
				lpc.textProperty().bind(I18N.createStringBinding("lb.lpc"));
				
				lv = new Label();
				lv.setFont(Font.font("Cambria",  25));		
				lv.textProperty().bind(I18N.createStringBinding("lb.lv"));
				
				lbcwc = new Label();
				lbcwc.setFont(Font.font("Cambria",  20));		
				lbcwc.textProperty().bind(I18N.createStringBinding("lb.lbcwc"));
				
				ln = new Label();
				ln.setFont(Font.font("Cambria",  25));		
				ln.textProperty().bind(I18N.createStringBinding("lb.ln"));
				
				lsa = new Label();
				lsa.setFont(Font.font("Cambria",  20));		
				lsa.textProperty().bind(I18N.createStringBinding("lb.lsa"));
				
				ltcbc = new Label();
				ltcbc.setFont(Font.font("Cambria",  20));		
				ltcbc.textProperty().bind(I18N.createStringBinding("lb.ltcbc"));
				
				lvide = new Label();
				lvide1 = new Label();
				lvide2 = new Label();
				//creation et initialisation des labels pour l'image 2
				
				lalchemy = new Label();
				lalchemy.setFont(Font.font("Cambria",  30));		
				lalchemy.textProperty().bind(I18N.createStringBinding("lb.lalchemy"));
				
				lfight = new Label();
				lfight.setFont(Font.font("Cambria",  30));		
				lfight.textProperty().bind(I18N.createStringBinding("lb.lfight"));
				
				lfarming = new Label();
				lfarming.setFont(Font.font("Cambria", 30));		
				lfarming.textProperty().bind(I18N.createStringBinding("lb.lfarming"));
				
				ltrade = new Label();
				ltrade.setFont(Font.font("Cambria",  30));		
				ltrade.textProperty().bind(I18N.createStringBinding("lb.ltrade"));
				
				lreligion = new Label();
				lreligion.setFont(Font.font("Cambria",  30));		
				lreligion.textProperty().bind(I18N.createStringBinding("lb.lreligion"));
				
				lmusic = new Label();
				lmusic.setFont(Font.font("Cambria", 30));		
				lmusic.textProperty().bind(I18N.createStringBinding("lb.lmusic"));
				
				ll1 = new Label();
				ll1.setFont(Font.font("Cambria", 25));		
				ll1.textProperty().bind(I18N.createStringBinding("lb.ll1"));
				
				ll2 = new Label();
				ll2.setFont(Font.font("Cambria", 25));		
				ll2.textProperty().bind(I18N.createStringBinding("lb.ll2"));
			}
				
		    
			// On créé les différents emplacements de l'interface (HBox, VBox), cela permet de structurer la page
		    private void createContainers() {

				// CREATION ET CONFIGURATION DE LA HBOX HAUTE/TITRE
				Htop = new HBox();
				Htop.setAlignment(Pos.CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vbottom = new VBox();
				Vbottom.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX GAUCHE
				Vleft = new VBox();
				Vleft.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX DROITE
				Vright = new VBox();
				Vright.setAlignment(Pos.BOTTOM_CENTER);

				// CREATION ET CONFIGURATION DE LA VBOX middle
				Vmiddle = new VBox();
				Vmiddle.setAlignment(Pos.CENTER);
				
				// CREATION ET CONFIGURATION DE LA VBOX pour la premiere image
				Vi1 = new VBox();
				Vi1.setAlignment(Pos.CENTER);
				
				Hi1 = new HBox();
				Hi1.setAlignment(Pos.CENTER);
				Hi1.setSpacing(50);
				
				Hi12 = new HBox();
				Hi12.setAlignment(Pos.CENTER);
				Hi12.setSpacing(32);
				
				// CREATION ET CONFIGURATION DE LA VBOX pour la deuxieme image
				Vi2 = new VBox();
				Vi2.setAlignment(Pos.CENTER);
				
				// CREATION ET CONFIGURATION DE LA HBOX pour la deuxieme image
				Hi2 = new HBox();
				Hi2.setAlignment(Pos.CENTER);
				Hi2.setSpacing(40);
				
				vl1 = new VBox();
				vl1.setAlignment(Pos.CENTER_LEFT);
				
				vl2 = new VBox();
				vl2.setAlignment(Pos.CENTER_LEFT);
		    }

			// On affecte les différents Label, Button dans leurs emplacements respectifs (HBox, VBox)
		    private void placementWidgetsInContainers() {
				
				// ON AJOUTE TITRE DANS TOP
				Htop.getChildren().add(lTitle);
						
				// ON AJOUTE JOUER DANS middle
				//Vmiddle.getChildren().add(); // Il faut rajouter le texte
				
				
				// ON AJOUTE REGLES DU JEU DANS VGAUCHE
				Vleft.getChildren().add(bBack1);

				// ON AJOUTE PARAMETRE DANS VDROITE
				Vright.getChildren().add(bBack2);

				vl1.getChildren().add(ll1);
				vl2.getChildren().add(ll2);
				
				Hi1.getChildren().add(lpc);
				Hi1.getChildren().add(lv);			
				Hi1.getChildren().add(lbcwc);
				Hi1.getChildren().add(lvide);	
				Hi1.getChildren().add(lvide1);	
				Hi1.getChildren().add(lvide2);
				Hi12.getChildren().add(ln);
				Hi12.getChildren().add(lsa);
				Hi12.getChildren().add(ltcbc);
				Hi12.getChildren().add(lvide);
				
				//Vi1.getChildren().add(ll1);
				Vi1.getChildren().add(Hi1);
				Vi1.getChildren().add(imageView1);
				Vi1.getChildren().add(Hi12);
				
				
				Hi2.getChildren().add(lalchemy);
				Hi2.getChildren().add(lfight);
				Hi2.getChildren().add(lfarming);
				Hi2.getChildren().add(ltrade);
				Hi2.getChildren().add(lreligion);
				Hi2.getChildren().add(lmusic);
				
				//Vi2.getChildren().add(ll2);
				Vi2.getChildren().add(imageView2);
				Vi2.getChildren().add(Hi2);
				
				
				// ON AJOUTE PARAMETRE DANS VMIDDLE
				Vmiddle.getChildren().add(vl1);
				Vmiddle.getChildren().add(lvide);
				Vmiddle.getChildren().add(Vi1);
				Vmiddle.getChildren().add(vl2);
				Vmiddle.getChildren().add(lvide1);
				Vmiddle.getChildren().add(Vi2);
				
				
			}

			// On définit l'emplacement des HBox et VBox dans l'interface
		    private void placementContainersInPane() {
			
				// ON ASSIGNE A CHAQUE BOX LEUR EMPLACEMENT
				this.setTop(Htop);
				this.setLeft(Vleft);
				this.setRight(Vright);
				this.setCenter(Vmiddle);
			}

			
		public MaterialRule(DialogueController cDialogue2) throws FileNotFoundException{

			cDialogue = cDialogue2;
			
			createWidgets();
		    createContainers();
		    placementWidgetsInContainers();
		    placementContainersInPane();
			
			//Creation de l'image
			Image image1 = Caches.BG;
			// Creation de l'image en arriere plan/ image de fond
			BackgroundImage backgroundimage = new BackgroundImage(image1,
			BackgroundRepeat.NO_REPEAT,
			BackgroundRepeat.NO_REPEAT,
			BackgroundPosition.DEFAULT,
			new BackgroundSize(1.0, 1.0, true, true, false, false));
		
			Background background = new Background(backgroundimage);
			//On affecte à l'écran courant l'image de fond défini
			this.setBackground(background);

			bBack1.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.COMPUTERRULES);
		});



			bBack2.setOnAction(event -> {
			cDialogue.getComputerSettings().effectSounds();
			cDialogue.showScreen(screenList.COMPUTERRULES);
		});
			
			

			this.setVisible(false);
			cDialogue.saveScreen(nameScreen, this);

		}
			
	}
