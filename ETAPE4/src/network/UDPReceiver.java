package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.charset.StandardCharsets;

import cd.DataController.screenList;
import gamePart.MP;
import util.Status;

public class UDPReceiver implements Runnable {

	private InetAddress group;
	private MP mainProgram;
	private MulticastSocket socket;
	private int portGroup;

	public UDPReceiver(MP mainProgram, InetAddress group, int portGroup) {
		this.mainProgram = mainProgram;
		this.group = group;
		this.portGroup = portGroup;
	}

	@Override
	public void run() {
		byte[] buffer = new byte[1024];

		try {
			socket = new MulticastSocket(portGroup);
			socket.joinGroup(group);

			while (true) {
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
				socket.receive(packet);
				String msg = new String(packet.getData(), packet.getOffset(), packet.getLength(),
						StandardCharsets.UTF_8);
				String[] data = msg.split("-");

				if (data[0].equals("ACP")) {

					int idp = Integer.parseInt(data[1].substring(1));
					String ip = data[2];
					int port = Integer.parseInt(data[3]);
					String name = data[4];

					int nbj = Integer.parseInt(data[5]);
					int nbjrm = Integer.parseInt(data[6]);
					int nbjvm = Integer.parseInt(data[7]);

					Status status = Status.valueOf(data[8]);

					if (name.equals(mainProgram.getGame().getName()) && idp == mainProgram.getGame().getId()
							&& status.equals(mainProgram.getGame().getStatus()) && MP.javafx) {
						mainProgram.getControllerTable().showScreen(screenList.PLACEMENT);
						mainProgram.getControllerTable().setIdp(idp);
					}

				} else if (data[0].equals("AMP")) {

					int idp = Integer.parseInt(data[1].substring(1));
					String ip = data[2];
					int port = Integer.parseInt(data[3]);
					String name = data[4];

					int nbj = Integer.parseInt(data[5]);
					int nbjrm = Integer.parseInt(data[6]);
					int nbjvm = Integer.parseInt(data[7]);

					int nbjrmc = Integer.parseInt(data[8]);
					int nbjvmc = Integer.parseInt(data[9]);

					Status status = Status.valueOf(data[10]);

					String address = ip + ":" + port + ":" + idp;

					if (!mainProgram.getNetwork().getServerList().contains(address))
						mainProgram.getNetwork().getServerList().add(address);

					if (idp == mainProgram.getGame().getId() && !mainProgram.isTable()) {
						mainProgram.getGame().setNumPlayer(nbj);
						mainProgram.getGame().setNumHuman(nbjrm);
						mainProgram.getGame().setNumBot(nbjvm);

						mainProgram.getGame().setNumHumanConnected(nbjvmc);
						mainProgram.getGame().setNumBotConnected(nbjvmc);

						mainProgram.getGame().setStatus(status);
					}
				}
/*
				if (MP.javafx)
					System.out.println(msg);*/
			}
			// socket.leaveGroup(group);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			socket.close();
		}
	}

}
