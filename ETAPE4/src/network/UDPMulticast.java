package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.charset.StandardCharsets;

import gamePart.MP;

public class UDPMulticast {

	private InetAddress group;
	private MulticastSocket socketServer = null;
	private String ip;
	private int port;
	private MP mainProgram;

	public UDPMulticast(String ip, int port, MP mainProgram) {
		this.ip = ip;
		this.port = port;
		this.mainProgram = mainProgram;
	}

	public void join() {
		if (socketServer != null)
			socketServer.close();

		try {
			socketServer = new MulticastSocket(port);
			group = InetAddress.getByName(ip);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Thread t = new Thread(new UDPReceiver(mainProgram, group, port));
		t.start();
	}

	public void send(String message) {
		byte[] msg = null;
		DatagramPacket packet = null;
		msg = message.getBytes(StandardCharsets.UTF_8);
		packet = new DatagramPacket(msg, msg.length, group, port);

		try {
			socketServer.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
