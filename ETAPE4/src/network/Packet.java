package network;

import java.util.ArrayList;

public class Packet {
	
	private ArrayList<String> messages = new ArrayList<>();
	
	public void add(String data) {
		messages.add(data);
	}

	public void add(int data) {
		messages.add("" + data);
	}

	public void clearMessage() {
		messages = new ArrayList<>();
	}
	
	public String encodeMessage() {
		String message = "";

		for (String data : messages)
			message += data + "-";

		if (message.length() == 0) 
			return "";
		
		return message.substring(0, message.length() - 1);
	}

}
