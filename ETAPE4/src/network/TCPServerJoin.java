package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import card.InfluenceCard;
import card.ObjectiveCard;
import cd.DataController.screenList;
import gamePart.Columns;
import gamePart.MP;
import player.Player;
import util.Color;
import util.Role;
import util.Status;

public class TCPServerJoin extends Thread {

	private Socket socket;
	private MP mainProgram;
	private TCPServer server;
	private Player player;
	private PrintWriter writer;

	public TCPServerJoin(Socket socket, MP mainProgram, TCPServer server, int pid) {
		this.socket = socket;
		this.mainProgram = mainProgram;
		this.server = server;
		player = new Player();
		player.setId("J" + pid);
	}

	@Override
	public void run() {
		try {
			InputStream input = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));

			OutputStream output = socket.getOutputStream();
			writer = new PrintWriter(output, true);

			while (true) {
				String message = reader.readLine();
				String[] data = message.split("-");

				if (MP.javafx)
					System.out.println("server -> " + message);

				if (data[0].equals("DCP")) {

					String name = data[1];
					String type = data[2];
					int gameId = Integer.parseInt(data[3]);

					boolean allow = false;

					if (type.equals("JR")
							&& mainProgram.getGame().getNumHumanConnected() < mainProgram.getGame().getNumHuman())
						allow = true;
					else if (type.equals("BOT")
							&& mainProgram.getGame().getNumBotConnected() < mainProgram.getGame().getNumBot())
						allow = true;

					if (mainProgram.getGame().getId() != gameId
							|| !mainProgram.getGame().getStatus().equals(Status.ATTENTE) || !allow)
						mainProgram.denyPlayer(gameId, player);

					else {

						if (type.equals("JR")) {
							player.setColor(Color.Bla);
						}

						else if (type.equals("BOT")) {
							player = mainProgram.getBots().get(name).getBot().duplicate();
							mainProgram.getGame().setNumBotConnected(mainProgram.getGame().getNumBotConnected() + 1);
						}

						// mainProgram.acceptPlayer(player);
						// mainProgram.getGame().getPlayers().add(player);

						player.setType(type);
						player.setPseudo(name);

						mainProgram.getGame().getUserRequest().add(player);
						// player.setAccepted(true);

						// player.setPriority(mainProgram.getGame().getPlayers().size());

						// mainProgram.updateGame();
						/*
						 * if (MP.javafx)
						 * mainProgram.getControllerTable().getPlacement().showGestionPlayer(name);
						 */
						// mainProgram.getControllerTable().getPlacement().newUnplacedPlayer(name);
						if (type.equals("BOT")) {
							mainProgram.allowPlayer(name);
							if ((mainProgram.getGame().getNumHumanConnected() + mainProgram.getGame()
									.getNumBotConnected() >= mainProgram.getGame().getNumPlayer()) && MP.javafx)
								mainProgram.getControllerTable().getPlacement().showLaunchButton();
						} else
							mainProgram.getControllerTable().getPlacement().showGestionPlayer(name);

					}

				} else if (data[0].equals("JCI")) {

					InfluenceCard card = new InfluenceCard(data[1], false);

					int column = Integer.parseInt(data[2]);
					int idp = Integer.parseInt(data[3]);
					int nm = Integer.parseInt(data[4]);
					String idj = data[5];

					Player playerPlayed = mainProgram.getGame().getPlayerById(idj);

					if ( /* nm == mainProgram.getGame().getRound() && */ mainProgram.getGame().getId() == idp
							&& playerPlayed != null
							&& mainProgram.getGame().getCurrentPlayer().getColor().equals(playerPlayed.getColor())) {
						int position = 0;

						for (int i = 0; i < playerPlayed.getHand().size(); i++) {
							InfluenceCard iC = playerPlayed.getHand().get(i);

							if (iC.getRole().equals(card.getRole()))
								position = i;

						}

						if (position != -1) {

							if (!mainProgram.getGame().getColumns().get(column).getIsPlacable()) {
								mainProgram.initTurn();
							} else {

								InfluenceCard newCard = playerPlayed.drawCard(card);

								if (MP.javafx) {
									for (Player player : mainProgram.getGame().getPlayers())
										mainProgram.getControllerTable().getGameTable().showDeckAndDiscard(
												player.hasDeck(), player.hasDiscard(), player.getPseudo(),
												player.getColor());

								}

								if (newCard != null) {

									Thread.sleep(150);
									mainProgram.fillPlayerHand(newCard, playerPlayed);
									Thread.sleep(150);

									if (MP.javafx) {
										mainProgram.getControllerTable().getGameTable().cardPlayed(card, column + 1);
									}
									/*
									 * for(InfluenceCard iC :
									 * mainProgram.getGame().getColumns().get(column).getContent()) {
									 * iC.setVisible(true); }
									 */
									mainProgram.getGame().getColumns().get(column).getContent().add(card);

									mainProgram.getGame().setCurrentPlayer(mainProgram.getGame().getPlayers()
											.get(playerPlayed.getPriority() % mainProgram.getGame().getNumPlayer()));

									mainProgram.getGame().useDirectlyEffect(column);
								} else {
									mainProgram.initTurn();
								}
							}

						} else {
							System.out.println("error");
						}

					} else {
						mainProgram.initTurn();
					}

				} else if (data[0].equals("JCT")) {
					int column = Integer.parseInt(data[1]);
					int idp = Integer.parseInt(data[2]);
					int nm = Integer.parseInt(data[3]);
					String idj = data[4];

					if (mainProgram.getGame().getTrCard().containsKey(idj)) {
						int prev_column = mainProgram.getGame().getTrCard().get(idj);

						ObjectiveCard prev_oC = mainProgram.getGame().getColumns().get(prev_column).getOCard();
						ObjectiveCard next_oC = mainProgram.getGame().getColumns().get(column).getOCard();

						mainProgram.getGame().getColumns().get(prev_column).setOCard(next_oC);
						mainProgram.getGame().getColumns().get(column).setOCard(prev_oC);

						if (mainProgram.getControllerTable() != null)
							mainProgram.getControllerTable().getGameTable().refreshCard();

						mainProgram.getGame().setPause(false);
						mainProgram.getGame().endTurn(column);
						
						mainProgram.sendObjectiveCard();
						
						/*
						for ( Player player : mainProgram.getGame().getPlayers() ) {
							mainProgram.getControllerPlayer().getHandCard().addObjectiveCardInColumn(prev_oC, column);
							mainProgram.getControllerPlayer().getHandCard().addObjectiveCardInColumn(next_oC, prev_column);
						}
						*/
					}
				} else if (data[0].equals("JCC")) {
					String idj = data[4];
					int column = mainProgram.getGame().getCaCard().get(idj);

					if (data[1].equals("null")) {
						mainProgram.getGame().setPause(false);
						mainProgram.getGame().endTurn(column);
					} else {

						InfluenceCard card = new InfluenceCard(data[1], false);
						int idp = Integer.parseInt(data[2]);
						int nm = Integer.parseInt(data[3]);

						Player player = mainProgram.getGame().getPlayerById(idj);

						if (mainProgram.getGame().getCaCard().containsKey(idj)) {

							for (InfluenceCard c : mainProgram.getGame().getColumns().get(column).getContent()) {
								if (c.getRole().equals(Role.Ci) && c.getColor().equals(player.getColor())) {
									c.setHiddenCard(card);

									for (int i = 0; i < player.getHand().size(); i++) {
										if (player.getHand().get(i).encode().equals(card.encode())) {
											InfluenceCard newCard = player.drawCard(card);
											mainProgram.fillPlayerHand(newCard, player);
										}
									}

									mainProgram.getGame().setPause(false);
									mainProgram.getGame().endTurn(column);
								}
							}
						}
					}
				}

			}
		} catch (IOException ex) {

			if (player.getType().equals("JR"))
				mainProgram.getGame().setNumHumanConnected(mainProgram.getGame().getNumHumanConnected() - 1);
			else if (player.getType().equals("BOT"))
				mainProgram.getGame().setNumBotConnected(mainProgram.getGame().getNumBotConnected() - 1);

			server.getPlayerList().remove(this);

			if (player.getAccepted()) {
				mainProgram.getGame().getPlayers().remove(player);

				for (Player _player : mainProgram.getGame().getPlayers()) {
					mainProgram.disconnectPlayer(_player);
				}

				mainProgram.getGame().setStatus(Status.ANNULEE);
				mainProgram.getControllerTable().showScreen(screenList.TABLEMENU);
			}

			mainProgram.updateGame();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Player getPlayer() {
		return player;
	}

	public void send(String message) {
		if (writer != null)
			writer.println(message);
	}

}
