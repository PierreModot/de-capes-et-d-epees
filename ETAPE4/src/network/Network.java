package network;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

import gamePart.MP;

public class Network {

	private TCPServer server;
	private TCPClient client;
	private UDPMulticast udp;

	private MP mainProgram;
	private boolean table;

	private String udpIp = "224.7.7.7";
	private int udpPort = 7778;
	private String tcpIp = "";
	private int tcpPort;

	private ArrayList<String> serverList = new ArrayList<>();
	private Thread tableThread;

	public Network(MP mainProgram, boolean table) {
		this.mainProgram = mainProgram;
		this.table = table;
		udp = new UDPMulticast(udpIp, udpPort, mainProgram);
		udp.join();
	}

	public Network(boolean b) {
		// TODO Auto-generated constructor stub
	}

	public void initTable() {
		if (tableThread != null) {
			tableThread.stop();
			tableThread = null;
			server.close();

		}
		tcpPort = (int) (Math.random() * (65535 - 1024)) + 1024;
		// tcpPort = 8080;

		server = new TCPServer(tcpPort, mainProgram);
		tableThread = new Thread(server);
		tableThread.start();
	}

	public void connectClient(int gameId) {
		client = new TCPClient(tcpPort, mainProgram, gameId, null);
		Thread t = new Thread(client);
		t.start();

	}

	public void sendUDP(Packet packet) {
		sendUDPMessage(packet.encodeMessage());
		packet.clearMessage();
	}

	public void sendTCP(Packet packet) {
		sendTCPMessage(packet.encodeMessage());
		packet.clearMessage();
	}

	public void sendTCP(Packet packet, String pid) {
		sendTCPMessage(packet.encodeMessage(), pid);
		packet.clearMessage();
	}

	public void sendUDPMessage(String message) {
		udp.send(message);
	}

	public void sendTCPMessage(String message) {
		if (!table)
			client.send(message);
	}

	public void sendTCPMessage(String message, String pid) {
		if (table) {
			for (TCPServerJoin client : server.getPlayerList()) {
				if (client.getPlayer().getId().equals(pid))
					client.send(message);
			}
		}
	}

	public String getUDPIp() {
		return udpIp;
	}

	public int getUDPPort() {
		return udpPort;
	}

	public TCPServer getServer() {
		return server;
	}

	public ArrayList<String> getServerList() {
		return serverList;
	}

	public void setServerList(ArrayList<String> serverList) {
		this.serverList = serverList;
	}

	public void setTcpPort(int port) {
		tcpPort = port;
	}

	public ArrayList<NetworkInterface> getUpNetworkInterfaces() {
		ArrayList<NetworkInterface> interfaceNames = new ArrayList<>();
		try {

			Enumeration<NetworkInterface> enumNetI = null;
			enumNetI = NetworkInterface.getNetworkInterfaces();

			while (enumNetI != null && enumNetI.hasMoreElements()) {
				NetworkInterface networkInterface = enumNetI.nextElement();

				if (networkInterface.isUp())
					interfaceNames.add(networkInterface);

			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return interfaceNames;
	}

	public void setClient(TCPClient client) {
		this.client = client;
	}

}
