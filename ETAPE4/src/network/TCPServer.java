package network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import gamePart.MP;

public class TCPServer implements Runnable {

	private int port;
	private MP mainProgram;
	private ArrayList<TCPServerJoin> playerList = new ArrayList<>();
	private int pid = 0;
	private String ip = "";
	private ServerSocket serverSocket = null;

	public TCPServer(int port, MP mainProgram) {
		this.port = port;
		this.mainProgram = mainProgram;
	}

	@Override
	public void run() {
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("Server is listening on port " + port);
			ip = InetAddress.getLocalHost().getHostAddress();

			while (true) {
				Socket socket = serverSocket.accept();
				/*
				if (MP.javafx)
					System.out.println("New client connected");
*/
				TCPServerJoin client = new TCPServerJoin(socket, mainProgram, this, getPid());
				pid = (pid + 1) % 10000;

				playerList.add(client);
				client.start();

			}

		} catch (IOException ex) {

		}
	}

	public ArrayList<TCPServerJoin> getPlayerList() {
		return playerList;
	}

	public int getPid() {
		return pid;
	}

	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}
	
	public void close() {
		if(serverSocket != null) {
			try {
				serverSocket.close();
				serverSocket = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
	}

	public void search() {
		Thread t = new Thread() {
			public void run() {
				while (true) {
					mainProgram.updateGame();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		t.start();
	}

}
