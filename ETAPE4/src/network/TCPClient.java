package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;

import card.InfluenceCard;
import card.ObjectiveCard;
import cd.DataController.screenList;
import gamePart.MP;
import player.Bot;
import player.Player;
import util.Color;
import util.Role;

public class TCPClient implements Runnable {

	private int port;
	private MP mainProgram;
	private PrintWriter writer;
	private int gameId = -1;

	private ArrayList<String> caches = new ArrayList<>();
	private NetworkInterface prefNetInterface = null;

	public TCPClient(int port, MP mainProgram, int gameId, NetworkInterface prefNetInterface) {
		this.port = port;
		this.mainProgram = mainProgram;
		this.gameId = gameId;
		this.prefNetInterface = prefNetInterface;
	}

	@Override
	public void run() {
		try {
			String ip = "";
			int port = 0;

			for (String address : mainProgram.getNetwork().getServerList()) {
				String[] data = address.split(":");

				if (Integer.parseInt(data[2]) == gameId) {
					ip = data[0];
					port = Integer.parseInt(data[1]);
				}
			}

			Socket socket = new Socket(ip, port);
			/*
			socket.bind(new InetSocketAddress(prefNetInterface.getInetAddresses().nextElement(), 0));
			socket.connect(new InetSocketAddress(ip, port));
			*/
			
			mainProgram.getNetwork().setClient(this);

			OutputStream output = socket.getOutputStream();
			writer = new PrintWriter(output, true);

			for (String message : caches)
				send(message);

			while (true) {
				InputStream input = socket.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(input));

				String message = reader.readLine();
				String[] data = message.split("-");

				if (MP.javafx)
					System.out.println("client -> " + message);

				if (data[0].equals("RCP")) {
					mainProgram.getControllerPlayer().showScreen(screenList.DENYDEMAND);
				}

				else if (data[0].equals("ADP")) {

					int gameId = Integer.parseInt(data[1]);
					String playerId = data[2];

					Player player = mainProgram.getGame().getPlayer();
					player.setId(playerId);

					if (mainProgram.getBot() != null) {
						player = mainProgram.getBot().duplicate();
						player.setId(playerId);
					}

					mainProgram.getGame().setPlayer(player);
					mainProgram.getGame().setId(gameId);
					this.gameId = gameId;

					if (!player.isBot())
						mainProgram.getControllerPlayer().showScreen(screenList.WAITING);

				}

				if (gameId != -1) {
					if (data[0].equals("ADJ")) {
						// retourner menu

					} else if (data[0].equals("ILP")) {

						String[] players = data[1].split(",");
						String[] colors = data[2].split(",");
						int idp = Integer.parseInt(data[3]);

						if (idp == mainProgram.getGame().getId()) {
							for (int i = 0; i < players.length; i++) {
								Player player = new Player();

								player.setPseudo(players[i]);
								player.setColor(Color.valueOf(colors[i]));

								if (player.getColor().equals(mainProgram.getGame().getPlayer().getColor()))
									mainProgram.getGame().getPlayer().setPseudo(player.getPseudo());

								if (players[i].indexOf("BOT") >= 0)
									player.setType("BOT");
								else
									player.setType("JR");

								mainProgram.getGame().getPlayers().add(player);
								if(player.getPseudo().equals(mainProgram.getGame().getPlayer().getPseudo()))
									mainProgram.getGame().getPlayer().setColor(player.getColor());
							}
							if (mainProgram.getControllerPlayer() != null)
								mainProgram.getControllerPlayer().getHandCard().setColumnButtons(players.length);

						}
						mainProgram.getGame().createColumns();

					} else if (data[0].equals("RTC")) {

						String[] cards = data[1].split(",");
						mainProgram.getGame().getPlayer().setHand(new ArrayList<>());

						if (mainProgram.getControllerPlayer() != null)
							mainProgram.getControllerPlayer().showScreen(screenList.HANDCARD);

						for (int i = 0; i < cards.length; i++) {
							InfluenceCard iC = new InfluenceCard(cards[i], true);

							if (!mainProgram.getGame().getPlayer().isBot())
								mainProgram.getControllerPlayer().getHandCard().changeACard(iC, i + 1);

							mainProgram.getGame().getPlayer().getHand().add(iC);
						}

					} else if (data[0].equals("ILM")) {

						String[] lobjectif = data[1].split(",");
						int idp = Integer.parseInt(data[2]);
						int nm = Integer.parseInt(data[3]);

						if (idp == mainProgram.getGame().getId()) {
							for (int i = 0; i < lobjectif.length; i++) {
								ObjectiveCard oC = new ObjectiveCard(lobjectif[i]);
								mainProgram.getGame().getObjectiveCards().add(oC);
								mainProgram.getGame().getColumns().get(i).setOCard(oC);
								
								  if (MP.javafx && mainProgram.getControllerPlayer() != null)
								  mainProgram.getControllerPlayer().getHandCard().addObjectiveCardInColumn(oC,
								  i + 1);
								 
							}
							mainProgram.getGame().setRound(nm);
							if (mainProgram.getControllerPlayer() != null)
								mainProgram.getControllerPlayer().getHandCard().enableButton();
						}

					} else if (data[0].equals("IDT")) {

						Color color = Color.valueOf(data[1]);
						int idp = Integer.parseInt(data[2]);
						int nm = Integer.parseInt(data[3]);

						Player player = mainProgram.getGame().getPlayerByColor(color);

						if (player != null && mainProgram.getControllerPlayer() != null) {
							mainProgram.getControllerPlayer().getHandCard().setTurnText(player.getPseudo());
							mainProgram.getControllerPlayer().getHandCard().canPlay(player.getColor().equals(mainProgram.getGame().getPlayer().getColor()));
						}

						if (color.equals(mainProgram.getGame().getPlayer().getColor())) {
							if (mainProgram.getGame().getPlayer().isBot()
									|| mainProgram.getControllerPlayer() == null) {

								try {
									if (MP.javafx)
										Thread.sleep(200);

									((Bot) mainProgram.getGame().getPlayer()).Play(mainProgram);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}

					} else if (data[0].equals("RMJ")) {

						InfluenceCard iC = new InfluenceCard(data[1], true);

						int idp = Integer.parseInt(data[2]);
						int nm = Integer.parseInt(data[3]);

						if (idp == mainProgram.getGame().getId()) {

							mainProgram.getGame().getPlayer().addDiscard(mainProgram.getGame().getPlayer().getHand()
									.get(mainProgram.getGame().getLastCardPlayed()));

							mainProgram.getGame().getPlayer().getHand().set(mainProgram.getGame().getLastCardPlayed(),
									iC);

							if (!mainProgram.getGame().getPlayer().isBot()) {
								for (int i = 0; i < mainProgram.getGame().getPlayer().getHand().size(); i++) {
									InfluenceCard card = mainProgram.getGame().getPlayer().getHand().get(i);
									mainProgram.getControllerPlayer().getHandCard().changeACard(card, i + 1);
								}
							}
						}

					} else if (data[0].equals("RRJ")) {

						Color color = Color.valueOf(data[1]);

						int idp = Integer.parseInt(data[2]);
						int nm = Integer.parseInt(data[3]);

						if (idp == mainProgram.getGame().getId()) {
							mainProgram.getGame().getPlayer().addDiscardToDeck();
						}

					} else if (data[0].equals("ICJ")) {

						Color color = Color.valueOf(data[1]);
						int column = Integer.parseInt(data[2]);
						String role = data[3];

						int idp = Integer.parseInt(data[4]);
						int nm = Integer.parseInt(data[5]);

						if (idp == mainProgram.getGame().getId()) {
							Player player = mainProgram.getGame().getPlayerByColor(color);

							if (!role.equals("NUL") && player != null) {
								InfluenceCard card = new InfluenceCard(role, true);

								boolean isBot = mainProgram.getControllerPlayer() == null;

								if (!isBot)
									mainProgram.getControllerPlayer().getHandCard()
											.updateInfoCardPlayed(player.getPseudo(), column + 1, card);

								if (!isBot && card.getRole().equals(Role.Te))
									mainProgram.getControllerPlayer().getHandCard().disableButton(column + 1);
								/*
								 * if (card.getRole().equals(Role.Tr) &&
								 * color.equals(mainProgram.getGame().getPlayer().getColor())) { if(isBot)
								 * mainProgram.setColumnTr(column); else
								 * mainProgram.getControllerPlayer().getHandCard().exchangeObjectiveCard(column)
								 * ; }
								 */
							}
						}

					} else if (data[0].equals("FDP")) {
						Color color = Color.valueOf(data[1]);

						String[] players = data[2].split(",");
						String[] points = data[3].split(",");

						int gameId = Integer.parseInt(data[4]);

						if (gameId == mainProgram.getGame().getId() && mainProgram.getControllerPlayer() != null) {
							mainProgram.getControllerPlayer().showScreen(screenList.ENDGAMECOMPUTER);
							for (int i = 0; i < players.length; i++)
								mainProgram.getControllerPlayer().getEndGameComputer().addPlayerInClassement(players[i],
										Integer.parseInt(points[i]));
						}
					} else if (data[0].equals("ECT")) {
						int column = Integer.parseInt(data[2]);
						int idp = Integer.parseInt(data[3]);
						int nm = Integer.parseInt(data[4]);

						if (idp == mainProgram.getGame().getId()) {

							boolean isBot = mainProgram.getControllerPlayer() == null;

							if (isBot) {
								Random rdm = new Random();
								mainProgram.setColumnTr(rdm.nextInt(mainProgram.getGame().getPlayers().size() - 1));
							} else
								mainProgram.getControllerPlayer().getHandCard().exchangeObjectiveCard(column + 1);

						}
					} else if (data[0].equals("CCI")) {
						int column = Integer.parseInt(data[1]);
						int idp = Integer.parseInt(data[2]);
						int nm = Integer.parseInt(data[3]);

						if (idp == mainProgram.getGame().getId()) {

							boolean isBot = mainProgram.getControllerPlayer() == null;

							if (isBot) {
								Random rdm = new Random();
								mainProgram
										.setColumnCa(mainProgram.getGame().getPlayer().getHand().get(rdm.nextInt(2)));
							} else
								mainProgram.getControllerPlayer().getHandCard().hideACard(column + 1);

						}
					}

				}
			}

		} catch (UnknownHostException ex) {
			System.out.println("Server not found: " + ex.getMessage());
		} catch (IOException ex) {
			System.out.println("I/O error: " + ex.getMessage());
			if (mainProgram.getControllerPlayer() != null)
				mainProgram.getControllerPlayer().showScreen(screenList.COMPUTERMENU);
		}
	}

	public void send(String message) {
		if (writer != null) {
			writer.println(message);
		} else {
			caches.add(message);
		}
	}

}
