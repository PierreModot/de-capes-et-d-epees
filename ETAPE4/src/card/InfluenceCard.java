package card;

import cd.DataController;
import cd.I18N;
import gamePart.Caches;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import util.Domain;
import util.Role;

public class InfluenceCard {
/// NORMAL CARDS
	private static Image IC = Caches.IC;
	private static Image ICB = Caches.ICB;
	private static Image ICG = Caches.ICG;
	private static Image ICP = Caches.ICP;
	private static Image ICW = Caches.ICW;
	private static Image ICY = Caches.ICY;

	private static Image C = Caches.C;
	private static Image CB = Caches.CB;
	private static Image CG = Caches.CG;
	private static Image CP = Caches.CP;
	private static Image CW = Caches.CW;
	private static Image CY = Caches.CY;

/// SPECIAL CARDS
	private static Image speIC = Caches.speIC;
	private static Image speICB = Caches.speICB;
	private static Image speICG = Caches.speICG;
	private static Image speICP = Caches.speICP;
	private static Image speICR = Caches.speICR;
	private static Image speICY = Caches.speICY;

	private static Image speC = Caches.speC;
	private static Image speCB = Caches.speCB;
	private static Image speCG = Caches.speCG;
	private static Image speCP = Caches.speCP;
	private static Image speCR = Caches.speCR;
	private static Image speCY = Caches.speCY;

	private double scale = .25;

	private Label titleCard = new Label();

	private util.Color color;
	private Role role;
	private boolean visible;
	private Domain symbol;

	private boolean executed = false;

	private ImageView frontCard; // Image de la carte révélée
	private ImageView backCard; // Image de la carte face cachée
	private double value; // Valeur de la carte courante
	private InfluenceCard hiddenCard; // Carte cache en fonction de si cette carte est la cape d'invisibilite
	private Text details = new Text();
	Border outline = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
			new BorderWidths(1), new Insets(5)));

	public InfluenceCard(InfluenceCard influenceCard) {
		this.titleCard = influenceCard.getTitle();
		this.role = influenceCard.getRole();
		this.value = influenceCard.getValue();
		this.color = influenceCard.getColor();
		this.visible = influenceCard.isVisible();
		this.frontCard = influenceCard.getFrontCard();
		this.backCard = influenceCard.getBackCard();
		this.symbol = influenceCard.getSymbol();
		this.hiddenCard = influenceCard.getHiddenCard();
		configureSymbol(this.role);
		
		if (DataController.theme) {
			C = Caches.speC;
			IC = Caches.speIC;
		} else {
			IC = Caches.IC;
			C = Caches.C;
		}
	}

	public Label getTitle() {
		return this.titleCard;
	}

	public void setTitle() {
		switch (this.role) {
		case Ro:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.king"));
			break;
		case Al:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.alchimiste"));
			break;
		case As:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.assassin"));
			break;
		case Ca:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.cardinal"));
			break;
		case Ci:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.cloakInvisibility"));
			break;
		case Dr:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.Dragon"));
			break;
		case Ec:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.Squire"));
			break;
		case Er:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.hermit"));
			break;
		case Ex:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.explorator"));
			break;
		case Ju:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.juliette"));
			break;
		case Ma:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.merchant"));
			break;
		case Md:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.weaponMaster"));
			break;
		case Me:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.beggar"));
			break;
		case Mg:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.wizard"));
			break;
		case Pg:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.litleGiant"));
			break;
		case Pr:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.prince"));
			break;
		case RO:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.Romeo"));
			break;
		case Re:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.queen"));
			break;
		case SO:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.double"));
			break;
		case Se:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.lord"));
			break;
		case So:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.witch"));
			break;
		case Tb:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.toubador"));
			break;
		case Te:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.tempete"));
			break;
		case Tm:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.ThreeMusketeers"));
			break;
		case Tr:
			titleCard.textProperty().bind(I18N.createStringBinding("cd.traitor"));
			break;
		default:
			titleCard = null;
			break;
		}
	}

	public Text getDetails() {
		return this.details;
	}

	public void setDetails() {
		switch (this.role) {
		case Ro:
			details.textProperty().bind(I18N.createStringBinding("details.Ro"));
			break;
		case Al:
			details.textProperty().bind(I18N.createStringBinding("details.Al"));
			break;
		case As:
			details.textProperty().bind(I18N.createStringBinding("details.As"));
			break;
		case Ca:
			details.textProperty().bind(I18N.createStringBinding("details.Ca"));
			break;
		case Ci:
			details.textProperty().bind(I18N.createStringBinding("details.Ci"));
			break;
		case Dr:
			details.textProperty().bind(I18N.createStringBinding("details.Dr"));
			break;
		case Ec:
			details.textProperty().bind(I18N.createStringBinding("details.Ec"));
			break;
		case Er:
			details.textProperty().bind(I18N.createStringBinding("details.Er"));
			break;
		case Ex:
			details.textProperty().bind(I18N.createStringBinding("details.Ex"));
			break;
		case Ju:
			details.textProperty().bind(I18N.createStringBinding("details.Ju"));
			break;
		case Ma:
			details.textProperty().bind(I18N.createStringBinding("details.Ma"));
			break;
		case Md:
			details.textProperty().bind(I18N.createStringBinding("details.Md"));
			break;
		case Me:
			details.textProperty().bind(I18N.createStringBinding("details.Me"));
			break;
		case Mg:
			details.textProperty().bind(I18N.createStringBinding("details.Mg"));
			break;
		case Pg:
			details.textProperty().bind(I18N.createStringBinding("details.Pg"));
			break;
		case Pr:
			details.textProperty().bind(I18N.createStringBinding("details.Pr"));
			break;
		case RO:
			details.textProperty().bind(I18N.createStringBinding("details.RO"));
			break;
		case Re:
			details.textProperty().bind(I18N.createStringBinding("details.Re"));
			break;
		case SO:
			details.textProperty().bind(I18N.createStringBinding("details.SO"));
			break;
		case Se:
			details.textProperty().bind(I18N.createStringBinding("details.Se"));
			break;
		case So:
			details.textProperty().bind(I18N.createStringBinding("details.So"));
			break;
		case Tb:
			details.textProperty().bind(I18N.createStringBinding("details.Tb"));
			break;
		case Te:
			details.textProperty().bind(I18N.createStringBinding("details.Te"));
			break;
		case Tm:
			details.textProperty().bind(I18N.createStringBinding("details.Tm"));
			break;
		case Tr:
			details.textProperty().bind(I18N.createStringBinding("details.Tr"));
			break;
		default:
			details = null;
			break;
		}
	}

	public boolean isVisible() {
		return visible;
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public double getValue() {
		return value;
	}

	public InfluenceCard(String encode, boolean visible) {
		if (DataController.theme) {
			C = Caches.speC;
			IC = Caches.speIC;
		} else {
			IC = Caches.IC;
			C = Caches.C;
		}
		this.visible = visible;
		decode(encode);
		setDetails();
		setTitle();
	}

	public InfluenceCard(Role role, util.Color color, boolean visible) {
		if (DataController.theme) {
			C = Caches.speC;
			IC = Caches.speIC;
		} else {
			IC = Caches.IC;
			C = Caches.C;
		}
		
		this.role = role;
		this.color = color;
		this.visible = visible;

		setDomain();
		setValue();

		setTitle();
		setDetails();
	}

	public InfluenceCard() {
		color = null;
		symbol = null;
		visible = true;
		symbol = null;

		if (DataController.theme) {
			C = Caches.speC;
			IC = Caches.speIC;
		} else {
			IC = Caches.IC;
			C = Caches.C;
		}
	}

	public Role getRole() {
		return role;
	}

	public util.Color getColor() {
		return color;
	}

	public String encode() {
		return "I" + color.name() + role.name();
	}

	public void decode(String card) {
		color = util.Color.valueOf(card.substring(1, 4));
		role = Role.valueOf(card.substring(4, 6));
		setDomain();
		setValue();
		configureSymbol(role);
	}

	public void setValue() {
		switch (role) {
		case Ro:
			setValue(20);
			break;
		case Al:
			setValue(8);
			break;
		case As:
			setValue(9.5);
			break;
		case Ca:
			setValue(8);
			break;
		case Ci:
			setValue(0);
			break;
		case Dr:
			setValue(11);
			break;
		case Ec:
			setValue(2);
			break;
		case Er:
			setValue(12);
			break;
		case Ex:
			setValue(13);
			break;
		case Ju:
			setValue(14);
			break;
		case Ma:
			setValue(8);
			break;
		case Md:
			setValue(8);
			break;
		case Me:
			setValue(4);
			break;
		case Mg:
			setValue(7);
			break;
		case Pg:
			setValue(2);
			break;
		case Pr:
			setValue(14);
			break;
		case RO:
			setValue(5);
			break;
		case Re:
			setValue(16);
			break;
		case SO:
			setValue(0);
			break;
		case Se:
			setValue(8);
			break;
		case So:
			setValue(1);
			break;
		case Tb:
			setValue(8);
			break;
		case Te:
			setValue(9);
			break;
		case Tm:
			setValue(11);
			break;
		case Tr:
			setValue(10);
			break;

		}
	}

	public void getImageColor() {
		if (DataController.theme) {
			switch (this.getColor()) {
			case Bla: // Blanc
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(speIC);
				else
					this.frontCard = new ImageView(speC);
				break;
			case Ble: // Bleu
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(speICB);
				else
					this.frontCard = new ImageView(speCB);
				break;
			case Jau:// Jaune
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(speICY);
				else
					this.frontCard = new ImageView(speCY);
				break;
			case Rou: // Rouge
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(speICR);
				else
					this.frontCard = new ImageView(speCR);
				break;
			case Ver: // Vert
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(speICG);
				else
					this.frontCard = new ImageView(speCG);
				break;
			case Vio: // Violet
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(speICP);
				else
					this.frontCard = new ImageView(speCP);
				break;
			}
		} else {
			switch (this.getColor()) {
			case Bla: // Blanc
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(ICW);
				else
					this.frontCard = new ImageView(CW);
				break;
			case Ble: // Bleu
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(ICB);
				else
					this.frontCard = new ImageView(CB);
				break;
			case Jau:// Jaune
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(ICY);
				else
					this.frontCard = new ImageView(CY);
				break;
			case Rou: // Rouge
				if (this.role != util.Role.SO)
					this.frontCard = new ImageView(IC);
				else
					this.frontCard = new ImageView(C);
				break;
			case Ver: // Vert
				if (this.role != util.Role.SO)

					this.frontCard = new ImageView(ICG);
				else
					this.frontCard = new ImageView(CG);
				break;
			case Vio: // Violet
				if (this.role != util.Role.SO)

					this.frontCard = new ImageView(ICP);
				else
					this.frontCard = new ImageView(CP);
				break;
			}
			frontCard.setFitHeight(100);
			frontCard.setFitWidth(100);
		}
	}

	public void setDomain() {
		switch (role) {
		// Roi
		case Ro:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(2680 * scale, 2024 * scale, 920 * scale, 920 * scale)); // Rogne
																											// l'image
																											// en
																											// fonction
																											// du rï¿½le
																											// de la
			// carte
			this.setBackCard(color);
			break;
		// Reine
		case Re:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(2684 * scale, 26 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Juliette
		case Ju:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(3681 * scale, 26 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Alchimiste
		case Al:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(2680 * scale, 1030 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Maitre d'armes
		case Md:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(5657 * scale, 3036 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Seigneur
		case Se:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(688 * scale, 26 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Marchand
		case Ma:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(3680 * scale, 3036 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Cardinal
		case Ca:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(5680 * scale, 26 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Troubadour
		case Tb:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(4680 * scale, 2024 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Explorateur
		case Ex:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(4669 * scale, 26 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Assasin
		case As:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(1690 * scale, 2024 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Tempete
		case Te:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(688 * scale, 1030 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Traitre
		case Tr:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(690 * scale, 3036 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Cape d'invusibilitï¿½
		case Ci:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(4675 * scale, 3036 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Les Trois Mousquetaires
		case Tm:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(1690 * scale, 1030 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Magicien
		case Mg:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(3680 * scale, 1030 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Sorciï¿½re
		case So:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(1695 * scale, 26 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Prince
		case Pr:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(3681 * scale, 2024 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Ecuyer
		case Ec:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(690 * scale, 2024 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Ermite
		case Er:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(5675 * scale, 2024 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Petit Gï¿½ant
		case Pg:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(4684 * scale, 1030 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Dragon
		case Dr:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(5678 * scale, 1030 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Romï¿½o
		case RO:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(2687 * scale, 3036 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Mendiant
		case Me:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(1690 * scale, 3036 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		// Sosie
		case SO:
			getImageColor();
			frontCard.setViewport(new Rectangle2D(5664 * scale, 28 * scale, 920 * scale, 920 * scale));
			this.setBackCard(color);
			break;
		}
		// Si la carte mis en paramï¿½tre n'est pas visible, alors c'est l'image differa
		// en fonction de la couleur de la carte

//		frontCard.setFitHeight(200);
//		frontCard.setFitWidth(200);
//		backCard.setFitHeight(200);
//		backCard.setFitWidth(200);
		frontCard.setFitHeight(100);
		frontCard.setFitWidth(100);
		backCard.setFitHeight(100);
		backCard.setFitWidth(100);
	}

	// }
	// Si la carte mis en param�tre n'est pas visible, alors c'est l'image differa
	// en fonction de la couleur de la carte

	public ImageView getFrontCard() {
		return frontCard;
	}

	public void setFrontCard(ImageView frontCard) {
		this.frontCard = frontCard;
	}

	public ImageView getBackCard() {
		return backCard;
	}

	public void setBackCard(util.Color color) {
		switch (color) {
		case Bla:
			this.backCard = new ImageView(C);
			backCard.setViewport(new Rectangle2D(1721 * scale, 28 * scale, 920 * scale, 920 * scale));
			break;
		// Bleu
		case Ble:
			this.backCard = new ImageView(C);
			backCard.setViewport(new Rectangle2D(5665 * scale, 1020 * scale, 920 * scale, 920 * scale));
			break;
		// Jaune
		case Jau:
			this.backCard = new ImageView(C);
			backCard.setViewport(new Rectangle2D(720 * scale, 28 * scale, 920 * scale, 920 * scale));
			break;
		// Rouge
		case Rou:
			this.backCard = new ImageView(C);
			backCard.setViewport(new Rectangle2D(4668 * scale, 28 * scale, 920 * scale, 920 * scale));
			break;
		// Vert
		case Ver:
			this.backCard = new ImageView(C);
			backCard.setViewport(new Rectangle2D(3673 * scale, 28 * scale, 920 * scale, 920 * scale));
			break;
		// Violet
		case Vio:
			this.backCard = new ImageView(C);
			backCard.setViewport(new Rectangle2D(2718 * scale, 28 * scale, 920 * scale, 920 * scale));
			break;
		}
		backCard.setFitHeight(100);
		backCard.setFitWidth(100);
	}

	public Domain getSymbol() {
		return symbol;
	}

	public void setSymbol(Domain symbol) {
		this.symbol = symbol;
	}

	public InfluenceCard getHiddenCard() {
		return hiddenCard;
	}

	public void setHiddenCard(InfluenceCard hiddenCard) {
		this.hiddenCard = hiddenCard;
	}

	public void setValue(double d) {
		this.value = d;
	}

	public boolean getExecuted() {
		return executed;
	}

	public void setExecuted(boolean executed) {
		this.executed = executed;
	}

	public void configureSymbol(Role role) {
		switch (role) {
		case Al:
			this.symbol = Domain.Alc;
			break;

		case Md:
			this.symbol = Domain.Cbt;
			break;

		case Se:
			this.symbol = Domain.Agr;
			break;

		case Ma:
			this.symbol = Domain.Com;
			break;

		case Ca:
			this.symbol = Domain.Rel;
			break;

		case Tb:
			this.symbol = Domain.Mus;
			break;

		default:
			this.symbol = null;
		}
	}

}
