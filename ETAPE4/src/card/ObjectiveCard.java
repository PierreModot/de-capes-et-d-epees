package card;

import cd.DataController;
import gamePart.Caches;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import util.Domain;

public class ObjectiveCard {
	
	private static Image C = Caches.C;
	private static Image OC = Caches.OC;
	
	private Domain domain;
	private int point;
	private ImageView picture;
	
	private double scale = .25;
	
	public ObjectiveCard(String encode) {
		decode(encode);
		objectif();
	}

	public ObjectiveCard(Domain symbol, int value) {
		domain = symbol;
		point = value;
		
		objectif();
	}
	
	public int getPoint() {
		return point;
	}
	
	public Domain getDomain() {
		return domain;
	}
	
	public void objectif() {
		
		if (DataController.theme) {
			C = Caches.speC;
			OC =  Caches.OCS;
		} else {
			C =  Caches.C;
			OC = Caches.OC;
		}

		// Attribue la carte Objectif selon le domaine et la valeur
		switch (this.domain) {
		// Agriculture
		case Agr:
			switch (this.point) {
			case 1:	// Initialise l'image de base
				this.picture = new ImageView(C);		// Associe l'image de la carte ï¿½ l'image de base
				picture.setViewport(new Rectangle2D(1402, 782, 975 * scale, 612 * scale)); // Rogne l'image en fonction du rï¿½le de la carte
				break;
			case 2:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(116, 2, 108, 69));
				break;
			case 3:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(616, 255, 975 * scale, 612 * scale));
				break;
			case 4:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(582, 3, 108, 69));
				break;
			case 5:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(965, 606, 975 * scale, 612 * scale));
				break;
			}
			break;
			// Alchimie
		case Alc:
			switch (this.point) {
			case 1:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(329, 948, 975 * scale, 612 * scale));
				break;
			case 2:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(293, 77, 108, 69));
				break;
			case 3:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(597, 782, 975 * scale, 612 * scale));
				break;
			case 4:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(59, 950, 975 * scale, 612 * scale));
				break;
			case 5:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(214, 352, 108, 69));
				break;
			}
			break;
			// Combat
		case Cbt:
			switch (this.point) {
			case 1:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(1136, 957, 975 * scale, 612 * scale));
				break;
			case 2:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(879, 427, 975 * scale, 612 * scale));
				break;
			case 3:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(59, 769, 975 * scale, 612 * scale));
				break;
			case 4:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(1151, 253, 975 * scale, 612 * scale));
				break;
			case 5:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(324, 281, 108, 69));
				break;
			}
			break;
			// Commerce
		case Com:
			switch (this.point) {
			case 1:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(1312 * scale, 3109 * scale, 975 * scale, 612 * scale));
				break;
			case 2:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(5597 * scale, 3831 * scale, 975 * scale, 612 * scale));
				break;
			case 3:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(3462 * scale, 3077 * scale, 975 * scale, 612 * scale));
				break;
			case 4:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(352, 2, 108, 69));
				break;
			case 5:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(330, 353 , 108, 69));
				break;
			}
			break;
			// Musicien
		case Mus:
			this.picture = new ImageView(C);
			switch (this.point) {
			case 1:
				picture.setViewport(new Rectangle2D(3452 * scale, 3810 * scale, 975 * scale, 612 * scale));
				break;
			case 2:
				picture.setViewport(new Rectangle2D(648 * scale, 2371 * scale, 975 * scale, 612 * scale));
				break;
			case 3:
				picture.setViewport(new Rectangle2D(2455 * scale, 1712 * scale, 975 * scale, 612 * scale));
				break;
			case 4:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(423, 77, 108, 69));
				break;
			case 5:
				picture.setViewport(new Rectangle2D(3541 * scale, 1017 * scale, 975 * scale, 612 * scale));
				break;
			}
			break;
			// Religion
		case Rel:
			switch (this.point) {
			case 1:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(466, 2, 108, 69));
				break;
			case 2:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(2382 * scale, 3815 * scale, 975 * scale, 612 * scale));
				break;
			case 3:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(1728 * scale, 2397 * scale, 975 * scale, 612 * scale));
				break;
			case 4:
				this.picture = new ImageView(OC);
				picture.setViewport(new Rectangle2D(233, 2, 108, 69));
				break;
			case 5:
				this.picture = new ImageView(C);
				picture.setViewport(new Rectangle2D(4537 * scale, 3130 * scale, 975 * scale, 612 * scale));
				break;
			}
			break;

		}
	}
	
	public ImageView getPicture() {
		return picture;
	}

	public String encode() {
		return "O" + domain.name() + point;
	}

	public void decode(String card) {
		domain = Domain.valueOf(card.substring(1, 4));
		point = Integer.parseInt(card.substring(4, 5));
	}
}
