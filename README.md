## Projet De Capes Et D'épées
Projet dans le cadre du DUT informatique, 2ème année, 2021/2022, S3.
Projet tutoré - Mise en situation professionnelle.

Ce dépot est réalisé 3 ans après la fin du projet.
Il n'est plus fonctionnel en l'état et certaines informations sont manquantes.

## Participants
Ce projet a été développé par groupe, une quizaine de personne, voire plus, si ma mémoire est bonne

## Description
Ce projet consiste à reproduire le jeu de société De Capes Et D'Épées en une version digitale et jouable en réseau.

### Fonctionnalités
- Parties jouables en réseau
- Bots
- Plusieurs langues disponibles
- Plusieurs textures disponibles

## Usage
Le projet n'est pas fonctionnel en l'état
